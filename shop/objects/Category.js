import API from "./API";

export default class Category {
    id: Number;
    name: String;
    description: String;
    background: String;

    toJsonObject() {
        return {
            id: this.id,
            name: this.name,
            description: this.description,
            background: this.background
        };
    }

    putCategory(categoryId: Number, onSuccess: Function, onError: Function) {
        API.archive.category.put(categoryId, onSuccess, onError);
    }

    deleteCategory(categoryId: Number, onSuccess: Function, onError: Function) {
        API.archive.category.delete(categoryId, onSuccess, onError);
    }

    static createCategory(onSuccess: Function, onError: Function) {
        API.archive.category.post(onSuccess, onError);
    }

    static getCategory(categoryId: Number, onSuccess: Function, onError: Function) {
        API.archive.category.get(categoryId, onSuccess, onError);
    }
}