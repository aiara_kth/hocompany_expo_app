import API from "./API";

export default class Review {
    shopId;
    reviewId;
    context: String;
    date: Date;

    createComment(text: String, onSuccess: Function, onError: Function) {
        API.revision2.comment.post(this.shopId, this.reviewId, text, new Date(), onSuccess, onError);
    }

    getComment(commentId, onSuccess: Function, onError: Function) {
        API.revision2.comment.get(this.shopId, this.reviewId, commentId, onSuccess, onError);
    }

    putComment(commentId, onSuccess: Function, onError: Function) {
        API.revision2.comment.put(this.shopId, this.reviewId, commentId, onSuccess, onError);
    }

    deleteComment(commentId, onSuccess: Function, onError: Function) {
        API.revision2.comment.delete(this.shopId, this.reviewId, commentId, onSuccess, onError);
    }

    like(likeOn: Boolean, onSuccess: Function, onError: Function) {
        if (likeOn) {
            API.revision2.likeReview.post(this.shopId, this.reviewId, onSuccess, onError);
            return;
        }
        API.revision2.likeReview.delete(this.shopId, this.reviewId, onSuccess, onError);
    }
}