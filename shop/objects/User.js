import { AsyncStorage } from 'react-native';
import * as firebase from 'firebase';
import Global from "./Global";
import Shop from "./Shop";
import API from "./API";

export default class User {
    static APP = {
        CLIENT: "client", SHOP: "shop"
    };
    static LOGIN_TYPE = {
        KAKAO: "kakao", FACEBOOK: "facebook", FIREBASE: "firebase"
    };

    token: String = "";

    userID: String = "";
    username: String = "";
    email: String = "";
    age: Number = -1;
    gender: String = "";
    birthday: String = "";
    phoneNumber:String = "";
    profilePhotoUrl:String = "";
    agreement: Object = {
        personalInformation: true,
        locationUsage: true,
        marketing: true,
        serviceUsage: true
    };
    userSetting: Object = {
        notification: true,
        call: true,
        event: true,
        marketing: true,
        review: true
    };
    app: String = "";
    loginType: String = "";
    auth: String = null;
    gotUserDataFromServer: Boolean = false;

    push_sound: Boolean = true;
    push_vibe: Boolean = true;

    shops: Array<Shop> = [];

    coupon: Array = [];

    setUser(token: String, nickname: String, email: String, phoneNumber: String, profilePhotoUrl: String, app: String, loginType: String) {
        this.token = token;
        this.username = nickname;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.profilePhotoUrl = profilePhotoUrl;
        this.app = app;
        this.loginType = loginType;
        return this;
    }

    getAuth(callback: Function) {
        if (this.auth === null) {
            Global.initFirebaseIfNotInitialized(firebase);
            firebase.auth().signInWithCustomToken(this.token).then(signInWithCustomTokenResult => {
                console.log("getAuth - signInWithCustomToken :", signInWithCustomTokenResult);
                firebase.auth().currentUser.getIdToken(true).then(idToken => {
                    this.auth = idToken;
                    console.log("getAuth - signInWithCustomToken - getIdToken :", this.auth);
                    if (!this.gotUserDataFromServer)
                        this.getUserDataFromServer();
                    this.saveToLocalStorage();
                    if (callback !== undefined && callback !== null && typeof callback === "function")
                        callback(this.auth);
                    return this.auth;
                });
                return null;
            });
            return null;
        } else {
            if (!this.gotUserDataFromServer)
                this.getUserDataFromServer();
            if (callback !== undefined && callback !== null && typeof callback === "function")
                callback(this.auth);
            return this.auth;
        }
    }

    fromJSONString(jsonString: String) {
        const json = JSON.parse(jsonString);
        this.token = json.kakaoToken;
        this.userID = json.userID;
        this.username = json.username;
        this.email = json.email;
        this.age = typeof json.age === Number ? json.age : json.age.length === 0 ? -1 : parseInt(json.age);
        this.gender = json.gender;
        this.birthday = json.birthday;
        this.phoneNumber = json.phoneNumber;
        this.profilePhotoUrl = json.profilePhotoUrl;
        this.agreement = json.agreement;
        this.userSetting = json.userSetting;
        this.app = json.app;
        this.loginType = json.loginType;
        this.auth = json.auth;
        this.gotUserDataFromServer = json.gotUserDataFromServer || false;
        this.push_sound = json.push_sound !== null && json.push_sound !== undefined ? json.push_sound : true;
        this.push_vibe = json.push_vibe !== null && json.push_vibe !== undefined ? json.push_vibe : true;
        return this;
    }

    toJSONString() {
        return JSON.stringify({
            kakaoToken: this.token,
            userID: this.userID,
            username: this.username,
            email: this.email,
            age: this.age,
            gender: this.gender,
            birthday: this.birthday,
            phoneNumber: this.phoneNumber,
            profilePhotoUrl: this.profilePhotoUrl,
            agreement: this.agreement,
            userSetting: this.userSetting,
            app: this.app,
            loginType: this.loginType,
            auth: this.auth,
            gotUserDataFromServer: this.gotUserDataFromServer,
            push_sound: this.push_sound,
            push_vibe: this.push_vibe
        });
    }

    saveToLocalStorage() {
        try {
            let userJsonString = this.toJSONString();
            AsyncStorage.setItem('@user', userJsonString);
        } catch (e) {}
    }

    getShops(onSuccess: Function, onError: Function) {
        if (this.shops.length > 0) {
            onSuccess(this.shops);
            return this.shops;
        }
        API.shop.admin.getUserShops(data => {
            // todo data를 Shop[]로 바꿔서 this.shops에 넣고 리턴
            onSuccess(data);
        }, onError)
    }

    getUserDataFromServer() {
        API.user.get(data => {
            // let needSet = false;
            if (data.userID !== undefined && data.userID !== null)
                this.userID = data.userID;
            // else
            //     needSet = true;
            if (data.username !== undefined && data.username !== null)
                this.username = data.username;
            // else
            //     needSet = true;
            if (data.email !== undefined && data.email !== null)
                this.email = data.email;
            // else
            //     needSet = true;
            if (data.age !== undefined && data.age !== null)
                this.age = data.age;
            // else
            //     needSet = true;
            if (data.gender !== undefined && data.gender !== null)
                this.gender = data.gender;
            // else
            //     needSet = true;
            if (data.birthday !== undefined && data.birthday !== null)
                this.birthday = data.birthday;
            // else
            //     needSet = true;
            if (data.phoneNumber !== undefined && data.phoneNumber !== null)
                this.phoneNumber = data.phoneNumber;
            // else
            //     needSet = true;
            if (data.profilePhotoUrl !== undefined && data.profilePhotoUrl !== null)
                this.profilePhotoUrl = data.profilePhotoUrl;
            // else
            //     needSet = true;
            if (data.agreement !== undefined && data.agreement !== null)
                this.agreement = data.agreement;
            // else
            //     needSet = true;
            if (data.userSetting !== undefined && data.userSetting !== null)
                this.userSetting = data.userSetting;
            // else
            //     needSet = true;
            if (data.app !== undefined && data.app !== null)
                this.app = data.app;
            // else
            //     needSet = true;
            if (data.loginType !== undefined && data.loginType !== null)
                this.loginType = data.loginType;
            // else
            //     needSet = true;
            // this.gotUserDataFromServer = true;
            this.saveToLocalStorage();
            // if (needSet) {
            //     this.saveToServer();
            // }
        }, error => {

        });
    }

    saveToServer() {
        API.user.put(this.userID, {
            userID: this.userID,
            username: this.username,
            email: this.email,
            age: this.age,
            gender: this.gender,
            birthday: this.birthday,
            phoneNumber: this.phoneNumber.replace("010", "+8210"),
            profilePhotoUrl: this.profilePhotoUrl,
            agreement: this.agreement,
            userSetting: this.userSetting,
            app: this.app,
            loginType: this.loginType
        }, data => {

        }, error => {

        });
    }

    getBookmarks(onSuccess: Function, onError: Function) {
        API.user.getUserBookmark(onSuccess, onError);
    }

    getJRequest(onSuccess: Function, onError: Function) {
        API.user.getUserJRequest(data => {
            // todo JRequest[]로 바꿔서 리턴
            onSuccess(data);
        }, onError);
    }

    getJRequestHistory(onSuccess: Function, onError: Function) {
        API.user.getUserJRequestHistory(data => {
            // todo JRequest[]로 바꿔서 리턴
            onSuccess(data);
        }, onError);
    }

    bookmarkShop(bookmarkOn: Boolean, shopId, onSuccess: Function, onError: Function) {
        if (bookmarkOn) {
            API.archive.userBookmark.post(shopId, this.userID, onSuccess, onError);
            return;
        }
        API.archive.userBookmark.delete(shopId, this.userID, onSuccess, onError);
    }

    static async loadFromLocalStorage() {
        let userJsonString = await AsyncStorage.getItem('@user');
        if (userJsonString !== null) {
            Global.user = new User().fromJSONString(userJsonString);
            Global.user.getAuth();
        } else {
            console.log("userJsonString === null, " + userJsonString);
        }
    }
};