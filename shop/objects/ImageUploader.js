import {ImagePicker, Permissions} from "expo";
import {Alert} from "react-native";

export default class ImageUploader {
    async _pickImageOnGranted(callback) {
        let result = await ImagePicker.launchImageLibraryAsync({
            allowsEditing: true,
            aspect: [4, 3],
        });

        // console.log(result);

        if (!result.cancelled) {
            if (result.type !== 'image') {
                Alert.alert(
                    '업로드 불가',
                    '이미지 파일만 업로드가 됩니다.',
                    [
                        {text: "확인", onPress: () => null}
                    ]
                );
                return false;
            }
            // todo 서버쪽으로 업로드해서 url 받아오기
            callback(result);
        }
    }

    async upload(callback) {
        const permission = await Permissions.getAsync(Permissions.CAMERA_ROLL);
        if (permission.status !== 'granted') {
            const newPermission = await Permissions.askAsync(Permissions.CAMERA_ROLL);
            if (newPermission.status === 'granted') {
                return this._pickImageOnGranted(callback);
            }
        } else {
            return this._pickImageOnGranted(callback);
        }
    }

    toRequiredImage(result) {
        return {
            uri: result.uri,
            cache: 'only-if-cached'
        };
    }
};