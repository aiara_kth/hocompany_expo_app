import * as firebase from 'firebase';
import ImageUploader from "./ImageUploader";
import User from "./User";

export default class Global {
    static baseURL: String = 'https://niivyxf0n6.execute-api.ap-northeast-2.amazonaws.com/Stage/';
    static app_version: String = '1.0.0';
    static cart: Array = [];
    static user: User = null;
    static firebaseIsInitialized: Boolean = false;
    static imageUploader: ImageUploader = new ImageUploader();
    static unavailableWords: Array<String> = [
        "shit", "fuck", "admin", "개새끼", "니기미", "니미랄", "등신", "미친넘", "미친놈", "벼엉신",
        "병신", "빙신", "쉐이", "시발넘", "시벌", "씹팔", "쌍년", "쌍놈", "쓰벌", "씨발",
        "씨발놈", "씨방새", "씨밸", "씨벌", "씨브럴", "씨팔", "씹망", "씹새", "씹세", "씹쌔",
        "조까", "좃까", "좃나", "좃또", "좆같은", "좆까", "좆됫", "좆밥", "지랄", "찌랄",
        "씹할", "썅년", "썅놈", "씨ㅂ", "씨ㅍ", "좆ㄲ", "새ㄲ", "미친년", "존나", "개년",
        "니미럴", "쥐랄", "니미", "씨방세", "개새", "좆", "씹", "개씹", "섹스", "좃깟",
        "닉네임", "영자", "운영자", "운영팀", "관리자", "복구팀", "개발팀", "매니저", "system", "운영팀",
        "어드민", "ㅅㅂ", "ㅆㅂ", "suck", "wtf", "dick", "pussy", "강간", "수간", "근친",
        "사까시", "빠구리", "간나", "걸레", "뒤치기", "펠라치오", "오랄", "젖꼭지", "nipple", "bitch"
    ];

    static checkIsAvailableWord(word: String): Boolean { // 사용 불가능한 단어들, 욕설, 성드립 등등 확인
        for(let i = 0; i < Global.unavailableWords.length; i++) {
            if (word === Global.unavailableWords[i] || word.indexOf(Global.unavailableWords[i]) !== -1) {
                return false;
            }
        }
        return true;
    }
    static checkIsAvailableEmail(email: String): Boolean { // 이메일 주소 형태 확인
        return email.match(/^[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*.[a-zA-Z]{2,3}$/i) != null;
    }
    static initFirebaseIfNotInitialized(): VoidFunction { // 파이어베이스 이니셜라이즈(안된 상태일때만)
        if (!Global.firebaseIsInitialized) {
            firebase.initializeApp({
                apiKey: "AIzaSyCxg5GBnyjGDGM3spfb6jvvJDB0BA7ZV7k",
                authDomain: "jaritso-1227e.firebaseapp.com"
            });
            Global.firebaseIsInitialized = true;
        }
    }
}
