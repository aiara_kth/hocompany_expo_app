import User from "./User";
import Location from "./Location";
import Category from "./Category";
import Global from "./Global";

class APIRequester {
    logLevel: Number = 2;

    method: String;
    url: String;
    user: User;
    body: Object;
    onSuccess: Function;
    onError: Function;

    static request(method: String, url: String, user: User, body: Object, onSuccess: Function, onError: Function) {
        let api = new APIRequester();
        api.method = method;
        api.url = url;
        api.user = user !== undefined && user !== null ? user : Global.user;
        api.body = body;
        api.onSuccess = onSuccess;
        api.onError = onError;
        api.requestAPI();
    }
    requestAPI() {
        if (this.logLevel > 2) console.log("start call", this.toString());
        fetch(Global.baseURL + this.url, this.getRequestInit()).then(response => response.json()).then(data => {
            if (this.logLevel > 1) console.log(this.toString());
            if (this.logLevel > 0) console.log(data);
            if ((data.errorCode !== undefined && data.errorCode !== "") || (data.message !== undefined && data.message === "Internal server error")) {
                this.callOnError(data);
            } else {
                this.callOnSuccess(data);
            }
        }).catch(error => {
            if (this.logLevel > 1) console.log(this.toString());
            if (this.logLevel > 0) console.log(error);
            this.callOnError(error);
        }).finally(() => {
            if (this.logLevel > 2) console.log("end call", this.toString());
        });
    }
    callOnSuccess(data) {
        if (this.onSuccess !== undefined && this.onSuccess !== null) {
            this.onSuccess(data);
        }
    }
    callOnError(error) {
        if (this.onError !== undefined && this.onError !== null) {
            this.onError(error);
        }
    }
    toString() {
        return this.method + " /" + this.url;
    }
    getRequestInit() {
        const noneAuthHeader = {
            "Accept": "application/json",
            "Content-Type": "application/json",
            "Connection": "close"
        };
        let authHeader = () => {
            if (this.user === null) {
                return noneAuthHeader;
            }
            return {
                "Accept": "application/json",
                "Content-Type": "application/json",
                "Connection": "close",
                Authorization: this.user.auth
            };
        };
        if (this.body === null) {
            return {
                method: this.method,
                headers: this.user !== null ? authHeader() : noneAuthHeader
            };
        }
        return {
            method: this.method,
            headers: this.user !== null ? authHeader() : noneAuthHeader,
            body: JSON.stringify(this.body)
        };
    }
}
const POST = "POST";
const GET = "GET";
const PUT = "PUT";
const DELETE = "DELETE";
const API = {
    Authentication: {
        getAuthToken: (app: String, loginType: String, phoneNumber: String, onSuccess: Function, onError: Function) =>
            APIRequester.request(GET, "auth/token?app=" + app + "&loginType=" + loginType + "&phoneNumber=" + phoneNumber, null, null, onSuccess, onError),
        postAuthSMS: (phoneNumber: String, onSuccess: Function, onError: Function) =>
            APIRequester.request(POST, "auth/sms", null, { phoneNumber: phoneNumber }, onSuccess, onError),
        postAuthVerify: (onSuccess: Function, onError: Function) =>
            APIRequester.request(POST, "auth/verify", null, null, onSuccess, onError),
        getAuthShop: (shopId, onSuccess: Function, onError: Function, user: ?User) =>
            APIRequester.request(GET, "auth/shop/" + shopId, user, null, onSuccess, onError)
    },
    shop: {
        admin: {
            post: (name: String, authNum: Number, shopNumber: String, address: String, onSuccess: Function, onError: Function, user: ?User) =>
                APIRequester.request(POST, "shop/admin", user, {
                shopName: name,
                bizCertNumber: authNum,
                shopNumber: shopNumber,
                shopAddress: address,
                location: {
                    latitude: 0,
                    longitude: 0
                },
                adminNumber: 0
            }, onSuccess, onError),
            put: (shopId, body: Object, onSuccess: Function, onError: Function, user: ?User) =>
                APIRequester.request(PUT, "shop/admin/" + shopId, user, body, onSuccess, onError),
            delete: (shopId, onSuccess: Function, onError: Function, user: ?User) =>
                APIRequester.request(DELETE, "shop/admin/" + shopId, user, null, onSuccess, onError),
            get: (shopId, onSuccess: Function, onError: Function, user: ?User) =>
                APIRequester.request(GET, "shop/admin/" + shopId, user, null, onSuccess, onError),
            getJRequest: (shopId, onSuccess: Function, onError: Function, user: ?User) =>
                APIRequester.request(GET, "shop/admin/" + shopId + "/jrequest", user, null, onSuccess, onError),
            getNotification: (shopId, onSuccess: Function, onError: Function, user: ?User) =>
                APIRequester.request(GET, "shop/admin/" + shopId + "/notification", user, null, onSuccess, onError),
            menu: {
                post: (shopId, name: String, price: Number, currency: String, menuProfileUrl: String, description: String, data: Object, onSuccess: Function, onError: Function, user: ?User) =>
                    APIRequester.request(POST, "shop/admin/" + shopId + "/menu", user, {
                    name: name,
                    price: price,
                    currency: currency,
                    menuProfileUrl: menuProfileUrl,
                    description: description,
                    data: data
                }, onSuccess, onError),
                get: (shopId, menuId, onSuccess: Function, onError: Function, user: ?User) =>
                    APIRequester.request(GET, "shop/admin/" + shopId + "/menu/" + menuId, user, null, onSuccess, onError),
                put: (shopId, menuId, body: Object, onSuccess: Function, onError: Function, user: ?User) =>
                    APIRequester.request(PUT, "shop/admin/" + shopId + "/menu/" + menuId, user, body, onSuccess, onError),
                delete: (shopId, menuId, onSuccess: Function, onError: Function, user: ?User) =>
                    APIRequester.request(DELETE, "shop/admin/" + shopId + "/menu/" + menuId, user, null, onSuccess, onError)
            },
            getUserShops: (onSuccess: Function, onError: Function, user: ?User) =>
                APIRequester.request(GET, "user/shops", user, { Authorization: "" }, onSuccess, onError)
        },
        getNearShops: (latitude, longitude, onSuccess: Function, onError: Function, user: ?User) =>
            APIRequester.request(GET, "shop?latitude=" + (typeof longitude !== Number ? Number(longitude) : longitude) + "&longitude=" + (typeof latitude !== Number ? Number(latitude) : latitude), user, null, onSuccess, onError),
        getShop: (shopId, onSuccess: Function, onError: Function, user: ?User) =>
            APIRequester.request(GET, "shop/" + shopId, user, null, onSuccess, onError),
        getShopMenu: (shopId, onSuccess: Function, onError: Function, user: ?User) =>
            APIRequester.request(GET, "shop/" + shopId + "/menu", user, null, onSuccess, onError),
        like: {
            put: (shopId, onSuccess: Function, onError: Function, user: ?User) =>
                APIRequester.request(PUT, "shop/" + shopId + "/like", user, null, onSuccess, onError),
            delete: (shopId, onSuccess: Function, onError: Function, user: ?User) =>
                APIRequester.request(DELETE, "shop/" + shopId + "/like", user, null, onSuccess, onError)
        },
        bookmark: {
            put: (shopId, onSuccess: Function, onError: Function, user: ?User) =>
                APIRequester.request(PUT, "shop/" + shopId + "/bookmark", user, null, onSuccess, onError),
            delete: (shopId, onSuccess: Function, onError: Function, user: ?User) =>
                APIRequester.request(DELETE, "shop/" + shopId + "/bookmark", user, null, onSuccess, onError)
        }
    },
    user: {
        post: (userFirebaseInstanceToken: String, phoneNumber: String, loginType: String, app: String, email: String, password: String, onSuccess: Function, onError: Function) =>
            APIRequester.request(POST, "user", {
            "userFirebaseInstanceToken": userFirebaseInstanceToken,
            "phoneNumber": phoneNumber,
            "loginType": loginType,
            "app": app,
            "email": email,
            "password": password
        }, onSuccess, onError),
        get: (onSuccess: Function, onError: Function, user: ?User) =>
            APIRequester.request(GET, "user", user, null, onSuccess, onError),
        put: (userID: String, body: Object, onSuccess: Function, onError: Function, user: ?User) =>
            APIRequester.request(PUT, "user", user, body, onSuccess, onError),
        delete: (onSuccess: Function, onError: Function, user: ?User) =>
            APIRequester.request(DELETE, "user", user, { Authorization: "" }, onSuccess, onError),
        getUserBookmark: (onSuccess: Function, onError: Function, user: ?User) =>
            APIRequester.request(GET, "user/bookmarks", user, null, onSuccess, onError),
        getUserJRequest: (onSuccess: Function, onError: Function, user: ?User) =>
            APIRequester.request(GET, "user/jrequest", user, null, onSuccess, onError),
        getUserJRequestHistory: (onSuccess: Function, onError: Function, user: ?User) =>
            APIRequester.request(GET, "user/jrequest/history", user, null, onSuccess, onError)
    },
    jRequest: {
        post: (location: Location, shopIdArray: Array, onSuccess: Function, onError: Function, user: ?User) =>
            APIRequester.request(POST, "jrequest", user, {
            location: {
                latitude: location.latitude,
                longitude: location.longitude
            },
            shopIDArray: shopIdArray
        }, onSuccess, onError),
        get: (jrequestId, onSuccess: Function, onError: Function, user: ?User) =>
            APIRequester.request(GET, "jrequest/" + jrequestId, user, null, onSuccess, onError),
        put: (jrequestId, operationCode, onSuccess: Function, onError: Function, user: ?User) =>
            APIRequester.request(PUT, "jrequest/" + jrequestId, user, { operationCode: operationCode }, onSuccess, onError)
    },
    menu: {
        like: {
            post: (menuId, onSuccess: Function, onError: Function, user: ?User) =>
                APIRequester.request(POST, "menu/" + menuId + "/like", user, null, onSuccess, onError),
            delete: (menuId, onSuccess: Function, onError: Function, user: ?User) =>
                APIRequester.request(DELETE, "menu/" + menuId + "/like", user, null, onSuccess, onError)
        }
    },
    preorder: {
        post: (jrequestId, onSuccess: Function, onError: Function, user: ?User) =>
            APIRequester.request(POST, "jrequest/" + jrequestId + "/preorder", user, null, onSuccess, onError),
        get: (jrequestId, onSuccess: Function, onError: Function, user: ?User) =>
            APIRequester.request(GET, "jrequest/" + jrequestId + "/preorder", user, null, onSuccess, onError),
        put: (jrequestId, onSuccess: Function, onError: Function, user: ?User) =>
            APIRequester.request(PUT, "jrequest/" + jrequestId + "/preorder", user, null, onSuccess, onError),
        delete: (jrequestId, onSuccess: Function, onError: Function, user: ?User) =>
            APIRequester.request(DELETE, "jrequest/" + jrequestId + "/preorder", user, null, onSuccess, onError)
    },
    waiting: {
        post: (jrequestId, onSuccess: Function, onError: Function, user: ?User) =>
            APIRequester.request(POST, "jrequest/" + jrequestId + "/waiting", user, null, onSuccess, onError),
        get: (jrequestId, onSuccess: Function, onError: Function, user: ?User) =>
            APIRequester.request(GET, "jrequest/" + jrequestId + "/waiting", user, null, onSuccess, onError),
        put: (jrequestId, onSuccess: Function, onError: Function, user: ?User) =>
            APIRequester.request(PUT, "jrequest/" + jrequestId + "/waiting", user, null, onSuccess, onError),
        delete: (jrequestId, onSuccess: Function, onError: Function, user: ?User) =>
            APIRequester.request(DELETE, "jrequest/" + jrequestId + "/waiting", user, null, onSuccess, onError)
    },
    utility: {
        getContents: (location: Location, geocode, page: Number, onSuccess: Function, onError: Function, user: ?User) =>
            APIRequester.request(GET, "contents?lat=" + Number(location.longitude) + "&lng=" + Number(location.latitude) + "&geocode=" + JSON.stringify(geocode) + "&page=" + page, user, null, onSuccess, onError)
    },
    archive: {
        postShopAdminJRequest: (shopId, onSuccess: Function, onError: Function, user: ?User) =>
            APIRequester.request(POST, "shop/admin/" + shopId + "/jrequest", user, null, onSuccess, onError),
        shopViews: {
            post: (shopId, userId, onSuccess: Function, onError: Function, user: ?User) =>
                APIRequester.request(POST, "shop/" + shopId + "/views", user, { userID: userId }, onSuccess, onError),
            get: (shopId, onSuccess: Function, onError: Function, user: ?User) =>
                APIRequester.request(GET, "shop/" + shopId + "/views", user, null, onSuccess, onError)
        },
        category: {
            post: (onSuccess: Function, onError: Function, user: ?User) =>
                APIRequester.request(POST, "category", user, null, onSuccess, onError),
            get: (categoryId, onSuccess: Function, onError: Function, user: ?User) =>
                APIRequester.request(GET, "category/" + categoryId, user, null, onSuccess, onError),
            put: (categoryId, onSuccess: Function, onError: Function, user: ?User) =>
                APIRequester.request(PUT, "category/" + categoryId, user, null, onSuccess, onError),
            delete: (categoryId, onSuccess: Function, onError: Function, user: ?User) =>
                APIRequester.request(DELETE, "category/" + categoryId, user, null, onSuccess, onError)
        },
        userBookmark: {
            post: (shopId, userId, onSuccess: Function, onError: Function, user: ?User) =>
                APIRequester.request(POST, "user/bookmarks/" + shopId, user, null, onSuccess, onError),
            delete: (shopId, userId, onSuccess: Function, onError: Function, user: ?User) =>
                APIRequester.request(DELETE, "user/bookmarks/" + shopId, user, null, onSuccess, onError)
        },
        menu: {
            post: (onSuccess: Function, onError: Function, user: ?User) =>
                APIRequester.request(POST, "menu", user, null, onSuccess, onError),
            get: (menuId, onSuccess: Function, onError: Function, user: ?User) =>
                APIRequester.request(GET, "menu/" + menuId, user, null, onSuccess, onError),
            put: (menuId, onSuccess: Function, onError: Function, user: ?User) =>
                APIRequester.request(PUT, "menu/" + menuId, user, null, onSuccess, onError),
            delete: (menuId, onSuccess: Function, onError: Function, user: ?User) =>
                APIRequester.request(DELETE, "menu/" + menuId, user, null, onSuccess, onError)
        }
    },
    revision2: {
        review: {
            post: (shopId, body, onSuccess: Function, onError: Function, user: ?User) => // todo body는 데이터 받아서 이 함수안에서 만드는걸로
                APIRequester.request(POST, "shop/" + shopId + "/review", user, body, onSuccess, onError),
            get: (shopId, onSuccess: Function, onError: Function, user: ?User) =>
                APIRequester.request(GET, "shop/" + shopId + "/review", user, null, onSuccess, onError),
            delete: (shopId, reviewId, onSuccess: Function, onError: Function, user: ?User) =>
                APIRequester.request(DELETE, "shop/" + shopId + "/review", user, null, onSuccess, onError)
        },
        comment: {
            post: (shopId, reviewId, text, currentDate, onSuccess: Function, onError: Function, user: ?User) =>
                APIRequester.request(POST, "shop/" + shopId + "/review/" + reviewId + "/comment", user, {
                context: text,
                date: currentDate.getFullYear() + "-" + currentDate.getMonth() + "-" + currentDate.getDate()
            }, onSuccess, onError),
            get: (shopId, reviewId, reviewCommentId, onSuccess: Function, onError: Function, user: ?User) =>
                APIRequester.request(GET, "shop/" + shopId + "/review/" + reviewId + "/comment/" + reviewCommentId, user, null, onSuccess, onError),
            put: (shopId, reviewId, reviewCommentId, onSuccess: Function, onError: Function, user: ?User) =>
                APIRequester.request(PUT, "shop/" + shopId + "/review/" + reviewId + "/comment/" + reviewCommentId, user, null, onSuccess, onError),
            delete: (shopId, reviewId, reviewCommentId, onSuccess: Function, onError: Function, user: ?User) =>
                APIRequester.request(DELETE, "shop/" + shopId + "/review/" + reviewId + "/comment/" + reviewCommentId, user, null, onSuccess, onError)
        },
        likeReview: {
            post: (shopId, reviewId, onSuccess: Function, onError: Function, user: ?User) =>
                APIRequester.request(POST, "shop/" + shopId + "/review/" + reviewId + "/like", user, null, onSuccess, onError),
            delete: (shopId, reviewId, onSuccess: Function, onError: Function, user: ?User) =>
                APIRequester.request(DELETE, "shop/" + shopId + "/review/" + reviewId + "/like", user, null, onSuccess, onError)
        }
    }
};
export default API;
