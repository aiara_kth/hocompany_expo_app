import React from 'react';
import {Text, View, StyleSheet, Image,TextInput,TouchableHighlight, Dimensions, Alert } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { Button } from 'react-native-elements';
import { Actions } from 'react-native-router-flux';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import ActionButton from 'react-native-action-button';
import {Ionicons} from '@expo/vector-icons';
import TimerCountdown from "react-native-timer-countdown";
import API from "../../objects/API";
import Global from "../../objects/Global";

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    Navigator : {
        marginTop : getStatusBarHeight(),
        width: '100%',
        height: 48,
        backgroundColor: '#fff',
    },
    back_button : {
        width:  48,
        height: 48,
        paddingTop: 12,
        paddingLeft: 12
    },
    back_button_b : {
        width: 24,
        height: 24
    },
    header : {
        marginTop: 48,
        width: '100%',
        height: 50
    },
    title: {
        paddingLeft: 13,
        width:'100%',
        height:'18%',
        justifyContent: 'center',
    },
    title_w : {
        fontSize: 32,
        height: 48,
        color: '#000000',
    },
    title_ws : {
        fontSize: 14,
        height: 24,
        color: '#96c86e'
    },
    contents: {
        width: Dimensions.get('window').width,
        height: 300,
        alignItems: 'center',
        justifyContent: 'center',
    },
    phonecert : {
        flex: 1,
        width: 320,
        // marginTop: (Dimensions.get('window').height / 8) ,
        // width: Dimensions.get('window').width,
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center',
    },
    phonecert_form : {
        width: 320,
        height:56,
        borderRadius: 2,
        backgroundColor: '#fafbfb',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection:'row',
        paddingLeft: 10,
    },
    phonecert_form_input : {
        width: 230,
    },
    phonecert_form_btn : {
        backgroundColor : '#fafbfb',
        width: 20,
        marginLeft: -10
    },
    infomark : {
        // textAlign: 'left',
        alignSelf: 'stretch',
        flexDirection:'row',
        marginTop: 8
    },
    infomark_icon : {
        width: 24,
        height: 20,
        borderRadius: 10,
        fontSize: 10,
        flexDirection : 'row',
        borderWidth : 1,
        borderColor: 'rgba(199,199,199,0.33)',
        paddingTop: 2,
        color: '#838383',
        backgroundColor : 'rgba(226, 226, 226, 0.23)',
        justifyContent: 'center',
        alignItems: 'center',
        textAlign: 'center',
        overflow : 'hidden'
    },
    infomark_text : {
        // width: 200,
        height: 22,
        paddingTop: 2,
        fontSize: 12,
        color: '#838383',
        textAlign: 'center',
    },
    arrow_button : {
        position: 'absolute',
        bottom: 0,
        right: 0,
    }

});

class User_Register_Phone_Step2 extends  React.Component {
    constructor(props) {
        super(props);
        this.state = {text: '', cert_check: 0, cert_num: 0, timervalue: 30, timer: 30, timeover: false, shownAlert: false};
        API.Authentication.postAuthSMS(this.props.phone_num, data => {
            if (data.code === undefined) {
                Alert.alert('휴대폰 인증', "서버문제로 인증번호 전송에 실패했습니다. 다시 시도해 주세요.", [{
                    text: "확인",
                    onPress: () => null//Actions.pop()
                }]);
                console.log("data : ", data);
                return;
            }
            this.setState({cert_num: data.code});
            console.log("cert_num : ", data.code);
        });
    }

    Check_phone_cert(useAlert = false) {
        if (this.state.timeover) {
            Alert.alert(
                '회원가입',
                '인증시간이 초과되었습니다. 다시 인증요청 해주세요.',
                [
                    {text: "확인", onPress: () => Actions.pop()}
                ]
            );
            return -2;
        }

        if (this.state.cert_num === this.state.text) {
            if (useAlert) {
                Alert.alert(
                    '회원가입',
                    '휴대폰인증이 성공하였습니다.',
                    [
                        {text: "확인", onPress: () => null}
                    ]
                );
                this.state.shownAlert = true;
            }
            return 1;
        } else {
            if (this.state.text === '') {
                if (useAlert) {
                    Alert.alert(
                        '회원가입',
                        '인증번호를 입력해주세요.',
                        [
                            {text: "확인", onPress: () => null}
                        ]
                    );
                    this.state.shownAlert = true;
                }
                return -1;
            } else {
                if (useAlert) {
                    Alert.alert(
                        '회원가입',
                        '인증번호를 잘못입력하였습니다.',
                        [
                            {text: "확인", onPress: () => null}
                        ]
                    );
                    this.state.shownAlert = true;
                }
                return 0;
            }
        }
    }

    go_next_page() {
        this.state.cert_check = this.Check_phone_cert();
        this.setState({cert_check: this.state.cert_check});
        if (!this.state.shownAlert) {
            switch (this.state.cert_check) {
                case -1:
                    Alert.alert(
                        '회원가입',
                        '인증번호를 입력해주세요.',
                        [
                            {text: "확인", onPress: () => null}
                        ]
                    );
                    break;
                case 0:
                    Alert.alert(
                        '회원가입',
                        '인증번호를 잘못입력하였습니다.',
                        [
                            {text: "확인", onPress: () => null}
                        ]
                    );
                    break;
                case 1:
                    Alert.alert(
                        '회원가입',
                        '휴대폰인증이 성공하였습니다.',
                        [
                            {
                                text: "확인",
                                onPress: () => Actions.User_Register_Phone_Step3({phone_num: this.props.phone_num})
                            }
                        ]
                    );
                    break;
            }
        } else {
            switch (this.state.cert_check) {
                case -1:
                case 0:
                    break;
                case 1:
                    Actions.User_Register_Phone_Step3({phone_num: this.props.phone_num});
                    break;
            }
        }
    }

    timerOnTick() {
        if (Actions.currentScene.toString() === "User_Register_Phone_Step2") {
            this.setState({timeover: true});
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.Navigator}>
                    <TouchableHighlight underlayColor={'transparent'} onPress={() => Actions.pop()}>
                        <View style={styles.back_button}>
                            <Image style={styles.back_button_b} source={require("../../assets/back_arrow.png")}/>
                        </View>
                    </TouchableHighlight>
                </View>
                <KeyboardAwareScrollView>
                    <View style={styles.header}>
                        <View style={styles.title}>
                            <Text style={styles.title_w}>인증번호</Text>
                            <Text style={styles.title_ws}>발송된 인증번호를 입력하고 인증을 눌러주세요.</Text>
                        </View>
                    </View>
                    <View style={styles.contents}>
                        <View style={styles.phonecert}>
                            <View style={styles.phonecert_form}>
                                <TextInput style={styles.phonecert_form_input} keyboardType='phone-pad'
                                           placeholder={'인증번호를 입력해주세요.'} underlineColorAndroid='transparent'
                                           returnKeyType="done" onChangeText={(text) => this.setState({text})}/>
                                <Button backgroundColor='#fafbfb' color="#7db840" title="인증"
                                        onPress={() => {
                                            this.state.cert_check = this.Check_phone_cert(true);
                                        }}/>
                            </View>
                            <View style={styles.infomark}>
                                <Text style={styles.infomark_icon}>!</Text>
                                <Text style={styles.infomark_text}> 발송된 인증번호를 입력해주세요.</Text>
                                <TimerCountdown initialSecondsRemaining={1000 * 30}
                                                // onTick={secondsRemaining => console.log('tick' + secondsRemaining)}
                                                onTimeElapsed={() => this.timerOnTick()}
                                                style={styles.infomark_text}/>
                            </View>
                        </View>
                    </View>
                </KeyboardAwareScrollView>
                <ActionButton hideShadow={false} buttonColor={this.Check_phone_cert() === 1 ? '#eb5847' : '#838383'}
                              renderIcon={(active) => (<Ionicons name='ios-arrow-forward' size={20} color={'#FFFFFF'}/>)}
                              onPress={() => this.go_next_page()}>
                </ActionButton>
            </View>
        )
    }
}

export default User_Register_Phone_Step2;
