import React from 'react';
import {Text, View, StyleSheet, Image,TextInput,TouchableHighlight, Dimensions, Alert } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { Actions } from 'react-native-router-flux';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import ActionButton from 'react-native-action-button';
import {Ionicons} from '@expo/vector-icons';
import Global from '../../objects/Global.js';
import * as firebase from "firebase";
import User from "../../objects/User";
import RNKakaoLogins from 'react-native-kakao-logins';
import API from "../../objects/API";


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    Navigator : {
        marginTop : getStatusBarHeight(),
        width: '100%',
        height: 48,
        backgroundColor: '#fff',
    },
    back_button : {
        width:  48,
        height: 48,
        paddingTop: 12,
        paddingLeft: 12
    },
    back_button_b : {
        width: 24,
        height: 24
    },
    header : {
        marginTop: 48,
        width: '100%',
        height: 50
    },
    title: {
        paddingLeft: 13,
        width:'100%',
        height:'18%',
        justifyContent: 'center',
    },
    title_w : {
        fontSize: 32,
        height: 48,
        color: '#000000',
    },
    title_ws : {
        fontSize: 14,
        height: 24,
        color: '#96c86e'
    },
    contents: {
        width: Dimensions.get('window').width,
        height: 300,
        alignItems: 'center',
        justifyContent: 'center',
    },
    input_form : {
        width: 320,
        justifyContent: 'center',
        alignItems: 'center',
    },
    input_box : {
        width: 320,
        height:56,
        borderRadius: 2,
        backgroundColor: '#fafbfb',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection:'row',
        marginBottom:20
    },
    input_dom : {
        marginLeft : 10,
        width: 300
    }

});

class User_Register_Phone_Step4 extends  React.Component {
    userFirebaseInstanceToken = "token";
    loginType = User.LOGIN_TYPE.FIREBASE;

    constructor(props) {
        super(props);
        this.state = {nickname: '', email: '', password: '', password_re: ''}
        Global.initFirebaseIfNotInitialized(firebase);
    }

    componentDidMount() {
        if (this.props.facebookToken !== undefined) {
            this.loginType = User.LOGIN_TYPE.FACEBOOK;
            this.userFirebaseInstanceToken = this.props.facebookToken;
            fetch('https://graph.facebook.com/v2.5/me?fields=email,name,friends&access_token=' + this.props.facebookToken)
                .then((response) => response.json())
                .then((json) => {
                    console.log(json);
                    this.setState({nickname: json.name, email: json.email});
                })
                .catch(() => {
                    reject('ERROR GETTING DATA FROM FACEBOOK');
                });
        }
        if (this.props.kakaoToken !== undefined) {
            this.loginType = User.LOGIN_TYPE.KAKAO;
            this.userFirebaseInstanceToken = this.props.kakaoToken;
            RNKakaoLogins.getProfile((err, result) => {
                console.log(err, result);
                if (err !== undefined && err !== null){
                    console.log(err);
                    return;
                }
                console.log(result);
                this.setState({nickname: result.nickname, email: result.email});
            });
        }
    }

    register() {
        // 닉네임 입력 체크
        if (this.state.nickname === '') {
            Alert.alert('회원가입', '닉네임을 입력해주새요.', [{text: "확인", onPress: () => null}]);
            this.refs.nickname_input.focus();
            return;
        }

        // 닉네임에 html 태그가 있는지 체크
        if (this.state.nickname.includes('<') && this.state.nickname.includes('>')) {
            Alert.alert('회원가입', '닉네임으로 사용할 수 없는 단어가 포함되어 있습니다.', [{text: "확인", onPress: () => null}]);
            this.refs.nickname_input.focus();
            return;
        }

        // 닉네임 비속어 필터링
        if (!Global.checkIsAvailableWord(this.state.nickname)) {
            Alert.alert('회원가입', '닉네임으로 사용할 수 없는 단어가 포함되어 있습니다.', [{text: "확인", onPress: () => null}]);
            this.refs.nickname_input.focus();
            return;
        }

        // 이메일 입력 체크
        if (this.state.email === '') {
            Alert.alert('회원가입', '이메일을 입력해주새요.', [{text: "확인", onPress: () => null}]);
            this.refs.email_input.focus();
            return;
        }

        // 이메일 형태 체크
        if (!Global.checkIsAvailableEmail(this.state.email)) {
            Alert.alert('회원가입', '이메일을 잘못 입력했습니다.', [{text: "확인", onPress: () => null}]);
            this.refs.email_input.focus();
            return;
        }

        // 비밀번호 입력 체크
        if (this.state.password === '') {
            Alert.alert('회원가입', '비밀번호를 입력해주세요.', [{text: "확인", onPress: () => null}]);
            this.refs.password_input.focus();
            return;
        }

        // 비밀번호 재입력 체크
        if (this.state.password_re === '') {
            Alert.alert('회원가입', '비말번호를 다시 한번 입력해주세요.', [{text: "확인", onPress: () => null}]);
            this.refs.password_re_input.focus();
            return;
        }

        // 비밀번호와 비밀번호 재입력이 동일한지 체크
        if (this.state.password != this.state.password_re) {
            Alert.alert('회원가입', '입력하신 비밀번호와 확인란 입력값와 다릅니다.\n확인후 다시 입력해주세요.', [{text: "확인", onPress: () => null}]);
            this.refs.password_re_input.focus();
            return;
        }

        Alert.alert('회원가입', '회원가입하시겠습니까?',
            [
                {text: "취소", onPress: () => null, styles: 'cancel'},
                // {text: "확인", onPress : () => Actions.Login_regist_step_end({ phone : this.props.phone_num, nickname: this.state.nickname, password: this.state.password})}
                {text: "확인", onPress: () => this.register_send_server()}
            ]
        );

    }

    register_send_server() {

        API.user.post(this.userFirebaseInstanceToken, this.props.phone_num, this.loginType, User.APP.CLIENT, this.state.email, this.state.password, data => {
            if (data.errorCode !== "user/requirements-unmet" && data.message !== "Internal server error") {
                console.log(data);
                Alert.alert('회원가입', "회원가입이 완료되었습니다.", [{
                    text: "확인",
                    onPress: () => Actions.User_Register_Phone_Step5({
                        customToken: data.customToken,
                        phone: this.props.phone_num,
                        email: this.state.email,
                        nickname: this.state.nickname,
                        password: this.state.password,
                        loginType: this.loginType
                    })
                }]);
            } else {
                console.log(data, data.customToken);
                Alert.alert('회원가입', '회원가입이 실패하였습니다.', [{text: "확인", onPress: () => null}]);
            }
        }, error => {
            Alert.alert('회원가입', '서버문제로 회원가입이 실패하였습니다. \n' + error, [{text: "확인", onPress: () => null}]);
        });
    }

    render() {
        return (
            <View style={styles.container} behavior="padding" enabled>
                <View style={styles.Navigator}>
                    <TouchableHighlight underlayColor={'transparent'} onPress={() => Actions.pop()}>
                        <View style={styles.back_button}>
                            <Image style={styles.back_button_b} source={require("../../assets/back_arrow.png")}/>
                        </View>
                    </TouchableHighlight>
                </View>
                <KeyboardAwareScrollView>
                    <View style={styles.header}>
                        <View style={styles.title}>
                            <Text style={styles.title_w}>정보입력</Text>
                            <Text style={styles.title_ws}>추가 정보를 입력해주세요.</Text>
                        </View>
                    </View>
                    <View style={styles.contents}>
                        <View style={styles.input_form}>
                            <View style={styles.input_box}>
                                <TextInput ref="nickname_input" style={styles.input_dom} placeholder={'닉네임을 입력해주세요.'}
                                           underlineColorAndroid='transparent' retrunKeyType="done" value={this.state.nickname}
                                           onChangeText={(text) => this.setState({nickname: text})}/>
                            </View>
                            <View style={styles.input_box}>
                                <TextInput ref="email_input" style={styles.input_dom} placeholder={'이메일을 입력해주세요.'}
                                           underlineColorAndroid='transparent' retrunKeyType="done" value={this.state.email} keyboardType='email-address'
                                           onChangeText={(text) => this.setState({email: text})}/>
                            </View>
                            <View style={styles.input_box}>
                                <TextInput ref="password_input" secureTextEntry={true} style={styles.input_dom}
                                           placeholder={'비밀번호를 입력해주세요.'} underlineColorAndroid='transparent'
                                           retrunKeyType="done"
                                           onChangeText={(text) => this.setState({password: text})}/>
                            </View>
                            <View style={styles.input_box}>
                                <TextInput ref="password_re_input" secureTextEntry={true} style={styles.input_dom}
                                           placeholder={'비밀번호를 다시 한번 입력해주세요.'} underlineColorAndroid='transparent'
                                           retrunKeyType="done"
                                           onChangeText={(text) => this.setState({password_re: text})}/>
                            </View>
                        </View>
                    </View>
                </KeyboardAwareScrollView>
                <ActionButton hideShadow={false}
                              buttonColor={this.state.nickname === '' || this.state.password === '' || this.state.password_re === '' ? '#838383' : '#eb5847'}
                              renderIcon={(active) => (<Ionicons name='ios-arrow-forward' size={20} color={'#FFFFFF'}/>)}
                              onPress={() => this.register()}>
                </ActionButton>
            </View>
        )
    }
}

export default User_Register_Phone_Step4;
