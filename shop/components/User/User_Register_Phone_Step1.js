import React from 'react';
import {Text, View, StyleSheet, Image,TextInput,TouchableHighlight, Dimensions, Alert } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { Actions } from 'react-native-router-flux';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import ActionButton from 'react-native-action-button';
import {Ionicons} from '@expo/vector-icons';
import Global from "../../objects/Global";

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        height: '100%'
    },
    Navigator : {
        marginTop : getStatusBarHeight(),
        width: '100%',
        height: 48,
        backgroundColor: '#fff',
    },
    back_button : {
        width:  48,
        height: 48,
        paddingTop: 12,
        paddingLeft: 12
    },
    back_button_b : {
        width: 24,
        height: 24
    },
    header : {
        marginTop: 48,
        width: '100%',
        height: 50
    },
    title: {
        paddingLeft: 13,
        width:'100%',
        height:'18%',
        justifyContent: 'center',
    },
    title_w : {
        fontSize: 32,
        height: 48,
        color: '#000000',
    },
    title_ws : {
        fontSize: 14,
        height: 24,
        color: '#96c86e'
    },
    contents: {
        width: Dimensions.get('window').width,
        height: 300,
        alignItems: 'center',
        justifyContent: 'center',
    },
    phonecert : {
        flex: 1,
        width: 320,
        // marginTop: '10%',
        // width: Dimensions.get('window').width,
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center',
    },
    phonecert_form : {
        width: 320,
        height:56,
        borderRadius: 2,
        backgroundColor: '#fafbfb',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection:'row'
    },
    phonecert_form_input : {
        marginLeft : 10,
        width: 300
    },
    phonecert_form_btn : {
        backgroundColor : '#fafbfb',
        width: 20,
        marginLeft: -10
    },
    infomark : {
        // textAlign: 'left',
        alignSelf: 'stretch',
        flexDirection:'row',
        marginTop: 8
    },
    infomark_icon : {
        width: 24,
        height: 20,
        borderRadius: 10,
        fontSize: 10,
        flexDirection : 'row',
        borderWidth : 1,
        borderColor: 'rgba(199,199,199,0.33)',
        paddingTop: 2,
        color: '#838383',
        backgroundColor : 'rgba(226, 226, 226, 0.23)',
        justifyContent: 'center',
        alignItems: 'center',
        textAlign: 'center',
        overflow : 'hidden'
    },
    infomark_text : {
        width: 160,
        height: 22,
        paddingTop: 2,
        fontSize: 12,
        color: '#838383',
        textAlign: 'center',
    }
});

class User_Register_Phone_Step1 extends  React.Component {
    constructor(props) {
        super(props);
        this.state = {text: ''};
        if (Global.user !== null) {
            Alert.alert(
                '회원가입',
                '이미 회원가입을 하셨습니다.',
                [
                    {text: "확인", onPress: () => Actions.pop()}
                ]
            );
        }
    }

    Check_Phone_Input() {
        if (this.state.text === '') {
            Alert.alert(
                '회원가입',
                '휴대폰번호를 입력해주세요.',
                [
                    {text: "확인", onPress: () => null}
                ]
            );
        } else {
            Actions.User_Register_Phone_Step2({cert_num: 0, phone_num: this.state.text});
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.Navigator}>
                    <TouchableHighlight underlayColor={'transparent'} onPress={() => Actions.pop()}>
                        <View style={styles.back_button}>
                            <Image style={styles.back_button_b} source={require("../../assets/back_arrow.png")}/>
                        </View>
                    </TouchableHighlight>
                </View>
                <KeyboardAwareScrollView>
                    <View style={styles.header}>
                        <View style={styles.title}>
                            <Text style={styles.title_w}>휴대폰 번호</Text>
                            <Text style={styles.title_ws}>휴대폰번호를 입력해주세요.</Text>
                        </View>
                    </View>
                    <View style={styles.contents}>
                        <View style={styles.phonecert}>
                            <View style={styles.phonecert_form}>
                                <TextInput style={styles.phonecert_form_input} keyboardType='phone-pad'
                                           placeholder={'휴대폰 번호를 입력해주세요.'} underlineColorAndroid='transparent'
                                           returnKeyType="done" onChangeText={(text) => this.setState({text})}/>
                            </View>
                            <View style={styles.infomark}>
                                <Text style={styles.infomark_icon}>!</Text>
                                <Text style={styles.infomark_text}>- 를 제외하고 입력해주세요.</Text>
                            </View>
                        </View>
                    </View>
                </KeyboardAwareScrollView>
                <ActionButton hideShadow={false} buttonColor={this.state.text == '' ? '#838383' : '#eb5847'}
                              renderIcon={(active) => (<Ionicons name='ios-arrow-forward' size={20} color={'#FFFFFF'}/>)}
                              onPress={() => this.Check_Phone_Input()}>
                </ActionButton>
            </View>

        )
    }
}

export default User_Register_Phone_Step1;
