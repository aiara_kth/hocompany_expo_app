import React from 'react';
import {Text, View, StyleSheet, Image, TouchableHighlight, Alert, Platform} from 'react-native';
import { Actions } from 'react-native-router-flux';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import RNKakaoLogins from 'react-native-kakao-logins';
import { FBLogin, FBLoginManager } from 'react-native-facebook-login';
import Global from "../../objects/Global";

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    Navigator : {
        marginTop : getStatusBarHeight(),
        width: '100%',
        height: 48,
        backgroundColor: '#fff',
    },
    back_button : {
        width:  48,
        height: 48,
        paddingTop: 12,
        paddingLeft: 12
    },
    back_button_b : {
        width: 24,
        height: 24
    },
    header : {
        marginTop: 48,
        width: '100%',
        height: 50
    },
    title: {
        paddingLeft: 13,
        width:'100%',
        height:'18%',
        justifyContent: 'center',
    },
    title_w : {
        fontSize: 32,
        height: 48,
        color: '#000000',
    },
    title_ws : {
        fontSize: 14,
        height: 24,
        color: '#96c86e'
    },
    btn_group: {
        position: 'absolute',
        bottom: '5%',
        width:'100%',
        justifyContent: 'center',
        alignItems: 'center',

    },
    btn_login: {
        width: 316,
        height: 56,
        marginBottom: 10
    }

});

class Login_regist_step_two extends  React.Component {
    constructor(props) {
        super(props);
        this.state = {
            token: '',
            facebookToken: '',
            kakaoToken: '',
            fbUserData: null
        };
    }

    kakaotalk_login_user() {
        RNKakaoLogins.login((err, result) => {
            if (err !== undefined && err !== null){
                console.log(err);
                return;
            }
            this.setState({kakaoToken: result.token});
            this.afterLoginKakao();
        });
    }

    afterLoginKakao() {
        console.log(this.state.kakaoToken);
        Actions.Login_regist_step_three({phone_num: this.props.phone_num, kakaoToken: this.state.kakaoToken});
    }

    fbLogin = null;

    afterLoginFacebook() {
        console.log(this.state.facebookToken);
        Actions.Login_regist_step_three({phone_num: this.props.phone_num, facebookToken: this.state.facebookToken});
    }

    onFacebookPress() {
        if (Platform.OS === 'android')
            this.fbLogin._onFacebookPress();
        else
        {
            this.fbLogin.FBLoginManager.login((data) => {
                // console.log(data);
            });
        }
    }

    onPressJaritso() {
        Actions.Login_regist_step_three({phone_num: this.props.phone_num});
    }

    render() {
        let _this = this;
        return (
            <View style={styles.container}>
                <View style={styles.Navigator}>
                    <View style={styles.back_button}>
                        <TouchableHighlight underlayColor={'transparent'} onPress={() => Actions.pop()}>
                            <Image style={styles.back_button_b} source={require("../../assets/back_arrow.png")}/>
                        </TouchableHighlight>
                    </View>
                </View>
                <View style={styles.header}>
                    <View style={styles.title}>
                        <Text style={styles.title_w} onPress={() => Actions.Login_regist_step_one()}>시작하기</Text>
                        <Text style={styles.title_ws}>회원정보가 필요해요.</Text>
                    </View>
                </View>
                <View style={styles.btn_group}>
                    <TouchableHighlight underlayColor={'transparent'} onPress={() => this.kakaotalk_login_user()}>
                        <Image style={styles.btn_login} source={require("../../assets/Login/login_kakao.png")}/>
                    </TouchableHighlight>
                    <TouchableHighlight underlayColor={'transparent'} onPress={() => this.onFacebookPress()}>
                        <Image style={styles.btn_login} source={require("../../assets/Login/login_facebook.png")}/>
                    </TouchableHighlight>
                    <FBLogin style={{ display: 'none' }}
                             ref={(fbLogin) => { this.fbLogin = fbLogin }}
                             permissions={["email","user_friends"]}
                             loginBehavior={FBLoginManager.LoginBehaviors.Native}
                             onLogin={function(data){
                                 // console.log("Logged in!");
                                 // console.log(data);
                                 _this.setState({ facebookToken: data.credentials.token, fbUserData: data });
                                 _this.afterLoginFacebook();
                             }}
                             onLogout={function(){
                                 console.log("Logged out.");
                                 _this.setState({ facebookToken: "", fbUserData: null });
                             }}
                             onLoginFound={function(data){
                                 // console.log("Existing login found.");
                                 // console.log(data);
                                 _this.setState({ facebookToken: data.credentials.token, fbUserData: data });
                                 Alert.alert('회원가입', '이미 페이스북 로그인이 되어 있습니다.\n페이스북으로 시작하시겠습니까?',
                                     [
                                         {text: "취소", onPress: () => null, styles: 'cancel'},
                                         // {text: "확인", onPress : () => Actions.Login_regist_step_end({ phone : this.props.phone_num, nickname: this.state.nickname, password: this.state.password})}
                                         {text: "확인", onPress: () => _this.afterLoginFacebook()}
                                     ]
                                 );
                             }}
                             onLoginNotFound={function(){
                                 console.log("No user logged in.");
                                 _this.setState({ facebookToken: "", fbUserData: null });
                             }}
                             onError={function(data){
                                 console.log("ERROR");
                                 console.log(data);
                             }}
                             onCancel={function(){
                                 console.log("User cancelled.");
                             }}
                             onPermissionsMissing={function(data){
                                 console.log("Check permissions!");
                                 console.log(data);
                             }}
                    />
                    <TouchableHighlight underlayColor={'transparent'}
                                        onPress={() => this.onPressJaritso()}>
                        <Image style={styles.btn_login} source={require("../../assets/Login/login_hoco.png")}/>
                    </TouchableHighlight>
                </View>
            </View>
        )
    }
}

export default Login_regist_step_two;
