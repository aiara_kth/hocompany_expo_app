import React from 'react';
import {
    Text,
    View,
    StyleSheet,
    Image,
    TouchableHighlight,
    Dimensions,
    AsyncStorage
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import {Ionicons} from '@expo/vector-icons';
import User from '../../objects/User'
import Global from "../../objects/Global";


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    Navigator : {
        marginTop : getStatusBarHeight(),
        width: '100%',
        height: 48,
        backgroundColor: '#fff',
    },
    back_button : {
        width:  48,
        height: 48,
        paddingTop: 12,
        paddingLeft: 12
    },
    back_button_b : {
        width: 24,
        height: 24
    },
    header : {
        marginTop: 48,
        width: '100%',
        alignItems: 'center',
    },
    title_regist_end : {
        marginTop : ((Dimensions.get('window').height / 8) - 48),
        alignItems: 'center',
    },
    title_regist_end_ts : {
        fontSize: 24,
        color: '#000000',
        alignItems: 'center',
    },
    register_end_check : {
        marginTop : (Dimensions.get('window').height / 8),
        width: 80,
        height: 80
    }, go_home_buttom : {
        position: 'absolute',
        bottom:0,
        left:0,
        width: (Dimensions.get('window').width),
        height: 56,
        backgroundColor: '#eb5847',
    }, go_home_buttom_text : {
        fontSize: 16,
        color:'#FFFFFF'
    }, go_home_touchable : {
        width: '100%',
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center'
    },

    dummy_input : {
        marginTop : (Dimensions.get('window').height / 32),
    }

});

class Login_regist_step_end extends  React.Component {
    constructor(props) {
        super(props);
        this.saveLoginData();
    }

    async saveLoginData() {
        try {
            AsyncStorage.removeItem('@auth');
            Global.user = new User().setUser(
                this.props.customToken,
                this.props.nickname,
                this.props.email,
                this.props.phone,
                this.props.profilePhotoUrl,
                User.APP.SHOP,
                this.props.loginType
            );
            Global.user.getAuth();
        } catch (e) {}
    }

    render() {
        return (
            <View style={styles.container} behavior="padding" enabled>
                <View style={styles.Navigator}>
                    <TouchableHighlight underlayColor={'transparent'} onPress={() => Actions.pop()}>
                        <View style={styles.back_button}>
                            <Image style={styles.back_button_b} source={require("../../assets/back_arrow.png")}/>
                        </View>
                    </TouchableHighlight>
                </View>
                <View style={styles.header}>
                    <View style={styles.title_regist_end}>
                        <Text style={styles.title_regist_end_ts}>회원가입이</Text>
                        <Text style={styles.title_regist_end_ts}>완료되었습니다.</Text>
                        <Image style={styles.register_end_check} source={require("../../assets/register_end.png")}/>
                    </View>
                    {/*<View style={styles.dummy_input}>*/}
                        {/*<Text>입력확인용(라이브 일시 삭제)</Text>*/}
                        {/*<Text>휴대폰번호 : {this.props.phone}</Text>*/}
                        {/*<Text>닉네임 : {this.props.nickname}</Text>*/}
                        {/*<Text>비밀번호 : {this.props.password}</Text>*/}
                    {/*</View>*/}
                </View>
                <View style={styles.go_home_buttom}>
                    <TouchableHighlight style={styles.go_home_touchable} underlayColor={'transparent'} onPress={() => Actions.Biz_change_shop_regist_one()}>
                        <Text style={styles.go_home_buttom_text}>가게 등록하기</Text>
                    </TouchableHighlight>
                </View>
            </View>
        )
    }
}

export default Login_regist_step_end;
