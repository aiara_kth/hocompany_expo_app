import React from 'react';
import {Text, View, StyleSheet, Image,TouchableHighlight, Dimensions, AsyncStorage,} from 'react-native';
import { Actions } from 'react-native-router-flux';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import {Ionicons} from '@expo/vector-icons';
import '../../../objects/Global.js';
import Global from "../../../objects/Global";


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f9f9f9',
    },
    Navigator : {
        flexDirection: 'row',
        marginTop : getStatusBarHeight(),
        width: Dimensions.get('window').width,
        height: 48,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    Navigator_menu: {
        width: 80,
        height: 48,
    },
    Navigator_menu_button :{
        width: 48,
        height: 48,
        paddingTop: 12,
        paddingLeft: 12,
    },
    Navigator_menu_image : {
        width: 24,
        height: 24
    },
    Navigator_title:{
        width : ((Dimensions.get('window').width) - 160),
        alignItems: 'center',
    },
    Navigator_title_text:{
        fontSize: 16,
        color:'#000000'
    },
    Navigator_start:{
        alignItems: 'flex-end',
        width: 80,
        height: 48,
        paddingRight: 12,
    },
    Navigator_start_text:{
        fontSize: 16,
        color:'#7db840',
        marginTop: 12,
        marginBottom: 12
    },
    sub_Navigator : {
        flexDirection: 'row',
        width: Dimensions.get('window').width,
        height: 40,
        backgroundColor: '#fff',
        alignItems: 'center',
    },
    sub_Navigator_menu: {
        width: 60,
        height: 40,
        alignItems: 'flex-end',
        justifyContent: 'center',
    },
    sub_Navigator_menu_on: {
        width: 40,
        height: 40,
        alignItems: 'center',
        justifyContent: 'center',
        borderBottomWidth: 2,
        borderBottomColor: '#000000',
    },
    sub_Navigator_menu_off: {
        width: 40,
        height: 40,
        alignItems: 'center',
        justifyContent: 'center',
        borderBottomWidth: 2,
        borderBottomColor: 'transparent',
    },
    sub_Navigator_name_on : {
        fontSize : 14,
        fontWeight: 'bold',
    },
    sub_Navigator_name_off : {
        fontSize : 14,
    },
    contents : {
        paddingTop :  9,
    },
    top_count : {
        width: Dimensions.get('window').width,
        height: 48,
        backgroundColor : '#30cbba',
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: 8
    },
    top_count_text : {
        fontSize: 16,
        color: '#fff'
    },
    contents_item : {
        width: Dimensions.get('window').width,
        height: 48,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    contents_item_sub : {
        flexDirection: 'row',
        width: Dimensions.get('window').width,
        backgroundColor: '#fff',
        marginBottom: 8,
        paddingLeft: 10,
        paddingRight: 10,
        alignItems: 'center',
        justifyContent: 'center',
    },
    item_date : {
        fontSize: 16,
        color: '#838383',
        fontWeight: '600',
        height: 20
    },
    item_group : {
        flex: 1,
        paddingLeft: 12,
    },
    item_text: {
        marginTop: 12,
        fontSize: 14,
        height: 20
    },
    item_text_sub : {
        fontSize: 12,
        lineHeight: 20,
        color: '#c4c4c4'
    },
    item_text_g: {
        marginTop: 12,
        fontSize: 14,
        height: 20,
        color: '#f9f9f9'
    },
    item_switch_top : {
        marginTop: 2
    },
    item_switch : {
        width: 48,
        height: 24
    },
    item_image_group : {
        marginLeft: 12,
        width: 24,
        height: 24
    },
    item_image : {
        width: 24,
        height: 24
    },

    main_bottom_bar : {
        flexDirection: 'row',
        width: Dimensions.get('window').width,
        height: 56,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
        position:'absolute',
        top: (Dimensions.get('window').height - 56),
        left: 0,
        bottom: 0
    },
    icon_buttom:{
        width: Dimensions.get('window').width / 5,
        height: 56,
        alignItems: 'center'
    },
    icon_buttom_image :{
        marginTop: 19,
        width: 18,
        height: 18,
        resizeMode: 'contain'
    },
});


class Manager_noti extends  React.Component {
    constructor(props) {
        super(props);
        this.state = {
            push_sound: true,
            push_vibe: true,
            ad_call: true,
            ad_event: true,
            ad_marketing: true,
            ad_review: true
        };
    }

    async componentDidMount() {
        if (Global.user !== null) {
            this.setState({
                push_sound: Global.user.push_sound,
                push_vibe: Global.user.push_vibe,
                ad_call: Global.user.userSetting.call,
                ad_event: Global.user.userSetting.event,
                ad_marketing: Global.user.userSetting.marketing,
                ad_review: Global.user.userSetting.review
            });
        } else {
            const savedState = await AsyncStorage.getItem('@setting');
            this.setState(JSON.parse(savedState));
        }
    }

    checkPushOn() {
        return this.state.push_sound && this.state.push_vibe;
    }

    checkAdOn() {
        return this.state.ad_call && this.state.ad_event && this.state.ad_marketing && this.state.ad_review;
    }

    saveChange(push_sound, push_vibe, ad_call, ad_event, ad_marketing, ad_review) {
        const newState = {
            push_sound: push_sound,
            push_vibe: push_vibe,
            ad_call: ad_call,
            ad_event: ad_event,
            ad_marketing: ad_marketing,
            ad_review: ad_review
        };
        this.setState(newState);
        if (Global.user !== null) {
            Global.user.userSetting.notification = push_sound && push_vibe;
            Global.user.push_sound = push_sound;
            Global.user.push_vibe = push_vibe;
            Global.user.ad_on = ad_call && ad_event && ad_marketing && ad_review;
            Global.user.userSetting.call = ad_call;
            Global.user.userSetting.event = ad_event;
            Global.user.userSetting.marketing = ad_marketing;
            Global.user.userSetting.review = ad_review;
            Global.user.saveToLocalStorage();
            Global.user.saveToServer();
        } else {
            const stateString = JSON.stringify(newState);
            AsyncStorage.setItem('@setting', stateString);
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={{height: Dimensions.get('window').height - getStatusBarHeight() - 56}}>
                    <View style={styles.Navigator}>
                        <View style={styles.Navigator_menu}>
                            <TouchableHighlight style={styles.Navigator_menu_button} underlayColor={'transparent'}
                                                onPress={() => Actions.pop()}>
                                <Image style={styles.Navigator_menu_image}
                                       source={require("../../../assets/back_arrow.png")}/>
                            </TouchableHighlight>
                        </View>
                        <View style={styles.Navigator_title}><Text
                            style={styles.Navigator_title_text}>알림설정</Text></View>
                        <TouchableHighlight underlayColor={'transparent'} onPress={() => null}>
                            <View style={styles.Navigator_start}><Text
                                style={styles.Navigator_start_text}></Text></View>
                        </TouchableHighlight>
                    </View>
                    <View style={{marginTop: 9}}>
                        <View style={styles.contents_item}>
                            <View style={styles.contents_item_sub}>
                                <View style={styles.item_group}>
                                    <Text style={[styles.item_text, {fontWeight: 'bold'}]}>푸쉬 알림 <Text
                                        style={styles.item_text_sub}>전체</Text></Text>
                                </View>
                                <TouchableHighlight underlayColor={'transparent'}
                                                    onPress={() => {
                                                        const pushOn = this.checkPushOn();
                                                        this.saveChange(
                                                            !pushOn,
                                                            !pushOn,
                                                            this.state.ad_call,
                                                            this.state.ad_event,
                                                            this.state.ad_marketing,
                                                            this.state.ad_review
                                                        );
                                                    }}>
                                    <View style={styles.item_switch_top}>
                                        <Image style={styles.item_switch}
                                               source={this.checkPushOn() ? require("../../../assets/common/switch_on.png") : require("../../../assets/common/switch_off.png")}/>
                                    </View>
                                </TouchableHighlight>
                            </View>
                        </View>
                    </View>
                    <View style={this.checkPushOn() ? {display: 'none', position: 'relative'} : {}}>
                        <View style={styles.contents_item}>
                            <View style={styles.contents_item_sub}>
                                <View style={styles.item_group}>
                                    <Text style={styles.item_text}>소리</Text>
                                </View>
                                <TouchableHighlight underlayColor={'transparent'}
                                                    onPress={() => this.saveChange(
                                                        !this.state.push_sound,
                                                        this.state.push_vibe,
                                                        this.state.ad_call,
                                                        this.state.ad_event,
                                                        this.state.ad_marketing,
                                                        this.state.ad_review)}>
                                    <View style={styles.item_switch_top}>
                                        <Image style={styles.item_switch}
                                               source={this.state.push_sound ? require("../../../assets/common/switch_on.png") : require("../../../assets/common/switch_off.png")}/>
                                    </View>
                                </TouchableHighlight>
                            </View>
                        </View>
                    </View>
                    <View style={this.checkPushOn() ? {display: 'none', position: 'relative'} : {}}>
                        <View style={styles.contents_item}>
                            <View style={styles.contents_item_sub}>
                                <View style={styles.item_group}>
                                    <Text style={styles.item_text}>진동</Text>
                                </View>
                                <TouchableHighlight underlayColor={'transparent'}
                                                    onPress={() => this.saveChange(
                                                        this.state.push_sound,
                                                        !this.state.push_vibe,
                                                        this.state.ad_call,
                                                        this.state.ad_event,
                                                        this.state.ad_marketing,
                                                        this.state.ad_review)}>
                                    <View style={styles.item_switch_top}>
                                        <Image style={styles.item_switch}
                                               source={this.state.push_vibe ? require("../../../assets/common/switch_on.png") : require("../../../assets/common/switch_off.png")}/>
                                    </View>
                                </TouchableHighlight>
                            </View>
                        </View>
                    </View>
                    <View style={{marginTop: 9}}>
                        <View style={styles.contents_item}>
                            <View style={styles.contents_item_sub}>
                                <View style={styles.item_group}>
                                    <Text style={[styles.item_text, {fontWeight: 'bold'}]}>광고 및 마케팅 알림 <Text
                                        style={styles.item_text_sub}>전체</Text></Text>
                                </View>
                                <TouchableHighlight underlayColor={'transparent'} onPress={() => {
                                    const adOn = this.checkAdOn();
                                    this.saveChange(
                                        this.state.push_sound,
                                        this.state.push_vibe,
                                        !adOn,
                                        !adOn,
                                        !adOn,
                                        !adOn
                                    );
                                }}>
                                    <View style={styles.item_switch_top}>
                                        <Image style={styles.item_switch}
                                               source={this.checkAdOn() ? require("../../../assets/common/switch_on.png") : require("../../../assets/common/switch_off.png")}/>
                                    </View>
                                </TouchableHighlight>
                            </View>
                        </View>
                    </View>
                    <View style={this.checkAdOn() ? {display: 'none', position: 'relative'} : {}}>
                        <View style={styles.contents_item}>
                            <View style={styles.contents_item_sub}>
                                <View style={styles.item_group}>
                                    <Text style={styles.item_text}>가게 콜 알림</Text>
                                </View>
                                <TouchableHighlight underlayColor={'transparent'}
                                                    onPress={() => this.saveChange(
                                                        this.state.push_sound,
                                                        this.state.push_vibe,
                                                        !this.state.ad_call,
                                                        this.state.ad_event,
                                                        this.state.ad_marketing,
                                                        this.state.ad_review)}>
                                    <View style={styles.item_switch_top}>
                                        <Image style={styles.item_switch}
                                               source={this.state.ad_call ? require("../../../assets/common/switch_on.png") : require("../../../assets/common/switch_off.png")}/>
                                    </View>
                                </TouchableHighlight>
                            </View>
                        </View>
                    </View>
                    <View style={this.checkAdOn() ? {display: 'none', position: 'relative'} : {}}>
                        <View style={styles.contents_item}>
                            <View style={styles.contents_item_sub}>
                                <View style={styles.item_group}>
                                    <Text style={styles.item_text}>이벤트 혜택 알림</Text>
                                </View>
                                <TouchableHighlight underlayColor={'transparent'}
                                                    onPress={() => this.saveChange(
                                                        this.state.push_sound,
                                                        this.state.push_vibe,
                                                        this.state.ad_call,
                                                        !this.state.ad_event,
                                                        this.state.ad_marketing,
                                                        this.state.ad_review)}>
                                    <View style={styles.item_switch_top}>
                                        <Image style={styles.item_switch}
                                               source={this.state.ad_event ? require("../../../assets/common/switch_on.png") : require("../../../assets/common/switch_off.png")}/>
                                    </View>
                                </TouchableHighlight>
                            </View>
                        </View>
                    </View>
                    <View style={this.checkAdOn() ? {display: 'none', position: 'relative'} : {}}>
                        <View style={styles.contents_item}>
                            <View style={styles.contents_item_sub}>
                                <View style={styles.item_group}>
                                    <Text style={styles.item_text}>마케팅 광고 알림</Text>
                                </View>
                                <TouchableHighlight underlayColor={'transparent'}
                                                    onPress={() => this.saveChange(
                                                        this.state.push_sound,
                                                        this.state.push_vibe,
                                                        this.state.ad_call,
                                                        this.state.ad_event,
                                                        !this.state.ad_marketing,
                                                        this.state.ad_review)}>
                                    <View style={styles.item_switch_top}>
                                        <Image style={styles.item_switch}
                                               source={this.state.ad_marketing ? require("../../../assets/common/switch_on.png") : require("../../../assets/common/switch_off.png")}/>
                                    </View>
                                </TouchableHighlight>
                            </View>
                        </View>
                    </View>
                    <View style={this.checkAdOn() ? {display: 'none', position: 'relative'} : {}}>
                        <View style={styles.contents_item}>
                            <View style={styles.contents_item_sub}>
                                <View style={styles.item_group}>
                                    <Text style={styles.item_text}>리뷰 알림</Text>
                                </View>
                                <TouchableHighlight underlayColor={'transparent'}
                                                    onPress={() => this.saveChange(
                                                        this.state.push_sound,
                                                        this.state.push_vibe,
                                                        this.state.ad_call,
                                                        this.state.ad_event,
                                                        this.state.ad_marketing,
                                                        !this.state.ad_review)}>
                                    <View style={styles.item_switch_top}>
                                        <Image style={styles.item_switch}
                                               source={this.state.ad_review ? require("../../../assets/common/switch_on.png") : require("../../../assets/common/switch_off.png")}/>
                                    </View>
                                </TouchableHighlight>
                            </View>
                        </View>
                    </View>
                </View>
                <View style={styles.main_bottom_bar}>
                    <TouchableHighlight underlayColor={'transparent'} onPress={() => Actions.pop()}>
                        <View style={styles.icon_buttom}>
                            <Image style={styles.icon_buttom_image}
                                   source={require("../../../assets/User/Bottom/home_gray.png")}/>
                        </View>
                    </TouchableHighlight>
                    <TouchableHighlight underlayColor={'transparent'} onPress={() => Actions.User_my_reserved()}>
                        <View style={styles.icon_buttom}>
                            <Image style={styles.icon_buttom_image}
                                   source={require("../../../assets/User/Bottom/utensils_gray.png")}/>
                        </View>
                    </TouchableHighlight>
                    <TouchableHighlight underlayColor={'transparent'} onPress={() => Actions.User_my_review()}>
                        <View style={styles.icon_buttom}>
                            <Image style={styles.icon_buttom_image}
                                   source={require("../../../assets/User/Bottom/chat_gray.png")}/>
                        </View>
                    </TouchableHighlight>
                    <TouchableHighlight underlayColor={'transparent'} onPress={() => Actions.User_my_contents()}>
                        <View style={styles.icon_buttom}>
                            <Image style={styles.icon_buttom_image}
                                   source={require("../../../assets/User/Bottom/newspaper_gray.png")}/>
                        </View>
                    </TouchableHighlight>
                    <TouchableHighlight underlayColor={'transparent'} onPress={() => Actions.User_my_main()}>
                        <View style={styles.icon_buttom}>
                            <Image style={styles.icon_buttom_image}
                                   source={require("../../../assets/User/Bottom/user_black.png")}/>
                        </View>
                    </TouchableHighlight>
                </View>
            </View>
        )
    }
}

export default Manager_noti;