import React from 'react';
import {Text, View, StyleSheet, Image,TouchableHighlight, Dimensions, ScrollView, Menu} from 'react-native';
import { Actions } from 'react-native-router-flux';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import {Ionicons} from '@expo/vector-icons';
import Global from "../../../objects/Global";
import API from "../../../objects/API";


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f9f9f9',
    },
    Navigator : {
        flexDirection: 'row',
       paddingTop : getStatusBarHeight(),
        width: Dimensions.get('window').width,
        height: 48 + getStatusBarHeight(),
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    Navigator_menu: {
        width: 80,
        height: 48,
    },
    Navigator_menu_button :{
        width: 48,
        height: 48,
        paddingTop: 12,
        paddingLeft: 12,
    },
    Navigator_menu_image : {
        width: 24,
        height: 24
    },
    Navigator_title:{
        width : ((Dimensions.get('window').width) - 160),
        alignItems: 'center',
    },
    Navigator_title_text:{
        fontSize: 16,
        color:'#000000'
    },
    Navigator_start:{
        alignItems: 'flex-end',
        width: 80,
        height: 48,
        paddingTop: 12,
        paddingRight: 12,
    },
    Navigator_start_text:{
        fontSize: 16,
        color:'#7f8fd1',
    },
    callout_box : {
        width : (Dimensions.get('window').width),
        height: 40,
        borderRadius: 2,
        backgroundColor: '#7f8fd1',
        alignItems: 'center',
        justifyContent: 'center',
        shadowOffset: {width: 2, height: 10 },
        shadowColor: 'rgba(0, 0, 0, 0.2)',
        paddingRight: 56
    },
    callout_box_text : {
        fontSize : 12,
        color: '#ffffff'
    },
    center_button:{
        width: Dimensions.get('window').width,
        height: ((Dimensions.get('window').height) - 48),
        alignItems: 'center',
        justifyContent: 'center',
    },
    buttom_button:{
        position: 'absolute',
        marginBottom : ((Dimensions.get('window').height /  2) - 120)
    },
    add_button:{
        width: Dimensions.get('window').width,
        height: 56,
    },
    add_button_text : {
        position: 'absolute',
        left: ((Dimensions.get('window').width / 2) - '2%'),
        top : ((Dimensions.get('window').height /  2) + 30)
    },
    userButton_group : {
        width: Dimensions.get('window').width,
        height: 81,
        paddingTop :  9,
        flexDirection: 'row',
    },
    userButton :{
        width: Dimensions.get('window').width / 4,
        height: 72,
        backgroundColor: '#fff',
        alignItems: 'center',
        flex: 1,
        paddingTop: 16
    },
    userButton_image: {
        width: 24,
        height: 24
    },
    userButton_text: {
        fontSize: 14
    },
    main_bottom_bar : {
        flexDirection: 'row',
        width: Dimensions.get('window').width,
        height: 56,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    icon_buttom:{
        width: Dimensions.get('window').width / 5,
        height: 56,
        alignItems: 'center'
    },
    icon_buttom_image :{
        marginTop: 19,
        width: 18,
        height: 18,
        resizeMode: 'contain'
    },
    contents : {
        paddingTop :  9,
        height: (Dimensions.get('window').height - (104 + getStatusBarHeight())),
    },
    contents_item: {
        width: (Dimensions.get('window').width - 40),
        height: 96,
        backgroundColor: '#ffffff',
        marginLeft: 20,
        marginBottom: 10,
        flexDirection: 'row'
    },
    imgicon: {
        width: 16,
        height: 16,
        resizeMode: 'contain'
    },
    imgicon_b: {
        width: 20,
        height: 20,
        resizeMode: 'contain'
    },
    imgicon_s: {
        width: 12,
        height: 12,
        resizeMode: 'contain'
    },
    imgicon_pin: {
        width: 10,
        height: 14,
        marginTop:1,
        resizeMode: 'contain'
    },
    star_image_dom: {
        width: 90,
        height: 96,
        paddingLeft:6,
        paddingTop:6
    },
    star_image:{
        width:84,
        height: 84
    },
    star_info: {
        width: (Dimensions.get('window').width - 124),
        height: 96,
        paddingLeft: 20
    },
    star_info_memter : {
        paddingLeft: (Dimensions.get('window').width - 220),
        marginTop: 6,
        paddingRight: 16,
        flexDirection: 'row'
    },
    star_info_memter_text : {
        fontSize : 12,
        color : '#7f8fd1',
    },
    star_info_store_name : {
        fontSize:12,
        color: '#7db840'
    },
    star_info_food_name : {
        fontSize: 20
    },
    star_info_extra : {
        marginTop : 12,
        fontSize: 12,
        lineHeight: 16,
    }

});

class User_my_star extends  React.Component {
    constructor(props) {
        super(props);
        this.state = {
            text: '',
            biz_shop_data: null,
            bookmarkShops: [
                {
                    shopId: 0,
                    type: "이자카야",
                    name: "공릉동 솔깨비",
                    distance: 200,
                    likeCount: 6,
                    startTime: "16:00",
                    endTime: "3:00"
                },
                {
                    shopId: 1,
                    type: "이자카야",
                    name: "공릉동 솔깨비",
                    distance: 200,
                    likeCount: 6,
                    startTime: "16:00",
                    endTime: "3:00"
                },
                {
                    shopId: 2,
                    type: "이자카야",
                    name: "공릉동 솔깨비",
                    distance: 200,
                    likeCount: 6,
                    startTime: "16:00",
                    endTime: "3:00"
                },
                {
                    shopId: 3,
                    type: "이자카야",
                    name: "공릉동 솔깨비",
                    distance: 200,
                    likeCount: 6,
                    startTime: "16:00",
                    endTime: "3:00"
                },
                {
                    shopId: 4,
                    type: "이자카야",
                    name: "공릉동 솔깨비",
                    distance: 200,
                    likeCount: 6,
                    startTime: "16:00",
                    endTime: "3:00"
                },
                {
                    shopId: 5,
                    type: "이자카야",
                    name: "공릉동 솔깨비",
                    distance: 200,
                    likeCount: 6,
                    startTime: "16:00",
                    endTime: "3:00"
                },
                {
                    shopId: 6,
                    type: "이자카야",
                    name: "공릉동 솔깨비",
                    distance: 200,
                    likeCount: 6,
                    startTime: "16:00",
                    endTime: "3:00"
                },
                {
                    shopId: 7,
                    type: "이자카야",
                    name: "공릉동 솔깨비",
                    distance: 200,
                    likeCount: 6,
                    startTime: "16:00",
                    endTime: "3:00"
                },
                {
                    shopId: 8,
                    type: "이자카야",
                    name: "공릉동 솔깨비",
                    distance: 200,
                    likeCount: 6,
                    startTime: "16:00",
                    endTime: "3:00"
                },
                {
                    shopId: 9,
                    type: "이자카야",
                    name: "공릉동 솔깨비",
                    distance: 200,
                    likeCount: 6,
                    startTime: "16:00",
                    endTime: "3:00"
                },
                {
                    shopId: 10,
                    type: "이자카야",
                    name: "공릉동 솔깨비",
                    distance: 200,
                    likeCount: 6,
                    startTime: "16:00",
                    endTime: "3:00"
                }
            ]
        };
        // todo 즐겨찾기 한 리스트를 받아오자
        if (API.user !== null) {
            API.user.getUserBookmark(data => {
                // todo this.state.bookmarkShops 갱신
            }, error => {

            });
        }
    }

    printBookmarkShop(shop) {
        return <View key={shop.shopId} style={styles.contents_item}>
            <View style={styles.star_image_dom}>
                <Image style={styles.star_image}
                       source={require("../../../assets/User/My/dummy_menu_images.png")}/>
                <Image style={{zIndex: 9998, width: 48, height: 16, marginTop: -88, marginLeft: -4}}
                       source={require("../../../assets/common/event_label_w.png")}/>
            </View>
            <View style={styles.star_info}>
                <View style={styles.star_info_memter}>
                    <Image style={styles.imgicon_pin} source={require("../../../assets/common/pin.png")}/>
                    <Text style={styles.star_info_memter_text}> {shop.distance}M</Text>
                </View>
                <Text style={styles.star_info_store_name}>{shop.type}</Text>
                <Text style={styles.star_info_food_name}>{shop.name}</Text>
                <Text style={styles.star_info_extra}>
                    <Text
                        style={{color: '#eb5847', lineHeight: 16,}}><Image style={styles.imgicon_b}
                                                                           source={require("../../../assets/common/heart.png")}/> {shop.likeCount}
                    </Text> <Text style={{color: '#c4c4c4', lineHeight: 16,}}><Image style={styles.imgicon_b}
                                                                                     source={require("../../../assets/common/clock.png")}/> {shop.startTime} ~ {shop.endTime}
                </Text>
                </Text>
            </View>
        </View>;
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.Navigator}>
                    <View style={styles.Navigator_menu}>
                        <TouchableHighlight style={styles.Navigator_menu_button} underlayColor={'transparent'}
                                            onPress={() => Actions.pop()}>
                            <Image style={styles.Navigator_menu_image}
                                   source={require("../../../assets/back_arrow.png")}/>
                        </TouchableHighlight>
                    </View>
                    <View style={styles.Navigator_title}><Text style={styles.Navigator_title_text}>즐겨찾기</Text></View>
                    <TouchableHighlight underlayColor={'transparent'} onPress={() => null}>
                        <View style={styles.Navigator_start}><Text style={styles.Navigator_start_text}/></View>
                    </TouchableHighlight>
                </View>
                <View style={styles.contents}>
                    <ScrollView>
                        {
                            this.state.bookmarkShops.map(shop => this.printBookmarkShop(shop))
                        }
                    </ScrollView>
                </View>
                <View style={styles.main_bottom_bar}>
                    <TouchableHighlight underlayColor={'transparent'} onPress={() => Actions.pop()}>
                        <View style={styles.icon_buttom}>
                            <Image style={styles.icon_buttom_image}
                                   source={require("../../../assets/User/Bottom/home_gray.png")}/>
                        </View>
                    </TouchableHighlight>
                    <TouchableHighlight underlayColor={'transparent'} onPress={() => Actions.User_my_reserved()}>
                        <View style={styles.icon_buttom}>
                            <Image style={styles.icon_buttom_image}
                                   source={require("../../../assets/User/Bottom/utensils_gray.png")}/>
                        </View>
                    </TouchableHighlight>
                    <TouchableHighlight underlayColor={'transparent'} onPress={() => Actions.User_my_review()}>
                        <View style={styles.icon_buttom}>
                            <Image style={styles.icon_buttom_image}
                                   source={require("../../../assets/User/Bottom/chat_gray.png")}/>
                        </View>
                    </TouchableHighlight>
                    <TouchableHighlight underlayColor={'transparent'} onPress={() => Actions.User_my_contents()}>
                        <View style={styles.icon_buttom}>
                            <Image style={styles.icon_buttom_image}
                                   source={require("../../../assets/User/Bottom/newspaper_gray.png")}/>
                        </View>
                    </TouchableHighlight>
                    <TouchableHighlight underlayColor={'transparent'} onPress={() => Actions.User_my_main()}>
                        <View style={styles.icon_buttom}>
                            <Image style={styles.icon_buttom_image}
                                   source={require("../../../assets/User/Bottom/user_black.png")}/>
                        </View>
                    </TouchableHighlight>
                </View>
            </View>
        )
    }
}

export default User_my_star;
