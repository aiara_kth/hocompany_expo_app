import React from 'react';
import {Text, View, StyleSheet, Image,TextInput,TouchableHighlight, Dimensions, ScrollView, Alert ,Menu} from 'react-native';
import { Actions } from 'react-native-router-flux';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import {Ionicons} from '@expo/vector-icons';
import API from '../../../objects/API';
import Global from "../../../objects/Global";

var commaNumber = require('comma-number');

const styles = StyleSheet.create({
    container: {
        flex: 1,
        // backgroundColor: '#f9f9f9',
        backgroundColor: '#fff',
    },
    Navigator : {
        flexDirection: 'row',
        marginTop : getStatusBarHeight(),
        width: Dimensions.get('window').width,
        height: 48,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    Navigator_menu: {
        width: 80,
        height: 48,
    },
    Navigator_menu_button :{
        width: 48,
        height: 48,
        paddingTop: 12,
        paddingLeft: 12,
    },
    Navigator_menu_image : {
        width: 24,
        height: 24
    },
    Navigator_title:{
        width : ((Dimensions.get('window').width) - 160),
        alignItems: 'center',
    },
    Navigator_title_text:{
        fontSize: 16,
        color:'#000000'
    },
    Navigator_start:{
        alignItems: 'flex-end',
        width: 80,
        height: 48,
        paddingTop: 12,
        paddingRight: 12,
    },
    Navigator_start_text:{
        fontSize: 16,
        color:'#7db840',
    },
    contents : {
        // height: (Dimensions.get('window').height - (104 + getStatusBarHeight())),
        // paddingTop : (Dimensions.get('window').height / 10),
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    contents_where : {
        width: Dimensions.get('window').width - 24,
        marginLeft: 12,
        fontSize: 16,
        color: '#000000'
    },
    contents_sub : {
        width: Dimensions.get('window').width - 24,
        marginLeft: 12,
        height: 220
    },
    contents_sub_s : {
        width: Dimensions.get('window').width - 24,
        marginLeft: 12,
        height: 128
    },
    contents_profile : {
        height: 25,
        marginTop: 16,
        flexDirection: 'row'
    },
    contents_profile_image : {
        width: 28,
        height: 28,
        marginTop: 2
    },
    contents_profile_name : {
        fontSize: 16,
        marginLeft: 10,
        marginTop: 4
    },
    contents_profile_date :{
        marginTop: 8,
        fontSize: 10,
        color: '#c4c4c4',
        marginLeft: 4
    },
    contents_info : {
        marginTop: 10,
        width: Dimensions.get('window').width - 36,
        fontSize: 14,
        paddingLeft: 40,
        color: '#606060'
    },
    contents_extra : {
        marginTop: 16,
        fontSize:12 ,
        color: '#606060'
    },
    contents_extra_image : {
        width: 20,
        height: 20
    },
    contents_store_icon : {
        width: 16,
        height: 16
    },
    contents_store : {
        width: Dimensions.get('window').width - 36,
        height: 56,
        flexDirection: 'row',
        borderWidth: 2,
        borderColor: 'rgba(0, 0, 0, 0.1)'
    },
    contents_store_left : {
        width: Dimensions.get('window').width - 74,
        paddingLeft: 20,
        paddingTop: 8,
    },
    contents_store_right : {
        width: 9,
        height:9,
        marginLeft: 16,
        marginRight: 23,
        marginTop: 23
    },
    contents_store_left_title : {
        fontSize: 16
    },
    contents_store_left_extra :{
        fontSize: 10
    },
    contents_replay : {

    },
    reply_add : {
        marginTop: 6,
        fontSize: 14,
        color: '#eb5847',
        paddingLeft: 40,
    },
    reply_input_left:{
        width: 40,
        height: 23
    },
    reply_input:{
        fontSize: 12,
        color: '#838383',
        width: Dimensions.get('window').width - 85,
        height: 23,
        borderTopWidth: 1,
        borderBottomWidth: 1
    },
    comment_input:{
        fontSize: 12,
        color: '#838383',
        width: Dimensions.get('window').width - 35,
        height: 23,
        borderTopWidth: 1,
        borderBottomWidth: 1
    }


});

const defaultProfileImgUri = "../../../assets/profile_dummy.png";
const defaultProfileImg = require(defaultProfileImgUri);

class User_my_review_view extends  React.Component {
    review = this.props.review;
    shop = this.props.shop;

    replyTextArray = [];

    constructor(props) {
        super(props);
        // todo 리뷰 데이터를 props로 받자
        this.review.comment.map(() => {
            this.replyTextArray.push("");
        });
        this.state = {
            text : '',
            biz_shop_data: null,
            isLiked: false, // todo 좋아요를 이미 누른 리뷰인지 체크
            writeComment: false,
            writeReplyTo: -1,
            commentText: "",
            replyText: this.replyTextArray
        };
    }

    onPressAddReply(replyTo) {
        this.setState({writeReplyTo: replyTo});
    }

    onReplyChange(text, index) {
        this.replyTextArray[index] = text;
        this.setState({replyText: this.replyTextArray});
    }

    addReply(index) { // comment 댓글
        const text = this.state.replyText[index];
        // todo 대댓글 작성 - api 있나?
        this.replyTextArray[index] = "";
        this.setState({writeReplyTo: -1, replyText: this.replyTextArray});
    }

    onPressAddComment() {
        this.setState({writeComment: !this.state.writeComment});
    }

    onCommentChange(text) {
        this.setState({commentText: text});
    }

    addComment() { // review 댓글
        const text = this.state.commentText;
        if (Global.user !== null) {
            API.revision2.comment.post(this.shop.shopId, this.review.reviewId, text, new Date(), data => {
                if (data.errorCode !== "user/requirements-unmet" && data.message !== "Internal server error") {
                    console.log(data);
                } else {
                    console.log(data);
                    this.review.comment.reverse();
                    this.review.comment.reverse();
                    this.review.comment.push({
                        commentUser: {
                            profilePhotoUrl: Global.user !== null ? {
                                uri: Global.user.profilePhotoUrl,
                                cache: 'only-if-cached'
                            } : defaultProfileImg,
                            nickname: Global.user !== null ? Global.user.username : "김민지"
                        },
                        commentTime: "방금전",
                        content: text
                    });
                    this.review.comment.reverse();
                }
            }, error => {
                Alert.alert('회원가입', '서버문제로 답글 작성에 실패하였습니다. \n' + error, [{text: "확인", onPress: () => null}]);
            });
        } else {
            this.review.comment.reverse();
            this.review.comment.reverse();
            this.review.comment.push({
                commentUser: {
                    profilePhotoUrl: Global.user !== null ? {
                        uri: Global.user.profilePhotoUrl,
                        cache: 'only-if-cached'
                    } : defaultProfileImg,
                    nickname: Global.user !== null ? Global.user.username : "김민지"
                },
                commentTime: "방금전",
                content: text
            });
            this.review.comment.reverse();
        }
        this.setState({writeComment: false, commentText: ""});
    }

    renderComment(comment, index) {
        return <View style={styles.contents_sub_s} key={index}>
            <View style={styles.contents_profile}>
                <Image style={styles.contents_profile_image} source={comment.commentUser.profilePhotoUrl}/>
                <Text style={styles.contents_profile_name}>{comment.commentUser.nickname}</Text>
                <Text style={styles.contents_profile_date}>{comment.commentTime}</Text>
            </View>
            <Text style={styles.contents_info}>{comment.content}</Text>
            {
                this.state.writeReplyTo === index
                    ? (
                        <View>
                            <View style={{flexDirection: 'row'}}>
                                <Text style={styles.reply_input_left}></Text>
                                <TextInput ref="user_name" style={styles.reply_input} placeholder={'답글 입력'}
                                           underlineColorAndroid='transparent' retrunKeyType="done"
                                           onChangeText={(text) => this.onReplyChange(text, index)}
                                           value={this.state.replyText[index]}/>
                            </View>
                            <TouchableHighlight underlayColor={'transparent'} onPress={() => this.addReply(index)}>
                                <Text style={styles.reply_add}>확인</Text>
                            </TouchableHighlight>
                        </View>
                    )
                    : (
                        <TouchableHighlight underlayColor={'transparent'} onPress={() => this.onPressAddReply(index)}>
                            <Text style={styles.reply_add}>답글작성</Text>
                        </TouchableHighlight>
                    )
            }
        </View>;
    }

    like(method) {
        if (method === 'POST') {
            API.revision2.likeReview.post(this.shop.shopId, this.review.reviewId, data => {
                console.log(data);
                this.setState({
                    heart: true,
                    modal: 1
                });
            }, error => {
                Alert.alert('좋아요', '서버문제로 리뷰를 좋아요 하는데 실패하였습니다. \n' + error, [{
                    text: "확인",
                    onPress: () => Actions.pop()
                }]);
            });
        } else if (method === 'DELETE') {
            API.revision2.likeReview.delete(this.shop.shopId, this.review.reviewId, data => {
                console.log(data);
                this.setState({
                    heart: true,
                    modal: 1
                });
            }, error => {
                Alert.alert('좋아요', '서버문제로 리뷰를 좋아요를 취소하는데 실패하였습니다. \n' + error, [{
                    text: "확인",
                    onPress: () => Actions.pop()
                }]);
            });
        }
    }

    toggleLike() {
        if (this.state.isLiked) {
            this.like('DELETE');
            this.setState({isLiked: false});
            this.review.likeCount--;
        } else {
            this.like('POST');
            this.setState({isLiked: true});
            this.review.likeCount++;
        }
    }

    render() {
        return (
            <View  style={styles.container}>
                <View style={styles.Navigator}>
                    <View style={styles.Navigator_menu}>
                        <TouchableHighlight style={styles.Navigator_menu_button} underlayColor={'transparent'} onPress={() => Actions.pop()}>
                            <Image style={styles.Navigator_menu_image} source={require("../../../assets/back_arrow.png")}/>
                        </TouchableHighlight>
                    </View>
                    <View style={styles.Navigator_title}><Text style={styles.Navigator_title_text}></Text></View>
                    <TouchableHighlight underlayColor={'transparent'} onPress={() => null}>
                        <View style={styles.Navigator_start}><Text style={styles.Navigator_start_text}></Text></View>
                    </TouchableHighlight>
                </View>
                <ScrollView>
                    <View style={styles.contents}>
                        <View style={[styles.contents_sub, this.state.writeComment ? {height: 266} : {height: 220}]}>
                            <View style={styles.contents_profile}>
                                <Image style={styles.contents_profile_image} source={require("../../../assets/profile_dummy.png")}/>
                                <Text style={styles.contents_profile_name}>{this.review.reviewer.name}</Text>
                                <Text style={styles.contents_profile_date}>{this.review.reviewedTime}</Text>
                            </View>
                            <Text style={styles.contents_info}>{this.review.content}</Text>
                            <View style={{flexDirection: 'row', paddingTop: 16,paddingBottom: 16}}>
                                <TouchableHighlight underlayColor={'transparent'} onPress={() => this.toggleLike()}>
                                    <View style={{flexDirection: 'row'}}>
                                        <Image style={styles.contents_extra_image} source={this.state.isLiked ? require("../../../assets/common/silver_heart.png") : require("../../../assets/common/heart_clear_icon.png")}/>
                                        <Text stlye={styles.contents_extra}> {this.review.likeCount}   </Text>
                                    </View>
                                </TouchableHighlight>
                                <TouchableHighlight underlayColor={'transparent'} onPress={() => this.onPressAddComment()}>
                                    <View style={{flexDirection: 'row'}}>
                                        <Image style={styles.contents_extra_image} source={require("../../../assets/common/reply_icon.png")}/>
                                        <Text stlye={styles.contents_extra}> {this.review.comment.length}</Text>
                                    </View>
                                </TouchableHighlight>
                            </View>
                            {
                                this.state.writeComment
                                    ? (
                                        <View>
                                            <View style={{flexDirection: 'row'}}>
                                                <TextInput ref="user_name" style={styles.comment_input}
                                                           placeholder={'답글 입력'}
                                                           underlineColorAndroid='transparent' retrunKeyType="done"
                                                           onChangeText={(text) => this.onCommentChange(text)}
                                                           value={this.state.commentText}/>
                                            </View>
                                            <TouchableHighlight underlayColor={'transparent'}
                                                                onPress={() => this.addComment()}>
                                                <Text style={[styles.reply_add, {paddingLeft: 0}]}>확인</Text>
                                            </TouchableHighlight>
                                        </View>
                                    )
                                    : (
                                        <Text></Text>
                                    )
                            }
                            <View style={styles.contents_store}>
                                <View style={styles.contents_store_left}>
                                    <Text style={styles.contents_store_left_title}>{this.shop.name}</Text>
                                    <View style={{flexDirection: 'row'}}>
                                        <Image style={styles.contents_store_icon} source={require("../../../assets/common/silver_heart.png")}/>
                                        <Text style={styles.contents_store_left_extra}>{commaNumber(this.shop.likeCount)}    </Text>
                                        <Image style={styles.contents_store_icon} source={require("../../../assets/common/silver_bookmark.png")}/>
                                        <Text style={styles.contents_store_left_extra}>{commaNumber(this.shop.review.length)}</Text>
                                    </View>
                                </View>
                                <Image style={styles.contents_store_right} source={require("../../../assets/common/arrow_right.png")}/>
                            </View>
                        </View>
                        <View style={styles.contents_replay}>
                            <Text style={{fontSize: 16, paddingTop: 20, paddingLeft: 20,paddingBottom: 10,width: Dimensions.get('window').width, backgroundColor: '#f9f9f9', fontWeight: '500'}}>{this.review.comment.length} 댓글</Text>
                            {
                                this.review.comment.map((comment, index) => this.renderComment(comment, index))
                            }
                        </View>
                    </View>
                </ScrollView>
            </View>
        )
    }
}

export default User_my_review_view;
