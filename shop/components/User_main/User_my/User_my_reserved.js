import React from 'react';
import {Text, View, StyleSheet, Image,TouchableHighlight, Dimensions, ScrollView, Menu} from 'react-native';
import { Actions } from 'react-native-router-flux';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import {Ionicons} from '@expo/vector-icons';
import Global from "../../../objects/Global";


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f9f9f9',
    },
    Navigator : {
        flexDirection: 'row',
        marginTop : getStatusBarHeight(),
        width: Dimensions.get('window').width,
        height: 48,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    Navigator_menu: {
        width: 80,
        height: 48,
    },
    Navigator_menu_button :{
        width: 48,
        height: 48,
        paddingTop: 12,
        paddingLeft: 12,
    },
    Navigator_menu_image : {
        width: 24,
        height: 24
    },
    Navigator_title:{
        width : ((Dimensions.get('window').width) - 160),
        alignItems: 'center',
    },
    Navigator_title_text:{
        fontSize: 16,
        color:'#000000'
    },
    Navigator_start:{
        alignItems: 'flex-end',
        width: 80,
        height: 48,
        paddingTop: 12,
        paddingRight: 12,
    },
    Navigator_start_text:{
        fontSize: 16,
        color:'#7f8fd1',
    },
    callout_box : {
        width : (Dimensions.get('window').width),
        height: 40,
        borderRadius: 2,
        backgroundColor: '#7f8fd1',
        alignItems: 'center',
        justifyContent: 'center',
        shadowOffset: {width: 2, height: 10 },
        shadowColor: 'rgba(0, 0, 0, 0.2)',
        paddingRight: 56
    },
    callout_box_text : {
        fontSize : 12,
        color: '#ffffff'
    },
    center_button:{
        width: Dimensions.get('window').width,
        height: ((Dimensions.get('window').height) - 48),
        alignItems: 'center',
        justifyContent: 'center',
    },
    buttom_button:{
        position: 'absolute',
        marginBottom : ((Dimensions.get('window').height /  2) - 120)
    },
    add_button:{
        width: Dimensions.get('window').width,
        height: 56,
    },
    add_button_text : {
        position: 'absolute',
        left: ((Dimensions.get('window').width / 2) - '2%'),
        top : ((Dimensions.get('window').height /  2) + 30)
    },
    userButton_group : {
        width: Dimensions.get('window').width,
        height: 81,
        paddingTop :  9,
        flexDirection: 'row',
    },
    userButton :{
        width: Dimensions.get('window').width / 4,
        height: 72,
        backgroundColor: '#fff',
        alignItems: 'center',
        flex: 1,
        paddingTop: 16
    },
    userButton_image: {
        width: 24,
        height: 24
    },
    userButton_text: {
        fontSize: 14
    },
    main_bottom_bar : {
        flexDirection: 'row',
        width: Dimensions.get('window').width,
        height: 56,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    icon_buttom:{
        width: Dimensions.get('window').width / 5,
        height: 56,
        alignItems: 'center'
    },
    icon_buttom_image :{
        marginTop: 19,
        width: 18,
        height: 18,
        resizeMode: 'contain'
    },
    contents : {
        paddingTop :  9,
        height: (Dimensions.get('window').height - (104 + getStatusBarHeight())),
    },
    contents_item: {
        width: (Dimensions.get('window').width),
        backgroundColor: '#ffffff',
        paddingLeft: 12,
        marginBottom: 8,
    },
    contents_item_date : {
        width: (Dimensions.get('window').width),
        height : 24,
        fontSize: 12,
        paddingTop: 5,
        fontWeight: 'bold'
    },
    contents_item_sub: {
        width: (Dimensions.get('window').width),
        height: 68,
        flexDirection: 'row'
    },
    contents_item_sub_info: {
        width: (Dimensions.get('window').width - 52),
        height: 68,
        paddingTop: 13
    },
    contents_item_sub_info_name: {
        fontSize: 16,
        color: '#606060'
    },
    contents_item_sub_info_extra: {
      fontSize: 12,
      color: '#838383'
    },
    bold : {
      fontWeight: '500'
    },
    contents_item_sub_arrow: {
        width: 40,
        height: 24,
        marginTop: 22,
        paddingLeft: 10
    },
    arrow_right : {
        width:24,
        height:24
    }

});

class User_my_reserved extends  React.Component {
    constructor(props) {
        super(props);
        this.state = {
            text: '',
            biz_shop_data: null,
            my_info : {name : '더미더미'},
            reservations: [
                {
                    reservationId: 0,
                    shopName: "온더보더 잠실점",
                    count: 3,
                    dateTime: '2018.02.05'
                },
                {
                    reservationId: 1,
                    shopName: "온더보더 잠실점",
                    count: 4,
                    dateTime: '2018.02.05'
                },
                {
                    reservationId: 2,
                    shopName: "온더보더 잠실점",
                    count: 8,
                    dateTime: '2018.02.06'
                },
                {
                    reservationId: 3,
                    shopName: "온더보더 잠실점",
                    count: 7,
                    dateTime: '2018.02.06'
                },
                {
                    reservationId: 4,
                    shopName: "온더보더 잠실점",
                    count: 6,
                    dateTime: '2018.02.07'
                },
                {
                    reservationId: 5,
                    shopName: "온더보더 잠실점",
                    count: 6,
                    dateTime: '2018.02.07'
                },
                {
                    reservationId: 6,
                    shopName: "온더보더 잠실점",
                    count: 4,
                    dateTime: '2018.02.08'
                }
            ]
        };
        // todo 나의 예약 내역 조회해서 render에서 뿌려주자
    }

    sortReservations() {
        let sorted = this.state.reservations.sort((r1, r2) => {
            return r2.reservationId - r1.reservationId;
        });
        let devidedByDate = [];
        for(let i = 0; i < sorted.length; i++) {
            let idx = devidedByDate.findIndex(r => sorted[i].dateTime === r.dateTime);
            if (idx === -1) {
                devidedByDate.push({
                    dateTime: sorted[i].dateTime,
                    array: [ sorted[i] ]
                });
            } else {
                devidedByDate[idx].array.push(sorted[i]);
            }
        }
        return devidedByDate;
    }

    printReservationStep2(reservation) {
        return <View key={reservation.reservationId} style={styles.contents_item_sub}>
            <View style={styles.contents_item_sub_info}>
                <Text style={styles.contents_item_sub_info_name}>{reservation.shopName}</Text>
                <Text style={styles.contents_item_sub_info_extra}><Text
                    style={styles.bold}>예약자</Text> {this.props.my_info && this.props.my_info.name ? this.props.my_info.name : this.state.my_info.name} <Text
                    style={styles.bold}>예약인원</Text> {reservation.count}</Text>
            </View>
            <Text style={styles.contents_item_sub_arrow}>
                <Image style={styles.arrow_right}
                       source={require("../../../assets/common/arrow_right.png")}/>
            </Text>
        </View>;
    }

    printReservation(date, reservations) {
        return <View key={date} style={styles.contents_item}>
            <Text style={styles.contents_item_date}>{date}</Text>
            {
                reservations.map(reservation => this.printReservationStep2(reservation))
            }
        </View>;
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.Navigator}>
                    <View style={styles.Navigator_menu}>
                        <TouchableHighlight style={styles.Navigator_menu_button} underlayColor={'transparent'}
                                            onPress={() => Actions.pop()}>
                            <Image style={styles.Navigator_menu_image}
                                   source={require("../../../assets/back_arrow.png")}/>
                        </TouchableHighlight>
                    </View>
                    <View style={styles.Navigator_title}><Text style={styles.Navigator_title_text}>나의 예약</Text></View>
                    <TouchableHighlight underlayColor={'transparent'} onPress={() => null}>
                        <View style={styles.Navigator_start}><Text style={styles.Navigator_start_text}> </Text></View>
                    </TouchableHighlight>
                </View>
                <View style={styles.contents}>
                    <ScrollView>
                        {
                            this.sortReservations().map(sorted => this.printReservation(sorted.dateTime, sorted.array))
                        }
                    </ScrollView>
                </View>
                <View style={styles.main_bottom_bar}>
                    <TouchableHighlight underlayColor={'transparent'} onPress={() => Actions.pop()}>
                        <View style={styles.icon_buttom}>
                            <Image style={styles.icon_buttom_image}
                                   source={require("../../../assets/User/Bottom/home_gray.png")}/>
                        </View>
                    </TouchableHighlight>
                    <TouchableHighlight underlayColor={'transparent'} onPress={() => Actions.User_my_reserved()}>
                        <View style={styles.icon_buttom}>
                            <Image style={styles.icon_buttom_image}
                                   source={require("../../../assets/User/Bottom/utensils_gray.png")}/>
                        </View>
                    </TouchableHighlight>
                    <TouchableHighlight underlayColor={'transparent'} onPress={() => Actions.User_my_review()}>
                        <View style={styles.icon_buttom}>
                            <Image style={styles.icon_buttom_image}
                                   source={require("../../../assets/User/Bottom/chat_gray.png")}/>
                        </View>
                    </TouchableHighlight>
                    <TouchableHighlight underlayColor={'transparent'} onPress={() => Actions.User_my_contents()}>
                        <View style={styles.icon_buttom}>
                            <Image style={styles.icon_buttom_image}
                                   source={require("../../../assets/User/Bottom/newspaper_gray.png")}/>
                        </View>
                    </TouchableHighlight>
                    <TouchableHighlight underlayColor={'transparent'} onPress={() => Actions.User_my_main()}>
                        <View style={styles.icon_buttom}>
                            <Image style={styles.icon_buttom_image}
                                   source={require("../../../assets/User/Bottom/user_black.png")}/>
                        </View>
                    </TouchableHighlight>
                </View>
            </View>
        )
    }
}

export default User_my_reserved;
