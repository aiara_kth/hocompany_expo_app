import React from 'react';
import {Text, View, StyleSheet, ScrollView} from 'react-native';
import { Button } from 'react-native-elements';
import {Actions} from 'react-native-router-flux';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    button : {
        marginBottom: 10,
    },

});

class Home_user extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isVisible: false
        }
    }

    toggleModal = (value) => {
        this.setState({isVisible: value})
    };

    render() {
        return (
            <View style={styles.container}>
                <ScrollView style={{ flex: 1}}>
                    <Button style={styles.button} title={"modal 키기"} onPress={() => this.toggleModal(true)}/>
                    <Button style={styles.button} title={"페이지이동_로그인/화원가입"} onPress={() => Actions.User_Register_Phone_Step1()}/>
                    <Button style={styles.button} title={"페이지이동_메인"} onPress={() => Actions.User_main()}/>
                    <Button style={styles.button} title={"페이지이동_마이"} onPress={() => Actions.User_my_main()}/>
                    <Button style={styles.button} title={"페이지이동_실시간자리잡기"} onPress={() => Actions.User_request_start()}/>
                    <Button style={styles.button} title={"페이지이동_상세페이지"} onPress={() => Actions.User_request_step5({from_request: false})}/>
                    <Button style={styles.button} title={"페이지이동_one시리즈"} onPress={() => Actions.Home_one()}/>
                    <Button style={styles.button} title={"dd_rs3"} onPress={() => Actions.dd_rs3()}/>
                    <Text>Home_user</Text>
                </ScrollView>
            </View>
        )
    }
}

export default Home_user;
