import React from 'react';
import {Text, View, StyleSheet, Image,TouchableHighlight, Dimensions, } from 'react-native';
import { Actions } from 'react-native-router-flux';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import {Ionicons} from '@expo/vector-icons';
import Global from "../../objects/Global";

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    Navigator : {
        marginTop : getStatusBarHeight(),
        width: '100%',
        height: 48,
        backgroundColor: '#fff',
    },
    back_button : {
        width:  48,
        height: 48,
        paddingTop: 12,
        paddingLeft: 12
    },
    back_button_b : {
        width: 24,
        height: 24
    },
    header : {
        marginTop: 48,
        width: '100%',
        alignItems: 'center',
    },
    title_regist_end : {
        marginTop : ((Dimensions.get('window').height / 8) - 48),
        alignItems: 'center',
    },
    title_regist_end_ts : {
        fontSize: 24,
        color: '#000000',
        alignItems: 'center',
    },
    title_regist_end_tsn : {
        fontSize: 14,
        lineHeight: 20,
        marginTop: 20,
        color: '#7f8fd1',
    },
    register_end_check : {
        marginTop : (Dimensions.get('window').height / 8),
        width: 80,
        height: 80
    }, go_home_buttom : {
        position: 'absolute',
        bottom:0,
        left:0,
        width: (Dimensions.get('window').width),
        height: 56,
        backgroundColor: '#eb5847',
        alignItems: 'center',
        justifyContent: 'center',
    }, go_home_buttom_text : {
        fontSize: 16,
        color:'#FFFFFF'
    },

    dummy_input : {
        marginTop : (Dimensions.get('window').height / 32),
    }

});

class Login_regist_step_end extends  React.Component {
    constructor(props) {
        super(props);
        // todo props로 주문한 메뉴 받아오기
    }


    render() {
        return (
            <View style={styles.container} behavior="padding" enabled>
                <View style={styles.Navigator}>
                    <TouchableHighlight underlayColor={'transparent'} onPress={() => Actions.pop()}>
                        <View style={styles.back_button}>
                            <Image style={styles.back_button_b} source={require("../../assets/back_arrow.png")}/>
                        </View>
                    </TouchableHighlight>
                </View>
                <View style={styles.header}>
                    <View style={styles.title_regist_end}>
                        <Text style={styles.title_regist_end_ts}>미리주문이</Text>
                        <Text style={styles.title_regist_end_ts}>성공적으로 신청되었습니다.</Text>
                        <Text style={styles.title_regist_end_tsn}>냠냠파전 외 2개의 메뉴</Text>
                        <Image style={styles.register_end_check} source={require("../../assets/register_end.png")}/>
                    </View>
                </View>
                <View style={styles.go_home_buttom}>
                    <Text style={styles.go_home_buttom_text} onPress={() => (Global.user === null ? Actions.Login() : Actions.admin_main())}>메인으로</Text>
                </View>
            </View>
        )
    }
}

export default Login_regist_step_end;
