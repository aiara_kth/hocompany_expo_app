import React from 'react';
import {Text, View, StyleSheet, Image,TouchableHighlight, Dimensions, Alert ,Menu,Animated, Easing} from 'react-native';
import { Actions } from 'react-native-router-flux';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import {Ionicons} from '@expo/vector-icons';
import API from '../../objects/API';
import Global from "../../objects/Global";

const styles = StyleSheet.create({
    container: {
        flex: 1,
        // backgroundColor: '#f9f9f9',
        backgroundColor: '#fff',
    },
    Navigator : {
        flexDirection: 'row',
        marginTop : getStatusBarHeight(),
        width: Dimensions.get('window').width,
        height: 48,
        backgroundColor: '#fff',
    },
    Navigator_menu: {
        width: 80,
        height: 48,
    },
    Navigator_menu_button :{
        width: 48,
        height: 48,
        paddingTop: 12,
        paddingLeft: 12,
    },
    Navigator_menu_image : {
        width: 24,
        height: 24
    },
    Navigator_title:{
        width : ((Dimensions.get('window').width) - 160),
        alignItems: 'center',
    },
    Navigator_title_text:{
        fontSize: 16,
        color:'#000000'
    },
    Navigator_start:{
        alignItems: 'flex-end',
        width: 80,
        height: 48,
        paddingTop: 12,
        paddingRight: 12,
    },
    Navigator_start_text:{
        fontSize: 16,
        color:'#7f8fd1',
    },
    callout_box : {
        width : (Dimensions.get('window').width),
        height: 40,
        borderRadius: 2,
        backgroundColor: '#7f8fd1',
        alignItems: 'center',
        justifyContent: 'center',
        shadowOffset: {width: 2, height: 10 },
        shadowColor: 'rgba(0, 0, 0, 0.2)',
        paddingRight: 56
    },
    callout_box_text : {
        fontSize : 12,
        color: '#ffffff'
    },
    contents : {
        height: (Dimensions.get('window').height - (121 + getStatusBarHeight())),
        flex: 1,
        paddingTop : 10,
        alignItems: 'center'
    },
    contents_title : {
        fontSize: 24,
        marginLeft: 20,
        width: Dimensions.get('window').width - 50,
        textAlign: 'center'
    },
    contents_subt : {
        marginTop : 10,
        fontSize: 12,
        color: '#7db840'
    },
    c_picker_group : {
        flexDirection: 'row',
        // height: (Dimensions.get('window').height - (108 + getStatusBarHeight())),
        flex: 1,
        marginTop: -100,
        justifyContent: 'center',
        alignItems: 'center'
    },
    c_picker: {
        width: 100
    },
    c_picker_word : {
        fontSize: 16
    },
    main_bottom_bar : {
        flexDirection: 'row',
        width: Dimensions.get('window').width,
        height: 42,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
        paddingTop: 11,
        paddingBottom: 11,
        borderTopWidth: 1,
        borderColor: 'rgba(230, 230, 230, 0.5)'
    },
    mini_icon : {
        width: 15,
        height: 15,
        resizeMode: 'contain'
    },
    mini_word : {
        fontSize: 12,
        color: '#606060'
    },
    animeweb : {
        width: 256,
        height: 256,
        position: 'absolute',
        marginTop : (Dimensions.get('window').height - 256) / 2 - 72,
    }
});

class User_request_step3 extends  React.Component {
    constructor(props) {
        super(props);
        this.state = {
            text: '',
            width: 256,
            height: 256,
            latitude: this.props.latitude,
            longitude: this.props.longitude,
            address: this.props.address,
            foundShops: null,
            angle: new Animated.Value(0)
        };
        this.startAnimation();
    }

    componentDidMount() {
        this.searchNearbyShops();
    }

    startAnimation() {
        Animated.timing(
            this.state.angle,
            {
                toValue: 360,
                duration: 300000,
                easing: Easing.linear,
                useNativeDriver: true
            }
        ).start(() => {
            this.startAnimation();
        });
    }

    gotoNextPage() {
        Actions.User_request_step4({
            latitude: this.props.latitude,
            longitude: this.props.longitude,
            time: this.props.time,
            count: this.props.count,
            foundShops: this.state.foundShops
        });
    }

    searchNearbyShops() {
        // if (user === null) {
        //     Alert.alert('주변 가게 찾기', '로그인해 주세요.', [{text: "확인", onPress: () => (Global.user === null ? Actions.Login() : Actions.admin_main())}]);
        //     return;
        // }
        API.shop.getNearShops(this.props.latitude, this.props.longitude, data => {
            this.gotoNextPage();
        }, error => {
            Alert.alert('주변 가게 찾기', '서버문제로 주변 가게 찾기에 실패하였습니다. \n' + error, [{
                text: "확인",
                onPress: () => Actions.pop()
            }]);
        });
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.Navigator}>
                    <View style={styles.Navigator_menu}>
                        <TouchableHighlight style={styles.Navigator_menu_button} underlayColor={'transparent'} onPress={() => Actions.pop()}>
                            <Image style={styles.Navigator_menu_image} source={require("../../assets/back_arrow.png")}/>
                        </TouchableHighlight>
                    </View>
                </View>
                <View style={styles.contents}>
                    <Text style={styles.contents_title}>예약이 가능한 매장을 실시간으로 검색중입니다.</Text>
                    <Animated.View style={[styles.animeweb, {transform: [{rotate: this.state.angle}]}]}>
                        <Image style={{width: this.state.width, height: this.state.height}} source={require("../../assets/common/circle.png")}/>
                    </Animated.View>
                    {/*<ActionButton hideShadow={false} buttonColor={'#eb5847'}*/}
                                  {/*renderIcon={(active) => <Ionicons name='ios-arrow-forward' size={20} color={'#FFFFFF'}/>}*/}
                                  {/*onPress={() => this.gotoNextPage()}>*/}
                    {/*</ActionButton>*/}
                </View>
                <View style={styles.main_bottom_bar}>
                    <Text style={styles.mini_word}>{this.props.time}분 이내 출발 인원 {this.props.count}명 위치 {this.state.address}</Text>
                </View>
            </View>
        )
    }
}

export default User_request_step3;
