import React from 'react';
import {
    Text,
    View,
    StyleSheet,
    Image,
    TouchableHighlight,
    Dimensions,
    ScrollView,
    Menu} from 'react-native';
import { Actions } from 'react-native-router-flux';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import ActionButton from 'react-native-action-button';
import {Ionicons} from '@expo/vector-icons';
import Global from "../../objects/Global";


const styles = StyleSheet.create({
    container: {
        flex: 1,
        // backgroundColor: '#f9f9f9',
        backgroundColor: '#fff',
    },
    Navigator : {
        flexDirection: 'row',
        marginTop : getStatusBarHeight(),
        width: Dimensions.get('window').width,
        height: 48,
        backgroundColor: '#fff',
    },
    Navigator_menu: {
        width: 80,
        height: 48,
    },
    Navigator_menu_button :{
        width: 48,
        height: 48,
        paddingTop: 12,
        paddingLeft: 12,
    },
    Navigator_menu_image : {
        width: 24,
        height: 24
    },
    Navigator_title:{
        width : ((Dimensions.get('window').width) - 160),
        alignItems: 'center',
    },
    Navigator_title_text:{
        fontSize: 16,
        color:'#000000'
    },
    Navigator_start:{
        alignItems: 'flex-end',
        width: 80,
        height: 48,
        paddingTop: 12,
        paddingRight: 12,
    },
    Navigator_start_text:{
        fontSize: 16,
        color:'#7f8fd1',
    },
    contents : {
        height: (Dimensions.get('window').height - (121 + getStatusBarHeight())),
        flex: 1,
        paddingTop : 10,
        alignItems: 'center'
    },
    contents_title : {
        fontSize: 24,
        width: Dimensions.get('window').width - 50,
        textAlign: 'center'
    },
    contents_subt : {
        marginTop : 10,
        fontSize: 12,
        color: '#7f8fd1'
    },
    contents_item: {
        width:  Dimensions.get('window').width - 40,
        height: 96,
        borderWidth : 1,
        borderRadius: 2,
        borderColor: '#ededed',
        flexDirection: 'row',
        marginBottom: 16,
    },
    request_image_dom: {
        width: 90,
        height: 96,
        paddingLeft:6,
        paddingTop:6
    },
    request_image:{
        width:84,
        height: 84
    },
    request_info: {
        width: (Dimensions.get('window').width - 191),
        height: 96,
        paddingLeft: 20
    },
    request_info_memter : {
        marginRight:  0,
        marginLeft: (Dimensions.get('window').width - 220),
        width: 52,
        fontSize : 12,
        color : '#7f8fd1',
        marginTop: 6,
        flexDirection: 'row',
    },
    request_info_store_name : {
        marginTop:25,
        fontSize:12,
        color: '#7db840'
    },
    request_info_food_name : {
        fontSize: 16
    },
    request_info_delete: {
        width: 100,
        height: 96,
        flex: 1,
        marginTop: 39
    },
    request_info_delete_btn : {
        fontSize: 12,
        color: '#eb5847'
    },
    contents_sub : {
        width: Dimensions.get('window').width,
        paddingLeft: 20,
        paddingRight: 20,
        backgroundColor: '#ffffff',
        marginTop : 28,
    },
    main_bottom_bar : {
        width: Dimensions.get('window').width,
        height: 42,
        flexDirection: 'row',
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
        paddingTop: 11,
        paddingBottom: 11,
        borderColor: 'rgba(230, 230, 230, 0.5)',
        borderTopWidth: 1,
    },
    mini_icon : {
        width: 15,
        height: 15,
        resizeMode: 'contain'
    },
    mini_word : {
        fontSize: 12,
        color: '#606060'
    }
});

class User_request_sec_step1 extends  React.Component {
    constructor(props) {
        super(props);
        this.state = {
            text: '',
            count: '1',
            shopIds: [0, 1, 2],
            maxDistance: this.getMaxDistance()
        };
    }

    getMaxDistance() {
        let max = 0;
        Global.cart.map(shop => {
            if (max < shop.distance)
                max = shop.distance;
        });
        return max;
    }

    shopElement(shop) {
        return <View key={shop.shopId}
                     style={[styles.contents_item, this.state.shopIds.findIndex(id => id === shop.shopId) === -1 ? {display: 'none'} : {}]}>
            <View style={styles.request_image_dom}>
                <Image style={styles.request_image} source={require("../../assets/User/My/dummy_menu_images.png")}/>
            </View>
            <View style={styles.request_info}>
                <Text style={styles.request_info_store_name}>{shop.type}</Text>
                <Text style={styles.request_info_food_name}>{shop.name}</Text>
            </View>
            <TouchableHighlight underlayColor={'transparent'} onPress={() => this.removeElement(shop.shopId)}>
                <View style={styles.request_info_delete}>
                    <Text style={styles.request_info_delete_btn}>삭제하기</Text>
                </View>
            </TouchableHighlight>
        </View>;
    }

    removeElement(shopId) {
        let idx = Global.cart.findIndex(s => s.shopId === shopId);
        if (idx !== -1) {
            Global.cart.splice(idx, 1);
        }
        let curShopIds = [];
        Global.cart.map(shop => curShopIds.push(shop.shopId));
        this.setState({shopIds: curShopIds, maxDistance: this.getMaxDistance()});
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.Navigator}>
                    <View style={styles.Navigator_menu}>
                        <TouchableHighlight style={styles.Navigator_menu_button} underlayColor={'transparent'}
                                            onPress={() => Actions.pop()}>
                            <Image style={styles.Navigator_menu_image} source={require("../../assets/back_arrow.png")}/>
                        </TouchableHighlight>
                    </View>
                </View>
                <View style={styles.contents}>
                    <Text style={styles.contents_title}>선택지 물어보기</Text>
                    <Text style={styles.contents_subt}>선택하신 가게에 자리가 있는지 물어볼까요?</Text>
                    <View style={styles.contents_sub}>
                        <ScrollView>
                            {
                                Global.cart.map(shop => this.shopElement(shop))
                            }
                        </ScrollView>
                    </View>
                    <ActionButton hideShadow={false} buttonColor={'#eb5847'} renderIcon={(active) => <Ionicons name='ios-arrow-forward' size={20} color={'#FFFFFF'}/>}
                                  onPress={() => Actions.User_request_sec_step2({shopIds: this.state.shopIds, distance: this.state.maxDistance})}>
                    </ActionButton>
                </View>
                <View style={styles.main_bottom_bar}>
                    <Image style={styles.mini_icon} source={require("../../assets/common/pin_gray.png")}/>
                    <Text style={styles.mini_word}> 거리 {this.state.maxDistance}M </Text>
                    <Image style={styles.mini_icon}
                           source={require("../../assets/common/home_gray_temp_need_change_transparent_background.png")}/>
                    <Text style={styles.mini_word}> 가게 {this.state.shopIds.length}곳</Text>
                </View>
            </View>
        )
    }
}

export default User_request_sec_step1;
