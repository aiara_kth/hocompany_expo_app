import React from 'react';
import {Text, View, StyleSheet, Image,TouchableHighlight, Platform, Dimensions, ScrollView, Alert ,Menu,Platfrom} from 'react-native';
import { Actions } from 'react-native-router-flux';
import MapView from 'react-native-map-clustering';
import {Marker} from 'react-native-maps';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import ActionButton from 'react-native-action-button';
import {Ionicons} from '@expo/vector-icons';

var commaNumber = require('comma-number');
import call from 'react-native-phone-call';
import TimerCountdown from "react-native-timer-countdown";
import API from "../../objects/API";
import Global from "../../objects/Global";

const styles = StyleSheet.create({
    container: {
        flex: 1,
        // backgroundColor: '#f9f9f9',
        backgroundColor: '#fff',
    },
    Navigator : {
        flexDirection: 'row',
        marginTop : getStatusBarHeight(),
        width: Dimensions.get('window').width,
        height: 48,
    },
    Navigator_menu: {
        width: 80,
        height: 48,
    },
    Navigator_extra :{
        width : ((Dimensions.get('window').width) - 80),
        height: 48,
        alignItems: 'flex-end',
        justifyContent: 'flex-end',
        paddingRight: 16,
        paddingTop: 12,
        flexDirection: 'row'
    },
    Navigator_menu_button :{
        width: 48,
        height: 48,
        paddingTop: 12,
        paddingLeft: 12,
    },
    Navigator_menu_image : {
        width: 24,
        height: 24
    },
    Navigator_title:{
        width : ((Dimensions.get('window').width) - 160),
        alignItems: 'center',
    },
    Navigator_title_text:{
        fontSize: 16,
        color:'#000000'
    },
    Navigator_start:{
        alignItems: 'flex-end',
        width: 80,
        height: 48,
        paddingTop: 12,
        paddingRight: 12,
    },
    Navigator_start_text:{
        fontSize: 16,
        color:'#7f8fd1',
    },
    contents : {
        // height: (Dimensions.get('window').height - (104 + getStatusBarHeight())),
        paddingTop: (Dimensions.get('window').width * 0.75) / 2 + 70,
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'transparent',
    },
    contents_title : {
        fontSize: 24,
    },
    contents_subt : {
        marginTop : 10,
        fontSize: 12,
        color: '#7db840'
    },
    c_picker_group : {
        flexDirection: 'row',
        // height: (Dimensions.get('window').height - (108 + getStatusBarHeight())),
        flex: 1,
        marginTop: -100,
        justifyContent: 'center',
        alignItems: 'center'
    },
    c_picker: {
        width: 100
    },
    c_picker_word : {
        fontSize: 16
    },
    main_bottom_bar : {
        flexDirection: 'row',
        width: Dimensions.get('window').width,
        height: 56,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
        paddingTop: 11,
        paddingBottom: 11
    },
    heart_icon : {
        width: 56,
        height: 56,
        paddingTop: 16,
        backgroundColor: '#eb5847',
        // color: '#fff',
        // fontSize: 16,
        // textAlign: 'center'
    },
    ext_icon : {
        width: Dimensions.get('window').width - 56,
        height: 56,
        paddingTop: 16,
        backgroundColor: '#77B844',
    },
    ext_icon_text : {
        color: '#fff',
        fontSize: 16,
        lineHeight: 24,
        textAlign: 'center'
    },
    mini_icon : {
        width: 15,
        height: 15,
        resizeMode: 'contain'
    },
    mini_word : {
        fontSize: 12,
        color: '#606060'
    },
    animeweb : {
        width: 256,
        height: 256
    },
    contents_item: {
        width: (Dimensions.get('window').width - 40),
        height: 96,
        backgroundColor: '#ffffff',
        // marginLeft: 20,
        marginBottom: 10,
        flexDirection: 'row',
        borderWidth: 1,
        borderColor: 'rgba(0, 0, 0, 0.1)'
    },
    imgicon: {
        width: 16,
        height: 16,
        resizeMode : 'contain'
    },
    imgicon_ios: {
        width: 16,
        height: 16,
        resizeMode : 'cover'
    },
    imgicon_s: {
        width: 12,
        height: 12,
        resizeMode: 'contain'
    },
    imgicon_pin: {
        width: 10,
        height: 14,
        marginTop:1,
        resizeMode: 'contain'
    },
    request_image_dom: {
        width: 90,
        height: 96,
        paddingLeft:6,
        paddingTop:6
    },
    request_image:{
        width:84,
        height: 84
    },
    request_info: {
        width: (Dimensions.get('window').width - 124),
        height: 96,
        paddingLeft: 20
    },
    request_info_memter : {
        marginRight:  0,
        marginLeft: (Dimensions.get('window').width - 220),
        width: 52,
        fontSize : 12,
        color : '#7f8fd1',
        marginTop: 6,
        flexDirection: 'row',
    },
    request_info_store_name : {
        fontSize:12,
        color: '#7db840'
    },
    request_info_food_name : {
        fontSize: 16
    },
    request_info_extra : {
        fontSize: 12
    },
    right: {
        alignItems: 'flex-end',
        justifyContent: 'flex-end'
    },
    top_image : {
        position: 'absolute',
        top: 0,
        left: 0,
        width: Dimensions.get('window').width,
        height: (Dimensions.get('window').width * 0.75),
        resizeMode: 'cover'
    },
    foods_info : {
        zIndex: 1000,
        position: 'absolute',
        top: (Dimensions.get('window').width * 0.75)/2,
        // marginTop: (Dimensions.get('window').width * 0.75) - 70,
        // marginTop: ,
        width: 320,
        height: 140,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center'
    },
    foods_info_memter: {
        width: 300,
        marginLeft: 20,
        paddingRight: 16,
        flexDirection: 'row',
        alignItems: 'flex-end',
        justifyContent: 'flex-end',
    },
    foods_info_memter_text : {
        fontSize :12,
        lineHeight: 16,
        color: '#7f8fd1'
    },
    foods_info_store_name : {
        fontSize: 12,
        color: '#7db840'
    },
    foods_info_food_name : {
        fontSize:20,
        lineHeight: 32
    },
    foods_info_extra : {
        marginTop: 10,
        marginBottom: 36,
        flexDirection: 'row'
    },
    foods_info_extra_word : {
        fontSize: 10,
        lineHeight: 16
    },
    clock_to_clock_dom : {
        width: Dimensions.get('window').width,
        height: 150,
        marginBottom: 8,
        backgroundColor: '#F9F9F9',
        alignItems: 'center',
        justifyContent: 'center'
    },
    clock_to_clock : {
        marginTop: 20,
        flexDirection: 'row'
    },
    clock_to_clock_text : {
        fontSize: 10,
        lineHeight: 16,
        fontWeight: '200',
        color: '#838383'
    },
    contents_sub : {
        width: Dimensions.get('window').width,
        paddingLeft: 12,
        paddingRight: 12,
        paddingTop: 28,
        backgroundColor: '#ffffff',
    },
    contents_sub_name : {
        width: Dimensions.get('window').width - 40,
        alignItems: 'center',
        justifyContent: 'center'
    },
    contents_sub_info : {
        flexDirection: 'row',

    },
    contents_sub_menu : {
        width: Dimensions.get('window').width - 120,
        height: 128,
        paddingTop: 16,
    },
    contents_sub_menu_title: {
        fontSize: 16,
    },
    contents_sub_menu_info : {
        marginTop: 8,
        fontSize: 14,
        color: '#606060'
    },
    contents_sub_menu_price : {
        marginTop: 8,
        fontSize: 10,
        color: '#606060',
        fontWeight: '500',
    },
    contents_sub_menu_image : {
        width: 96,
        height: 96,
        marginTop: 16
    },
    contents_more : {
        width: Dimensions.get('window').width - 40,
        height: 32,
        paddingTop: 4,
        fontSize: 12,
        color: '#eb5847',
        textAlign:'center'
    },
    event : {
        width: 320,
        height: 32,
        flexDirection : 'row',
        backgroundColor: '#ffffff',
        borderWidth: 1,
        borderColor: '#f2f2f2',
        marginTop: 74,
    },
    event_icon : {
        paddingLeft: 10,
        width: 60,
    },
    event_text : {
        width: 200,
        textAlign: 'center',
        color: '#f5a623',
        fontSize: 12,
        paddingTop: 7
    },
    event_arrow : {
        width: 60,
        alignItems: 'flex-end',
        justifyContent: 'flex-end',
        paddingRight: 7
    },

    contents_profile : {
        height: 25,
        marginTop: 16,
        flexDirection: 'row'
    },
    contents_profile_image : {
        width: 28,
        height: 28,
        marginTop: 2
    },
    contents_profile_name : {
        fontSize: 16,
        marginLeft: 10,
        marginTop: 4
    },
    contents_profile_date :{
        marginTop: 8,
        fontSize: 10,
        color: '#c4c4c4',
        marginLeft: 4
    },
    contents_info : {
        marginTop: 10,
        width: Dimensions.get('window').width - 24,
        paddingLeft: 40,
        fontSize: 14,
        lineHeight: 20,
        color: '#606060'
    },
    contents_info_reply: {
        marginTop:6,
        paddingLeft: 40,
        color: '#eb5847',
        fontSize: 14,
        lineHeight: 20
    },
    contents_maps : {
        width:  Dimensions.get('window').width,
        height: 200,
        marginLeft: -12,
        marginTop: 10
    },
    contents_maps_view : {
        flex: 1,
    },
    c_modal : {
        zIndex: 9999,
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height - getStatusBarHeight(),
        // flexDirection: 'row',
        position: 'absolute',
        left: 0,
        top: getStatusBarHeight(),
        alignItems: 'center',
        justifyContent: 'center'
    },
    c_modal_circle: {
        marginTop: (Dimensions.get('window').height - getStatusBarHeight() - 96) / 2,
        backgroundColor: 'rgba(255, 255, 255, 0.8)',
        width: 96,
        height: 96,
        borderRadius: 54,
        alignItems: 'center',
        justifyContent: 'center'
    },
    c_modal_circle_image : {
        width: 60,
        height:60,
        marginTop: 10,
    }
});

const MODAL_SHOW_TIME = 120; // seconds

class User_request_one_shop_detail extends  React.Component {
    shopInfo = {
        shopId: 0,
        type: "이자카야",
        name: "레스토랑 명 입력 자리",
        distance: 200,
        likeCount: 255,
        bookmarkCount: 12353,
        phoneNumber: "010-1234-1234",
        event: "할인이벤트를 진행합니다.",
        startTime: "12:00 PM",
        endTime: "02:00 AM",
        description: "안녕하세요 저희 레스토랑은 어쩌고 저쩌고 내용이 들어갑니다. 안녕하세요 저희 레스토랑은 어쩌고 저쩌고 내용이 들어갑니다.",
        menu: [
            {
                name: "냠냠파전",
                description: "냠냠파전은 바삭바삭한 끝 부분을 살리고 해산물을 가득 살린 메뉴",
                price: 35000,
                image: require("../../assets/User/My/dummy_menu_images.png")
            },
            {
                name: "냠냠파전",
                description: "냠냠파전은 바삭바삭한 끝 부분을 살리고 해산물을 가득 살린 메뉴",
                price: 35000,
                image: require("../../assets/User/My/dummy_menu_images.png")
            },
            {
                name: "냠냠파전",
                description: "냠냠파전은 바삭바삭한 끝 부분을 살리고 해산물을 가득 살린 메뉴",
                price: 35000,
                image: require("../../assets/User/My/dummy_menu_images.png")
            },
            {
                name: "냠냠파전",
                description: "냠냠파전은 바삭바삭한 끝 부분을 살리고 해산물을 가득 살린 메뉴",
                price: 35000,
                image: require("../../assets/User/My/dummy_menu_images.png")
            },
            {
                name: "냠냠파전",
                description: "냠냠파전은 바삭바삭한 끝 부분을 살리고 해산물을 가득 살린 메뉴",
                price: 35000,
                image: require("../../assets/User/My/dummy_menu_images.png")
            }
        ],
        review: [
            {
                reviewId: 0,
                reviewer: {
                    name: "김민지",
                    profileImage: require("../../assets/profile_dummy.png")
                },
                reviewedTime: "2일전",
                content: "냠냠파전은 바삭바삭한 끝 부분을 살리고 해산물을 가득 살린 메뉴",
                likeCount: 135,
                comment: [
                    {
                        commentUser : {
                            profilePhotoUrl : require("../../assets/profile_dummy.png"),
                            nickname: "김민지"
                        },
                        commentTime: "2일전",
                        content: "냠냠파전은 바삭바삭한 끝에부분을 살리고 해산물을 가득살린 메뉴"
                    },
                    {
                        commentUser : {
                            profilePhotoUrl : require("../../assets/profile_dummy.png"),
                            nickname: "김민지"
                        },
                        commentTime: "2일전",
                        content: "냠냠파전은 바삭바삭한 끝에부분을 살리고 해산물을 가득살린 메뉴"
                    },
                    {
                        commentUser : {
                            profilePhotoUrl : require("../../assets/profile_dummy.png"),
                            nickname: "김민지"
                        },
                        commentTime: "2일전",
                        content: "냠냠파전은 바삭바삭한 끝에부분을 살리고 해산물을 가득살린 메뉴"
                    },
                    {
                        commentUser : {
                            profilePhotoUrl : require("../../assets/profile_dummy.png"),
                            nickname: "김민지"
                        },
                        commentTime: "2일전",
                        content: "냠냠파전은 바삭바삭한 끝에부분을 살리고 해산물을 가득살린 메뉴"
                    }
                ]
            },
            {
                reviewId: 1,
                reviewer: {
                    name: "김민지",
                    profileImage: require("../../assets/profile_dummy.png")
                },
                reviewedTime: "2일전",
                content: "냠냠파전은 바삭바삭한 끝 부분을 살리고 해산물을 가득 살린 메뉴",
                likeCount: 135,
                comment: [
                    {
                        commentUser : {
                            profilePhotoUrl : require("../../assets/profile_dummy.png"),
                            nickname: "김민지"
                        },
                        commentTime: "2일전",
                        content: "냠냠파전은 바삭바삭한 끝에부분을 살리고 해산물을 가득살린 메뉴"
                    },
                    {
                        commentUser : {
                            profilePhotoUrl : require("../../assets/profile_dummy.png"),
                            nickname: "김민지"
                        },
                        commentTime: "2일전",
                        content: "냠냠파전은 바삭바삭한 끝에부분을 살리고 해산물을 가득살린 메뉴"
                    }
                ]
            },
            {
                reviewId: 2,
                reviewer: {
                    name: "김민지",
                    profileImage: require("../../assets/profile_dummy.png")
                },
                reviewedTime: "2일전",
                content: "냠냠파전은 바삭바삭한 끝 부분을 살리고 해산물을 가득 살린 메뉴",
                likeCount: 135,
                comment: []
            },
            {
                reviewId: 3,
                reviewer: {
                    name: "김민지",
                    profileImage: require("../../assets/profile_dummy.png")
                },
                reviewedTime: "2일전",
                content: "냠냠파전은 바삭바삭한 끝 부분을 살리고 해산물을 가득 살린 메뉴",
                likeCount: 135,
                comment: [
                    {
                        commentUser : {
                            profilePhotoUrl : require("../../assets/profile_dummy.png"),
                            nickname: "김민지"
                        },
                        commentTime: "2일전",
                        content: "냠냠파전은 바삭바삭한 끝에부분을 살리고 해산물을 가득살린 메뉴"
                    }
                ]
            }
        ],
        location: {
            latitude: 37.517204,
            longitude: 127.041289
        }
    };

    constructor(props) {
        super(props);
        this.state = {
            text: '',
            count: '1',
            heart: false,
            modal: 0,
            modalTimer: MODAL_SHOW_TIME,
            bookmark: false,
            scroll: 0,
            shopId: this.props.selected_shop,
            viewAllMenu: false,
            viewAllReview: false,
            cartCount: Global.cart.length
        };
        if (this.state.shopId !== undefined && this.state.shopId !== null) {
            API.shop.getShop(this.state.shopId, data => {
                // todo
            }, error => {
                Alert.alert('가게 정보 조회', '서버문제로 가게의 정보를 가져오는데 실패하였습니다. \n' + error, [{
                    text: "확인",
                    onPress: () => Actions.pop()
                }]);
            });
        }
        if (this.props.shopInfo !== undefined) {
            this.shopInfo = this.props.shopInfo;
        }
    }

    toggleBookmark() {
        this.setState({
            bookmark: !this.state.bookmark,
            modal: 2,
            modalTimer: MODAL_SHOW_TIME
        });
        if (this.state.bookmark) {
            this.bookmark('DELETE');
        } else {
            this.bookmark('PUT');
        }
    }

    bookmark(method) {
        if (Global.user === null)
            return;
        if (method === 'PUT') {
            API.shop.bookmark.put(this.state.shopId, data => {
                console.log(data);
            }, error => {
                Alert.alert('북마크 추가', '서버문제로 가게를 북마크에 추가하는데 실패하였습니다. \n' + error, [{
                    text: "확인",
                    onPress: () => Actions.pop()
                }]);
            });
        } else if (method === 'DELETE') {
            API.shop.bookmark.delete(this.state.shopId, data => {
                console.log(data);
            }, error => {
                Alert.alert('북마크 삭제', '서버문제로 가게를 북마크에 삭제하는데 실패하였습니다. \n' + error, [{
                    text: "확인",
                    onPress: () => Actions.pop()
                }]);
            });
        }
    }

    toggleLike() {
        this.setState({
            heart: !this.state.heart,
            modal: 1,
            modalTimer: MODAL_SHOW_TIME
        });
        if (this.state.heart) {
            this.like('DELETE');
        } else {
            this.like('PUT');
        }
    }

    like(method) {
        if (Global.user === null)
            return;
        if (method === 'PUT') {
            API.shop.like.put(this.state.shopId, data => {
                console.log(data);
            }, error => {
                Alert.alert('좋야요 추가', '서버문제로 가게를 좋아요 목록에 추가하는데 실패하였습니다. \n' + error, [{
                    text: "확인",
                    onPress: () => Actions.pop()
                }]);
            });
        } else if (method === 'DELETE') {
            API.shop.like.delete(this.state.shopId, data => {
                console.log(data);
            }, error => {
                Alert.alert('좋아요 삭제', '서버문제로 가게를 좋아요 목록에 삭제하는데 실패하였습니다. \n' + error, [{
                    text: "확인",
                    onPress: () => Actions.pop()
                }]);
            });
        }
    }

    addToCart() {
        // todo 장바구니에 추가 - 현재 로컬에 배열 만들어서 추가시키는데 api로 db에 저장하는게 있는지 확인 필요
        let idx = Global.cart.findIndex(s => s.shopId === (this.state.shopId !== undefined && this.state.shopId !== null ? this.state.shopId : 0));
        if (idx === -1) {
            Global.cart.push({
                shopId: this.state.shopId !== undefined && this.state.shopId !== null ? this.state.shopId : 0,
                type: this.shopInfo.type,
                name: this.shopInfo.name,
                distance: this.shopInfo.distance
            });
        } else {
            Global.cart.splice(idx, 1);
        }
        this.setState({cartCount: Global.cart.length});
    }

    timerOnTick() {
        let modalTimer = this.state.modalTimer;
        if (modalTimer > 0)
            modalTimer--;
        else
            modalTimer = 0;
        this.setState({modalTimer: modalTimer});
        if (modalTimer === 0) {
            this.setState({modal: 0});
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <TouchableHighlight
                    style={[styles.c_modal, this.state.modal !== 0 ? {} : {display: 'none', position: 'relative'}]}
                    underlayColor={'transparent'} onPress={() => this.setState({modal: 0})}>
                    <View style={{flex: 1}}>
                        <View style={styles.c_modal_circle}>
                            <Image style={styles.c_modal_circle_image}
                                   source={this.state.modal === 1 ? require("../../assets/common/silver_heart_2x.png") : require("../../assets/common/silver_bookmark_2x.png")}/>
                            <TimerCountdown initialSecondsRemaining={MODAL_SHOW_TIME}
                                            //onTick={() => this.timerOnTick()}
                                            onTimeElapsed={() => this.timerOnTick()}
                                            style={{display: 'none'}}/>
                        </View>
                    </View>
                </TouchableHighlight>
                <ScrollView onScroll={(value) => {
                    this.setState({scroll: value.nativeEvent.contentOffset.y > (Dimensions.get('window').width * 0.75) - getStatusBarHeight() - (Platform.OS !== 'ios' ? 50 : 0)})
                }}>
                    <Image style={styles.top_image} source={require("../../assets/User/My/dummy_menu_images.png")}/>
                    <View
                        style={[styles.Navigator, this.state.scroll ? {backgroundColor: '#fff'} : {backgroundColor: 'transparent'}]}>
                        <View style={styles.Navigator_menu}>
                            <TouchableHighlight style={styles.Navigator_menu_button} underlayColor={'transparent'}
                                                onPress={() => Actions.pop()}>
                                <Image style={styles.Navigator_menu_image}
                                       source={require("../../assets/back_arrow_w.png")}/>
                            </TouchableHighlight>
                        </View>
                        <View style={styles.Navigator_extra}>
                            <TouchableHighlight style={styles.Navigator_menu_button} underlayColor={'transparent'}
                                                onPress={() => this.toggleBookmark()}>
                                <Image style={styles.Navigator_menu_image}
                                       source={this.state.bookmark ? require("../../assets/common/bookmark_on.png") : require("../../assets/common/bookmark_icon.png")}/>
                            </TouchableHighlight>
                            <Text style={{width: 10}}> </Text>
                            <TouchableHighlight style={styles.Navigator_menu_button} underlayColor={'transparent'}
                                                onPress={() => call({number: this.shopInfo.phoneNumber.replace('-', ''), prompt: false}).catch(console.error)}>
                                <Image style={styles.Navigator_menu_image}
                                       source={require("../../assets/common/phone_icon.png")}/>
                            </TouchableHighlight>
                        </View>
                    </View>
                    <View style={styles.contents}>
                        <View style={styles.foods_info}>
                            <View style={styles.foods_info_memter}><Image style={styles.imgicon_pin}
                                                                          source={require("../../assets/common/pin.png")}/><Text
                                style={styles.foods_info_memter_text}> {commaNumber(this.shopInfo.distance)}M</Text></View>
                            <Text style={styles.foods_info_store_name}>{this.shopInfo.type}</Text>
                            <Text style={styles.foods_info_food_name}>{this.shopInfo.name}</Text>
                            <View style={styles.foods_info_extra}>
                                <Image style={styles.imgicon} source={require("../../assets/common/silver_heart.png")}/>
                                <Text
                                    style={styles.foods_info_extra_word}> {commaNumber(this.shopInfo.likeCount)} </Text>
                                <Image style={styles.imgicon}
                                       source={require("../../assets/common/silver_bookmark.png")}/>
                                <Text
                                    style={styles.foods_info_extra_word}> {commaNumber(this.shopInfo.bookmarkCount)}</Text>
                            </View>
                        </View>
                        <View style={styles.clock_to_clock_dom}>
                            <View style={styles.event}>
                                <View style={styles.event_icon}>
                                    <Image style={{width: 48, height: 16, marginTop: 8}}
                                           source={require("../../assets/common/event_label_w.png")}/>
                                </View>
                                <Text style={styles.event_text}>{this.shopInfo.event}</Text>
                                <View style={styles.event_arrow}>
                                    <Image style={styles.Navigator_menu_image}
                                           source={require("../../assets/common/dropbox.png")}/>
                                </View>
                            </View>
                            <View style={styles.clock_to_clock}>
                                <Image style={styles.imgicon} source={require("../../assets/common/clock_b.png")}/>
                                <Text
                                    style={styles.clock_to_clock_text}> {this.shopInfo.startTime} - {this.shopInfo.endTime}</Text>
                            </View>
                        </View>
                        <View style={styles.contents_sub}>
                            <View style={styles.contents_sub_name}>
                                <Text style={{fontSize: 14}}>소개</Text>
                                <Text style={{fontSize: 12}}>―</Text>
                            </View>
                            <View style={styles.contents_sub_info}>
                                <Text style={{fontSize: 14, color: '#606060'}}>{this.shopInfo.description}</Text>
                            </View>
                        </View>
                        <View style={styles.contents_sub}>
                            <View style={styles.contents_sub_name}>
                                <Text style={{fontSize: 14}}>메뉴</Text>
                                <Text style={{fontSize: 12}}>―</Text>
                            </View>
                            {
                                this.shopInfo.menu.map((menu, index) => (
                                    <View key={menu.name + "_" + index}
                                          style={[index > 2 && !this.state.viewAllMenu ? {display: 'none'} : {}]}>
                                        <View style={styles.contents_sub_info}>
                                            <View style={styles.contents_sub_menu}>
                                                <Text style={styles.contents_sub_menu_title}>{menu.name}</Text>
                                                <Text style={styles.contents_sub_menu_info}>{menu.description}</Text>
                                                <Text
                                                    style={styles.contents_sub_menu_price}>{commaNumber(menu.price)}원</Text>
                                            </View>
                                            <Image style={styles.contents_sub_menu_image}
                                                   source={menu.image}/>
                                        </View>
                                        <View style={{backgroundColor: '#eff1f3', width: '100%', height: 1}}/>
                                    </View>
                                ))
                            }
                        </View>
                        <TouchableHighlight underlayColor={'transparent'}
                                            onPress={() => this.setState({viewAllMenu: true})}
                                            style={[this.state.viewAllMenu ? {display: 'none'} : {}]}>
                            <Text style={styles.contents_more}>+ 더보기</Text>
                        </TouchableHighlight>
                        <View style={styles.contents_sub}>
                            <View style={styles.contents_sub_name}>
                                <Text style={{fontSize: 14}}>리뷰</Text>
                                <Text style={{fontSize: 12}}>―</Text>
                            </View>
                            {
                                this.shopInfo.review.map((review, index) => (
                                    <TouchableHighlight underlayColor={'transparent'}
                                                        key={review.reviewerName + "_" + index}
                                                        style={[index > 2 && !this.state.viewAllReview ? {display: 'none'} : {}]}
                                                        onPress={() => Actions.User_my_review_view({review: review, shop: this.shopInfo})}>
                                        <View style={styles.contents_sub_info}>
                                            <View style={styles.contents_review_group}>
                                                <View style={styles.contents_profile}>
                                                    <Image
                                                        style={styles.contents_profile_image}
                                                        source={review.reviewer.profileImage}/>
                                                    <Text
                                                        style={styles.contents_profile_name}>{review.reviewer.name}</Text>
                                                    <Text
                                                        style={styles.contents_profile_date}>{review.reviewedTime}</Text>
                                                </View>
                                                <Text style={styles.contents_info}>{review.content}</Text>
                                                <Text style={styles.contents_info_reply}>답글작성</Text>
                                            </View>
                                        </View>
                                    </TouchableHighlight>
                                ))
                            }
                        </View>

                        <View style={{
                            backgroundColor: '#eff1f3',
                            width: (Dimensions.get('window').width - 24) / 2,
                            height: 1
                        }}/>
                        <TouchableHighlight underlayColor={'transparent'}
                                            onPress={() => this.setState({viewAllReview: true})}
                                            style={[this.state.viewAllReview ? {display: 'none'} : {}]}>
                            <Text style={styles.contents_more}>+ 더보기</Text>
                        </TouchableHighlight>
                        <View style={styles.contents_sub}>
                            <View style={styles.contents_sub_name}>
                                <Text style={{fontSize: 14}}>위치</Text>
                                <Text style={{fontSize: 12}}>―</Text>
                            </View>
                            <View style={styles.contents_maps}>
                                <MapView style={styles.contents_maps_view} region={{
                                    latitude: this.shopInfo.location.latitude,
                                    longitude: this.shopInfo.location.longitude,
                                    latitudeDelta: 0.0922,
                                    longitudeDelta: 0.0421,
                                }}>
                                    <Marker key={1} coordinate={{
                                        latitude: this.shopInfo.location.latitude,
                                        longitude: this.shopInfo.location.longitude
                                    }}
                                            title={this.shopInfo.type} description={this.shopInfo.name}
                                            image={Platform.OS === 'android' ? require('../../assets/map/marker_store.png') : undefined}>
                                        {Platform.OS === 'ios' &&
                                        <View><Image source={require('../../assets/map/marker_store.png')}
                                                     style={{width: 60, height: 60}}/></View>}
                                    </Marker>
                                </MapView>
                            </View>
                        </View>
                    </View>
                </ScrollView>
                <ActionButton hideShadow={false} style={{marginBottom: 56}} buttonColor={'#eb5847'}
                              renderIcon={(active) => (
                                  <View style={{alignItems: 'center', justifyContent: 'center'}}>
                                      <Text style={{fontSize: 10, zIndex: 12, color: '#ffffff'}}>{this.state.cartCount}</Text>
                                      <Image style={{width: 32, height: 32, zIndex: 13, marginTop: -5}}
                                             source={require('../../assets/common/cart_icon.png')}/>
                                  </View>)}
                              onPress={() => this.addToCart()}>
                </ActionButton>
                <View style={styles.main_bottom_bar}>
                    <TouchableHighlight underlayColor={'transparent'} onPress={() => this.toggleLike()}>
                        <View style={styles.heart_icon}><Image
                            source={this.state.heart ? require('../../assets/common/heart_w.png') : require('../../assets/common/heart_clear_icon_w.png')}
                            style={{width: 24, height: 24, marginLeft: 16}}/></View>
                    </TouchableHighlight>
                    <TouchableHighlight underlayColor={'transparent'} onPress={() => (this.state.cartCount === 0 ? null : Actions.User_request_sec_step1())}>
                        <View style={styles.ext_icon}>
                            <Text
                                style={styles.ext_icon_text}>{this.state.cartCount === 0 ? ("예약하기") : (this.state.cartCount + "곳 예약 알아보기")}</Text>
                        </View>
                    </TouchableHighlight>
                </View>
            </View>
        )
    }
}

export default User_request_one_shop_detail;
