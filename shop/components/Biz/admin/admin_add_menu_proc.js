import React from 'react';
import {Text, View, StyleSheet, Image,TextInput,TouchableHighlight, Dimensions, Alert ,AsyncStorage,Menu} from 'react-native';
import { Actions } from 'react-native-router-flux';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import {Ionicons} from '@expo/vector-icons';
import Global from "../../../objects/Global";
import API from "../../../objects/API";

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f9f9f9',
    },
    Navigator : {
        flexDirection: 'row',
        paddingTop :  getStatusBarHeight(),
        width: Dimensions.get('window').width,
        height: 48 +  getStatusBarHeight(),
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    Navigator_menu: {
        width: 80,
        height: 48,
    },
    Navigator_menu_button :{
        width: 48,
        height: 48,
        paddingTop: 12,
        paddingLeft: 12,
    },
    Navigator_menu_image : {
        width: 24,
        height: 24
    },
    Navigator_title:{
        width : ((Dimensions.get('window').width) - 160),
        alignItems: 'center',
    },
    Navigator_title_text:{
        fontSize: 16,
        color:'#000000'
    },
    Navigator_start:{
        alignItems: 'flex-end',
        width: 80,
        height: 48,
        paddingTop: 12,
        paddingRight: 12,
    },
    Navigator_start_text:{
        fontSize: 16,
        color:'#7db840',
    },
    sub_Navigator : {
        flexDirection: 'row',
        width: Dimensions.get('window').width,
        height: 40,
        backgroundColor: '#fff',
        alignItems: 'center',
    },
    sub_Navigator_menu: {
        width: 85,
        height: 40,
        alignItems: 'flex-end',
        justifyContent: 'center',
        marginLeft: 15,
        marginRight: 15
    },
    sub_Navigator_menu_on: {
        width: 65,
        height: 40,
        alignItems: 'center',
        justifyContent: 'center',
        borderBottomWidth: 2,
        borderBottomColor: '#000000',
    },
    sub_Navigator_menu_off: {
        width: 65,
        height: 40,
        alignItems: 'center',
        justifyContent: 'center',
        borderBottomWidth: 2,
        borderBottomColor: 'transparent',
    },
    sub_Navigator_name_on : {
        fontSize : 14,
        fontWeight: 'bold',
        alignItems: 'center',
    },
    sub_Navigator_name_off : {
        fontSize : 14,
        alignItems: 'center',
    },
    title_ws : {
        fontSize: 12,
        height: 24,
        color: '#7f8fd1'
    },
    contents : {
        marginTop :  9,
        paddingTop: 11,
        paddingBottom:11,
        width: Dimensions.get('window').width,
        backgroundColor: '#ffffff',
        alignItems: 'center',
        justifyContent: 'center',
        paddingLeft: 20,
        paddingRight: 20,
    },
    form_top: {
      flexDirection: 'row',
      width: Dimensions.get('window').width,
      height: 104,
      paddingLeft: 20,
    },
    inputfile : {
        width: 104,
        height: 104
    },
    form_top_input_group : {
        width: (Dimensions.get('window').width - 144),
    },
    input_t1_form : {
        width: Dimensions.get('window').width - 156,
        height: 48,
        marginLeft : 12,
        backgroundColor: '#fafbfb',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection:'row'
    },
    input_t1: {
        width: (Dimensions.get('window').width - 180),
        // paddingLeft: 12,
        paddingRight: 12,
    },
    form_bottom : {
        width: (Dimensions.get('window').width - 40),
        height: 110,
        marginTop: 8,
        backgroundColor: '#fafbfb',
    },
    input_t2 : {
        width: (Dimensions.get('window').width - 40),
        height: 100,
        paddingTop: 12,
        paddingLeft: 12,
        paddingRight: 12,
        paddingBottom :12,
        textAlignVertical: 'top'
    }


});

class admin_add_memu_proc extends  React.Component {
    constructor(props) {
        super(props);
        this.state = {
            show_modal: false,
            photo: null,
            name: '',
            price: '',
            desc: ''
        };
    }

    add_start() {
        if (this.state.photo === null) {
            Alert.alert('메뉴 등록', '사진을 업로드 해주세요.', [{text: "확인", onPress: () => null}]);
            return;
        }

        if (this.state.name === '') {
            Alert.alert('메뉴 등록', '메뉴명을 입력해주세요.', [{text: "확인", onPress: () => null}]);
            this.refs.name.focus();
            return;
        }

        if (this.state.price === '') {
            Alert.alert('메뉴 등록', '가격을 입력해주세요.', [{text: "확인", onPress: () => null}]);
            this.refs.price.focus();
            return;
        }

        if (this.state.desc === '') {
            Alert.alert('메뉴 등록', '메뉴 설명을 입력해주세요.', [{text: "확인", onPress: () => null}]);
            this.refs.desc.focus();
            return;
        }

        Alert.alert('메뉴 등록', '메뉴를 추가하시겠습니까?',
            [
                {text: "취소", onPress: () => null, styles: 'cancel'},
                {text: "확인", onPress: () => this.registMenu()}
            ]
        );

    }

    registMenu() {
        if (this.props.shop === undefined || this.props.shop === null) {
            Actions.pop();
            return;
        }
        let menu = {
            name: this.state.name,
            description: this.state.desc,
            price: parseInt(this.state.price),
            photo: this.state.photo
        };
        let shop = this.props.shop;
        shop.menu.push(menu);
        AsyncStorage.setItem("@shop", JSON.stringify(shop), () => {
            console.log(shop);
            API.shop.admin.menu.post(shop.shopId, menu.name, menu.price, "", menu.photo, menu.description, {}, data => {
                Actions.pop({refresh: {shop: shop}});
            }, error => {
                Actions.pop({refresh: {shop: shop}});
            });
        });
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.Navigator}>
                    <View style={styles.Navigator_menu}>
                        <TouchableHighlight style={styles.Navigator_menu_button} underlayColor={'transparent'}
                                            onPress={() => Actions.pop()}>
                            <Image style={styles.Navigator_menu_image}
                                   source={require("../../../assets/back_arrow.png")}/>
                        </TouchableHighlight>
                    </View>
                    <View style={styles.Navigator_title}><Text style={styles.Navigator_title_text}>메뉴등록</Text></View>
                    <TouchableHighlight underlayColor={'transparent'} onPress={() => this.add_start()}>
                        <View style={styles.Navigator_start}><Text style={styles.Navigator_start_text}>완료</Text></View>
                    </TouchableHighlight>
                </View>
                <View style={styles.contents}>
                    <View style={styles.form_top}>
                        <View style={{width: 104}}>
                            <TouchableHighlight underlayColor={'transparent'}
                                                onPress={() => Global.imageUploader.upload(result => {
                                                    this.setState({photo: Global.imageUploader.toRequiredImage(result)});
                                                })}>
                                <Image style={styles.inputfile}
                                       source={this.state.photo !== null ? this.state.photo : require("../../../assets/common/input_image_dummy.png")}/>
                            </TouchableHighlight>
                        </View>
                        <View style={styles.form_top_input_group}>
                            <View style={[styles.input_t1_form, {marginBottom: 8}]}>
                                <TextInput style={styles.input_t1} placeholder={'메뉴명 입력'} ref="name"
                                           underlineColorAndroid='transparent' returnKeyType="done"
                                           value={this.state.name}
                                           onChangeText={text => this.setState({name: text})}/>
                            </View>
                            <View style={styles.input_t1_form}>
                                <TextInput style={styles.input_t1} keyboardType='phone-pad' placeholder={'가격입력'}
                                           ref="price"
                                           underlineColorAndroid='transparent' returnKeyType="done"
                                           value={this.state.price}
                                           onChangeText={text => this.setState({price: text})}/>
                            </View>
                        </View>
                    </View>
                    <View style={styles.form_bottom}>
                        <TextInput style={styles.input_t2} multiline={true} placeholder={'메뉴설명'} ref="desc"
                                   underlineColorAndroid='transparent' returnKeyType="done" value={this.state.desc}
                                   onChangeText={text => this.setState({desc: text})}/>
                    </View>
                </View>
            </View>
        )
    }
}

export default admin_add_memu_proc;