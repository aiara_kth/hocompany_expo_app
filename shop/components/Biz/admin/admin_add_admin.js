import React from 'react';
import {Text, View, StyleSheet, Image,TextInput,TouchableHighlight, Dimensions, Alert ,AsyncStorage,Menu} from 'react-native';
import { Actions } from 'react-native-router-flux';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import {Ionicons} from '@expo/vector-icons';
import Global from "../../../objects/Global";
import API from "../../../objects/API";

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fafbfb',
    },
    Navigator : {
        flexDirection: 'row',
        marginTop : getStatusBarHeight(),
        width: Dimensions.get('window').width,
        height: 48,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    Navigator_menu: {
        width: 80,
        height: 48,
    },
    Navigator_menu_button :{
        width: 48,
        height: 48,
        paddingTop: 12,
        paddingLeft: 12,
    },
    Navigator_menu_image : {
        width: 24,
        height: 24
    },
    Navigator_title:{
        width : ((Dimensions.get('window').width) - 160),
        alignItems: 'center',
    },
    Navigator_title_text:{
        fontSize: 16,
        color:'#000000'
    },
    Navigator_start:{
        alignItems: 'flex-end',
        width: 80,
        height: 48,
        paddingTop: 12,
        paddingRight: 12,
    },
    Navigator_start_text:{
        fontSize: 16,
        color:'#7db840',
    },
    sub_Navigator : {
        flexDirection: 'row',
        width: Dimensions.get('window').width,
        height: 40,
        backgroundColor: '#fff',
        alignItems: 'center',
    },
    sub_Navigator_menu: {
        width: 85,
        height: 40,
        alignItems: 'flex-end',
        justifyContent: 'center',
        marginLeft: 15,
        marginRight: 15
    },
    sub_Navigator_menu_on: {
        width: 65,
        height: 40,
        alignItems: 'center',
        justifyContent: 'center',
        borderBottomWidth: 2,
        borderBottomColor: '#000000',
    },
    sub_Navigator_menu_off: {
        width: 65,
        height: 40,
        alignItems: 'center',
        justifyContent: 'center',
        borderBottomWidth: 2,
        borderBottomColor: 'transparent',
    },
    sub_Navigator_name_on : {
        fontSize : 14,
        fontWeight: 'bold',
        alignItems: 'center',
    },
    sub_Navigator_name_off : {
        fontSize : 14,
        alignItems: 'center',
    },
    contents : {
        marginTop :  9,
        width: Dimensions.get('window').width,
        backgroundColor: '#ffffff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    input_form : {
        width: Dimensions.get('window').width,
        height: 184,
        justifyContent: 'center',
        alignItems: 'center',
    },
    input_box : {
        width: Dimensions.get('window').width - 40,
        height:48,
        borderRadius: 2,
        backgroundColor: '#fafbfb',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection:'row',
        marginBottom:8
    },
    input_dom : {
        marginLeft : 12,
        width: Dimensions.get('window').width - 76,
    },
    input_dom_s : {
        marginLeft : 12,
        width : Dimensions.get('window').width - 100,
    },
    dropbox_arrow :{
        width : 24,
        height: 24,
        marginRight: 10
    },
    none_selectbox : {
        width: Dimensions.get('window').width,
        height: (Dimensions.get('window').height - 208),
        marginLeft: -10
    },
    selectbox : {
        backgroundColor: '#ffffff',
        width: Dimensions.get('window').width,
        height: 208,
    },
    selectbox_noselect : {
        width: Dimensions.get('window').width,
        height: 56,
    },
    selectbox_selected : {
        backgroundColor:'#606060',
        color:'#ffffff'
    },

    c_modal : {
        zIndex: 9999,
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height - getStatusBarHeight(),
        // flexDirection: 'row',
        position: 'absolute',
        left: 0,
        top: getStatusBarHeight()
    }, c_modal_item:{
        width: Dimensions.get('window').width,
        height: 264,
        backgroundColor: '#ffffff',
        alignItems: 'center',
        justifyContent: 'center'
    },
    c_modal_shadow : {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height - getStatusBarHeight() - 264,
        backgroundColor: 'rgba(0, 0, 0, 0.4)'
    } ,
    c_modal_button_group : {
        width: Dimensions.get('window').width,
        alignItems: 'center',
        justifyContent: 'center',
    },
    c_modal_button : {
        width: Dimensions.get('window').width,
        height: 56,
        paddingLeft: 12,
        paddingRight: 12,
        paddingTop: 16,
        fontSize: 16,
        lineHeight:24,
        marginTop: 1,
        fontWeight: '500',
        textAlign: 'center'
    },



});

const adminType = [
    {
        num: 1,
        name: "관리자"
    },
    {
        num: 2,
        name: "알바생"
    }
];

class admin_add_admin extends  React.Component {
    constructor(props) {
        super(props);
        this.state = {
            show_modal: false,
            name: this.props.admin !== null ? this.props.admin.name : '',
            phone: this.props.admin !== null ? this.props.admin.phone : '',
            type_name: this.props.admin !== null ? (this.props.admin.type === 1 ? '관리자' : '알바생') : '',
            select_type: this.props.admin !== null ? this.props.admin.type : 0
        };
    }

    add_start() {
        if (this.state.name === '') {
            Alert.alert('관리자 관리', '관리자명을 입력해주세요.', [{text: "확인", onPress: () => null}]);
            this.refs.admin_name.focus();
            return;
        }

        if (this.state.phone === '') {
            Alert.alert('관리자 관리', '관리자 번호를 입력해주세요.', [{text: "확인", onPress: () => null}]);
            this.refs.admin_phone.focus();
            return;
        }

        if (this.state.select_type === '') {
            Alert.alert('관리자 관리', '관리자 유형을 선택해주세요.', [{text: "확인", onPress: () => null}]);
            this.refs.admin_type.focus();
            return;
        }


        Alert.alert('관리자 관리', '관리자를 ' + (this.props.admin === null ? '추가' : '수정') + '하시겠습니까?',
            [
                {text: "취소", onPress: () => null, styles: 'cancel'},
                {text: "확인", onPress: () => this.addAdmin()}
            ]
        );

    }

    addAdmin() {
        let shop = this.props.shop;
        if (this.props.admin === null) {
            // 추가
            shop.admin[this.state.select_type].push({
                name: this.state.name,
                phone: this.state.phone,
                type: this.state.select_type
            });
        } else {
            // 수정
            let index = shop.admin[this.props.admin.type].findIndex(a => a.name === this.props.admin.name && a.phone === this.props.admin.phone);
            if (this.props.admin.type === this.state.select_type) {
                // 유형 그대로
                shop.admin[this.props.admin.type][index].name = this.state.name;
                shop.admin[this.props.admin.type][index].phone = this.state.phone;
            } else {
                // 유형 변경
                shop.admin[this.props.admin.type].splice(index, 1);
                shop.admin[this.state.select_type].push({
                    name: this.state.name,
                    phone: this.state.phone,
                    type: this.state.select_type
                });
            }
        }
        AsyncStorage.setItem("@shop", JSON.stringify(shop), () => {
            let shopAdmins = [];
            for(let i = 0; i < shop.admin[1].length; i++) {
                // todo 회원 정보 가져오기
                shopAdmins.push({
                    userID: "",
                    adminRank: "master",
                    adminNumber: "",
                    permission: 400,
                    pushSettings: true,
                    userFirebaseInstanceToken: ""
                });
            }
            for(let i = 0; i < shop.admin[2].length; i++) {
                // todo 회원 정보 가져오기
                shopAdmins.push({
                    userID: "",
                    adminRank: "user",
                    adminNumber: "",
                    permission: 200,
                    pushSettings: true,
                    userFirebaseInstanceToken: ""
                });
            }
            API.shop.admin.put(shop.shopId, {
                shopAdmins: shopAdmins
            }, data => {
                Actions.pop({refresh: {menu: 2, shop: shop}});
            }, error => {
                Actions.pop({refresh: {menu: 2, shop: shop}});
            });
        });
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={[styles.c_modal, this.state.show_modal ? {} : {display: 'none', position: 'relative'}]}>
                    <TouchableHighlight underlayColor={'transparent'} onPress={() => this.setState({
                        show_modal: false,
                        select_type: null,
                        type_name: ''
                    })}>
                        <View style={styles.c_modal_shadow}/>
                    </TouchableHighlight>
                    <View style={styles.c_modal_item}>
                        <Text style={{fontSize: 14, color: '#606060', marginTop: 12, marginBottom: 8}}>관리자 유형을 선택해
                            주세요.</Text>
                        {
                            adminType.map(type => (
                                <Text key={type.num + '_' + type.name}
                                      style={[styles.c_modal_button, this.state.select_type === type.num ? styles.selectbox_selected : {}]}
                                      onPress={() => this.setState({
                                          select_type: type.num,
                                          type_name: type.name
                                      })}>{type.name}</Text>
                            ))
                        }
                        <TouchableHighlight underlayColor={'transparent'}
                                            onPress={() => this.setState({show_modal: false})}>
                            <Text style={[styles.c_modal_button, {
                                backgroundColor: '#606060',
                                color: '#ffffff'
                            }]}>확인</Text>
                        </TouchableHighlight>
                    </View>
                </View>
                <View style={styles.Navigator}>
                    <View style={styles.Navigator_menu}>
                        <TouchableHighlight style={styles.Navigator_menu_button} underlayColor={'transparent'}
                                            onPress={() => Actions.pop()}>
                            <Image style={styles.Navigator_menu_image}
                                   source={require("../../../assets/back_arrow.png")}/>
                        </TouchableHighlight>
                    </View>
                    <View style={styles.Navigator_title}><Text style={styles.Navigator_title_text}>관리자 관리</Text></View>
                    <TouchableHighlight underlayColor={'transparent'} onPress={() => this.add_start()}>
                        <View style={styles.Navigator_start}><Text style={styles.Navigator_start_text}>완료 </Text></View>
                    </TouchableHighlight>
                </View>
                <View style={styles.contents}>
                    <View style={styles.input_form}>
                        <View style={[styles.input_box, {marginTop: 12}]}>
                            <TextInput ref="admin_name" style={styles.input_dom} placeholder={'관리자명 입력'}
                                       underlineColorAndroid='transparent' retrunKeyType="done" value={this.state.name}
                                       onChangeText={(text) => this.setState({name: text})}/>
                        </View>
                        <View style={styles.input_box}>
                            <TextInput ref="admin_phone" secureTextEntry={false} style={styles.input_dom}
                                       placeholder={'관리자 번호 입력'} underlineColorAndroid='transparent'
                                       value={this.state.phone}
                                       retrunKeyType="done" onChangeText={(text) => this.setState({phone: text})}/>
                        </View>
                        <TouchableHighlight underlayColor={'transparent'}
                                            onPress={() => this.setState({show_modal: true})}>
                            <View style={styles.input_box}>
                                <TextInput ref="admin_type" secureTextEntry={false} editable={false}
                                           style={styles.input_dom_s} placeholder={'관리자 유형 선택'}
                                           underlineColorAndroid='transparent'
                                           onPress={() => this.setState({show_modal: true})}
                                           value={this.state.type_name}/>
                                <Image style={styles.dropbox_arrow}
                                       source={require("../../../assets/common/dropbox.png")}/>
                            </View>
                        </TouchableHighlight>
                    </View>
                </View>
            </View>
        )
    }
}

export default admin_add_admin;