import React from 'react';
import {Text, View, StyleSheet, Image,TextInput,TouchableHighlight, Dimensions, ScrollView, Alert ,AsyncStorage,Menu} from 'react-native';
import Triangle from 'react-native-triangle';
import { Actions } from 'react-native-router-flux';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import {Ionicons} from '@expo/vector-icons';
import Global from "../../../objects/Global";
import API from "../../../objects/API";


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f9f9f9',
    },
    Navigator : {
        flexDirection: 'row',
        paddingTop : getStatusBarHeight(),
        width: Dimensions.get('window').width,
        height: 48 + getStatusBarHeight(),
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    Navigator_menu: {
        width: 80,
        height: 48,
    },
    Navigator_menu_button :{
        width: 48,
        height: 48,
        paddingTop: 12,
        paddingLeft: 12,
    },
    Navigator_menu_image : {
        width: 24,
        height: 24
    },
    Navigator_title:{
        width : ((Dimensions.get('window').width) - 160),
        alignItems: 'center',
    },
    Navigator_title_text:{
        fontSize: 16,
        color:'#000000'
    },
    Navigator_start:{
        alignItems: 'flex-end',
        width: 80,
        height: 48,
        paddingTop: 12,
        paddingRight: 12,
    },
    Navigator_start_text:{
        fontSize: 16,
        color:'#7db840',
    },
    sub_Navigator : {
        flexDirection: 'row',
        width: Dimensions.get('window').width,
        height: 40,
        backgroundColor: '#fff',
        alignItems: 'center',
    },
    sub_Navigator_menu: {
        width: 85,
        height: 40,
        alignItems: 'flex-end',
        justifyContent: 'center',
        marginLeft: 15,
        marginRight: 15
    },
    sub_Navigator_menu_on: {
        width: 65,
        height: 40,
        alignItems: 'center',
        justifyContent: 'center',
        borderBottomWidth: 2,
        borderBottomColor: '#000000',
    },
    sub_Navigator_menu_off: {
        width: 65,
        height: 40,
        alignItems: 'center',
        justifyContent: 'center',
        borderBottomWidth: 2,
        borderBottomColor: 'transparent',
    },
    sub_Navigator_name_on : {
        fontSize : 14,
        fontWeight: 'bold',
        alignItems: 'center',
        textAlign: 'center'
    },
    sub_Navigator_name_off : {
        fontSize : 14,
        alignItems: 'center',
        textAlign: 'center'
    },
    contents : {
        paddingTop :  9,
    },
    contents_item : {
        width: Dimensions.get('window').width,
        backgroundColor: '#fff',
        marginBottom: 10
    },
    contents_item_sub : {
        width: Dimensions.get('window').width,
        height: 114,
        backgroundColor: '#fff',
        marginBottom: 8,
        paddingLeft: 20,
        paddingRight: 20
    },
    contents_item_info:{
        flexDirection: 'row',
        flex: 1,
        height: 48,
        paddingLeft: 12,
        paddingRight: 12,
        paddingTop: 16,
        paddingBottom: 16
    },
    admin_type_top: {
        width: 45
    },
    admin_type : {
      fontSize: 16
    },
    admin_name_top : {
        width: (Dimensions.get('window').width - 185),
    },
    admin_name : {
        paddingLeft: 10,
        fontSize: 16,
    },
    admin_phone_top: {
        width: 130,
    },
    admin_phone : {
        fontSize: 16,
    },
    color_red : {
        color : '#a25757'
    },
    color_red_s : {
        color : '#eb5847'
    },
    color_green : {
        color: '#7db840'
    },
    color_purple : {
        color:  'rgba(127, 143, 209,0.5)'
    },
    color_purple_s : {
        color:  '#7f8fd1'
    },
    color_gray : {
        color:  '#c4c4c4'
    },

    shop_main_info : {
        height: 'auto',
        backgroundColor : '#fff'
    },

    shop_name : {
        marginTop :12,
        fontSize: 20,
        lineHeight: 24,
        width: (Dimensions.get('window').width),
        textAlign: 'center'
    },

    shop_status : {
        marginTop: 3,
        width: (Dimensions.get('window').width),
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
        // marginBottom: 16
    },


    shop_main_image_bin : {
        width: (Dimensions.get('window').width),
        height: 'auto'
    },
    shop_main_image : {
        // width: (Dimensions.get('window').width),
        // height: (Dimensions.get('window').width / 1.8),
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').width * 0.75,
        resizeMode: 'stretch'
    },

    ViewLeft : {
        width: '50%',
        paddingLeft: 1
    },
    ViewRight : {
        width: '50%',
        alignItems: 'flex-end',
        justifyContent: 'flex-end',
        paddingRight: 12
    },

    shop_process_bar_group : {
        marginTop: 16,
        flex : 1,
        flexDirection: 'row',
        paddingLeft: 12
    },
    shop_process_bar : {
        textAlign: 'right',
        width: ((Dimensions.get('window').width - 40) / 5),
        height: 6,
        marginRight: 4,
    },
    shop_main_noti : {
        width: (Dimensions.get('window').width - 24) ,
        marginTop: 10,
        marginLeft: 12,
        marginBottom: 17,
        paddingLeft: 12,
        paddingTop: 8,
        height: 56,
        backgroundColor: '#30cbba'

    },
    timescircle:{
        width:12,
        height: 12,
        position: 'absolute',
        right: 8,
        top: 8
    },
    shop_manager_button_group : {
        width: Dimensions.get('window').width ,
        flex : 1,
        flexDirection: 'row',
        height: 72,
        marginTop: 8,
        backgroundColor: '#ffffff'
    },
    shop_manager_button : {
        width: (Dimensions.get('window').width / 4),
        height: 72,
        alignItems: 'center',
        justifyContent: 'center',
    },
    shop_manager_button_image : {
        width: 24,
        height: 24,
        marginBottom:2
    },
    shop_manager_button_text : {
        fontSize: 14,
    },
    add_admin_button_group : {
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 24
    },
    add_admin_button : {
        width: 100,
        height: 32,
        borderRadius: 12,
        borderColor: '#e8e8e8',
        borderWidth: 1,
        // paddingTop : 8,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    add_admin_button_s : {
        width: 40,
        height: 24,
        borderRadius: 12,
        borderColor: '#e8e8e8',
        borderWidth: 1,
        // paddingTop :  5,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    add_admin_button_text : {
        fontSize: 12,
        // height: 32,
        color: '#8e8e8e'
    },
    add_admin_button_text_s : {
        fontSize: 12,
        // height: 24,
        color: '#8e8e8e'
    },
    callout_box : {
        width: 195,
        height: 40,
        borderRadius: 2,
        backgroundColor: '#7f8fd1',
        alignItems: 'center',
        justifyContent: 'center',
        shadowOffset: {width: 2, height: 10 },
        shadowColor: 'rgba(0, 0, 0, 0.2)'
    },
    callout_box_text : {
        fontSize : 12,
        color: '#ffffff'
    },
    center_button:{
        width: Dimensions.get('window').width,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 8
    },
    buttom_button:{
        position: 'absolute',
        marginBottom : ((Dimensions.get('window').height /  2) - 120)
    },
    add_button:{
        width: Dimensions.get('window').width,
        height: 56,
    },
    add_button_text : {
        position: 'absolute',
        left: ((Dimensions.get('window').width / 2) - '2%'),
        top : ((Dimensions.get('window').height /  2) + 30)
    },
    cert_num_sendbox : {
        width : (Dimensions.get('window').width),
        height: (Dimensions.get('window').width),
        backgroundColor: '#ffffff',
        flex : 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    cert_num_big : {
        fontSize: 40,
        fontWeight: 'bold'
    },
    cert_num_small : {
        marginTop: 20,
        marginBottom: 20,
        fontSize: 16,
        color: '#eb5847',
    },
    c_modal : {
        zIndex: 9999,
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height - getStatusBarHeight(),
        // flexDirection: 'row',
        position: 'absolute',
        left: 0,
        top: getStatusBarHeight()
    }, c_modal_item:{
        width: Dimensions.get('window').width,
        height: 210,
        backgroundColor: '#ffffff',
        alignItems: 'center',
        justifyContent: 'center'
    },
    c_modal_shadow : {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height - getStatusBarHeight() - 210,
        backgroundColor: 'rgba(0, 0, 0, 0.4)'
    } ,
    c_modal_button_group : {
        width: Dimensions.get('window').width,
        alignItems: 'center',
        justifyContent: 'center',
    },
    c_modal_button : {
        width: Dimensions.get('window').width,
        height: 48,
        marginTop: 1,
        paddingLeft: 12,
        paddingRight: 12,
        paddingTop: 16,
        fontSize: 16,
        textAlign: 'center'
    },

});

let shop = {
    name: "치킨먹자",
    description: "치킨집입니다",
    openTime: "11:00",
    closeTime: "03:00",
    address: "서울시 강남구 논현동",
    nowOpen: true,
    admin: [
        // 운영자
        null,
        // 관리자
        [
            {
                name: "김민지",
                phone: "01079669945",
                type: 1
            },
            {
                name: "김사랑",
                phone: "01079669945",
                type: 1
            },
            {
                name: "김우정",
                phone: "01079669945",
                type: 1
            }
        ],
        // 알바생
        []
    ],
    photo: null,
    authNum: "0575",
    menu: [
        {
            name: "후라이드 치킨",
            description: "튀기면 신발도 맛있다는데 후라이드가 맛이 없을리가...",
            price: 12000,
            photo: require("../../../assets/Biz/dummy_menu_images.png")
        },
        {
            name: "양념 치킨",
            description: "양념이 거의 마약수준",
            price: 13000,
            photo: require("../../../assets/Biz/dummy_menu_images.png")
        },
        {
            name: "간장 치킨",
            description: "단짠단짠",
            price: 13000,
            photo: require("../../../assets/Biz/dummy_menu_images.png")
        },
        {
            name: "생맥주 1000cc",
            description: "치킨엔 생맥주지",
            price: 5000,
            photo: require("../../../assets/Biz/dummy_menu_images.png")
        }
    ],
    event: ""
};

class admin_main extends  React.Component {
    constructor(props) {
        super(props);
        if (shop.admin[0] === null) {
            shop.admin[0] = {
                name: Global.user !== null ? Global.user.username : "김길수",
                phone: Global.user !== null ? Global.user.phoneNumber : "01079669945",
                type: 0
            };
        }
        // AsyncStorage.setItem("@shop", JSON.stringify(shop));
        this.state = {
            menu: this.props.menu !== undefined ? this.props.menu : 1,
            shop_success: 40,
            admin_popup: 0,
            shop_success_advice: [],
            shop: this.props.shop !== undefined ? this.props.shop : shop,
            selectedAdmin: null,
            authNumInput: '',
            editAuthNum: false
        };
    }

    componentDidMount() {
        this.loadSavedShop();
    }

    async loadSavedShop() {
        let loadedShop = await AsyncStorage.getItem("@shop");
        if (loadedShop === undefined || loadedShop === null) {
            this.checkShopSuccess();
            return;
        }
        shop = JSON.parse(loadedShop);
        this.setState({shop: shop});
        this.checkShopSuccess();
    }

    componentWillReceiveProps() {
        this.checkShopSuccess();
    }

    checkShopSuccess() {
        let total = 0;
        let shopSuccess = 0;
        let advice = [];
        total++;
        if (this.state.shop.name !== "") {
            shopSuccess++;
        } else {
            if (advice.length === 0)
                advice = ["식당명을 아직 입력 안하셨습니다."];
        }
        total++;
        if (this.state.shop.address !== "") {
            shopSuccess++;
        } else {
            if (advice.length === 0)
                advice = ["매장 주소를 아직 입력 안하셨습니다."];
        }
        total++;
        if (this.state.shop.authNum !== "") {
            shopSuccess++;
        } else {
            if (advice.length === 0)
                advice = ["예약 인증 번호를 아직 등록 안하셨습니다."];
        }
        total++;
        if (this.state.shop.openTime !== "") {
            shopSuccess++;
        } else {
            if (advice.length === 0)
                advice = ["매장 영업 시작 시간을 아직 입력 안하셨습니다.", "매장 영업 시작 시간을 입력하셔서 고객들이 영업시간 중에 찾아올 수 있게 도와주세요."];
        }
        total++;
        if (this.state.shop.closeTime !== "") {
            shopSuccess++;
        } else {
            if (advice.length === 0)
                advice = ["매장 영업 종료 시간을 아직 입력 안하셨습니다.", "매장 영업 종료 시간을 입력하셔서 고객들이 영업시간 중에 찾아올 수 있게 도와주세요."];
        }
        total++;
        if (this.state.shop.description !== "") {
            shopSuccess++;
        } else {
            if (advice.length === 0)
                advice = ["매장 소개를 아직 입력 안하셨습니다.", "매장소개를 입력하셔서 고객들에게 매장을 소개해 보세요."];
        }
        total++;
        if (this.state.shop.admin[1].length > 0) {
            shopSuccess++;
        } else {
            if (advice.length === 0)
                advice = ["매장 관리자를 아직 추가 안하셨습니다.", "매장 관리자를 추가하셔서 더욱 편하게 매장을 관리해 보세요."];
        }
        total++;
        if (this.state.shop.photo !== null) {
            shopSuccess++;
        } else {
            if (advice.length === 0)
                advice = ["매장 사진을 아직 등록 안하셨습니다.", "매장 사진을 등록하셔서 고객 유치율을 상승시켜 보세요."];
        }
        total++;
        if (this.state.shop.menu.length > 0) {
            shopSuccess++;
        } else {
            if (advice.length === 0)
                advice = ["메뉴를 아직 등록 안하셨습니다.", "메뉴를 등록하시면 고객들이 매장에 도착하기 전에 선주문을 할 수 있습니다."];
        }
        if (shopSuccess === 0) {
            this.setState({shop_success: 0, shop_success_advice: advice});
            return;
        }
        if (shopSuccess === total) {
            this.setState({shop_success: 100, shop_success_advice: ""});
            return;
        }
        let percent = Math.floor((shopSuccess / total) * 100);
        this.setState({shop_success: percent, shop_success_advice: advice});
    }

    shopInfo() {
        const color_on = '#eb5847';
        const color_off = '#f2f2f2';
        return <ScrollView ref="shop_info" style={[styles.contents, this.state.menu === 1 ? null : {display: 'none'}]}>
            <View style={styles.shop_main_image_bin}>
                <Image style={styles.shop_main_image}
                       source={this.state.shop.photo === null ? require("../../../assets/common/add_image_dummy.png") : this.state.shop.photo}/>
            </View>
            <View style={styles.shop_main_info}>
                <View>
                    <Text style={styles.shop_name}>{this.state.shop.name}</Text>
                </View>
                <View style={styles.shop_status}>
                    <Text style={{color: '#7f8fd1', fontSize: 12}}>영업중</Text>
                    <Text style={{color: '#b6b6b6', fontSize: 12, marginLeft: 8}}>관리자 <Text
                        style={{
                            color: '#838383',
                            fontSize: 12
                        }}>{this.state.shop.admin[1].length}</Text></Text>
                </View>
                <View>
                    <View style={styles.shop_process_bar_group}>
                        {
                            [20, 40, 60, 80, 100].map(percent => (
                                <Text key={percent}
                                      style={[
                                          styles.shop_process_bar,
                                          this.state.shop_success >= percent ? {backgroundColor: color_on} : {backgroundColor: color_off}
                                      ]}> </Text>
                            ))
                        }
                    </View>
                    <View style={styles.shop_process_bar_group}>
                        <Text style={[styles.ViewLeft, {fontSize: 16, color: '#eb5847'}]}>점포완성도</Text>
                        <View style={styles.ViewRight}><Text
                            style={{fontSize: 16, color: '#eb5847', fontWeight: 'bold'}}>{this.state.shop_success}<Text
                            style={{fontSize: 12}}>%</Text></Text></View>
                    </View>
                </View>
                {
                    this.state.shop_success_advice.length === 2
                        ? (
                            <View style={styles.shop_main_noti}>
                                <Image style={styles.timescircle}
                                       source={require("../../../assets/common/timescircle.png")}/>
                                <Text style={{
                                    fontSize: 12,
                                    color: '#ffffff',
                                    marginBottom: 8
                                }}>{this.state.shop_success_advice[0]}</Text>
                                <Text style={{fontSize: 12, color: '#ffffff'}}>{this.state.shop_success_advice[1]}</Text>
                            </View>
                        )
                        : this.state.shop_success_advice.length === 1
                        ? (
                            <View style={styles.shop_main_noti}>
                                <Image style={styles.timescircle}
                                       source={require("../../../assets/common/timescircle.png")}/>
                                <Text
                                    style={{fontSize: 12, color: '#ffffff'}}>{this.state.shop_success_advice[0]}</Text>
                            </View>
                        )
                        : (<View></View>)
                }
            </View>

            <View style={styles.shop_manager_button_group}>
                <TouchableHighlight underlayColor={'transparent'}
                                    onPress={() => Actions.admin_add_info_edit({shop: this.state.shop})}>
                    <View style={styles.shop_manager_button}>
                        <Image style={styles.shop_manager_button_image}
                               source={require("../../../assets/common/pen_cricle.png")}/>
                        <Text style={styles.shop_manager_button_text}>매장정보수정</Text>
                    </View>
                </TouchableHighlight>
                <TouchableHighlight underlayColor={'transparent'}
                                    onPress={() => Actions.admin_add_info({shop: this.state.shop})}>
                    <View style={styles.shop_manager_button}>
                        <Image style={styles.shop_manager_button_image}
                               source={require("../../../assets/common/store_icon.png")}/>
                        <Text style={styles.shop_manager_button_text}>매장소개</Text>
                    </View>
                </TouchableHighlight>
                <TouchableHighlight underlayColor={'transparent'}
                                    onPress={() => Actions.admin_add_menu({shop: this.state.shop})}>
                    <View style={styles.shop_manager_button}>
                        <Image style={styles.shop_manager_button_image}
                               source={require("../../../assets/common/burgur_icon.png")}/>
                        <Text style={styles.shop_manager_button_text}>메뉴수정</Text>
                    </View>
                </TouchableHighlight>
                <TouchableHighlight underlayColor={'transparent'} onPress={() => null}>
                    <View style={styles.shop_manager_button}>
                        <Image style={styles.shop_manager_button_image}
                               source={require("../../../assets/common/star_circle.png")}/>
                        <Text style={styles.shop_manager_button_text}>이벤트등록</Text>
                    </View>
                </TouchableHighlight>
            </View>
        </ScrollView>;
    }

    phoneNumberString(phone) {
        if (phone.length === 11) {
            return phone.substr(0, 3) + "-" + phone.substr(3, 4) + "-" + phone.substr(7, 4);
        }
        if (phone.length === 10) {
            return phone.substr(0, 3) + "-" + phone.substr(3, 3) + "-" + phone.substr(6, 4);
        }
        return phone;
    }

    onPressAdmin(admin) {
        this.setState({admin_popup: 1, selectedAdmin: admin});
    }

    onPressDeleteAdmin(admin) {
        Alert.alert('관리자 관리', '관리자를 삭제하시겠습니까?',
            [
                {text: "취소", onPress: () => null, styles: 'cancel'},
                {
                    text: "확인", onPress: () => {
                        let newAdmin = [];
                        shop.admin[admin.type].map(ad => {
                            if (ad.name !== admin.name || ad.phone !== admin.phone) {
                                newAdmin.push(ad);
                            }
                        });
                        shop.admin[admin.type] = newAdmin;
                        this.setState({admin_popup: 0, selectedAdmin: null, shop: shop});
                        AsyncStorage.setItem("@shop", JSON.stringify(shop), () => {
                            let shopAdmins = [];
                            for(let i = 0; i < shop.admin[1].length; i++) {
                                // todo 회원 정보 가져오기
                                shopAdmins.push({
                                    userID: "",
                                    adminRank: "master",
                                    adminNumber: "",
                                    permission: 400,
                                    pushSettings: true,
                                    userFirebaseInstanceToken: ""
                                });
                            }
                            for(let i = 0; i < shop.admin[2].length; i++) {
                                // todo 회원 정보 가져오기
                                shopAdmins.push({
                                    userID: "",
                                    adminRank: "user",
                                    adminNumber: "",
                                    permission: 200,
                                    pushSettings: true,
                                    userFirebaseInstanceToken: ""
                                });
                            }
                            API.shop.admin.put(shop.shopId, {
                                shopAdmins: shopAdmins
                            }, data => {
                                Actions.pop({refresh: {menu: 2, shop: shop}});
                            }, error => {
                                Actions.pop({refresh: {menu: 2, shop: shop}});
                            });
                        });
                    }
                }
            ]
        );
    }

    printAdmin(admin, type) {
        return <TouchableHighlight underlayColor={'transparent'} onPress={() => this.onPressAdmin(admin)}
                                   key={admin.name + admin.phone}>
            <View style={styles.contents_item_info}>
                <View style={styles.admin_type_top}>
                    <Text
                        style={[styles.admin_type, admin.type === 0 ? styles.color_red_s : styles.color_purple_s]}>{type}</Text>
                </View>
                <View style={styles.admin_name_top}>
                    <Text style={styles.admin_name}>{admin.name}</Text>
                </View>
                <View style={styles.admin_phone_top}>
                    <Text style={styles.admin_phone}>{this.phoneNumberString(admin.phone)}</Text>
                </View>
            </View>
        </TouchableHighlight>;
    }

    admin() {
        return <ScrollView ref="admin" style={[styles.contents, this.state.menu === 2 ? null : {display: 'none'}]}>
            <View style={styles.contents_item}>
                {this.printAdmin(this.state.shop.admin[0], "운영자")}
            </View>
            <View style={[styles.contents_item, this.state.shop.admin[1].length === 0 ? {display: 'none'} : {}]}>
                {
                    this.state.shop.admin[1].map(admin => this.printAdmin(admin, "관리자"))
                }
            </View>
            <View style={[styles.contents_item, this.state.shop.admin[2].length === 0 ? {display: 'none'} : {}]}>
                {
                    this.state.shop.admin[2].map(admin => this.printAdmin(admin, "알바생"))
                }
            </View>
            <View style={styles.add_admin_button_group}>
                <TouchableHighlight underlayColor={'transparent'}
                                    onPress={() => Actions.admin_add_admin({shop: this.state.shop, admin: null})}>
                    <View style={styles.add_admin_button}>
                        <Text style={styles.add_admin_button_text}>관리자 추가</Text>
                    </View>
                </TouchableHighlight>
                <View style={styles.center_button}>
                    <Triangle width={18} height={8} color={"#7f8fd1"} direction={"up"}/>
                    <View style={styles.callout_box}>
                        <Text style={styles.callout_box_text}>관리자추가 버튼을 눌러서</Text>
                        <Text style={styles.callout_box_text}>관리자를 추가해보세요.</Text>
                    </View>
                </View>
            </View>
        </ScrollView>;
    }

    onPressChangeAuthNum() {
        if (this.state.editAuthNum) {
            if (this.state.shop.authNum === this.state.authNumInput) {
                this.setState({editAuthNum: !this.state.editAuthNum});
                return;
            }
            shop.authNum = this.state.authNumInput;
            this.setState({shop: shop});
            AsyncStorage.setItem("@shop", JSON.stringify(shop));
        } else {
            this.setState({authNumInput: this.state.shop.authNum});
        }
        this.setState({editAuthNum: !this.state.editAuthNum});
        if (this.state.editAuthNum) {
            this.refs.authNumInput.focus();
        }
    }

    certNum() {
        return <ScrollView ref="cert_num" style={[styles.contents, this.state.menu === 3 ? null : {display: 'none'}]}>
            <View style={styles.cert_num_sendbox}>
                {
                    this.state.editAuthNum
                        ? (<TextInput style={[styles.cert_num_big, {
                            backgroundColor: '#fcfcfc',
                            width: Dimensions.get('window').width * .5,
                            textAlign: 'center'
                        }]} placeholder={""}
                                      keyboardType='numeric'
                                      value={this.state.authNumInput} ref="authNumInput"
                                      underlineColorAndroid={'transparent'}
                                      onChangeText={(text) => this.setState({authNumInput: text})}/>)
                        : (<Text style={styles.cert_num_big}>{this.state.shop.authNum}</Text>)
                }
                <Text style={styles.cert_num_small}>예약인증번호</Text>
                <TouchableHighlight underlayColor={'transparent'} onPress={() => this.onPressChangeAuthNum()}>
                    <View style={styles.add_admin_button_s}>
                        <Text style={styles.add_admin_button_text_s}>{this.state.editAuthNum ? '저장' : '변경'}</Text>
                    </View>
                </TouchableHighlight>
            </View>
        </ScrollView>;
    }

    topbar() {
        return <View style={styles.Navigator}>
            <View style={styles.Navigator_menu}>
                <TouchableHighlight style={styles.Navigator_menu_button} underlayColor={'transparent'}
                                    onPress={() => Actions.pop()}>
                    <Image style={styles.Navigator_menu_image} source={require("../../../assets/menu.png")}/>
                </TouchableHighlight>
            </View>
            <View style={styles.Navigator_title}><Text style={styles.Navigator_title_text}>점포운영관리</Text></View>
            <TouchableHighlight underlayColor={'transparent'} onPress={() => null}>
                <View style={styles.Navigator_start}><Text style={styles.Navigator_start_text}> </Text></View>
            </TouchableHighlight>
        </View>;
    }

    topmenu() {
        return <View style={styles.sub_Navigator}>
            <TouchableHighlight underlayColor={'transparent'} onPress={() => this.setState({menu: 1})}>
                <View style={[styles.sub_Navigator_menu, {width: 55}]}>
                    <View
                        style={[this.state.menu === 1 ? styles.sub_Navigator_menu_on : styles.sub_Navigator_menu_off, {width: 55}]}>
                        <Text
                            style={[this.state.menu === 1 ? styles.sub_Navigator_name_on : styles.sub_Navigator_name_off, {width: 55}]}>점포관리</Text>
                    </View>
                </View>
            </TouchableHighlight>
            <TouchableHighlight underlayColor={'transparent'} onPress={() => this.setState({menu: 2})}>
                <View style={[styles.sub_Navigator_menu, {width: 45}]}>
                    <View
                        style={[this.state.menu === 2 ? styles.sub_Navigator_menu_on : styles.sub_Navigator_menu_off, {width: 45}]}>
                        <Text
                            style={[this.state.menu === 2 ? styles.sub_Navigator_name_on : styles.sub_Navigator_name_off, {width: 45}]}>관리자</Text>
                    </View>
                </View>
            </TouchableHighlight>
            <TouchableHighlight underlayColor={'transparent'} onPress={() => this.setState({menu: 3})}>
                <View style={[styles.sub_Navigator_menu, {width: 85}]}>
                    <View
                        style={[this.state.menu === 3 ? styles.sub_Navigator_menu_on : styles.sub_Navigator_menu_off, {width: 85}]}>
                        <Text
                            style={[this.state.menu === 3 ? styles.sub_Navigator_name_on : styles.sub_Navigator_name_off, {width: 85}]}>예약인증번호</Text>
                    </View>
                </View>
            </TouchableHighlight>
        </View>;
    }

    adminmodel() {
        return <View style={[styles.c_modal, this.state.admin_popup === 1 && this.state.selectedAdmin !== null ? {} : {
            display: 'none',
            position: 'relative'
        }]}>
            <TouchableHighlight underlayColor={'transparent'} onPress={() => this.setState({admin_popup: 0})}>
                <View style={styles.c_modal_shadow}/>
            </TouchableHighlight>
            <View style={[styles.c_modal_item, {backgroundColor: '#fcfcfc'}]}>
                <Text style={{
                    marginTop: 13,
                    marginBottom: 3,
                    fontSize: 12,
                    lineHeight: 16,
                    color: '#7f8fd1'
                }}>{this.state.selectedAdmin === null ? "" : this.state.selectedAdmin.type === 0 ? "운영자" : this.state.selectedAdmin.type === 1 ? "관리자" : "알바생"}</Text>
                <Text style={{
                    fontSize: 20,
                    fontWeight: 'bold',
                    marginBottom: 6
                }}>{this.state.selectedAdmin === null ? "" : this.state.selectedAdmin.name}</Text>
                <Text style={{
                    marginBottom: 25,
                    fontSize: 16,
                    lineHeight: 16,
                }}>{this.state.selectedAdmin === null ? "" : this.phoneNumberString(this.state.selectedAdmin.phone)}</Text>
                <TouchableHighlight underlayColor={'transparent'} onPress={() => {
                    this.setState({admin_popup: 0});
                    Actions.admin_add_admin({
                        shop: this.state.shop,
                        admin: this.state.selectedAdmin
                    });
                }}>
                    <Text style={[styles.c_modal_button, {backgroundColor: '#ffffff'}]}>관리자 정보 수정</Text>
                </TouchableHighlight>
                <TouchableHighlight underlayColor={'transparent'}
                                    onPress={() => this.onPressDeleteAdmin(this.state.selectedAdmin)}>
                    <Text style={[styles.c_modal_button, {
                        backgroundColor: '#ffffff',
                        marginTop: 1,
                        color: '#d0021b'
                    }]}>선택한 관리자 삭제</Text>
                </TouchableHighlight>
            </View>
        </View>;
    }

    render() {
        return (
            <View style={styles.container}>
                {this.adminmodel()}
                {this.topbar()}
                {this.topmenu()}
                {this.shopInfo()}
                {this.admin()}
                {this.certNum()}
            </View>
        )
    }
}

export default admin_main;