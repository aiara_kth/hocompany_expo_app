import React from 'react';
import {Text, View, StyleSheet, Image,TouchableHighlight, Dimensions, ScrollView, Menu} from 'react-native';
import { Actions } from 'react-native-router-flux';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import {Ionicons} from '@expo/vector-icons';
import Global from "../../../objects/Global";
import API from "../../../objects/API";
var commaNumber = require('comma-number');

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f9f9f9',
    },
    Navigator : {
        flexDirection: 'row',
        paddingTop : getStatusBarHeight(),
        width: Dimensions.get('window').width,
        height: 48 + getStatusBarHeight(),
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    Navigator_menu: {
        width: 80,
        height: 48,
    },
    Navigator_menu_button :{
        width: 48,
        height: 48,
        paddingTop: 12,
        paddingLeft: 12,
    },
    Navigator_menu_image : {
        width: 24,
        height: 24
    },
    Navigator_title:{
        width : ((Dimensions.get('window').width) - 160),
        alignItems: 'center',
    },
    Navigator_title_text:{
        fontSize: 16,
        color:'#000000'
    },
    Navigator_start:{
        alignItems: 'flex-end',
        width: 80,
        height: 48,
        paddingTop: 12,
        paddingRight: 12,
    },
    Navigator_start_text:{
        fontSize: 16,
        color:'#7db840',
    },
    sub_Navigator : {
        flexDirection: 'row',
        width: Dimensions.get('window').width,
        height: 40,
        backgroundColor: '#fff',
        alignItems: 'center',
    },
    sub_Navigator_menu: {
        width: 85,
        height: 40,
        alignItems: 'flex-end',
        justifyContent: 'center',
        marginLeft: 15,
        marginRight: 15
    },
    sub_Navigator_menu_on: {
        width: 65,
        height: 40,
        alignItems: 'center',
        justifyContent: 'center',
        borderBottomWidth: 2,
        borderBottomColor: '#000000',
    },
    sub_Navigator_menu_off: {
        width: 65,
        height: 40,
        alignItems: 'center',
        justifyContent: 'center',
        borderBottomWidth: 2,
        borderBottomColor: 'transparent',
    },
    sub_Navigator_name_on : {
        fontSize : 14,
        fontWeight: 'bold',
        alignItems: 'center',
    },
    sub_Navigator_name_off : {
        fontSize : 14,
        alignItems: 'center',
    },
    title_ws : {
        width: (Dimensions.get('window').width ),
        fontSize: 12,
        height: 24,
        color: '#7f8fd1'
    },
    contents : {
        // paddingTop :  9,
        width: Dimensions.get('window').width,
        // backgroundColor: '#ffffff',
        height : Dimensions.get('window').height - (96 + getStatusBarHeight()),
        alignItems: 'center',
        justifyContent: 'center',
    },
    contents_line :{
        width: Dimensions.get('window').width - 30,
        marginLeft: 15,
        height: 1,
        backgroundColor: '#eef0f2',
    },

    contents_list : {
        width: (Dimensions.get('window').width ),
        marginTop : 8,
    },

    header_1 : {
        width: (Dimensions.get('window').width ),
        height: 128,
        backgroundColor: '#ffffff'
    },
    title: {
        paddingLeft: 13,
        width: (Dimensions.get('window').width ),
        height: 128,
        justifyContent: 'center',
        paddingRight:56
    },
    title_w : {
        fontSize: 32,
        height: 48,
        width: (Dimensions.get('window').width ),
        color: '#000000',
    },
    item_menu : {
        // marginTop: 9,
        width: (Dimensions.get('window').width ),
        flexDirection: 'row',
        backgroundColor:'#ffffff'
    },
    item_menu_text : {
        width: (Dimensions.get('window').width - 116),
        paddingLeft: 12,
        paddingTop : 16,
    },
    item_menu_text_name : {
        paddingTop: 8,
        fontSize: 16,
        color: '#000000',

    },
    item_menu_text_info : {
        paddingTop: 8,
        fontSize: 14,
        color: '#606060'
    },
    item_menu_text_price : {
        paddingTop: 8,
        fontSize: 10,
        fontWeight: '500',
        color: '#606060'
    },
    item_menu_image_box : {
        width: 116,
        paddingTop: 16,
        paddingBottom: 16,
        paddingLeft: 8,
        paddingRight: 8
    },
    item_menu_image : {
        width: 96,
        height: 96,
        resizeMode : 'contain'
    },

    add_admin_buttom_group : {
        flexDirection: 'row',
        width: Dimensions.get('window').width,
        height:48,
        alignItems: 'center',
        justifyContent: 'center',
    },

    add_admin_button : {
        width: 100,
        height: 32,
        borderRadius: 12,
        borderColor: '#eb5847',
        borderWidth: 1,
        // paddingTop : 7,
        //  marginTop: 10,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    add_admin_button_text : {
        paddingTop: 8,
        fontSize: 12,
        height: 32,
        color: '#eb5847'
    },
    c_modal : {
        zIndex: 9999,
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height - getStatusBarHeight(),
        flexDirection: 'row',
        position: 'absolute',
        left: 0,
        top: getStatusBarHeight()
    },


});

class admin_add_memu extends  React.Component {
    constructor(props) {
        super(props);
        this.state = {
            show_modal: false,
            menu: this.props.shop !== undefined && this.props.shop !== null ? this.props.shop.menu : []
        };
    }

    componentDidMount() {
        // todo 가게 정보 데이터에서 menus로 this.state.menu 변경
    }

    printMenu(menu) {
        return <View key={menu.name}>
            <View style={styles.item_menu}>
                <View style={styles.item_menu_text}>
                    <Text style={styles.item_menu_text_name}>{menu.name}</Text>
                    <Text style={styles.item_menu_text_info}>{menu.description}</Text>
                    <Text style={styles.item_menu_text_price}>{commaNumber(menu.price)}원</Text>
                </View>
                <View style={styles.item_menu_image_box}>
                    <Image style={styles.item_menu_image}
                           source={menu.photo}/>
                </View>
            </View>
            <View style={styles.contents_line}></View>
        </View>
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.Navigator}>
                    <View style={styles.Navigator_menu}>
                        <TouchableHighlight style={styles.Navigator_menu_button} underlayColor={'transparent'}
                                            onPress={() => Actions.pop()}>
                            <Image style={styles.Navigator_menu_image}
                                   source={require("../../../assets/back_arrow.png")}/>
                        </TouchableHighlight>
                    </View>
                    <View style={styles.Navigator_title}><Text style={styles.Navigator_title_text}> </Text></View>
                    <TouchableHighlight underlayColor={'transparent'} onPress={() => Actions.pop()}>
                        <View style={styles.Navigator_start}><Text style={styles.Navigator_start_text}>완료</Text></View>
                    </TouchableHighlight>
                </View>
                <View style={styles.contents}>
                    <View style={styles.header_1}>
                        <View style={styles.title}>
                            <Text style={styles.title_w}>메뉴등록</Text>
                            <Text style={styles.title_ws}>점포에 대한 메뉴를 입력해주세요.</Text>
                            <Text style={styles.title_ws}>사진도 함꼐 업로드하는 경우가 고객유입율이 더 높아집니다.</Text>
                        </View>
                    </View>
                    <ScrollView>
                        <View style={styles.contents_list}>
                            {
                                this.state.menu.map(menu => this.printMenu(menu))
                            }
                        </View>
                    </ScrollView>
                </View>
                <TouchableHighlight underlayColor={'transparent'} onPress={() => Actions.admin_add_menu_proc({shop: this.props.shop})}>
                    <View style={styles.add_admin_buttom_group}>
                        <View style={styles.add_admin_button}>
                            <Text style={styles.add_admin_button_text}>메뉴 추가</Text>
                        </View>
                    </View>
                </TouchableHighlight>
            </View>
        )
    }
}

export default admin_add_memu;