import React from 'react';
import {Text, View, StyleSheet, Image,TouchableHighlight, Dimensions, Alert ,AsyncStorage} from 'react-native';
import { Actions } from 'react-native-router-flux';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import {Ionicons} from '@expo/vector-icons';
import email from 'react-native-email'
import '../../../objects/Global';
import API from "../../../objects/API";
import Global from "../../../objects/Global";

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f9f9f9',
    },
    Navigator : {
        flexDirection: 'row',
        marginTop : getStatusBarHeight(),
        width: Dimensions.get('window').width,
        height: 48,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    Navigator_menu: {
        width: 80,
        height: 48,
    },
    Navigator_menu_button :{
        width: 48,
        height: 48,
        paddingTop: 12,
        paddingLeft: 12,
    },
    Navigator_menu_image : {
        width: 24,
        height: 24
    },
    Navigator_title:{
        width : ((Dimensions.get('window').width) - 160),
        alignItems: 'center',
    },
    Navigator_title_text:{
        fontSize: 20,
        color:'#000000'
    },
    Navigator_start:{
        alignItems: 'flex-end',
        width: 80,
        height: 48,
        paddingRight: 12,
    },
    Navigator_start_text:{
        fontSize: 16,
        color:'#7db840',
        marginTop: 12,
        marginBottom: 12
    },
    sub_Navigator : {
        flexDirection: 'row',
        width: Dimensions.get('window').width,
        height: 40,
        backgroundColor: '#fff',
        alignItems: 'center',
    },
    sub_Navigator_menu: {
        width: 60,
        height: 40,
        alignItems: 'flex-end',
        justifyContent: 'center',
    },
    sub_Navigator_menu_on: {
        width: 40,
        height: 40,
        alignItems: 'center',
        justifyContent: 'center',
        borderBottomWidth: 2,
        borderBottomColor: '#000000',
    },
    sub_Navigator_menu_off: {
        width: 40,
        height: 40,
        alignItems: 'center',
        justifyContent: 'center',
        borderBottomWidth: 2,
        borderBottomColor: 'transparent',
    },
    sub_Navigator_name_on : {
        fontSize : 14,
        fontWeight: 'bold',
    },
    sub_Navigator_name_off : {
        fontSize : 14,
    },
    contents : {
        paddingTop :  9,
    },
    top_count : {
        width: Dimensions.get('window').width,
        height: 48,
        backgroundColor : '#30cbba',
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: 8
    },
    top_count_text : {
        fontSize: 16,
        color: '#fff'
    },
    contents_item : {
        width: Dimensions.get('window').width,
        height: 48,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    contents_item_sub : {
        flexDirection: 'row',
        width: Dimensions.get('window').width,
        backgroundColor: '#fff',
        marginBottom: 8,
        paddingLeft: 10,
        paddingRight: 10,
        alignItems: 'center',
        justifyContent: 'center',
    },
    item_date : {
        fontSize: 16,
        color: '#838383',
        fontWeight: '600',
        height: 20
    },
    item_group : {
        paddingLeft: 12,
        flex: 1
    },
    item_text: {
        marginTop: 12,
        fontSize: 14,
        height: 20
    },
    item_text_g: {
        marginTop: 12,
        fontSize: 14,
        height: 20,
        color: 'rgba(0,0,0,0.4)'
    },
    item_switch_top : {
      marginTop: 2
    },
    item_switch : {
        width: 48,
        height: 24
    },
    item_image_group : {
        marginLeft: 12,
        width: 24,
        height: 24
    },
    item_image : {
        width: 24,
        height: 24
    },
    version_text : {
        marginTop: 2,
        marginRight: 5
    }
});

const ON = "on";
const OFF = "off";

class Manager_noti extends  React.Component {
    constructor(props) {
        super(props);
        this.state = {menu: 1, push_enable: ON};
        this.getKey("push_enable", true);
    }

    async getKey(id, save) {
        try {
            const value = await AsyncStorage.getItem('@' + id);
            console.log("get" + id + ">>>" + value);
            if (save) {
                this.setState({[id]: value})
            }
        } catch (error) {
            console.log("Error retrieving data" + error);
        }
    }

    async saveKey(id, value, save) {
        try {
            await AsyncStorage.setItem('@' + id, value);
            if (save) {
                this.setState({[id]: value})
            }
        } catch (error) {
            console.log("Error saving data" + data);
        }
    }

    async resetKey(id) {
        try {
            await AsyncStorage.removeItem('@' + id);
            const value = await AsyncStorage.getItem('@' + id);
            return value;
        } catch (error) {
            console.log("Error resetting data" + error);
        }
    }

    togglePushEnable() {
        this.saveKey('push_enable', this.state.push_enable === ON ? OFF : ON, true);
    }

    user_delete() {
        Alert.alert(
            '회원탈퇴',
            '회원탈퇴하시겠습니까?',
            [
                {text: "취소", onPress: () => null, styles: 'cancel'},
                {text: "확인", onPress: () => this.user_delete_start()}
            ]
        );
    }

    user_delete_start() {
        API.user.delete(data => {
            // AsyncStorage.setItem("@ShopListData",data);
            Global.user = null;
            AsyncStorage.removeItem('@user');
            Alert.alert(
                '회원탈퇴',
                '회원탈퇴 되었습니다.',
                [
                    {text: "확인", onPress: () => (Global.user === null ? Actions.Login() : Actions.admin_main())}
                ]
            );
        }, error => {
            Alert.alert('회원탈퇴', '서버문제로 회원탈퇴가 실패하였습니다. \n' + error, [{text: "확인", onPress: () => null}]);
        });
    }

    onPressSupportMail() {
        // 메일 앱 열어서 메일 보내게 하는 함수
        email('support@jaritso.com', { // to 메일 주소
            subject: '[자리있소 문의] 제목을 입력해 주세요.', // default 메일 제목
            body: '문의 내용을 입력해 주세요.' // default 메일 내용
        }).catch(console.error);
    }

    onPressLogout() {
        Global.user = null;
        AsyncStorage.removeItem('@user');
        Alert.alert(
            '로그아웃',
            '로그아웃 되었습니다.',
            [
                {text: "확인", onPress: () => (Global.user === null ? Actions.Login() : Actions.admin_main())}
            ]
        );
    }

    render() {

        return (
            <View style={styles.container}>
                <View style={styles.Navigator}>
                    <View style={styles.Navigator_menu}>
                        <TouchableHighlight style={styles.Navigator_menu_button} underlayColor={'transparent'}
                                            onPress={() => Actions.pop()}>
                            <Image style={styles.Navigator_menu_image}
                                   source={require("../../../assets/back_arrow.png")}/>
                        </TouchableHighlight>
                    </View>
                    <View style={styles.Navigator_title}><Text style={styles.Navigator_title_text}>설정</Text></View>
                    <TouchableHighlight underlayColor={'transparent'} onPress={() => null}>
                        <View style={styles.Navigator_start}><Text style={styles.Navigator_start_text}></Text></View>
                    </TouchableHighlight>
                </View>
                <View style={{marginTop: 9}}>
                    <TouchableHighlight underlayColor={'transparent'} onPress={() => this.togglePushEnable()}>
                        <View style={styles.contents_item}>
                            <View style={styles.contents_item_sub}>
                                <View style={styles.item_group}>
                                    <Text style={styles.item_text}>푸쉬 알림 설정</Text>
                                </View>
                                <View style={styles.item_switch_top}>
                                    <Image style={styles.item_switch} source={this.state.push_enable === ON ? require("../../../assets/common/switch_on.png") : require("../../../assets/common/switch_off.png")}/>
                                </View>
                            </View>
                        </View>
                    </TouchableHighlight>
                </View>
                <View style={{marginTop: 8}}>
                    <TouchableHighlight underlayColor={'transparent'} onPress={() => Actions.Manager_policy()}>
                        <View style={styles.contents_item}>
                            <View style={styles.contents_item_sub}>
                                <View style={styles.item_group}>
                                    <Text style={styles.item_text}>약관 및 정책</Text>
                                </View>
                            </View>
                        </View>
                    </TouchableHighlight>
                    <View style={styles.contents_item}>
                        <View style={styles.contents_item_sub}>
                            <View style={styles.item_group}>
                                <Text style={styles.item_text}>버전 정보</Text>
                            </View>
                            <View style={styles.version_text}>
                                <Text>{Global.app_version}</Text>
                            </View>
                        </View>
                    </View>
                    <TouchableHighlight underlayColor={'transparent'} onPress={() => this.onPressSupportMail()}>
                        <View style={styles.contents_item}>
                            <View style={styles.contents_item_sub}>
                                <View style={styles.item_group}>
                                    <Text style={styles.item_text}>문의 메일 보내기</Text>
                                </View>
                            </View>
                        </View>
                    </TouchableHighlight>
                </View>
                <View style={{marginTop: 8}}>
                    <TouchableHighlight underlayColor={'transparent'} onPress={() => this.onPressLogout()}>
                        <View style={styles.contents_item}>
                            <View style={styles.contents_item_sub}>
                                <View style={styles.item_group}>
                                    <Text style={styles.item_text_g}>로그아웃</Text>
                                </View>
                            </View>
                        </View>
                    </TouchableHighlight>
                    <TouchableHighlight underlayColor={'transparent'} onPress={() => this.user_delete()}>
                        <View style={styles.contents_item}>
                            <View style={styles.contents_item_sub}>
                                <View style={styles.item_group}>
                                    <Text style={styles.item_text_g}>회원 탈퇴</Text>
                                </View>
                            </View>
                        </View>
                    </TouchableHighlight>
                </View>
            </View>
        )
    }
}

export default Manager_noti;