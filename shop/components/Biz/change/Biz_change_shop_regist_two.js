import React from 'react';
import {Text, View, StyleSheet, Image,TextInput,TouchableHighlight, Dimensions, Alert } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { Actions } from 'react-native-router-flux';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import ActionButton from 'react-native-action-button';
import {Ionicons} from '@expo/vector-icons';
import Global from "../../../objects/Global";

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    Navigator : {
        marginTop : getStatusBarHeight(),
        width: '100%',
        height: 48,
        backgroundColor: '#fff',
    },
    back_button : {
        width:  48,
        height: 48,
        paddingTop: 12,
        paddingLeft: 12
    },
    back_button_b : {
        width: 24,
        height: 24
    },
    header : {
        marginTop: 48,
        width: '100%',
        height: 50
    },
    title: {
        paddingLeft: 13,
        width:'100%',
        height:'18%',
        justifyContent: 'center',
    },
    title_w : {
        fontSize: 32,
        height: 48,
        color: '#000000',
    },
    title_ws : {
        width: 290,
        fontSize: 14,
        height: 48,
        color: '#eb5847'
    },
    contents: {
        width: Dimensions.get('window').width,
        height: 300,
        alignItems: 'center',
        justifyContent: 'center',
    },
    input_form : {
        width: 320,
        justifyContent: 'center',
        alignItems: 'center',
    },
    input_box : {
        width: 320,
        height:56,
        borderRadius: 2,
        backgroundColor: '#fafbfb',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection:'row',
        marginBottom:20
    },
    input_dom : {
        marginLeft : 10,
        width: 300
    }

});


class Biz_change_shop_regist_two extends  React.Component {
    constructor(props) {
        super(props);
        this.state = {
            bizphone: '',
            bizaddress: ''
        };
    }

    next() {
        if (this.state.bizphone === '') {
            Alert.alert('점포등록', '매장 전화번호를 입력해주세요.', [{text: "확인", onPress: () => this.refs.bizphone_input.focus()}]);
            return;
        }

        if (this.state.bizaddress === '') {
            Alert.alert('점포등록', '매장 주소를 입력해주세요.', [{text: "확인", onPress: () => this.refs.bizaddress_input.focus()}])
            return;
        }

        Actions.Biz_change_shop_regist_three({
            bizname: this.props.bizname,
            biznumber: this.props.biznumber,
            bizphone: this.state.bizphone,
            bizaddress: this.state.bizaddress
        });

    }

    render() {
        return (
            <View style={styles.container} behavior="padding" enabled>
                <View style={styles.Navigator}>
                    <TouchableHighlight underlayColor={'transparent'} onPress={() => Actions.pop()}>
                        <View style={styles.back_button}>
                            <Image style={styles.back_button_b} source={require("../../../assets/back_arrow.png")}/>
                        </View>
                    </TouchableHighlight>
                </View>
                <KeyboardAwareScrollView>
                    <View style={styles.header}>
                        <View style={styles.title}>
                            <Text style={styles.title_w}>주소와 전화번호</Text>
                            <Text style={styles.title_ws}>매장의 주소와 손님이 바로 전화할 수 있는 전화번호를 입력해주세요.</Text>
                        </View>
                    </View>
                    <View style={styles.contents}>
                        <View style={styles.input_form}>
                            <View style={styles.input_box}>
                                <TextInput ref="bizphone_input" style={styles.input_dom} placeholder={'전화번호'}
                                           keyboardType='phone-pad' underlineColorAndroid='transparent'
                                           retrunKeyType="done"
                                           onChangeText={(text) => this.setState({bizphone: text})}/>
                            </View>
                            <View style={styles.input_box}>
                                <TextInput ref="bizaddress_input" style={styles.input_dom} placeholder={'주소'}
                                           underlineColorAndroid='transparent' retrunKeyType="done"
                                           onChangeText={(text) => this.setState({bizaddress: text})}/>
                            </View>
                        </View>
                    </View>
                </KeyboardAwareScrollView>
                <ActionButton hideShadow={false}
                              buttonColor={this.state.bizphone === '' || this.state.bizaddress === '' ? '#838383' : '#eb5847'}
                              renderIcon={(active) => (active ? (
                                  <Ionicons name='ios-arrow-forward' size={20} color={'#FFFFFF'}/>) : (
                                  <Ionicons name='ios-arrow-forward' size={20} color={'#FFFFFF'}/>))}
                              onPress={() => this.next()}>
                </ActionButton>
            </View>
        )
    }
}

export default Biz_change_shop_regist_two;
