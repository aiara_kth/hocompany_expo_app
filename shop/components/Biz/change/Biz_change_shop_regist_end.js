import React from 'react';
import {Text, View, StyleSheet, Image,TouchableHighlight, Dimensions, } from 'react-native';
import { Actions } from 'react-native-router-flux';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import {Ionicons} from '@expo/vector-icons';
import Shop from '../../../objects/Shop';
import Global from "../../../objects/Global";

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    Navigator : {
        marginTop : getStatusBarHeight(),
        width: '100%',
        height: 48,
        backgroundColor: '#fff',
    },
    back_button : {
        width:  48,
        height: 48,
        paddingTop: 12,
        paddingLeft: 12
    },
    back_button_b : {
        width: 24,
        height: 24
    },
    header : {
        width: '100%',
        alignItems: 'center',
    },
    biz_title_regist_end : {
        alignItems: 'center',
    },
    biz_title_regist_end_ts : {
        fontSize: 24,
        color: '#eb5847',
        alignItems: 'center',
    },
    biz_register_end_check : {
        marginTop : (Dimensions.get('window').height / 8),
        width: 80,
        height: 80
    }, go_home_buttom : {
        position: 'absolute',
        bottom:0,
        left:0,
        width: (Dimensions.get('window').width),
        height: 56,
        backgroundColor: '#eb5847',
        alignItems: 'center',
        justifyContent: 'center',
    }, go_home_buttom_text : {
        fontSize: 16,
        color:'#FFFFFF'
    },

    biz_register_end_word : {
        marginTop : (Dimensions.get('window').height / 16),
        alignItems: 'center',
    },
    biz_register_end_word_ts :{
        fontSize: 14,
        marginBottom: 5
    }

});

class Login_regist_step_end extends  React.Component {
    constructor(props) {
        super(props);
        Global.user.shops.push(new Shop(
            this.props.shopId,
            this.props.bizname,
            this.props.biznumber,
            this.props.bizphone,
            this.props.bizaddress,
            this.props.authnum
        ));
    }

    render() {
        return (
            <View style={styles.container} behavior="padding" enabled>
                <View style={styles.Navigator}>
                    <TouchableHighlight underlayColor={'transparent'} onPress={() => Actions.pop()}>
                        <View style={styles.back_button}>
                            <Image style={styles.back_button_b} source={require("../../../assets/back_arrow.png")}/>
                        </View>
                    </TouchableHighlight>
                </View>
                <View style={styles.header}>
                    <View style={styles.biz_title_regist_end}>
                        <Image style={styles.biz_register_end_check}
                               source={require("../../../assets/register_end_red.png")}/>
                        <Text style={styles.biz_title_regist_end_ts}>점포등록이</Text>
                        <Text style={styles.biz_title_regist_end_ts}>완료되었습니다.</Text>
                    </View>
                    <View style={styles.biz_register_end_word}>
                        <Text style={styles.biz_register_end_word_ts}>이제 점포의 메뉴 사진등을 넣어서</Text>
                        <Text style={styles.biz_register_end_word_ts}>고객들이 방문할 수 있게 정보를 입력해주세요.</Text>
                    </View>
                </View>
                <View style={styles.go_home_buttom}>
                    <Text style={styles.go_home_buttom_text} onPress={() => Actions.Biz_change_shop()}>확인</Text>
                </View>
            </View>
        )
    }
}

export default Login_regist_step_end;
