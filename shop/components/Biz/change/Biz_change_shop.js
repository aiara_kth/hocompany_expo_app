import React from 'react';
import {Text, View, StyleSheet, Image, TouchableHighlight, Dimensions, AsyncStorage, ScrollView} from 'react-native';
import Triangle from 'react-native-triangle';
import { Actions } from 'react-native-router-flux';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import ActionButton from 'react-native-action-button';
import {Ionicons} from '@expo/vector-icons';
import '../../../objects/Global';
import Global from "../../../objects/Global";
import API from "../../../objects/API";

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f9f9f9',
    },
    Navigator : {
        flexDirection: 'row',
        marginTop : getStatusBarHeight(),
        width: Dimensions.get('window').width,
        height: 48,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    Navigator_menu: {
        width: 80,
        height: 48,
    },
    Navigator_menu_button :{
        width: 48,
        height: 48,
        paddingTop: 12,
        paddingLeft: 12,
    },
    Navigator_menu_image : {
        width: 24,
        height: 24
    },
    Navigator_title:{
        width : ((Dimensions.get('window').width) - 160),
        alignItems: 'center',
    },
    Navigator_title_text:{
        fontSize: 16,
        color:'#000000'
    },
    Navigator_start:{
        alignItems: 'flex-end',
        width: 80,
        height: 48,
        paddingTop: 12,
        paddingRight: 12,
    },
    Navigator_start_text:{
        fontSize: 16,
        color:'#7db840',
    },
    callout_box : {
        width : 195,
        height: 40,
        borderRadius: 2,
        backgroundColor: '#7f8fd1',
        alignItems: 'center',
        justifyContent: 'center',
        shadowOffset: {width: 2, height: 10 },
        shadowColor: 'rgba(0, 0, 0, 0.2)',
        marginTop: -10
    },
    callout_box_text : {
        fontSize : 12,
        color: '#ffffff',
        textAlign: 'center'
    },
    center_button:{
        width: Dimensions.get('window').width,
        height: ((Dimensions.get('window').height) - 48),
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1
    },
    buttom_button:{
        position: 'absolute',
        marginBottom : ((Dimensions.get('window').height /  2) - 120)
    },
    add_button:{
        width: Dimensions.get('window').width,
        height: 56,
    },
    add_button_text : {
        position: 'absolute',
        left: ((Dimensions.get('window').width / 2) - '2%'),
        top : ((Dimensions.get('window').height /  2) + 30)
    },
    c_modal : {
        zIndex: 9999,
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height - getStatusBarHeight(),
        flexDirection: 'row',
        position: 'absolute',
        left: 0,
        top: getStatusBarHeight()
    },
    c_modal_sidebar : {
        width: 256,
        height: Dimensions.get('window').height,
    },
    c_modal_shadow : {
        width: Dimensions.get('window').width - 256,
        height: Dimensions.get('window').height - getStatusBarHeight(),
        backgroundColor: 'rgba(0, 0, 0, 0.4)'
    },
    c_modal_user : {
        width: 256,
        height: 208,
        paddingTop: 48,
        paddingLeft: 22,
        backgroundColor: '#7f8fd1'
    },
    c_modal_user_images : {
        width: 48,
        height: 48,
        marginLeft: 12
    },
    c_modal_user_name : {
        marginTop: 16,
        fontSize: 20,
        color: "#ffffff"
    },
    c_modal_user_email : {
        fontSize: 12,
        color: "rgba(255,255,255,0.4)"
    },
    c_modal_button_group : {
        width: 256,
        height: Dimensions.get('window').height - (getStatusBarHeight()+208)
    },
    c_modal_button : {
        width: 256,
        height: 48,
        paddingLeft: 12,
        paddingTop: 12,
        fontSize: 16
    },

});


class Biz_change_shop extends  React.Component {
    data = [];

    constructor(props) {
        super(props);
        this.state = {
            text: '',
            biz_shop_data: null,
            sidemenu: 0,
            profileImage: Global.user !== null && Global.user.profilePhotoUrl !== undefined && Global.user.profilePhotoUrl !== null ? Global.user.profilePhotoUrl : require("../../../assets/profile_dummy.png"),
            userName: Global.user !== null ? Global.user.username : "홍길동",
            userEmail: Global.user !== null ? Global.user.email : "hong@jaritso.com"
        };
        this.data = JSON.parse(AsyncStorage.getItem("@ShopListData"));
    }

    render_shoplist() {
        return this.data.map(shop => {
            this.setState({[shop.shopID]: false});
        }, (
            <TouchableHighlight underlayColor={'transparent'}
                                onPress={() => this.setState({[shop.shopID]: !this.state.shop.shopID})}>
                <View style={[styles.biz_item, this.state.checkbox2 ? styles.biz_item_on : {}]}>
                    <View style={styles.biz_item_title}>
                        <Text
                            style={[styles.biz_item_title_main, this.state.shop.shopID ? styles.bold : {}]}>{shop.shopName}</Text>
                        <Text style={[styles.biz_item_title_main_sub, this.state.shop.shopID ? {} : {color: '#fff'}]}>해당
                            레스토랑으로 활성화되었습니다.</Text>
                    </View>
                    <View style={styles.biz_item_checkbox_bind}>
                        <Image style={styles.biz_item_checkbox}
                               source={this.state.checkbox2 ? require("../../../assets/common/checkbox_checked.png") : require("../../../assets/common/checkbox_uncheck.png")}/>
                    </View>
                </View>
            </TouchableHighlight>
        ));
    }

    render() {
        return (
            <View style={styles.container}>
                {/*<Biz_side_menu style={[styles.c_modal,this.state.sidemenu == 1 ? {} : {display: 'none', position: 'relative']}*/}
                <View
                    style={[styles.c_modal, this.state.sidemenu === 1 ? {} : {display: 'none', position: 'relative'}]}>
                    <View style={styles.c_modal_sidebar}>
                        <View style={styles.c_modal_user}>
                            <Image style={styles.c_modal_user_images}
                                   source={this.state.profileImage}/>
                            <Text style={styles.c_modal_user_name}>{this.state.userName} 점주님</Text>
                            <Text style={styles.c_modal_user_email}>{this.state.userEmail}</Text>
                        </View>
                        <View style={styles.c_modal_button_group}>
                            <Text style={styles.c_modal_button} onPress={() => Actions.Manager_user_info()}>회원정보</Text>
                            <Text style={styles.c_modal_button} onPress={() => Actions.Manager_settting()}>설정</Text>
                            <Text style={styles.c_modal_button} onPress={() => Actions.Manager_noti()}>알림</Text>
                            <Text style={styles.c_modal_button} onPress={() => Actions.Manager_notice()}>공지사항</Text>
                            <Text style={styles.c_modal_button} onPress={() => Actions.Manager_guide()}>사용방법</Text>
                            <Text style={styles.c_modal_button} onPress={() => Actions.pop()}>뒤로가기</Text>
                        </View>
                    </View>
                    <TouchableHighlight underlayColor={'transparent'} onPress={() => this.setState({sidemenu: 0})}>
                        <View style={styles.c_modal_shadow}>

                        </View>
                    </TouchableHighlight>
                </View>
                <View style={styles.Navigator}>
                    <View style={styles.Navigator_menu}>
                        <TouchableHighlight style={styles.Navigator_menu_button} underlayColor={'transparent'}
                                            onPress={() => this.setState({sidemenu: 1})}>
                            <Image style={styles.Navigator_menu_image} source={require("../../../assets/menu.png")}/>
                        </TouchableHighlight>
                    </View>
                    <View style={styles.Navigator_title}><Text style={styles.Navigator_title_text}>점포변경</Text></View>
                    <TouchableHighlight underlayColor={'transparent'} onPress={() => null}>
                        <View style={styles.Navigator_start}><Text
                            style={styles.Navigator_start_text}>시작하기</Text></View>
                    </TouchableHighlight>
                </View>
                {
                    this.data.length === 0
                        ? (
                            <View style={[styles.center_button, this.state.sidemenu === 0 ? {} : {
                                display: 'none',
                                position: 'relative'
                            }]}>
                                <View style={styles.callout_box}>
                                    <Text style={styles.callout_box_text}>점포 등록하기 버튼를 눌려서</Text>
                                    <Text style={styles.callout_box_text}>첫 점포를 등록해 보세요!</Text>
                                </View>
                                <Triangle style={{marginBottom: 90}} width={18} height={8} color={"#7f8fd1"}
                                          direction={"down"}/>
                                <View style={[styles.add_button_text, this.state.sidemenu === 0 ? {} : {
                                    display: 'none',
                                    position: 'relative'
                                }]}>
                                    <Text style={styles.add_button_text_text}>점포등록하기</Text>
                                </View>
                            </View>
                        )
                        : (
                            <View style={styles.biz_list}>
                                <ScrollView>
                                    {this.render_shoplist()}
                                    <TouchableHighlight underlayColor={'transparent'}
                                                        onPress={() => this.setState({checkbox1: !this.state.checkbox1})}>
                                        <View style={[styles.biz_item, this.state.checkbox1 ? styles.biz_item_on : {}]}>
                                            <View style={styles.biz_item_title}>
                                                <Text
                                                    style={[styles.biz_item_title_main, this.state.checkbox1 ? styles.bold : {}]}>레스토랑
                                                    명 입력자리</Text>
                                                <Text
                                                    style={[styles.biz_item_title_main_sub, this.state.checkbox1 ? {} : {color: '#fff'}]}>해당
                                                    레스토랑으로 활성화되었습니다.</Text>
                                            </View>
                                            <View style={styles.biz_item_checkbox_bind}>
                                                <TouchableHighlight underlayColor={'transparent'}
                                                                    onPress={() => this.setState({checkbox1: !this.state.checkbox1})}>
                                                    <Image style={styles.biz_item_checkbox}
                                                           source={this.state.checkbox1 ? require("../../../assets/common/checkbox_checked.png") : require("../../../assets/common/checkbox_uncheck.png")}/>
                                                </TouchableHighlight>
                                            </View>
                                        </View>
                                    </TouchableHighlight>
                                    <TouchableHighlight underlayColor={'transparent'}
                                                        onPress={() => this.setState({checkbox2: !this.state.checkbox2})}>
                                        <View style={[styles.biz_item, this.state.checkbox2 ? styles.biz_item_on : {}]}>
                                            <View style={styles.biz_item_title}>
                                                <Text
                                                    style={[styles.biz_item_title_main, this.state.checkbox2 ? styles.bold : {}]}>레스토랑
                                                    명 입력자리</Text>
                                                <Text
                                                    style={[styles.biz_item_title_main_sub, this.state.checkbox2 ? {} : {color: '#fff'}]}>해당
                                                    레스토랑으로 활성화되었습니다.</Text>
                                            </View>
                                            <View style={styles.biz_item_checkbox_bind}>
                                                <Image style={styles.biz_item_checkbox}
                                                       source={this.state.checkbox2 ? require("../../../assets/common/checkbox_checked.png") : require("../../../assets/common/checkbox_uncheck.png")}/>
                                            </View>
                                        </View>
                                    </TouchableHighlight>
                                </ScrollView>
                            </View>
                        )
                }
                <ActionButton hideShadow={false} style={[styles.buttom_button, this.state.sidemenu === 0 ? {} : {
                    display: 'none',
                    position: 'relative'
                }]} position="center" buttonColor={"rgba(235,88,71,1)"}
                              onPress={() => Actions.Biz_change_shop_regist_one()}/>
                {
                    this.data.length === 0 ? (<View></View>)
                        : (
                            <View style={styles.add_button_dom}>
                                <View style={[styles.add_button_text, this.state.sidemenu === 0 ? {} : {
                                    display: 'none',
                                    position: 'relative'
                                }]}>
                                    <Text style={styles.add_button_text_text}>점포등록하기</Text>
                                </View>
                            </View>
                        )
                }
            </View>
        )
    }
}

export default Biz_change_shop;


