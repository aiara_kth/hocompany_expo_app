export default class Location {
    latitude: Number;
    longitude: Number;

    constructor(lat, lng) {
        this.latitude = lat;
        this.longitude = lng;
    }

    set(lat, lng) {
        this.latitude = lat;
        this.longitude = lng;
    }
}