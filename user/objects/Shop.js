import Location from "./Location";
import Menu from "./Menu";
import JRequest from "./JRequest";
import API from "./API";

export default class Shop {
    static ADMIN_RANK = {
        MASTER: "master", MANAGER: "manager", USER: "user"
    };
    static ADMIN_PERMISSION = {
        MASTER: 400, MANAGER: 300, USER: 200
    };

    shopID: Number;
    shopName: String;
    shopAddress: String;
    shopDescription: String;
    shopEvent: String;
    mainProfileUrl: String;
    subProfileUrl: Array = []; // { order: number, url: string }
    location: Location = new Location();
    shopLikes: Number;
    shopBookmarks: Number;
    category: Object = {
        main: "", // string
        sub: "", // string
        tags: "" // string
    };
    operationHours: Array = []; // { days: ["mon", "tue", "wed"], openHours: "20-05" }
    menus: Array<Menu> = [];
    shopAdminNumber: Number;
    jrequests: Array<JRequest> = [];
    shopAdmins: Array = []; // { userID: "", adminRank: ADMIN_RANK_MASTER, permission: ADMIN_PERMISSION_MASTER, pushSettings: true, userFirebaseInstanceToken: "" }

    shopNumber: String = "";
    phoneNumber: String = "";
    authNum: String = "";

    constructor(id, name, number, phone, address, authnum) {
        this.shopID = id;
        this.shopName = name;
        this.shopNumber = number;
        this.phoneNumber = phone;
        this.shopAddress = address;
        this.authNum = authnum;
    }

    getMenu(onSuccess: Function, onError: Function) {
        if (this.menus.length > 0) {
            onSuccess(this.menus);
            return this.menus;
        }
        API.shop.getShopMenu(this.shopID, data => {
            // todo data를 Menu[]로 바꿔서 this.menus에 넣고 리턴
            onSuccess(data);
        }, onError);
    }

    like(likeOn: Boolean, onSuccess: Function, onError: Function) {
        if (likeOn) {
            API.shop.like.put(this.shopID, data => {
                this.shopLikes++;
                onSuccess(data);
            }, onError);
            return;
        }
        API.shop.like.delete(this.shopID, data => {
            this.shopLikes--;
            onSuccess(data);
        }, onError);
    }

    bookmark(bookmarkOn: Boolean, onSuccess: Function, onError: Function) {
        if (bookmarkOn) {
            API.shop.bookmark.put(this.shopID, data => {
                this.shopBookmarks++;
                onSuccess(data);
            }, onError);
            return;
        }
        API.shop.bookmark.delete(this.shopID, data => {
            this.shopBookmarks--;
            onSuccess(data);
        }, onError);
    }

    getJRequests(onSuccess: Function, onError: Function) {
        API.shop.admin.getJRequest(this.shopID, data => {
            // todo data를 jrequest[]로 바꿔서 리턴
            onSuccess(data);
        }, onError);
    }

    getNotification(onSuccess: Function, onError: Function) {
        API.shop.admin.getNotification(this.shopID, onSuccess, onError);
    }

    createMenu(name: String, price: Number, currency: String, menuProfileUrl: String, description: String, data: Object, onSuccess: Function, onError: Function) {
        API.shop.admin.menu.post(this.shopID, name, price, currency, menuProfileUrl, description, data, data => {
            // let menu = new Menu(id, name, description, "", price);
            // todo data에서 menuID 받아서 Menu 인스턴스에 값 전달하고 this.menus에 push 하고 리턴
            // this.menus.push(menu);
            onSuccess(data);
        }, onError);
    }

    getViews(onSuccess: Function, onError: Function) {
        API.archive.shopViews.get(this.shopID, onSuccess, onError);
    }

    postViews(onSuccess: Function, onError: Function) {
        API.archive.shopViews.post(this.shopID, onSuccess, onError);
    }

    createReview(body, onSuccess: Function, onError: Function) {
        API.revision2.review.post(this.shopID, body, onSuccess, onError);
    }

    getReview(onSuccess: Function, onError: Function) {
        API.revision2.review.get(this.shopID, data => {
            // todo data를 Review[]로 바꿔서 리턴
            onSuccess(data);
        }, onError);
    }

    deleteReview(reviewId, onSuccess: Function, onError: Function) {
        API.revision2.review.delete(this.shopID, onSuccess, onError);
    }

    static nearShops(location: Location, onSuccess: Function, onError: Function) {
        API.shop.getNearShops(location.latitude, location.longitude, data => {
            // todo data를 Shop[]로 바꿔서 리턴
            onSuccess(data);
        }, onError);
    }

    static createShop(name: String, authNum: Number, shopNumber: String, address: String, onSuccess: Function, onError: Function) {
        API.shop.admin.post(name, authNum, shopNumber, address, data => {
            // let shop = new Shop();
            // todo data에서 받은 데이터들 shop에 넣어서 리턴
            // shop.shopName = name;
            // shop.authNum = authNum;
            // shop.shopNumber = shopNumber;
            // shop.shopAddress = address;
            onSuccess(data);
        }, onError);
    }

    static getShop(shopId, onSuccess: Function, onError: Function) {
        API.shop.get(shopId, data => {
            // todo data를 Shop 인스턴스로 바꿔서 리턴
            onSuccess(data);
        }, onError);
    }

    static getShopAdmin(shopId, onSuccess: Function, onError: Function) {
        API.shop.admin.get(shopId, data => {
            // todo data를 Shop 인스턴스로 바꿔서 리턴
            onSuccess(data);
        }, onError);
    }

    static getContents(location: Location, geocode, page: Number, onSuccess: Function, onError: Function) {
        API.utility.getContents(location, geocode, page, data => {
            // todo data를 Shop[]로 바꿔서 리턴
            onSuccess(data);
        }, onError);
    }
}