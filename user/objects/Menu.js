import API from "./API";

export default class Menu {
    id: Number;
    name: String;
    description: String;
    background: String;
    price: Number;

    constructor(id: Number, name: String, description: String, background: String, price: Number) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.background = background;
        this.price = price;
    }

    like(likeOn: Boolean, onSuccess: Function, onError: Function) {
        if (likeOn) {
            API.menu.like.post(this.id, onSuccess, onError);
            return;
        }
        API.menu.like.delete(this.id, onSuccess, onError);
    }

    getMenu(menuId, onSuccess: Function, onError: Function) {
        API.archive.menu.get(menuId, onSuccess, onError);
    }

    putMenu(menuId, onSuccess: Function, onError: Function) {
        API.archive.menu.put(menuId, onSuccess, onError);
    }

    deleteMenu(menuId, onSuccess: Function, onError: Function) {
        API.archive.menu.delete(menuId, onSuccess, onError);
    }

    static createMenu(onSuccess: Function, onError: Function) {
        API.archive.menu.post(onSuccess, onError);
    }
}