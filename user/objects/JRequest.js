import API from "./API";
import Location from "./Location";

export default class JRequest {
    jrequestID: String;
    userID: String;
    latitude: Number;
    longitude: Number;
    capacity: Number;
    timestamp: Number;
    preorderMenu: Array<Number> = []; // menuIds

    constructor(jrequestID: String, userID: String, latitude: Number, longitude: Number, capacity: Number, timestamp: Number, preorderMenu: Array<Number>) {
        this.jrequestID = jrequestID;
        this.userID = userID;
        this.latitude = latitude;
        this.longitude = longitude;
        this.capacity = capacity;
        this.timestamp = timestamp;
        this.preorderMenu = preorderMenu;
    }

    saveToServer(onSuccess: Function, onError: Function) {
        // todo operationCode가 뭔지 확인해봐야 함
        // API.jRequest.put(this.jrequestID, operationCode, onSuccess, onError);
    }

    createPreorder(onSuccess: Function, onError: Function) {
        API.preorder.post(this.jrequestID, data => {
            // todo this.preorderMenu에 id값 넣기
            onSuccess(data);
        }, onError);
    }

    getPreorder(onSuccess: Function, onError: Function) {
        if (this.preorderMenu.length > 0) {
            onSuccess(this.preorderMenu);
            return this.preorderMenu;
        }
        API.preorder.get(this.jrequestID, data => {
            // todo this.preorderMenu에 id값 넣기
            onSuccess(data);
        }, onError);
    }

    putPreorder(onSuccess: Function, onError: Function) {
        API.preorder.put(this.jrequestID, data => {
            // todo this.preorderMenu 수정
            onSuccess(data);
        }, onError);
    }

    deletePreorder(onSuccess: Function, onError: Function) {
        API.preorder.delete(this.jrequestID, data => {
            // todo this.preorderMenu 초기화
            onSuccess(data);
        }, onError);
    }

    createWaiting(onSuccess: Function, onError: Function) {
        API.waiting.post(this.jrequestID, onSuccess, onError);
    }

    getWaiting(onSuccess: Function, onError: Function) {
        API.waiting.get(this.jrequestID, onSuccess, onError);
    }

    putWaiting(onSuccess: Function, onError: Function) {
        API.waiting.get(this.jrequestID, onSuccess, onError);
    }

    deleteWaiting(onSuccess: Function, onError: Function) {
        API.waiting.get(this.jrequestID, onSuccess, onError);
    }

    static createJRequest(location: Location, shopIdArray: Array, onSuccess: Function, onError: Function) {
        API.jRequest.post(location, shopIdArray, data => {
            // todo data에서 각 데이터를 뽑아서 JRequest 인스턴스를 리턴
            onSuccess(data);
        }, onError);
    }

    static getJRequest(jRequestId: String, onSuccess: Function, onError: Function) {
        API.jRequest.get(jRequestId, data => {
            // todo data에서 각 데이터를 뽑아서 JRequest 인스턴스를 리턴
            onSuccess(data);
        }, onError);
    }
}