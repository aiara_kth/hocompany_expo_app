import React  from 'react';
import { YellowBox } from 'react-native';
import {Router, Stack, Scene} from 'react-native-router-flux';
import {setCustomText, setCustomTextInput} from 'react-native-global-props';
import Home from './components/home/Home';
import Home_user from './components/home/Home_user';
import Home_one from './components/home/Home_one';
import SNS_Login_Test from './components/SNS_Login_Test';
import dd_rs3 from './components/User_request/User_request_step3';
import Global from "./objects/Global";
import User from './objects/User';

// 회원가입
import Login from './components/login/Login';
import Login_regist_step_one from './components/login/Login_regist_step_one';
import Login_regist_step_two from './components/login/Login_regist_step_two';
import Login_regist_step_three from './components/login/Login_regist_step_three';
import Login_regist_step_end from './components/login/Login_regist_step_end';
//정보변경(점포관련)
import Biz_change_shop from './components/Biz/change/Biz_change_shop';
import Biz_change_shop_regist_one from './components/Biz/change/Biz_change_shop_regist_one';
import Biz_change_shop_regist_two from './components/Biz/change/Biz_change_shop_regist_two';
import Biz_change_shop_regist_three from './components/Biz/change/Biz_change_shop_regist_three';
import Biz_change_shop_regist_end from './components/Biz/change/Biz_change_shop_regist_end';
//예약관리
import Manager_guide from './components/Biz/manager/Manager_guide';
import Manager_main from './components/Biz/manager/Manager_main';
import Manager_sub from './components/Biz/manager/Manager_sub';
import Manager_noti from './components/Biz/manager/Manager_noti';
import Manager_notice from './components/Biz/manager/Manager_notice';
import Manager_settting from './components/Biz/manager/Manager_settting';
import Manager_policy from "./components/Biz/manager/Manager_policy";
import Manager_policy_detail from "./components/Biz/manager/Manager_policy_detail";
import Manager_user_info from './components/Biz/manager/Manager_user_info';
//점포운영관리
import admin_main from './components/Biz/admin/admin_main';
import admin_add_admin from './components/Biz/admin/admin_add_admin';
import admin_add_info from './components/Biz/admin/admin_add_info';
import admin_add_info_edit from './components/Biz/admin/admin_add_info_edit';
import admin_add_menu from './components/Biz/admin/admin_add_memu';
import admin_add_menu_proc from './components/Biz/admin/admin_add_menu_proc';
//회원가입(유저)yarn
import User_Register_Phone_Step1 from './components/User/User_Register_Phone_Step1';
import User_Register_Phone_Step2 from './components/User/User_Register_Phone_Step2';
import User_Register_Phone_Step3 from './components/User/User_Register_Phone_Step3';
import User_Register_Phone_Step4 from './components/User/User_Register_Phone_Step4';
import User_Register_Phone_Step5 from './components/User/User_Register_Phone_Step5';
//메인(유저)
import User_main from './components/User_main/User_main';
import User_my_main from './components/User_main/User_my/User_my_main';
import User_my_star from './components/User_main/User_my/User_my_star';
import User_my_reserved from './components/User_main/User_my/User_my_reserved';
import User_my_review from './components/User_main/User_my/User_my_review';
import User_my_review_view from './components/User_main/User_my/User_my_review_view';
import User_my_review_add from './components/User_main/User_my/User_my_review_add';
import User_my_review_add_end from './components/User_main/User_my/User_my_review_add_end';
import User_my_noti from './components/User_main/User_my/User_my_noti';
import User_my_setting from './components/User_main/User_my/User_my_setting';
import User_my_contents from './components/User_main/User_my/User_my_contents';
//실시간 자리잡기(유져)
import User_request_start from './components/User_request/User_request_start';
import User_request_step1 from './components/User_request/User_request_step1';
import User_request_step2 from './components/User_request/User_request_step2';
import User_request_step3 from './components/User_request/User_request_step3';
import User_request_step4 from './components/User_request/User_request_step4';
import User_request_step5 from './components/User_request/User_request_step5';
import User_request_step6 from './components/User_request/User_request_step6';
import User_request_sec_step1 from './components/User_request/User_request_sec_step1';
import User_request_sec_step2 from './components/User_request/User_request_sec_step2';
import User_request_sec_step3 from './components/User_request/User_request_sec_step3';
import User_request_sec_step4 from './components/User_request/User_request_sec_step4';
import User_request_sec_step5 from './components/User_request/User_request_sec_step5';
import User_request_sec_fail from './components/User_request/User_request_sec_fail';
import User_preorder_list from './components/User_request/User_preorder_list';
import User_preorder_menu from './components/User_request/User_preorder_menu';
import User_preorder_end from './components/User_request/User_preorder_end';
//실시간 자리잡기(one)
import User_request_one_shop_detail from './components/User_request/User_request_one_shop_detail';
import User_request_one_exec from './components/User_request/User_request_one_exec';
import User_request_one_exec_wait from './components/User_request/User_request_one_exec_wait';
import User_request_one_fail from './components/User_request/User_request_one_fail';
import User_request_one_wait from './components/User_request/User_request_one_wait';

const customTextProps = {
    allowFontScaling : false,
    // fontFamily:  Platform.OS == 'ios' ? 'AppleSDGothicNeo-Regular': 'sans-serif',
    style : {
        // fontFamily:  Platform.OS == 'ios' ? 'AppleSDGothicNeo-Regular': 'sans-serif',
        includeFontPadding: false,
        // color: '#f8ea06'
    }
};

const customTextInputProps = {
    allowFontScaling : false,
    // fontFamily:  Platform.OS == 'ios' ? 'AppleSDGothicNeo-Regular': 'sans-serif',
    style: {
        // fontFamily:  Platform.OS == 'ios' ? 'AppleSDGothicNeo-Regular': 'sans-serif',
        includeFontPadding: false,
        // color: '#99d703'
    }
};


setCustomText(customTextProps);
setCustomTextInput(customTextInputProps);

export default class App extends React.Component {

    constructor(props) {
        super(props);
        YellowBox.ignoreWarnings([
            'Class RCTCxxModule',
            'Class EXDisabledRedBox',
            'Class EXDisabledDevMenu',
            'Class EXTest',
            'Class EXHomeModule',
            'Module RCTMFBLoginManager'
        ]);
    }

    async componentDidMount() {
        await User.loadFromLocalStorage();
        if (Global.user !== null) {
            console.log(Global.user);
        }
    }

    render() {
        return (
            <Router>
                <Stack key={"root"}>
                    <Scene key="User_main" component={User_main} initial={true} hideNavBar={true}/>

                    <Scene key="Home" component={Home}/>
                    <Scene key="Home_user" component={Home_user}/>
                    <Scene key="Home_one" component={Home_one}/>
                    <Scene key="Login" component={Login} hideNavBar={true}/>
                    <Scene key="SNS_Login_Test" component={SNS_Login_Test}/>
                    <Scene key="dd_rs3" component={dd_rs3} hideNavBar={true}/>
                    {/*회원가입*/}
                    <Scene key="Login_regist_step_one" component={Login_regist_step_one} hideNavBar={true}/>
                    <Scene key="Login_regist_step_two" component={Login_regist_step_two} hideNavBar={true}/>
                    <Scene key="Login_regist_step_three" component={Login_regist_step_three} hideNavBar={true}/>
                    <Scene key="Login_regist_step_end" component={Login_regist_step_end} hideNavBar={true}/>
                    {/*정보변경(점포관련)*/}
                    <Scene key="Biz_change_shop" component={Biz_change_shop} hideNavBar={true}/>
                    <Scene key="Biz_change_shop_regist_one" component={Biz_change_shop_regist_one} hideNavBar={true}/>
                    <Scene key="Biz_change_shop_regist_two" component={Biz_change_shop_regist_two} hideNavBar={true}/>
                    <Scene key="Biz_change_shop_regist_three" component={Biz_change_shop_regist_three} hideNavBar={true}/>
                    <Scene key="Biz_change_shop_regist_end" component={Biz_change_shop_regist_end} hideNavBar={true}/>
                    {/*예약관리*/}
                    <Scene key="Manager_guide" component={Manager_guide} hideNavBar={true}/>
                    <Scene key="Manager_main" component={Manager_main} hideNavBar={true}/>
                    <Scene key="Manager_sub" component={Manager_sub} hideNavBar={true}/>
                    <Scene key="Manager_noti" component={Manager_noti} hideNavBar={true}/>
                    <Scene key="Manager_notice" component={Manager_notice} hideNavBar={true}/>
                    <Scene key="Manager_settting" component={Manager_settting} hideNavBar={true}/>
                    <Scene key="Manager_policy" component={Manager_policy} hideNavBar={true}/>
                    <Scene key="Manager_policy_detail" component={Manager_policy_detail} hideNavBar={true}/>
                    <Scene key="Manager_user_info" component={Manager_user_info} hideNavBar={true}/>
                    {/*점포운영관리*/}
                    <Scene key="admin_main" component={admin_main} hideNavBar={true}/>
                    <Scene key="admin_add_admin" component={admin_add_admin} hideNavBar={true}/>
                    <Scene key="admin_add_info" component={admin_add_info} hideNavBar={true}/>
                    <Scene key="admin_add_info_edit" component={admin_add_info_edit} hideNavBar={true}/>
                    <Scene key="admin_add_menu" component={admin_add_menu} hideNavBar={true}/>
                    <Scene key="admin_add_menu_proc" component={admin_add_menu_proc} hideNavBar={true}/>
                    {/*회원가입(유저)*/}
                    <Scene key="User_Register_Phone_Step1" component={User_Register_Phone_Step1} hideNavBar={true}/>
                    <Scene key="User_Register_Phone_Step2" component={User_Register_Phone_Step2} hideNavBar={true}/>
                    <Scene key="User_Register_Phone_Step3" component={User_Register_Phone_Step3} hideNavBar={true}/>
                    <Scene key="User_Register_Phone_Step4" component={User_Register_Phone_Step4} hideNavBar={true}/>
                    <Scene key="User_Register_Phone_Step5" component={User_Register_Phone_Step5} hideNavBar={true}/>
                    {/*메인(유저)*/}
                    {/*<Scene key="User_main" component={User_main} hideNavBar={true}/>*/}
                    <Scene key="User_my_main" component={User_my_main} hideNavBar={true}/>
                    <Scene key="User_my_star" component={User_my_star} hideNavBar={true}/>
                    <Scene key="User_my_reserved" component={User_my_reserved} hideNavBar={true}/>
                    <Scene key="User_my_review" component={User_my_review} hideNavBar={true}/>
                    <Scene key="User_my_review_view" component={User_my_review_view} hideNavBar={true}/>
                    <Scene key="User_my_review_add" component={User_my_review_add} hideNavBar={true}/>
                    <Scene key="User_my_review_add_end" component={User_my_review_add_end} hideNavBar={true}/>
                    <Scene key="User_my_noti" component={User_my_noti} hideNavBar={true}/>
                    <Scene key="User_my_setting" component={User_my_setting} hideNavBar={true}/>
                    <Scene key="User_my_contents" component={User_my_contents} hideNavBar={true}/>
                    {/*실시간 자리잡기(유져)*/}
                    <Scene key="User_request_start" component={User_request_start} hideNavBar={true}/>
                    <Scene key="User_request_step1" component={User_request_step1} hideNavBar={true}/>
                    <Scene key="User_request_step2" component={User_request_step2} hideNavBar={true}/>
                    <Scene key="User_request_step3" component={User_request_step3} hideNavBar={true}/>
                    <Scene key="User_request_step4" component={User_request_step4} hideNavBar={true}/>
                    <Scene key="User_request_step5" component={User_request_step5} hideNavBar={true}/>
                    <Scene key="User_request_step6" component={User_request_step6} hideNavBar={true}/>
                    <Scene key="User_request_sec_step1" component={User_request_sec_step1} hideNavBar={true}/>
                    <Scene key="User_request_sec_step2" component={User_request_sec_step2} hideNavBar={true}/>
                    <Scene key="User_request_sec_step3" component={User_request_sec_step3} hideNavBar={true}/>
                    <Scene key="User_request_sec_step4" component={User_request_sec_step4} hideNavBar={true}/>
                    <Scene key="User_request_sec_step5" component={User_request_sec_step5} hideNavBar={true}/>
                    <Scene key="User_request_sec_fail" component={User_request_sec_fail} hideNavBar={true}/>
                    <Scene key="User_preorder_list" component={User_preorder_list} hideNavBar={true}/>
                    <Scene key="User_preorder_menu" component={User_preorder_menu} hideNavBar={true}/>
                    <Scene key="User_preorder_end" component={User_preorder_end} hideNavBar={true}/>
                    {/*실시간 자리잡기(one)*/}
                    <Scene key="User_request_one_shop_detail" component={User_request_one_shop_detail} hideNavBar={true}/>
                    <Scene key="User_request_one_exec" component={User_request_one_exec} hideNavBar={true}/>
                    <Scene key="User_request_one_exec_wait" component={User_request_one_exec_wait} hideNavBar={true}/>
                    <Scene key="User_request_one_fail" component={User_request_one_fail} hideNavBar={true}/>
                    <Scene key="User_request_one_wait" component={User_request_one_wait} hideNavBar={true}/>
                </Stack>
            </Router>
        );
    }
}
