import React from 'react';
import {
    Text,
    View,
    StyleSheet,
    Image,
    TextInput,
    TouchableHighlight,
    Dimensions,
    ScrollView,
    Alert,
    Menu,
    Platform
} from 'react-native';
import { Permissions, Location } from 'expo';
import MapView from 'react-native-map-clustering';
import {Marker} from 'react-native-maps';
import {FontAwesome } from '@expo/vector-icons';
import { Actions } from 'react-native-router-flux';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import {Ionicons} from '@expo/vector-icons';
import API from '../../objects/API';
import Global from "../../objects/Global";

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f9f9f9',
    },
    color_red : {
        color : '#a25757'
    },
    color_green : {
        color: '#7db840'
    },
    color_purple : {
        color:  'rgba(127, 143, 209,0.5)'
    },
    color_gray : {
        color:  '#c4c4c4'
    },
    maps: {
        marginTop: getStatusBarHeight(),
        width: Dimensions.get('window').width,
        height:  (Dimensions.get('window').height - (56 + getStatusBarHeight())),
    },
    searchgroup : {
        width: Dimensions.get('window').width - 40,
        height: 98,
        position: 'absolute',
        top: 43,
        left: 20,
        // backgroundColor: '#ffffff',
        // flexDirection: 'row'
    },
    main_bottom_bar : {
        flexDirection: 'row',
        width: Dimensions.get('window').width,
        height: 56,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
        position:'absolute',
        top: (Dimensions.get('window').height - 56),
        left: 0,
        bottom: 0
    },
    icon_buttom:{
        width: Dimensions.get('window').width / 5,
        height: 56,
        alignItems: 'center'
    },
    icon_buttom_image :{
        marginTop: 19,
        width: 18,
        height: 18,
        resizeMode: 'contain'
    },
    search_box_icon : {
        width: 24,
        height : 24,
        marginLeft: 12,
        marginRight: 12,
        marginTop: 16,
        marginBottom: 16
    },search_box_input : {
        width: Dimensions.get('window').width - 136,
        height: 56,
    },contents_item: {
        width: (Dimensions.get('window').width - 40),
        height: 96,
        backgroundColor: '#ffffff',
        marginLeft: 20,
        marginRight : 20,
        marginBottom: 10,
        flexDirection: 'row',
        borderWidth: 1,
        borderColor: 'rgba(0, 0, 0, 0.1)'
    },
    imgicon: {
        width: 30,
        height: 30,
        resizeMode : 'cover'
    },
    imgicon_n: {
        width: 16,
        height: 16,
        resizeMode : 'contain'
    },
    imgicon_ios: {
        width: 12,
        height: 12,
        resizeMode : 'cover'
    },

    request_image_dom: {
        width: 90,
        height: 96,
        paddingLeft:6,
        paddingTop:6
    },
    request_image:{
        width:84,
        height: 84
    },
    request_info: {
        width: (Dimensions.get('window').width - 124),
        height: 96,
        paddingLeft: 20
    },
    request_info_memter : {
        // marginRight: ,
        marginLeft: (Dimensions.get('window').width - 220),
        width: 52,
        // fontSize : 12,
        // color : '#7f8fd1',
        marginTop: 6,
        flexDirection: 'row',
    },
    request_info_store_name : {
        fontSize:12,
        color: '#7db840'
    },
    request_info_food_name : {
        fontSize: 16
    },
    request_info_extra : {
        flexDirection: 'row',
        marginTop: 12
    },
    map_store_list : {
        position: 'absolute',
        left: 0,
        bottom: 10,
        // marginTop: 96
    },
    reload_box : {
        width: 137,
        height: 32,
        marginTop: 10,
        marginLeft: ( Dimensions.get('window').width - 177) / 2,
        backgroundColor: 'rgba(255, 255, 255, 0.8)',
        borderWidth: 1,
        borderColor : '#f2f2f2',
        borderRadius: 16,
        flexDirection: 'row',
    },
    reload_name :{
        width: 104,
        fontSize: 12,
        color: '#606060',
        paddingLeft:20,
        paddingTop:5,
        textAlign:'center',
        lineHeight: 20,
    },
    reload_refresh : {
        width: 16,
        height: 16,
        marginLeft: 8,
        marginTop: 8
    },
    setting_icon_image : {
        width: 20,
        height:  "100%"
    },
    sort_word : {
        fontSize: 14,
        lineHeight: 20,
        color: '#838383'
    },
    sortable: {
        flexDirection: 'row',
        marginTop: 30,
        marginBottom: 30,
        width: Dimensions.get('window').width - 40,
        paddingLeft: 20
    },
    sortable_block : {
        width: (Dimensions.get('window').width - 40) / 2,
        height: 20,
        flexDirection: 'row',
    },
    right: {
        alignItems: 'flex-end',
        justifyContent: 'flex-end'
    },
    imgicon_s: {
        width: 16,
        height: 16,
        resizeMode: 'contain'
    },
    imgicon_pin: {
        width: 10,
        height: 14,
        marginTop:1,
        resizeMode: 'contain'
    },

    request_info_memter_text : {
        fontSize : 12,
        color : '#7f8fd1',
    },
    service_noti : {
        width: 320,
        marginLeft: 40,
        paddingTop: -8,
        flexDirection: 'row',
        height: 32
    },

    service_noti_line : {
        width: 10,
        height: 16,
        marginTop: -8,
        marginLeft: -16,
        borderColor: '#f1a9a2',
        borderWidth: 1,
        borderBottomStartRadius: 5,
        borderTopWidth : 0,
        borderRightWidth: 0
    },
    service_noti_icon: {
        backgroundColor: 'rgba(235, 88, 71, 0.23)',
        width: 60,
        height: 16,
        borderColor: '#f1a9a2',
        borderWidth: 1,
        borderRadius: 12,
        paddingTop:1.5,
        alignItems: 'center',
        justifyContent: 'center'
    },
    service_noti_icon_word : {
        fontSize: 10,
        color: '#eb5847',
    },
    service_noti_name : {
        marginLeft: 8,
        fontSize: 12,
        color: '#eb5847'
    },

    c_modal : {
        zIndex: 9999,
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height - getStatusBarHeight(),
        // flexDirection: 'row',
        position: 'absolute',
        left: 0,
        top: getStatusBarHeight()
    }, c_modal_item_filter:{
        width: Dimensions.get('window').width,
        height: 333,
        backgroundColor: '#ffffff',
        alignItems: 'center',
        justifyContent: 'center'
    },
    c_modal_shadow_filter : {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height - getStatusBarHeight() - 333,
        backgroundColor: 'rgba(0, 0, 0, 0.4)'
    } , c_modal_item_sort :{
        width: Dimensions.get('window').width,
        height: 144,
        backgroundColor: '#ffffff',
        alignItems: 'center',
        justifyContent: 'center'
    },
    c_modal_shadow_sort : {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height - getStatusBarHeight() - 144,
        backgroundColor: 'rgba(0, 0, 0, 0.4)'
    } ,
    c_modal_button_group : {
        width: Dimensions.get('window').width,
        alignItems: 'center',
        justifyContent: 'center',
    },
    c_modal_button : {
        width: Dimensions.get('window').width,
        height: 56,
        marginTop: 1,
        paddingLeft: 12,
        paddingRight: 12,
        paddingTop: 16,
        fontSize: 16,
        lineHeight: 24,
        color: '#ffffff',
        textAlign: 'center'
    },
    filter_header:{
        width: Dimensions.get('window').width,
        height: 48,
        flexDirection:'row',
        alignItems :'center',
        justifyContent: 'center'
    },
    filter_header_item : {
        fontSize: 12,
        lineHeight: 16,
        color: '#838383'
    },
    filter_header_menu : {
        width: 85,
        height: 26,
        alignItems: 'center',
        justifyContent: 'center',
        marginLeft: 12,
        marginRight: 12
    },
    filter_header_menu_on: {
        width: 65,
        height: 26,
        // fontSize: 12,
        // lineHeight: 16,
        alignItems: 'center',
        justifyContent: 'center',
        borderBottomWidth: 2,
        borderBottomColor: '#000000',
    },
    filter_header_menu_off: {
        width: 65,
        height: 26,
        // fontSize: 12,
        // lineHeight: 16,
        alignItems: 'center',
        justifyContent: 'center',
        borderBottomWidth: 1,
        borderBottomColor: 'transparent',
        borderBottomStartRadius : 5,
        borderBottomEndRadius: 5
    },

    filter_header_name_on : {
        fontSize : 12,
        lineHeight: 16,
        fontWeight: 'bold',
        alignItems: 'center',
        textAlign: 'center'
    },
    filter_header_name_off : {
        fontSize : 12,
        lineHeight: 16,
        alignItems: 'center',
        textAlign: 'center'
    },
    filter_scroll : {
        width: Dimensions.get('window').width,
        height: 229
    },
    filter_scroll_item : {
        width: Dimensions.get('window').width,
        height: 48,
        flexDirection: 'row'
    },
    filter_scroll_left:{
        width: Dimensions.get('window').width - 40,
        paddingLeft: 16,
        lineHeight:46,
        fontSize: 16,
        color: '#606060'
    },
    filter_scroll_right: {
        width: 40,
        paddingRight: 16,
    },
    filter_scroll_right_image : {
        marginTop: 7.5,
        width:25,
        height: 25
    },

    filter_scroll_right_sort_image : {
        width: 6.7 ,
        height: 25,
        marginTop: 6.3,
        resizeMode: 'contain'
    },
    slideup_bottom : {
        zIndex:999,
        width:320,
        height: 45,
        backgroundColor: '#fff',
        paddingTop: 12,
        flexDirection: 'row'
    },
    slideup_bottom_s:{
        marginLeft: ( Dimensions.get('window').width - 320) / 2,
        width:320,
        height: 45,
        position: 'absolute',
        bottom:0
    },
    sildeup_image : {
        marginLeft: 10,
        width: 24,
        height: 24
    },
    slider_storename : {
        width: 217,
        marginLeft: 8,
        fontSize: 14,
        lineHeight: 24,
    },
    slider_timer : {
        fontSize: 14,
        lineHeight: 24,
        fontWeight: 'bold',
        color: '#eb5847'
    },
    c_modal_card_shadow : {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        backgroundColor: 'rgba(0, 0, 0, 0.4)',
        // flex:1
    },
    popup_card: {
        position: 'absolute',
        top: (Dimensions.get('window').height - 346) / 2,
        left:  (Dimensions.get('window').width - 320) / 2,
        width: 320,
        height: 346,
        backgroundColor : '#ffffff',
        alignItems:'center',
        justifyContent: 'center',
    },
    popup_card_meter : {
        width: 320,
        paddingRight: 16,
        marginTop:12,
        alignItems:'flex-end',
        justifyContent: 'flex-end',
        flexDirection: 'row'
    },
    memter_text : {
        fontSize: 12,
        lineHeight: 16,
        color: '#7f8fd1'
    },
    card_title_sub : {
        color: '#7db840',
        fontSize: 12,
        lineHeight: 16,
        marginTop: 8
    },
    card_title_main : {
        fontSize:20,
        lineHeight: 32
    },
    card_table :  {
        marginTop: 17,
        paddingLeft:16,
        paddingRight: 16
    },
    card_table_row : {
        width:288,
        flexDirection: 'row'
    },
    card_table_left : {
        width:60,
        fontSize:14,
        lineHeight: 24,
        color:'#838383',
        marginBottom: 8
    },
    card_table_right : {
        width:223,
        fontSize:14,
        lineHeight: 24,
        textAlign: 'right'
    },
    card_waiting_level : {
        width:320,
        textAlign:'right',
        color:'#7db840',
        fontSize:10,
        lineHeight:16,
        paddingRight:16
    },
    card_button_group : {
        width:137,
        marginTop:20,
        marginBottom :20,
        height:32,
        borderColor:'#f2f2f2',
        borderWidth:1,
        borderRadius:16,
        paddingTop:6,
        alignItems: 'center',
        justifyContent: 'center'
    },
    card_button_block : {
        fontSize:12,
        lineHeight:20,
        marginTop: -6,
        color:'#606060'
    }

});

const dummyData = [
    {
        latitude: 37.4826942,
        longitude: 127.8975251,
        title: '커리어넷1',
        desc: '커리어넷1 설명'
    },
    {
        latitude: 37.519361,
        longitude: 127.040641,
        title: '커리어넷2',
        desc: '커리어넷2 설명'
    },
    {
        latitude: 37.518357,
        longitude: 127.038420,
        title: '커리어넷3',
        desc: '커리어넷3 설명'
    },
    {
        latitude: 37.518519,
        longitude: 127.041875,
        title: '커리어넷4',
        desc: '커리어넷4 설명'
    }
];

const categorys = [
    {
        name: "주점",
        sub_categorys : [
            "일반 주점",
            "포차",
            "맥주, 호프"
        ]
    },
    {
        name: "음식점",
        sub_categorys : [
            "일반 음식점",
            "치킨, 닭강정",
            "곱창, 고기"
        ]
    },
    {
        name: "카페",
        sub_categorys : [
            "일반 카페"
        ]
    }
];

class User_main extends  React.Component {
    constructor(props) {
        super(props);
        Permissions.askAsync(Permissions.LOCATION);
        this.state = {
            menu: 1,
            sub_menu: [
                [true, false, false],
                [false, false, false],
                [false]
            ],
            sub_menu_on_filter_popup_open: [
                [true, false, false],
                [false, false, false],
                [false]
            ],
            sort_method: 0,
            shop_success: 40,
            item_switch: 0,
            search_text: '',
            filter_switch: false,
            sort_switch: false,
            show_card: false,
            cart_num: 0,
            latitude: null,
            longitude: null,
            geo_error: null,
            address: "",
            popupReservationShop: {
                type: "이자카야",
                name: "공릉동 술깨비",
                distance: 200,
                service: "소주 1병",
                reservation: {
                    name: "김공릉",
                    count: 4,
                    dateTime: "2018년 2월 26일 오후 6시 34분",
                    waitingNum: 35
                },
                currentWaitingNum: 23
            },
            popupPreOderShop: {
                type: "이자카야",
                name: "온더보더 김문점",
                distance: 200,
                reservation: {
                    name: "김문수",
                    count: 10,
                    dateTime: "2018년 2월 23일 오후 6시 34분",
                    timeRemained: "10:00"
                }
            },
            shops: [
                {
                    shopID: 1570,
                    shopName: "킹갓맥주",
                    shopAddress: "",
                    shopNumber: "",
                    shopDescription: "맛있는 생맥주와 감자튀김을 팔고 있습니다",
                    shopEvent: "8명 이상 감자튀김 서비스!",
                    mainProfileUrl: "https://media-cdn.tripadvisor.com/media/photo-s/08/2c/a7/13/cloud-9-sky-bar-lounge.jpg",
                    subProfileUrl: [
                        {
                            order: 0,
                            url: ""
                        }
                    ],
                    location: {
                        latitude: 0,
                        longitude: 0
                    },
                    shopLikes: 57,
                    shopBookmarks: 21,
                    category: {
                        main: "맥주집",
                        sub: "펍",
                        tags: [
                            ""
                        ]
                    },
                    operationHours: [
                        {
                            days: [
                                "mon, tue, wed"
                            ],
                            openHours: "20-05"
                        }
                    ],
                    menus: [
                        {
                            id: 0,
                            name: "",
                            description: "",
                            background: "",
                            price: 0
                        }
                    ]
                }
            ]
        };
        this.componentDidmount();
    }

    componentDidmount() {
        this.onPress_reload_refresh();
        this.applySortMethod();
    }

    onPress_reload_refresh() {
        navigator.geolocation.getCurrentPosition(
            (position) => {
                if (position != null) {
                    // console.log(position.timestamp);
                    this.setState({
                        latitude: position.coords.latitude,
                        longitude: position.coords.longitude,
                        geo_error: null
                    });
                } else {
                    this.setState({
                        latitude: 37.517204,
                        longitude: 127.041289,
                        geo_error: null
                    });
                }
                this.geocoding();
                this.searchNearbyShops();
            }, (error) => this.setState({
                latitude: 37.517204,
                longitude: 127.041289,
                geo_error: error.message
            }),
        );
    }

    searchNearbyShops() {
        API.shop.getNearShops(this.state.latitude, this.state.longitude, data => {
            if (data.count === 0) {
                Alert.alert('주변 가게 찾기', '주변에 자리있소에 등록된 가게가 없습니다.', [{text: "확인", onPress: () => null}]);
            } else {
                this.setState({shops: data.shops});
                /* ex)
                {
                  "page": 1,
                  "count": 1,
                  "shops": [
                    {
                      "shopID": 1570,
                      "shopName": "킹갓맥주",
                      "shopAddress": "string",
                      "shopNumber": "string",
                      "shopDescription": "맛있는 생맥주와 감자튀김을 팔고 있습니다",
                      "shopEvent": "8명 이상 감자튀김 서비스!",
                      "mainProfileUrl": "https://media-cdn.tripadvisor.com/media/photo-s/08/2c/a7/13/cloud-9-sky-bar-lounge.jpg",
                      "subProfileUrl": [
                        {
                          "order": 0,
                          "url": "string"
                        }
                      ],
                      "location": {
                        "latitude": 0,
                        "longitude": 0
                      },
                      "shopLikes": 57,
                      "shopBookmarks": 21,
                      "category": {
                        "main": "맥주집",
                        "sub": "펍",
                        "tags": [
                          "string"
                        ]
                      },
                      "operationHours": [
                        {
                          "days": [
                            "mon, tue, wed"
                          ],
                          "openHours": "20-05"
                        }
                      ],
                      "menus": [
                        {
                          "id": 0,
                          "name": "string",
                          "description": "string",
                          "background": "string",
                          "price": 0
                        }
                      ]
                    }
                  ]
                }*/
            }
        }, error => {
            Alert.alert('주변 가게 찾기', '서버문제로 주변 가게 찾기에 실패하였습니다. \n' + error, [{
                text: "확인",
                onPress: () => Actions.pop()
            }]);
        });
    }

    async geocoding() {
        try {
            let result = await Location.reverseGeocodeAsync({
                latitude: this.state.latitude,
                longitude: this.state.longitude
            });
            // console.log(result);
            if (result.length > 0) {
                this.setState({address: result[0].street});
            }
        } catch (e) {
            console.error(e);
        }
    }

    onPressMarker(marker) {
        // todo 가게보기
    }

    onPressSubCategory(index, index_sub) {
        let sub_menu = [];
        for(let i = 0; i < this.state.sub_menu_on_filter_popup_open.length; i++) {
            sub_menu[i] = [];
            for(let j = 0; j < this.state.sub_menu_on_filter_popup_open[i].length; j++) {
                if (i === index && j === index_sub)
                    sub_menu[i][j] = !this.state.sub_menu_on_filter_popup_open[i][j];
                else
                    sub_menu[i][j] = this.state.sub_menu_on_filter_popup_open[i][j];
            }
        }
        this.setState({sub_menu_on_filter_popup_open: sub_menu});
    }

    onPressFilterPopupApply() {
        this.setState({
            filter_switch: false,
            sub_menu: this.state.sub_menu_on_filter_popup_open
        });
    }

    filterSwitch() {
        return <View style={[styles.c_modal, this.state.filter_switch ? {} : {display: 'none', position: 'relative'}]}>
            <TouchableHighlight underlayColor={'transparent'}
                                onPress={() => this.setState({filter_switch: false})}>
                <View style={styles.c_modal_shadow_filter}/>
            </TouchableHighlight>
            <View style={styles.c_modal_item_filter}>
                <View style={styles.filter_header}>
                    {
                        categorys.map((category, index) => (
                            <TouchableHighlight key={'category_' + index} underlayColor={'transparent'} onPress={() => this.setState({menu: (index + 1)})}>
                                <View style={[styles.filter_header_menu, {width: (index !== 1 ? 23 : 34)}]}>
                                    <View
                                        style={[(this.state.menu === index + 1) ? styles.filter_header_menu_on : styles.filter_header_menu_off, {width: (index !== 1 ? 25 : 36)}]}>
                                        <Text
                                            style={[(this.state.menu === index + 1) ? styles.filter_header_name_on : styles.filter_header_name_off, {
                                                width: (index !== 1 ? 25 : 36),
                                                textAlign: 'center'
                                            }]}>{category.name}</Text>
                                    </View>
                                </View>
                            </TouchableHighlight>
                        ))
                    }
                </View>
                <View style={styles.filter_scroll}>
                    {
                        categorys.map((category, index) => (
                            <ScrollView key={index}
                                        style={(this.state.menu === index + 1 ? [] : [{display: 'none'}])}>
                                {category.sub_categorys.map((sub_category, index_sub) => (
                                    <TouchableHighlight key={'sub_category_' + index + '_' + index_sub} underlayColor={'transparent'} onPress={() => this.onPressSubCategory(index, index_sub)}>
                                        <View style={styles.filter_scroll_item}>
                                            <Text style={styles.filter_scroll_left}>{sub_category}</Text>
                                            <View style={styles.filter_scroll_right}>
                                                {this.state.sub_menu_on_filter_popup_open[index][index_sub]
                                                && <Image style={styles.filter_scroll_right_image}
                                                          source={require('../../assets/common/check_circle_red.png')}/>}
                                            </View>
                                        </View>
                                    </TouchableHighlight>
                                ))}
                            </ScrollView>
                        ))
                    }
                </View>
                <View>
                    <Text style={[styles.c_modal_button, {
                        backgroundColor: 'rgba(235,88,71,0.9)',
                        marginTop: 1,
                        color: '#ffffff',
                    }]} onPress={() => this.onPressFilterPopupApply()}><Text
                        style={[styles.c_modal_button, {backgroundColor: 'transparent'}]}>적용하기</Text></Text>
                </View>
            </View>
        </View>;
    }

    onChangeSortMethod(method) {
        this.setState({sort_method: method, sort_switch: false});
        this.applySortMethod(method);
    }

    sortSwitch() {
        return <View style={[styles.c_modal, this.state.sort_switch ? {} : {display: 'none', position: 'relative'}]}>
            <TouchableHighlight underlayColor={'transparent'}
                                onPress={() => this.setState({sort_switch: false})}>
                <View style={styles.c_modal_shadow_sort}/>
            </TouchableHighlight>
            <View style={styles.c_modal_item_sort}>
                <TouchableHighlight underlayColor={'transparent'} onPress={() => this.onChangeSortMethod(0)}>
                    <View style={styles.filter_scroll_item}>
                        <Text style={styles.filter_scroll_left}>좋아요 순</Text>
                        <View style={styles.filter_scroll_right}>
                            <Image style={styles.filter_scroll_right_image}
                                   source={require('../../assets/common/arrow_right_square.png')}/>
                        </View>
                    </View>
                </TouchableHighlight>
                <TouchableHighlight underlayColor={'transparent'} onPress={() => this.onChangeSortMethod(1)}>
                    <View style={styles.filter_scroll_item}>
                        <Text style={styles.filter_scroll_left}>거리 순</Text>
                        <View style={styles.filter_scroll_right}>
                            <Image style={styles.filter_scroll_right_image}
                                   source={require('../../assets/common/arrow_right_square.png')}/>
                        </View>
                    </View>
                </TouchableHighlight>
                <TouchableHighlight underlayColor={'transparent'} onPress={() => this.onChangeSortMethod(2)}>
                    <View style={styles.filter_scroll_item}>
                        <Text style={styles.filter_scroll_left}>최신 순</Text>
                        <View style={styles.filter_scroll_right}>
                            {/*<Image style={styles.filter_scroll_right_image} source={require('../../assets/common/arrow_right.png')}/>*/}
                        </View>
                    </View>
                </TouchableHighlight>
            </View>
        </View>;
    }

    showCard() {
        return <View style={[styles.c_modal, this.state.show_card ? {} : {display: 'none', position: 'relative'}]}>
            <TouchableHighlight underlayColor={'transparent'} onPress={() => this.setState({show_card: false})}>
                <View style={styles.c_modal_card_shadow}>
                </View>
            </TouchableHighlight>
            {this.popupReservation()}
            {this.popupPreOrder()}
        </View>;
    }

    popupReservation() {
        return <View style={[styles.popup_card, this.state.cart_num === 2 ? {} : {
            display: 'none',
            position: 'relative'
        }]}>
            <View style={styles.popup_card_meter}>
                <Image style={styles.imgicon_n} source={require("../../assets/common/pin.png")}/>
                <Text style={styles.memter_text}> {this.state.popupReservationShop.distance}M</Text>
            </View>
            <Text style={styles.card_title_sub}>{this.state.popupReservationShop.type}</Text>
            <Text style={styles.card_title_main}>{this.state.popupReservationShop.name}</Text>
            {this.state.popupReservationShop.service !== "" &&
            <View style={{marginTop: 17, flexDirection: 'row'}}>
                <View style={[styles.service_noti_icon, {marginLeft: -30}]}>
                    <Text style={styles.service_noti_icon_word}>서비스제안</Text>
                </View>
                <Text style={[styles.service_noti_name]}>{this.state.popupReservationShop.service}</Text>
            </View>}
            {this.state.popupReservationShop.service !== "" &&
            <View style={{
                width: 320,
                paddingRight: 16,
                alignItems: 'flex-end',
                justifyContent: 'flex-end',
                marginTop: -22
            }}>
                <Image style={{width: 24, height: 24}}
                       source={require("../../assets/common/dropbox_r.png")}/>
            </View>}
            <View style={styles.card_table}>
                <View style={styles.card_table_row}>
                    <Text style={styles.card_table_left}>예약자</Text>
                    <Text style={styles.card_table_right}>{this.state.popupReservationShop.reservation.name}</Text>
                </View>
                <View style={styles.card_table_row}>
                    <Text style={styles.card_table_left}>예약인원</Text>
                    <Text style={styles.card_table_right}>{this.state.popupReservationShop.reservation.count}명</Text>
                </View>
                <View style={styles.card_table_row}>
                    <Text style={styles.card_table_left}>예약일시</Text>
                    <Text style={styles.card_table_right}>{this.state.popupReservationShop.reservation.dateTime}</Text>
                </View>
                <View style={styles.card_table_row}>
                    <Text style={[styles.card_table_left, {marginBottom: 0}]}>대기번호</Text>
                    <Text
                        style={[styles.card_table_right, {color: '#eb5847', textAlign: 'right'}]}>{this.state.popupReservationShop.reservation.waitingNum}번</Text>
                </View>
            </View>
            <Text style={styles.card_waiting_level}>{this.state.popupReservationShop.reservation.name}님 앞으로 {this.state.popupReservationShop.reservation.waitingNum - this.state.popupReservationShop.currentWaitingNum}팀이 대기중입니다.</Text>
            <TouchableHighlight underlayColor={'transparent'} onPress={() => Actions.User_preorder_menu()}>
                <View style={styles.card_button_group}>
                    <Text style={styles.card_button_block}>선주문 하기</Text>
                </View>
            </TouchableHighlight>
        </View>;
    }

    popupPreOrder() {
        return <View style={[styles.popup_card, {height: 296}, this.state.cart_num === 1 ? {} : {
            display: 'none',
            position: 'relative'
        }]}>
            <View style={styles.popup_card_meter}>
                <Image style={styles.imgicon_n} source={require("../../assets/common/pin.png")}/>
                <Text style={styles.memter_text}> {this.state.popupPreOderShop.distance}M</Text>
            </View>
            <Text style={styles.card_title_sub}>{this.state.popupPreOderShop.type}</Text>
            <Text style={styles.card_title_main}>{this.state.popupPreOderShop.name}</Text>
            <View style={styles.card_table}>
                <View style={styles.card_table_row}>
                    <Text style={styles.card_table_left}>예약자</Text>
                    <Text style={styles.card_table_right}>{this.state.popupPreOderShop.reservation.name}</Text>
                </View>
                <View style={styles.card_table_row}>
                    <Text style={styles.card_table_left}>예약인원</Text>
                    <Text style={styles.card_table_right}>{this.state.popupPreOderShop.reservation.count}명</Text>
                </View>
                <View style={styles.card_table_row}>
                    <Text style={styles.card_table_left}>예약일시</Text>
                    <Text style={styles.card_table_right}>{this.state.popupPreOderShop.reservation.dateTime}</Text>
                </View>
                <View style={styles.card_table_row}>
                    <Text style={[styles.card_table_left, {marginBottom: 0}]}>유효기간</Text>
                    <Text style={[styles.card_table_right, {
                        color: '#eb5847',
                        textAlign: 'right'
                    }]}>{this.state.popupPreOderShop.reservation.timeRemained}</Text>
                </View>
            </View>
            <TouchableHighlight underlayColor={'transparent'} onPress={() => Actions.User_preorder_list()}>
                <View style={styles.card_button_group}>
                    <Text style={styles.card_button_block}>선주문 확인하기</Text>
                </View>
            </TouchableHighlight>
        </View>;
    }

    itemSwitch0() {
        // return <View style={this.state.item_switch === 0 ? {} : {display: 'none', position: 'relative'}}>
        return <View style={this.state.item_switch === 0 ? {} : {position: 'absolute', top: Dimensions.get("window").height}}>
            <View style={styles.maps}>
                <MapView style={[{flex: 1}, {}]}
                         region={{
                             latitude: (this.state.latitude == null ? 37.517204 : this.state.latitude),
                             longitude: (this.state.longitude == null ? 127.041289 : this.state.longitude),
                             latitudeDelta: 0.0922,
                             longitudeDelta: 0.0421,
                         }}>
                    {
                        dummyData.map((marker, index) => (
                            <Marker
                                key={'marker_' + index}
                                coordinate={{latitude: marker.latitude, longitude: marker.longitude}}
                                title={marker.title}
                                description={marker.desc}
                                image={Platform.OS === 'android' ? require("../../assets/map/marker_mini.png") : undefined}
                                onPress={() => this.onPressMarker(marker)}
                            >{Platform.OS === 'ios' && <View><Image source={require('../../assets/map/marker_mini.png')} style={{width: 58, height: 58}}/></View>}
                            </Marker>
                        ))
                    }
                    <Marker
                        key={0}
                        coordinate={{
                            latitude: (this.state.latitude == null ? 37.517204 : this.state.latitude),
                            longitude: (this.state.longitude == null ? 127.041289 : this.state.longitude),
                        }}
                        title={'나'}
                        description={'나'}
                        visiliable = {true}
                        image={Platform.OS === 'android' ? require("../../assets/map/marker_me.png") : undefined}
                    >{Platform.OS === 'ios' && <View><Image source={require('../../assets/map/marker_me.png')} style={{width: 66, height: 72}}/></View>}
                    </Marker>
                </MapView>
            </View>
            <TouchableHighlight style={[styles.slideup_bottom_s, this.state.show_card !== 1 ? {} : {
                display: 'none',
                position: 'relative'
            }]} underlayColor={'transparent'} onPress={() => this.setState({show_card: 1, cart_num: 1})}>
                <View style={styles.slideup_bottom}>
                    <Image style={styles.sildeup_image} source={require("../../assets/common/slide_up.png")}/>
                    <Text style={styles.slider_storename}>온더보더 김문점</Text>
                    <Text style={styles.slider_timer}>10:00</Text>
                </View>
            </TouchableHighlight>
            <View style={[styles.map_store_list, this.state.show_card ? {bottom: 10} : {bottom: 55}]}>
                <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                    <TouchableHighlight underlayColor={'transparent'}
                                        onPress={() => Actions.User_request_step5()}>
                        <View style={styles.contents_item}>
                            <View style={styles.request_image_dom}>
                                <Image style={styles.request_image}
                                       source={require("../../assets/User/My/dummy_menu_images.png")}/>
                                <Image style={{
                                    zIndex: 9998,
                                    width: 48,
                                    height: 16,
                                    marginTop: -88,
                                    marginLeft: -4
                                }} source={require("../../assets/common/event_label_w.png")}/>
                            </View>
                            <View style={styles.request_info}>
                                <View style={styles.request_info_memter}><Image style={styles.imgicon_pin}
                                                                                source={require("../../assets/common/pin.png")}/><Text
                                    style={styles.request_info_memter_text}> 200M</Text></View>
                                <Text style={styles.request_info_store_name}>이자카야</Text>
                                <Text style={styles.request_info_food_name}>공릉동 술깨비</Text>
                                <View style={[styles.request_info_extra, {marginTop: 6}]}>
                                    <Image style={styles.imgicon_s}
                                           source={require("../../assets/common/heart.png")}/>
                                    <Text style={{color: '#eb5847', lineHeight: 16, fontSize: 12}}> 6</Text>
                                    <View style={{width: 13, height: 16, paddingTop: 6}}>
                                        <View style={{
                                            width: 1,
                                            height: 4,
                                            backgroundColor: '#c4c4c4',
                                            marginLeft: 6
                                        }}/>
                                    </View>
                                    <Image style={styles.imgicon_s}
                                           source={require("../../assets/common/clock.png")}/>
                                    <Text style={{color: '#c4c4c4', lineHeight: 16, fontSize: 12}}> 16:00 ~
                                        3:00</Text>
                                </View>
                            </View>
                        </View>
                    </TouchableHighlight>
                    <TouchableHighlight underlayColor={'transparent'}
                                        onPress={() => this.setState({show_card: 1, cart_num: 2})}>
                        <View style={styles.contents_item}>
                            <View style={styles.request_image_dom}>
                                <Image style={styles.request_image}
                                       source={require("../../assets/User/My/dummy_menu_images.png")}/>
                                <Image style={{
                                    zIndex: 9998,
                                    width: 48,
                                    height: 16,
                                    marginTop: -88,
                                    marginLeft: -4
                                }} source={require("../../assets/common/event_label_w.png")}/>
                            </View>
                            <View style={styles.request_info}>
                                <View style={styles.request_info_memter}><Image style={styles.imgicon_pin}
                                                                                source={require("../../assets/common/pin.png")}/><Text
                                    style={styles.request_info_memter_text}> 200M</Text></View>
                                <Text style={styles.request_info_store_name}>이자카야</Text>
                                <Text style={styles.request_info_food_name}>공릉동 술깨비</Text>
                                <View style={[styles.request_info_extra, {marginTop: 6}]}>
                                    <Image style={styles.imgicon_s}
                                           source={require("../../assets/common/heart.png")}/>
                                    <Text style={{color: '#eb5847', lineHeight: 16, fontSize: 12}}> 6</Text>
                                    <View style={{width: 13, height: 16, paddingTop: 6}}>
                                        <View style={{
                                            width: 1,
                                            height: 4,
                                            backgroundColor: '#c4c4c4',
                                            marginLeft: 6
                                        }}/>
                                    </View>
                                    <Image style={styles.imgicon_s}
                                           source={require("../../assets/common/clock.png")}/>
                                    <Text style={{color: '#c4c4c4', lineHeight: 16, fontSize: 12}}> 16:00 ~
                                        3:00</Text>
                                </View>
                            </View>
                        </View>
                    </TouchableHighlight>
                    <TouchableHighlight underlayColor={'transparent'}
                                        onPress={() => this.setState({show_card: 1, cart_num: 3})}>
                        <View style={styles.contents_item}>
                            <View style={styles.request_image_dom}>
                                <Image style={styles.request_image}
                                       source={require("../../assets/User/My/dummy_menu_images.png")}/>
                                <Image style={{
                                    zIndex: 9998,
                                    width: 48,
                                    height: 16,
                                    marginTop: -88,
                                    marginLeft: -4
                                }} source={require("../../assets/common/event_label_w.png")}/>
                            </View>
                            <View style={styles.request_info}>
                                <View style={styles.request_info_memter}><Image style={styles.imgicon_pin}
                                                                                source={require("../../assets/common/pin.png")}/><Text
                                    style={styles.request_info_memter_text}> 200M</Text></View>
                                <Text style={styles.request_info_store_name}>이자카야</Text>
                                <Text style={styles.request_info_food_name}>공릉동 술깨비</Text>
                                <View style={[styles.request_info_extra, {marginTop: 6}]}>
                                    <Image style={styles.imgicon_s}
                                           source={require("../../assets/common/heart.png")}/>
                                    <Text style={{color: '#eb5847', lineHeight: 16, fontSize: 12}}> 6</Text>
                                    <View style={{width: 13, height: 16, paddingTop: 6}}>
                                        <View style={{
                                            width: 1,
                                            height: 4,
                                            backgroundColor: '#c4c4c4',
                                            marginLeft: 6
                                        }}/>
                                    </View>
                                    <Image style={styles.imgicon_s}
                                           source={require("../../assets/common/clock.png")}/>
                                    <Text style={{color: '#c4c4c4', lineHeight: 16, fontSize: 12}}> 16:00 ~
                                        3:00</Text>
                                </View>
                            </View>
                        </View>
                    </TouchableHighlight>
                    <TouchableHighlight underlayColor={'transparent'}
                                        onPress={() => this.setState({show_card: 1, cart_num: 4})}>
                        <View style={styles.contents_item}>
                            <View style={styles.request_image_dom}>
                                <Image style={styles.request_image}
                                       source={require("../../assets/User/My/dummy_menu_images.png")}/>
                                <Image style={{
                                    zIndex: 9998,
                                    width: 48,
                                    height: 16,
                                    marginTop: -88,
                                    marginLeft: -4
                                }} source={require("../../assets/common/event_label_w.png")}/>
                            </View>
                            <View style={styles.request_info}>
                                <View style={styles.request_info_memter}><Image style={styles.imgicon_pin}
                                                                                source={require("../../assets/common/pin.png")}/><Text
                                    style={styles.request_info_memter_text}> 200M</Text></View>
                                <Text style={styles.request_info_store_name}>이자카야</Text>
                                <Text style={styles.request_info_food_name}>공릉동 술깨비</Text>
                                <View style={[styles.request_info_extra, {marginTop: 6}]}>
                                    <Image style={styles.imgicon_s}
                                           source={require("../../assets/common/heart.png")}/>
                                    <Text style={{color: '#eb5847', lineHeight: 16, fontSize: 12}}> 6</Text>
                                    <View style={{width: 13, height: 16, paddingTop: 6}}>
                                        <View style={{
                                            width: 1,
                                            height: 4,
                                            backgroundColor: '#c4c4c4',
                                            marginLeft: 6
                                        }}/>
                                    </View>
                                    <Image style={styles.imgicon_s}
                                           source={require("../../assets/common/clock.png")}/>
                                    <Text style={{color: '#c4c4c4', lineHeight: 16, fontSize: 12}}> 16:00 ~
                                        3:00</Text>
                                </View>
                            </View>
                        </View>
                    </TouchableHighlight>
                </ScrollView>
            </View>
        </View>;
    }

    itemSwitch1Dummy = [
        {
            id: 0,
            type: "이자카야",
            name: "공릉동 술깨비",
            distance: 200,
            likeCount: 6,
            startTime: "16:00",
            endTime: "3:00",
            event: ""
        },
        {
            id: 1,
            type: "이자카야",
            name: "공릉동 술깨비 2",
            distance: 250,
            likeCount: 8,
            startTime: "16:00",
            endTime: "3:00",
            event: "이벤트 내용이 들어갑니다."
        },
        {
            id: 2,
            type: "이자카야",
            name: "공릉동 술깨비 3",
            distance: 150,
            likeCount: 9,
            startTime: "16:00",
            endTime: "3:00",
            event: ""
        },
        {
            id: 3,
            type: "이자카야",
            name: "공릉동 술깨비 4",
            distance: 50,
            likeCount: 2,
            startTime: "16:00",
            endTime: "3:00",
            event: ""
        }
    ];

    itemSwitch1Element(shop) {
        return <View key={"shop_" + shop.id}>
            <View style={styles.contents_item}>
                <View style={styles.request_image_dom}>
                    <Image style={styles.request_image}
                           source={require("../../assets/User/My/dummy_menu_images.png")}/>
                    <Image style={{zIndex: 9998, width: 48, height: 16, marginTop: -88, marginLeft: -4}}
                           source={require("../../assets/common/event_label_w.png")}/>
                </View>
                <View style={styles.request_info}>
                    <View style={styles.request_info_memter}><Image style={styles.imgicon_pin}
                                                                    source={require("../../assets/common/pin.png")}/><Text
                        style={styles.request_info_memter_text}> {shop.distance}M</Text></View>
                    <Text style={styles.request_info_store_name}>{shop.type}</Text>
                    <Text style={styles.request_info_food_name}>{shop.name}</Text>
                    <View style={styles.request_info_extra}>
                        <Image style={styles.imgicon_s} source={require("../../assets/common/heart.png")}/>
                        <Text style={{color: '#eb5847', lineHeight: 16, fontSize: 12}}> {shop.likeCount}</Text>
                        <View style={{width: 13, height: 16, paddingTop: 6}}>
                            <View style={{
                                width: 1,
                                height: 4,
                                backgroundColor: '#c4c4c4',
                                marginLeft: 6
                            }}/>
                        </View>
                        <Image style={styles.imgicon_s} source={require("../../assets/common/clock.png")}/>
                        <Text style={{color: '#c4c4c4', lineHeight: 16, fontSize: 12}}> {shop.startTime} ~ {shop.endTime}</Text>
                    </View>
                </View>
            </View>
            {
                shop.event !== "" &&
                <View style={styles.service_noti}>
                    <View style={styles.service_noti_line}/>
                    <View style={styles.service_noti_icon}><Text
                        style={styles.service_noti_icon_word}>이벤트</Text></View>
                    <Text style={styles.service_noti_name}>{shop.event}</Text>
                </View>
            }
        </View>;
    }

    applySortMethod(method = -1) {
        switch (method === -1 ? this.state.sort_method : method) {
            case 0: // 좋아요 순
                this.itemSwitch1Dummy.sort((shop1, shop2) => shop2.likeCount - shop1.likeCount);
                break;
            case 1: // 거리 순
                this.itemSwitch1Dummy.sort((shop1, shop2) => shop1.distance - shop2.distance);
                break;
            case 2: // 최신 순
                this.itemSwitch1Dummy.sort((shop1, shop2) => shop1.id - shop2.id);
                break;
        }
    }

    itemSwitch1() {
        return <View style={[this.state.item_switch === 1 ? {} : {
            display: 'none',
            position: 'relative'
        }, {height: Dimensions.get('window').height - 56, paddingTop: 127}]}>
            <View style={styles.sortable}>
                <TouchableHighlight underlayColor={'transparent'}
                                    onPress={() => this.setState({sort_switch: 1})}>
                    <View style={styles.sortable_block}>
                        <Text style={styles.sort_word}>{this.state.sort_method === 0 ? ("좋아요 순") : this.state.sort_method === 1 ? ("거리 순") : ("최신 순")}</Text>
                        <Image style={styles.setting_icon_image}
                               source={require("../../assets/common/dropbox.png")}/>
                    </View>
                </TouchableHighlight>
                <TouchableHighlight underlayColor={'transparent'}
                                    onPress={() => this.setState({filter_switch: 1})}>
                    <View style={[styles.sortable_block, styles.right]}>
                        <Text style={styles.sort_word}>필터</Text>
                        <Image style={styles.setting_icon_image}
                               source={require("../../assets/common/setting_icon.png")}/>
                    </View>
                </TouchableHighlight>
            </View>
            <ScrollView>
                {
                    this.itemSwitch1Dummy.map(shop => this.itemSwitch1Element(shop))
                }
            </ScrollView>
        </View>;
    }

    onSearch (){
        if(this.state.search_text.trim() == ''){
            Alert.alert(
                '검색',
                '검색어를 입력해주세요.',
                [
                    {text: "확인", onPress: () => null}
                ]
            );
        }else{
            //Todo 가계 검색 API 필요
            if(this.state.item_switch === 0){
                this.setState({item_switch: 1});
            }
        }
    }

    searchGroup() {
        return <View style={styles.searchgroup}>
            <View style={{backgroundColor: '#ffffff', flexDirection: 'row'}}>
                <TouchableHighlight underlayColor={'transparent'} onPress={() => this.onSearch() }>
                    <Image style={styles.search_box_icon} source={require("../../assets/common/search_icon.png")}/>
                </TouchableHighlight>
                <TextInput style={styles.search_box_input} underlineColorAndroid={'transparent'}
                           placeholder={"어디를 예약할까요?"} onChangeText={(text) => this.setState({search_text : text})}/>
                <TouchableHighlight underlayColor={'transparent'} onPress={() => this.setState({item_switch: this.state.item_switch === 1 ? 0 : 1})}>
                    <Image style={styles.search_box_icon}
                           source={this.state.item_switch !== 1 ? require("../../assets/common/burger_icon.png") : require("../../assets/common/map_icon.png")}/>
                </TouchableHighlight>
            </View>
            <View style={styles.reload_box}>
                <Text style={styles.reload_name}>{this.state.address}</Text>
                <View style={{borderColor: '#f2f2f2', width: 1, height: 32}}/>
                <TouchableHighlight underlayColor={'transparent'} onPress={() => this.onPress_reload_refresh()}>
                    <Image style={styles.reload_refresh} source={require("../../assets/common/refresh.png")}/>
                </TouchableHighlight>
            </View>
        </View>;
    }

    bottomMenu() {
        return <View style={styles.main_bottom_bar}>
            <TouchableHighlight underlayColor={'transparent'} onPress={() => Actions.pop()}>
                <View style={styles.icon_buttom}>
                    <Image style={styles.icon_buttom_image}
                           source={require("../../assets/User/Bottom/home_black.png")}/>
                </View>
            </TouchableHighlight>
            <TouchableHighlight underlayColor={'transparent'} onPress={() => Actions.User_my_reserved(Global.user === undefined ? {} : {my_info: {name: Global.user.username}})}>
                <View style={styles.icon_buttom}>
                    <Image style={styles.icon_buttom_image}
                           source={require("../../assets/User/Bottom/utensils_gray.png")}/>
                </View>
            </TouchableHighlight>
            <TouchableHighlight underlayColor={'transparent'} onPress={() => Actions.User_my_review()}>
                <View style={styles.icon_buttom}>
                    <Image style={styles.icon_buttom_image}
                           source={require("../../assets/User/Bottom/chat_gray.png")}/>
                </View>
            </TouchableHighlight>
            <TouchableHighlight underlayColor={'transparent'} onPress={() => Actions.User_my_contents()}>
                <View style={styles.icon_buttom}>
                    <Image style={styles.icon_buttom_image}
                           source={require("../../assets/User/Bottom/newspaper_gray.png")}/>
                </View>
            </TouchableHighlight>
            <TouchableHighlight underlayColor={'transparent'} onPress={() => Actions.User_my_main()}>
                <View style={styles.icon_buttom}>
                    <Image style={styles.icon_buttom_image}
                           source={require("../../assets/User/Bottom/user_gray.png")}/>
                </View>
            </TouchableHighlight>
        </View>;
    }

    render() {
        return (
            <View style={styles.container}>
                {this.filterSwitch()}
                {this.sortSwitch()}
                {this.showCard()}
                {this.itemSwitch0()}
                {this.itemSwitch1()}
                {this.searchGroup()}
                {this.bottomMenu()}
            </View>
        )
    }
}

export default User_main;