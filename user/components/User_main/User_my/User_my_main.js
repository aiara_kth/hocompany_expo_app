import React from 'react';
import {Text, View, StyleSheet, Image,TextInput,TouchableHighlight, Dimensions, ScrollView, Alert ,Menu} from 'react-native';
import { Actions } from 'react-native-router-flux';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import {Ionicons} from '@expo/vector-icons';
import email from 'react-native-email'
import Global from "../../../objects/Global";

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f9f9f9',
    },
    Navigator : {
        flexDirection: 'row',
        paddingTop : getStatusBarHeight(),
        width: Dimensions.get('window').width,
        height: 48 + getStatusBarHeight(),
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    Navigator_menu: {
        width: 80,
        height: 48,
    },
    Navigator_menu_button :{
        width: 48,
        height: 48,
        paddingTop: 12,
        paddingLeft: 12,
    },
    Navigator_menu_image : {
        width: 24,
        height: 24
    },
    Navigator_title:{
        width : ((Dimensions.get('window').width) - 48),
        paddingLeft: 14
    },
    Navigator_title_text:{
        fontSize: 16,
        color:'#000000'
    },
    Navigator_start:{
        alignItems: 'flex-end',
        width: 48,
        height: 48,
        paddingTop: 12,
        paddingRight: 12,
    },
    Navigator_start_text:{
        fontSize: 16,
        color:'#7db840',
    },
    sub_Navigator : {
        flexDirection: 'row',
        width: Dimensions.get('window').width,
        height: 40,
        backgroundColor: '#fff',
        alignItems: 'center',
    },
    sub_Navigator_menu: {
        width: 85,
        height: 40,
        alignItems: 'flex-end',
        justifyContent: 'center',
        marginLeft: 15,
        marginRight: 15
    },
    sub_Navigator_menu_on: {
        width: 65,
        height: 40,
        alignItems: 'center',
        justifyContent: 'center',
        borderBottomWidth: 2,
        borderBottomColor: '#000000',
    },
    sub_Navigator_menu_off: {
        width: 65,
        height: 40,
        alignItems: 'center',
        justifyContent: 'center',
        borderBottomWidth: 2,
        borderBottomColor: 'transparent',
    },
    sub_Navigator_name_on : {
        fontSize : 14,
        fontWeight: 'bold',
        alignItems: 'center',
    },
    sub_Navigator_name_off : {
        fontSize : 14,
        alignItems: 'center',
    },
    contents : {
        paddingTop :  9,
    },
    user_info : {
        backgroundColor: '#fff',
        width: Dimensions.get('window').width,
        height: 204,
        paddingLeft: 12
    },
    user_info_image_dom:{
        marginTop: 38,
        width:48,
        height:48,
    },
    user_info_image : {
        //marginTop: 38,
        width:48,
        height:48,
        borderRadius: 12
    },
    user_info_hello :{
        marginTop: 17,
        fontSize: 20,
        height: 29
    },
    user_info_name:{
        fontSize: 12,
        color: '#838383',
        height: 18
    },
    user_info_hello_input:{
        fontSize: 17,
        color: '#000',
        width: 100,
        height: 34,
        borderTopWidth: 1,
        // borderBottomWidth: 1
    },
    user_info_name_input:{
        fontSize: 12,
        color: '#838383',
        width: 100,
        height: 23,
        borderTopWidth: 1,
        borderBottomWidth: 1
    },
    user_info_edit: {
        marginTop: 10,
        fontSize: 12,
        color: '#eb5847',
        height: 18,
        width: 40
    },
    user_info_save: {
        marginTop: 10,
        fontSize: 12,
        color: '#eb5847',
        height: 18,
        width: 40
    },
    user_info_cancel: {
        marginTop: 10,
        fontSize: 12,
        color: '#eb5847',
        height: 18,
        width: 40
    },
    touchable_container: {
        flexDirection: 'row'
    },
    touchable_element: {
        height: 28,
        width: 40,
        flex: 1
    },
    userButton_group : {
        width: Dimensions.get('window').width,
        height: 81,
        paddingTop :  9,
        flexDirection: 'row',
    },
    userButton :{
        width: Dimensions.get('window').width / 4,
        height: 72,
        backgroundColor: '#fff',
        alignItems: 'center',
        flex: 1,
        paddingTop: 16
    },
    userButton_image: {
        width: 24,
        height: 24
    },
    userButton_image_s: {
        width: 16,
        height: 16,
        marginTop: 4,
        marginBottom: 4
    },
    userButton_image_r: {
        width: 12,
        height: 16,
        marginTop: 4,
        marginBottom: 4
    },
    userButton_text: {
        fontSize: 14
    },
    main_bottom_bar : {
        flexDirection: 'row',
        width: Dimensions.get('window').width,
        height: 56,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
        position:'absolute',
        top: (Dimensions.get('window').height - (105 + getStatusBarHeight())),
        left: 0,
        bottom: 0
    },
    icon_buttom:{
        width: Dimensions.get('window').width / 5,
        height: 56,
        alignItems: 'center'
    },
    icon_buttom_image :{
        marginTop: 19,
        width: 18,
        height: 18,
        resizeMode: 'contain'
    },
    noti_Navigator : {
        flexDirection: 'row',
        paddingTop : getStatusBarHeight(),
        width: Dimensions.get('window').width,
        height: 48 + getStatusBarHeight(),
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    noti_Navigator_menu: {
        width: 80,
        height: 48,
    },
    noti_Navigator_menu_button :{
        width: 48,
        height: 48,
        paddingTop: 12,
        paddingLeft: 12,
    },
    noti_Navigator_menu_image : {
        width: 24,
        height: 24
    },
    noti_Navigator_title:{
        width : ((Dimensions.get('window').width) - 160),
        alignItems: 'center',
    },
    noti_Navigator_title_text:{
        fontSize: 20,
        color:'#000000'
    },
    noti_Navigator_start:{
        alignItems: 'flex-end',
        width: 80,
        height: 48,
        paddingRight: 12,
    },
    noti_Navigator_start_text:{
        fontSize: 16,
        color:'#7db840',
        marginTop: 12,
        marginBottom: 12
    },
    noti_contents_item : {
        width: Dimensions.get('window').width,
        height: 68,
        backgroundColor: '#fff',
        paddingBottom: 10,
        alignItems: 'center',
        justifyContent: 'center',
    },
    noti_contents_item_sub : {
        flexDirection: 'row',
        width: Dimensions.get('window').width,
        backgroundColor: '#fff',
        marginBottom: 8,
        paddingLeft: 10,
        paddingRight: 10,
        alignItems: 'center',
        justifyContent: 'center',
    },
    noti_item_date : {
        fontSize: 16,
        color: '#838383',
        fontWeight: '600',
        height: 20
    },
    noti_item_group : {
        flex: 1,
        paddingLeft: 12
    },
    noti_item_top: {
        marginTop: 12,
        fontSize: 14,
        height: 20
    },
    noti_item_bottom: {
        marginTop: 4,
        fontSize: 12,
        color: '#c4c4c4'
    },
    noti_item_image_group : {
        marginLeft: 12,
        width: 24,
        height: 24
    },
    noti_item_image : {
        width: 24,
        height: 24
    }
});

const defaultUsername = "안녕하세요";
const defaultSubUsername = "홍길동";
const defaultProfileImgUri = "../../../assets/profile_dummy.png";
const defaultProfileImg = require(defaultProfileImgUri);

class User_my_main extends  React.Component {
    constructor(props) {
        super(props);
        let profileImg = defaultProfileImg;
        let profileImgUri = defaultProfileImgUri;
        if (Global.user !== null && Global.user.profilePhotoUrl !== "") {
            profileImg = {
                uri: Global.user.profilePhotoUrl,
                cache: 'only-if-cached'
            };
            profileImgUri = Global.user.profilePhotoUrl;
        }
        let username = defaultUsername;
        if (Global.user !== null) {
            username = Global.user.username;
        }
        let usersubname = defaultSubUsername;
        if(Global.user !== null){
            //Todo 확인후 상태정보인지 확인필요
            usersubname = Global.user.usersubname;
        }


        this.state = {
            menu: 1,
            shop_success: 40,
            show_main: true,
            show_noti: false,
            my_info: {
                name: username,
                subname : usersubname,
                profile_img: profileImg
            },
            edit_mode: false,
            tempName: username,
            tempSubName : usersubname,
            profileImg: profileImg,
            profileImgUri: profileImgUri
        };
    }

    checkUsernameInput() {
        // 닉네임 입력 체크
        if (this.state.tempName === '') {
            Alert.alert('닉네임', '닉네임을 입력해주새요.', [{text: "확인", onPress: () => null}]);
            this.refs.user_name.focus();
            return false;
        }

        // 닉네임에 html 태그가 있는지 체크
        if (this.state.tempName.indexOf('<') !== -1 || this.state.tempName.indexOf('>') !== -1 ) {
            Alert.alert('닉네임', '닉네임으로 사용할 수 없는 단어가 포함되어 있습니다.', [{text: "확인", onPress: () => null}]);
            this.refs.user_name.focus();
            return false;
        }

        // 닉네임 비속어 필터링
        if (!Global.checkIsAvailableWord(this.state.tempName)) {
            Alert.alert('닉네임', '닉네임으로 사용할 수 없는 단어가 포함되어 있습니다.', [{text: "확인", onPress: () => null}]);
            this.refs.user_name.focus();
            return false;
        }

        // 상태정보 입력 체크
        if (this.state.tempSubName === '') {
            Alert.alert('상태정보', '상태정보를 입력해주세요.', [{text: "확인", onPress: () => null}]);
            this.refs.user_sub_name.focus();
            return false;
        }

        // 상태정보에 html 태그가 있는지 체크
        if (this.state.tempSubName.indexOf('<') !== -1  || this.state.tempSubName.indexOf('>') !== -1 ) {
            Alert.alert('상태정보', '상태정보으로 사용할 수 없는 단어가 포함되어 있습니다.', [{text: "확인", onPress: () => null}]);
            this.refs.user_sub_name.focus();
            return false;
        }

        // 상태정보 비속어 필터링
        if (!Global.checkIsAvailableWord(this.state.tempSubName)) {
            Alert.alert('상태정보', '상태정보으로 사용할 수 없는 단어가 포함되어 있습니다.', [{text: "확인", onPress: () => null}]);
            this.refs.user_sub_name.focus();
            return false;
        }

        return true;
    }

    onPressSaveBtn() {
        if (!this.checkUsernameInput()) {
            return;
        }
        let usernameChangeCheck = true;
        let usersubnameChangeCheck = true;
        let profileImageChangeCheck = true;
        let info = this.state.my_info;
        if (Global.user !== null && Global.user.username === this.state.tempName) {
            usernameChangeCheck = false;
        }
        if (Global.user !== null && Global.user.usersubname === this.state.tempSubName) {
            usersubnameChangeCheck = false;
        }
        if (Global.user !== null && Global.user.profilePhotoUrl === this.state.profileImgUri) {
            profileImageChangeCheck = false;
        }
        if (!usernameChangeCheck && !usersubnameChangeCheck &&  !profileImageChangeCheck) {
            this.setState({edit_mode: false});
            return;
        }
        info.name = this.state.tempName;
        info.subname = this.state.tempSubName;
        info.profile_img = this.state.profileImg;
        if (Global.user !== null) {
            Global.user.username = this.state.tempName;
            Global.user.usersubname = this.state.tempSubName;
            Global.user.profilePhotoUrl = this.state.profileImgUri !== defaultProfileImgUri ? this.state.profileImgUri : "";
            Global.user.saveToServer();
            Global.user.saveToLocalStorage();
        }
        this.setState({edit_mode: false, my_info: info});
    }

    onPressCancelBtn() {
        this.setState({edit_mode: false});
    }

    onPressEditBtn() {
        this.setState({edit_mode: true, tempName: this.state.my_info.name, tempSubName : this.state.my_info.subname});
    }

    onNameChange(newName,position) {
        if(position === 'sub'){
            this.setState({tempSubName: newName});
        }else{
            this.setState({tempName: newName});
        }
    }

    onPressSupportMail() {
        // 메일 앱 열어서 메일 보내게 하는 함수
        email('support@jaritso.com', { // to 메일 주소
            subject: '[자리있소] 제목을 입력해 주세요.', // default 메일 제목
            body: '내용을 입력해 주세요.' // default 메일 내용
        }).catch(console.error);
    }

    onPressCoupon() {
        if (Global.user === null) {
            Alert.alert('쿠폰', '로그인 해주세요.', [{text: "확인", onPress: () => null}]);
            return;
        }
        if (Global.user.coupon.length === 0) {
            Alert.alert('쿠폰', '보유한 쿠폰이 없습니다.', [{text: "확인", onPress: () => null}]);
            return;
        }
        // todo 쿠폰 확인 기능
    }

    onPressManageAccount() {
        console.log("onPressManageAccount");
        // todo 계정 관리 기능
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={[{}, this.state.show_main ? {} : {position: 'absolute', left: -10000, top: -10000}]}>
                    <View style={styles.Navigator}>
                        <View style={styles.Navigator_title}><Text style={styles.Navigator_title_text}>마이</Text></View>
                        <TouchableHighlight underlayColor={'transparent'}
                                            onPress={() => this.setState({show_noti: true, show_main: false})}>
                            <View style={styles.Navigator_start}>
                                <Image style={styles.Navigator_menu_image}
                                       source={require("../../../assets/alarm.png")}/>
                            </View>
                        </TouchableHighlight>
                    </View>
                    <View style={styles.contents}>
                        <View style={styles.user_info}>
                            <TouchableHighlight underlayColor={'transparent'} onPress={() => this.state.edit_mode ? Global.imageUploader.upload(result =>
                                this.setState({profileImg: Global.imageUploader.toRequiredImage(result), profileImgUri: result.uri})
                            ) : null}>
                                <View style={styles.user_info_image_dom}>
                                    <Image style={styles.user_info_image} source={this.state.profileImg}/>
                                    {/*{this.state.profileImage_dom}*/}
                                </View>
                            </TouchableHighlight>
                            {
                                this.state.edit_mode
                                    ?
                                    <View style={{marginTop: 15}}>
                                        <TextInput ref="user_name" style={styles.user_info_hello_input} placeholder={'이름 입력'}
                                                   underlineColorAndroid='transparent' retrunKeyType="done"
                                                   onChangeText={(text) => this.onNameChange(text,'primary')}
                                                   value={this.state.tempName}/>
                                        <TextInput ref="user_sub_name" style={styles.user_info_name_input} placeholder={'상태정보'}
                                                   underlineColorAndroid='transparent' retrunKeyType="done"
                                                   onChangeText={(text) => this.onNameChange(text,'sub')}
                                                   value={this.state.tempSubName}/>
                                    </View>

                                    :
                                    <View>
                                        <Text style={styles.user_info_hello}>{this.state.my_info.name}</Text>
                                        <Text style={styles.user_info_name}>{this.state.my_info.subname}</Text>
                                    </View>

                            }
                            <View style={styles.touchable_container}>
                                {
                                    this.state.edit_mode
                                        ? <TouchableHighlight underlayColor={'transparent'}
                                                              style={styles.touchable_element}
                                                              onPress={() => this.onPressSaveBtn()}>
                                            <Text style={styles.user_info_save}>저장</Text>
                                        </TouchableHighlight>
                                        : <TouchableHighlight underlayColor={'transparent'}
                                                              style={styles.touchable_element}
                                                              onPress={() => this.onPressEditBtn()}>
                                            <Text style={styles.user_info_edit}>편집</Text>
                                        </TouchableHighlight>
                                }
                                {
                                    this.state.edit_mode
                                        ? <TouchableHighlight underlayColor={'transparent'}
                                                              style={styles.touchable_element}
                                                              onPress={() => this.onPressCancelBtn()}>
                                            <Text style={styles.user_info_cancel}>취소</Text>
                                        </TouchableHighlight>
                                        : <Text style={styles.touchable_element}/>
                                }
                                <Text style={styles.touchable_element}/>
                                <Text style={styles.touchable_element}/>
                                <Text style={styles.touchable_element}/>
                            </View>
                        </View>
                        <View style={styles.userButton_group}>
                            <TouchableHighlight underlayColor={'transparent'}
                                                onPress={() => Actions.User_my_reserved({my_info: this.state.my_info})}>
                                <View style={styles.userButton}>
                                    <Image style={styles.userButton_image_r}
                                           source={require("../../../assets/common/reserv_icon.png")}/>
                                    <Text style={styles.userButton_text}>나의 예약</Text>
                                </View>
                            </TouchableHighlight>
                            <TouchableHighlight underlayColor={'transparent'} onPress={() => Actions.User_my_star({my_info: this.state.my_info})}>
                                <View style={styles.userButton}>
                                    <Image style={styles.userButton_image}
                                           source={require("../../../assets/User/My/star_icon.png")}/>
                                    <Text style={styles.userButton_text}>즐겨찾기</Text>
                                </View>
                            </TouchableHighlight>
                            <TouchableHighlight underlayColor={'transparent'} onPress={() => Actions.User_my_review({my_info: this.state.my_info})}>
                                <View style={styles.userButton}>
                                    <Image style={styles.userButton_image}
                                           source={require("../../../assets/User/My/review_icon.png")}/>
                                    <Text style={styles.userButton_text}>리뷰</Text>
                                </View>
                            </TouchableHighlight>
                            <TouchableHighlight underlayColor={'transparent'}
                                                onPress={() => this.onPressCoupon()}>
                                <View style={styles.userButton}>
                                    <Image style={styles.userButton_image}
                                           source={require("../../../assets/User/My/coupon_icon.png")}/>
                                    <Text style={styles.userButton_text}>쿠폰</Text>
                                </View>
                            </TouchableHighlight>
                        </View>
                        <View style={styles.userButton_group}>
                            <TouchableHighlight underlayColor={'transparent'} onPress={() => this.onPressManageAccount()}>
                                <View style={styles.userButton}>
                                    <Image style={styles.userButton_image_s}
                                           source={require("../../../assets/common/user_icon.png")}/>
                                    <Text style={styles.userButton_text}>계정관리</Text>
                                </View>
                            </TouchableHighlight>
                            <TouchableHighlight underlayColor={'transparent'} onPress={() => Actions.User_my_setting()}>
                                <View style={styles.userButton}>
                                    <Image style={styles.userButton_image}
                                           source={require("../../../assets/alarm.png")}/>
                                    <Text style={styles.userButton_text}>알림 설정</Text>
                                </View>
                            </TouchableHighlight>
                            <TouchableHighlight underlayColor={'transparent'} onPress={() => this.onPressSupportMail()}>
                                <View style={styles.userButton}>
                                    <Image style={styles.userButton_image}
                                           source={require("../../../assets/User/My/helpdesk_icon.png")}/>
                                    <Text style={styles.userButton_text}>고객센터</Text>
                                </View>
                            </TouchableHighlight>
                            <TouchableHighlight underlayColor={'transparent'} onPress={() => null}>
                                <View style={styles.userButton}>
                                    <Image style={styles.userButton_image}
                                           source={require("../../../assets/User/My/ad_icon.png")}/>
                                    <Text style={styles.userButton_text}>광고</Text>
                                </View>
                            </TouchableHighlight>
                        </View>
                        <View style={styles.main_bottom_bar}>
                            <TouchableHighlight underlayColor={'transparent'} onPress={() => Actions.pop()}>
                                <View style={styles.icon_buttom}>
                                    <Image style={styles.icon_buttom_image}
                                           source={require("../../../assets/User/Bottom/home_gray.png")}/>
                                </View>
                            </TouchableHighlight>
                            <TouchableHighlight underlayColor={'transparent'}
                                                onPress={() => Actions.User_my_reserved()}>
                                <View style={styles.icon_buttom}>
                                    <Image style={styles.icon_buttom_image}
                                           source={require("../../../assets/User/Bottom/utensils_gray.png")}/>
                                </View>
                            </TouchableHighlight>
                            <TouchableHighlight underlayColor={'transparent'} onPress={() => Actions.User_my_review()}>
                                <View style={styles.icon_buttom}>
                                    <Image style={styles.icon_buttom_image}
                                           source={require("../../../assets/User/Bottom/chat_gray.png")}/>
                                </View>
                            </TouchableHighlight>
                            <TouchableHighlight underlayColor={'transparent'}
                                                onPress={() => Actions.User_my_contents()}>
                                <View style={styles.icon_buttom}>
                                    <Image style={styles.icon_buttom_image}
                                           source={require("../../../assets/User/Bottom/newspaper_gray.png")}/>
                                </View>
                            </TouchableHighlight>
                            <TouchableHighlight underlayColor={'transparent'} onPress={() => Actions.User_my_main()}>
                                <View style={styles.icon_buttom}>
                                    <Image style={styles.icon_buttom_image}
                                           source={require("../../../assets/User/Bottom/user_black.png")}/>
                                </View>
                            </TouchableHighlight>
                        </View>
                    </View>
                </View>
                <View style={[{}, this.state.show_noti ? {} : {display: 'none', position: 'relative'}]}>
                    <View style={styles.noti_Navigator}>
                        <View style={styles.noti_Navigator_menu}>
                            <TouchableHighlight style={styles.noti_Navigator_menu_button} underlayColor={'transparent'}
                                                onPress={() => this.setState({show_noti: false, show_main: true})}>
                                <Image style={styles.noti_Navigator_menu_image}
                                       source={require("../../../assets/common/circle_x_b.png")}/>
                            </TouchableHighlight>
                        </View>
                        <View style={styles.noti_Navigator_title}><Text
                            style={styles.Navigator_title_text}>알림</Text></View>
                        <TouchableHighlight underlayColor={'transparent'} onPress={() => null}>
                            <View style={styles.noti_Navigator_start}><Text style={styles.Navigator_start_text}/></View>
                        </TouchableHighlight>
                    </View>
                    <View style={[styles.contents, {
                        width: Dimensions.get('window').width,
                        height: Dimensions.get('window').height - (getStatusBarHeight() + 104)
                    }]}>
                        <ScrollView ref="queue">
                            <View style={styles.noti_contents_item}>
                                <View style={styles.noti_contents_item_sub}>
                                    <View style={styles.noti_item_group}>
                                        <Text style={styles.noti_item_top}><Text
                                            style={{fontWeight: 'bold'}}>김민지</Text> 님이 작성하신리뷰에 댓글을 남겼습니다.</Text>
                                        <Text style={styles.noti_item_bottom}> 5시간 전</Text>
                                    </View>
                                </View>
                            </View>
                            <View style={styles.noti_contents_item}>
                                <View style={styles.noti_contents_item_sub}>
                                    <View style={styles.noti_item_group}>
                                        <Text style={styles.noti_item_top}>예약이 성공적으로 되었습니다.</Text>
                                        <Text style={styles.noti_item_bottom}> 5시간 전</Text>
                                    </View>
                                </View>
                            </View>
                            <View style={[styles.noti_contents_item, {backgroundColor: '#f8f8f8'}]}>
                                <View style={[styles.noti_contents_item_sub, {backgroundColor: '#f8f8f8'}]}>
                                    <View style={styles.noti_item_group}>
                                        <Text style={styles.noti_item_top}>방문하신 식당이 즐거우셨나요? 리뷰를 작성해주세요.</Text>
                                        <Text style={styles.noti_item_bottom}> 5시간 전</Text>
                                    </View>
                                </View>
                            </View>
                            <View style={styles.noti_contents_item}>
                                <View style={styles.noti_contents_item_sub}>
                                    <View style={styles.noti_item_group}>
                                        <Text style={styles.noti_item_top}>예약이 성공적으로 되었습니다.</Text>
                                        <Text style={styles.noti_item_bottom}> 5시간 전</Text>
                                    </View>
                                </View>
                            </View>
                            <View style={styles.noti_contents_item}>
                                <View style={styles.noti_contents_item_sub}>
                                    <View style={styles.noti_item_group}>
                                        <Text style={styles.noti_item_top}>예약이 성공적으로 되었습니다.</Text>
                                        <Text style={styles.noti_item_bottom}> 5시간 전</Text>
                                    </View>
                                </View>
                            </View>
                            <View style={styles.noti_contents_item}>
                                <View style={styles.noti_contents_item_sub}>
                                    <View style={styles.noti_item_group}>
                                        <Text style={styles.noti_item_top}>예약이 성공적으로 되었습니다.</Text>
                                        <Text style={styles.noti_item_bottom}> 5시간 전</Text>
                                    </View>
                                </View>
                            </View>
                        </ScrollView>
                        <View style={styles.main_bottom_bar}>
                            <TouchableHighlight underlayColor={'transparent'} onPress={() => Actions.pop()}>
                                <View style={styles.icon_buttom}>
                                    <Image style={styles.icon_buttom_image}
                                           source={require("../../../assets/User/Bottom/home_gray.png")}/>
                                </View>
                            </TouchableHighlight>
                            <TouchableHighlight underlayColor={'transparent'}
                                                onPress={() => Actions.User_my_reserved()}>
                                <View style={styles.icon_buttom}>
                                    <Image style={styles.icon_buttom_image}
                                           source={require("../../../assets/User/Bottom/utensils_gray.png")}/>
                                </View>
                            </TouchableHighlight>
                            <TouchableHighlight underlayColor={'transparent'} onPress={() => Actions.User_my_review()}>
                                <View style={styles.icon_buttom}>
                                    <Image style={styles.icon_buttom_image}
                                           source={require("../../../assets/User/Bottom/chat_gray.png")}/>
                                </View>
                            </TouchableHighlight>
                            <TouchableHighlight underlayColor={'transparent'}
                                                onPress={() => Actions.User_my_contents()}>
                                <View style={styles.icon_buttom}>
                                    <Image style={styles.icon_buttom_image}
                                           source={require("../../../assets/User/Bottom/newspaper_gray.png")}/>
                                </View>
                            </TouchableHighlight>
                            <TouchableHighlight underlayColor={'transparent'} onPress={() => Actions.User_my_main()}>
                                <View style={styles.icon_buttom}>
                                    <Image style={styles.icon_buttom_image}
                                           source={require("../../../assets/User/Bottom/user_black.png")}/>
                                </View>
                            </TouchableHighlight>
                        </View>
                    </View>
                </View>
            </View>
        )
    }
}

export default User_my_main;