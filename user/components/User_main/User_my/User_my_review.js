import React from 'react';
import {
    Text,
    View,
    StyleSheet,
    Image,
    TextInput,
    TouchableHighlight,
    Dimensions,
    ScrollView,
    Menu,
    Alert
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import ActionButton from 'react-native-action-button';
import {Ionicons} from '@expo/vector-icons';
import API from "../../../objects/API";
import Global from "../../../objects/Global";

var commaNumber = require('comma-number');

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f9f9f9',
        // backgroundColor: '#fff',
    },
    Navigator : {
        // flexDirection: 'row',
        marginTop : getStatusBarHeight(),
        width: Dimensions.get('window').width,
        height: 92,
        backgroundColor: '#fff',
    },
    Navigator_title: {
        fontSize: 24,
        marginTop:18,
        marginLeft: 14
    },
    Navigator_sub : {
        marginTop: 14,
        marginLeft: 14,
        color: '#90d04e',
        fontSize: 12,
        marginBottom: 18
    },
    contents : {
        // height: (Dimensions.get('window').height - (104 + getStatusBarHeight())),
        // paddingTop : (Dimensions.get('window').height / 10),
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    main_bottom_bar : {
        flexDirection: 'row',
        width: Dimensions.get('window').width,
        height: 56,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    icon_buttom:{
        width: Dimensions.get('window').width / 5,
        height: 56,
        alignItems: 'center'
    },
    icon_buttom_image :{
        marginTop: 19,
        width: 18,
        height: 18,
        resizeMode: 'contain'
    },
    request_box: {

    },
    request_start : {
        width: 280,
        height:304,
    },
    request_btn : {
        width: 280,
        height: 56,
        backgroundColor: '#eb5847',
        fontSize: 20,
        paddingTop: 13.5,
        color: '#ffffff',
        alignItems: 'center',
        textAlign: 'center',
        fontWeight: 'bold'
    },
    reload_box : {
        marginTop: 35,
        marginBottom: 17,
        width: 137,
        height: 32,
        backgroundColor: 'rgba(255, 255, 255, 0.8)',
        borderWidth: 1,
        borderColor : '#f2f2f2',
        borderRadius: 16,
        flexDirection: 'row',
    },
    reload_name :{
        width: 104,
        fontSize: 12,
        color: '#606060',
        paddingLeft:20,
        paddingTop:6
    },
    reload_refresh : {
        width: 16,
        height: 16,
        marginLeft: 8,
        marginTop: 8
    },
    contents_where : {
        width: Dimensions.get('window').width - 24,
        marginLeft: 12,
        fontSize: 16,
        paddingTop: 10,
        paddingBottom: 10,
        color: '#000000'
    },
    contents_sub : {
        width: Dimensions.get('window').width,
        paddingLeft: 12,
        marginBottom: 8,
        // paddingRight: 12,
        height: 220,
        backgroundColor: '#ffffff'
    },
    contents_profile : {
        height: 25,
        marginTop: 16,
        flexDirection: 'row'
    },
    contents_profile_image : {
        width: 28,
        height: 28
    },
    contents_profile_name : {
        fontSize: 16,
        marginLeft: 10,
        // lineHeight: 20,
        marginTop: 4
    },
    contents_profile_date :{
        marginTop: 8,
        fontSize: 10,
        // lineHeight: 20,
        color: '#c4c4c4',
        marginLeft: 4
    },
    contents_info : {
        marginTop: 12,
        width: Dimensions.get('window').width - 24,
        fontSize: 14,
        color: '#606060'
    },
    contents_extra : {
        fontSize:12 ,
        lineHeight: 20,
        color: '#606060'
    },
    contents_extra_2 : {
        fontSize:12 ,
        lineHeight: 20,
        color: '#606060',
    },
    contents_extra_image : {
        width: 20,
        height: 20
    },
    contents_store_icon : {
        width: 16,
        height: 16
    },
    contents_store : {
        width: Dimensions.get('window').width - 24,
        height: 56,
        flexDirection: 'row',
        borderWidth: 1,
        borderColor: 'rgba(0, 0, 0, 0.1)'
    },
    contents_store_left : {
        width: Dimensions.get('window').width - 74,
        paddingLeft: 20,
        paddingTop: 8,
    },
    contents_store_right : {
        width: 9,
        height:9,
        marginLeft: 16,
        marginRight: 23,
        marginTop: 23
    },
    contents_store_left_title : {
        fontSize: 16
    },
    contents_store_left_extra :{
        paddingLeft: 2,
        fontSize: 10,
        lineHeight: 16
    },
    input_t2 : {
        width: '100%',
        height: 48,
        marginBottom: 10,
    }
});

class User_request_start extends  React.Component {
    constructor(props) {
        super(props);
        this.state = {
            text: '',
            biz_shop_data: null,
            shops: [
                {
                    shopId: 0,
                    name: "레스토랑 명 입력자리",
                    likeCount: 35,
                    bookmarkCount: 12353
                }
            ],
            reviews: [
                {
                    reviewId: 0,
                    reviewerName: "김민지",
                    date: "2일전",
                    content: "냠냠파전은 바삭바삭한 끝 부분을 살리고 해산물을 가득살린 메뉴",
                    likeCount: 135,
                    comment: [
                        {
                            commentUser : {
                                profilePhotoUrl : require("../../../assets/profile_dummy.png"),
                                nickname: "김민지"
                            },
                            commentTime: "2일전",
                            content: "냠냠파전은 바삭바삭한 끝에부분을 살리고 해산물을 가득살린 메뉴"
                        },
                        {
                            commentUser : {
                                profilePhotoUrl : require("../../../assets/profile_dummy.png"),
                                nickname: "김민지"
                            },
                            commentTime: "2일전",
                            content: "냠냠파전은 바삭바삭한 끝에부분을 살리고 해산물을 가득살린 메뉴"
                        }
                    ],
                    shopId: 0,
                    like: false,
                    writeComment: false
                },
                {
                    reviewId: 1,
                    reviewerName: "김민지",
                    date: "2일전",
                    content: "냠냠파전은 바삭바삭한 끝 부분을 살리고 해산물을 가득살린 메뉴",
                    likeCount: 135,
                    comment: [
                        {
                            commentUser : {
                                profilePhotoUrl : require("../../../assets/profile_dummy.png"),
                                nickname: "김민지"
                            },
                            commentTime: "2일전",
                            content: "냠냠파전은 바삭바삭한 끝에부분을 살리고 해산물을 가득살린 메뉴"
                        }
                    ],
                    shopId: 0,
                    like: false,
                    writeComment: false
                },
                {
                    reviewId: 2,
                    reviewerName: "김민지",
                    date: "2일전",
                    content: "냠냠파전은 바삭바삭한 끝 부분을 살리고 해산물을 가득살린 메뉴",
                    likeCount: 135,
                    comment: [
                        {
                            commentUser : {
                                profilePhotoUrl : require("../../../assets/profile_dummy.png"),
                                nickname: "김민지"
                            },
                            commentTime: "2일전",
                            content: "냠냠파전은 바삭바삭한 끝에부분을 살리고 해산물을 가득살린 메뉴"
                        }
                    ],
                    shopId: 0,
                    like: false,
                    writeComment: false
                },
                {
                    reviewId: 3,
                    reviewerName: "김민지",
                    date: "2일전",
                    content: "냠냠파전은 바삭바삭한 끝 부분을 살리고 해산물을 가득살린 메뉴",
                    likeCount: 135,
                    comment: [
                        {
                            commentUser : {
                                profilePhotoUrl : require("../../../assets/profile_dummy.png"),
                                nickname: "김민지"
                            },
                            commentTime: "2일전",
                            content: "냠냠파전은 바삭바삭한 끝에부분을 살리고 해산물을 가득살린 메뉴"
                        }
                    ],
                    shopId: 0,
                    like: false,
                    writeComment: false
                },
                {
                    reviewId: 4,
                    reviewerName: "김민지",
                    date: "2일전",
                    content: "냠냠파전은 바삭바삭한 끝 부분을 살리고 해산물을 가득살린 메뉴",
                    likeCount: 135,
                    comment: [
                        {
                            commentUser : {
                                profilePhotoUrl : require("../../../assets/profile_dummy.png"),
                                nickname: "김민지"
                            },
                            commentTime: "2일전",
                            content: "냠냠파전은 바삭바삭한 끝에부분을 살리고 해산물을 가득살린 메뉴"
                        }
                    ],
                    shopId: 0,
                    like: false,
                    writeComment: false
                },
                {
                    reviewId: 5,
                    reviewerName: "김민지",
                    date: "2일전",
                    content: "냠냠파전은 바삭바삭한 끝 부분을 살리고 해산물을 가득살린 메뉴",
                    likeCount: 135,
                    comment: [
                        {
                            commentUser : {
                                profilePhotoUrl : require("../../../assets/profile_dummy.png"),
                                nickname: "김민지"
                            },
                            commentTime: "2일전",
                            content: "냠냠파전은 바삭바삭한 끝에부분을 살리고 해산물을 가득살린 메뉴"
                        }
                    ],
                    shopId: 0,
                    like: false,
                    writeComment: false
                }
            ]
        };
        // todo 내가 쓴 리뷰를 조회해 오자
    }

    searchNearShopsReview() {
        // 근처 식당들의 리뷰
        let reviews = [];
        const sortReviews = () => {
            // todo 시간순으로 정렬
        };
        const getNearShops = (latitude, longitude) => {
            API.shop.getNearShops(latitude, longitude, data => {
                // todo 리뷰들을 reviews에 넣고 sortReviews 호출
            }, error => {

            });
        };
        navigator.geolocation.getCurrentPosition(
            (position) => {
                if (position != null) {
                    getNearShops(position.coords.latitude, position.coords.longitude);
                } else {
                    getNearShops(37.517204, 127.041289);
                }
                this.requestReservation();
            }, (error) => getNearShops(37.517204, 127.041289)
        );
    }

    onPressLikeReview(review) {
        let reviews = this.state.reviews;
        let idx = reviews.findIndex(r => r.reviewId === review.reviewId);
        if (idx === -1) {
            return false;
        }
        if (reviews[idx].like) {
            reviews[idx].likeCount--;
            reviews[idx].like = false;
            this.likeReview('DELETE', reviews[idx]);
        } else {
            reviews[idx].likeCount++;
            reviews[idx].like = true;
            this.likeReview('POST', reviews[idx]);
        }
        this.setState({reviews: reviews});
    }

    likeReview(method, review) {
        if (Global.user === null) {
            return;
        }
        if (method === 'POST') {
            API.revision2.likeReview.post(review.shopId, review.reviewId, data => {

            }, error => {
                Alert.alert('좋아요', '서버문제로 리뷰를 좋아요 하는데 실패하였습니다. \n' + error, [{
                    text: "확인",
                    onPress: () => Actions.pop()
                }]);
            });
        } else if (method === 'DELETE') {
            API.revision2.likeReview.delete(review.shopId, review.reviewId, data => {

            }, error => {
                Alert.alert('좋아요', '서버문제로 리뷰를 좋아요를 취소하는데 실패하였습니다. \n' + error, [{
                    text: "확인",
                    onPress: () => Actions.pop()
                }]);
            });
        }
    }

    onPressCommentToReview(review) {
        let reviews = this.state.reviews;
        let idx = reviews.findIndex(r => r.reviewId === review.reviewId);
        if (idx === -1) {
            return false;
        }
        reviews[idx].writeComment = !reviews[idx].writeComment;
        this.setState({reviews: reviews, text: ''});
    }

    onPressEndWriteComment(review) {
        if (this.state.text !== '') {
            API.revision2.review.post(review.shopId, review.reviewId, data => {}, error => {
                Alert.alert('작성오류', '작성된 리뷰를 서버에 오르는 과정에서 오류가 발생하였습니다. \n' + error, [{
                    text: "확인",
                    onPress: () => Actions.pop()
                }]);
            });
        }
        let reviews = this.state.reviews;
        let idx = reviews.findIndex(r => r.reviewId === review.reviewId);
        if (idx === -1) {
            return false;
        }
        reviews[idx].writeComment = false;
        if (this.state.text !== '') {
            reviews[idx].comment.push({
                commentUser : {
                    profilePhotoUrl : require("../../../assets/profile_dummy.png"),
                    nickname: "김민지"
                },
                commentTime: "2일전",
                content: "냠냠파전은 바삭바삭한 끝에부분을 살리고 해산물을 가득살린 메뉴"
            });
        }
        this.setState({reviews: reviews, text: ''});
    }

    printReview(review) {
        const shopIdx = this.state.shops.findIndex(s => s.shopId === review.shopId);
        if (shopIdx !== -1) {
            const shop = this.state.shops[shopIdx];
            return <View key={review.reviewId} style={[styles.contents_sub, review.writeComment ? {height: 308} : {height: 220}]}>
                <View style={styles.contents_profile}>
                    <Image style={styles.contents_profile_image}
                           source={require("../../../assets/profile_dummy.png")}/>
                    <Text style={styles.contents_profile_name}>{review.reviewerName}</Text>
                    <Text style={styles.contents_profile_date}>{review.date}</Text>
                </View>
                <TouchableHighlight underlayColor={'transparent'}
                                    onPress={() => Actions.User_my_review_view({review: review, shop: shop})}>
                    <Text style={styles.contents_info}>{review.content}</Text>
                </TouchableHighlight>
                <View style={{flexDirection: 'row', paddingTop: 16, paddingBottom: 16}}>
                    <TouchableHighlight underlayColor={'transparent'} onPress={() => this.onPressLikeReview(review)}>
                        <Image style={styles.contents_extra_image}
                               source={review.like ? require("../../../assets/common/heart.png") : require("../../../assets/common/heart_clear_icon.png")}/>
                    </TouchableHighlight>
                    <TouchableHighlight underlayColor={'transparent'} onPress={() => this.onPressLikeReview(review)}>
                        <Text stlye={styles.contents_extra}> {review.likeCount} </Text>
                    </TouchableHighlight>
                    <TouchableHighlight underlayColor={'transparent'} onPress={() => this.onPressCommentToReview(review)}>
                        <Image style={styles.contents_extra_image}
                               source={require("../../../assets/common/reply_icon.png")}/>
                    </TouchableHighlight>
                    <TouchableHighlight underlayColor={'transparent'} onPress={() => this.onPressCommentToReview(review)}>
                        <Text stlye={styles.contents_extra}> {review.comment.length}</Text>
                    </TouchableHighlight>
                </View>
                <View style={review.writeComment ? {} : {display: 'none'}}>
                    <TextInput style={styles.input_t2} multiline={true} placeholder={'댓글 내용을 입력해주세요.'} underlineColorAndroid='transparent' returnKeyType="done" value={this.state.text} onChangeText = {(text) => this.setState({text: text})}/>
                    <TouchableHighlight underlayColor={'transparent'} onPress={() => this.onPressEndWriteComment(review)}>
                        <Text stlye={styles.contents_extra_2}> 작성 완료</Text>
                    </TouchableHighlight>
                    <Text stlye={styles.contents_extra_2}> </Text>
                </View>
                <TouchableHighlight underlayColor={'transparent'}
                                    onPress={() => Actions.User_request_step5()}>
                    <View style={styles.contents_store} shadownColor={"rgba(0, 0, 0, 0.1)"}
                          shadowOffset={{width: 20, height: 20}}>
                        <View style={styles.contents_store_left}>
                            <Text style={styles.contents_store_left_title}>{shop.name}</Text>
                            <View style={{flexDirection: 'row'}}>
                                <Image style={styles.contents_store_icon}
                                       source={require("../../../assets/common/silver_heart.png")}/>
                                <Text style={styles.contents_store_left_extra}>{shop.likeCount} </Text>
                                <Image style={styles.contents_store_icon}
                                       source={require("../../../assets/common/silver_bookmark.png")}/>
                                <Text style={styles.contents_store_left_extra}>{commaNumber(shop.bookmarkCount)}</Text>
                            </View>
                        </View>
                        <Image style={styles.contents_store_right}
                               source={require("../../../assets/common/arrow_right.png")}/>
                    </View>
                </TouchableHighlight>
            </View>;
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.Navigator}>
                    <Text style={styles.Navigator_title}>리뷰</Text>
                    <Text style={styles.Navigator_sub}>현재 계신곳 근처의 식당들의 리뷰가 실시간으로 올라오고 있습니다.</Text>
                </View>
                <ScrollView>
                    <View style={styles.contents}>
                        <Text style={styles.contents_where}>1KM 내의 리뷰</Text>
                        {
                            this.state.reviews.map(review => this.printReview(review))
                        }
                    </View>
                </ScrollView>
                <ActionButton hideShadow={false} style={{marginBottom: 56}} buttonColor={'#eb5847'}
                              onPress={() => Actions.User_my_review_add({near_shop_list: this.state.shops})}>
                </ActionButton>
                <View style={styles.main_bottom_bar}>
                    <TouchableHighlight underlayColor={'transparent'} onPress={() => Actions.pop()}>
                        <View style={styles.icon_buttom}>
                            <Image style={styles.icon_buttom_image}
                                   source={require("../../../assets/User/Bottom/home_gray.png")}/>
                        </View>
                    </TouchableHighlight>
                    <TouchableHighlight underlayColor={'transparent'} onPress={() => null}>
                        <View style={styles.icon_buttom}>
                            <Image style={styles.icon_buttom_image}
                                   source={require("../../../assets/User/Bottom/utensils_gray.png")}/>
                        </View>
                    </TouchableHighlight>
                    <TouchableHighlight underlayColor={'transparent'} onPress={() => null}>
                        <View style={styles.icon_buttom}>
                            <Image style={styles.icon_buttom_image}
                                   source={require("../../../assets/User/Bottom/chat_black.png")}/>
                        </View>
                    </TouchableHighlight>
                    <TouchableHighlight underlayColor={'transparent'} onPress={() => Actions.User_my_contents()}>
                        <View style={styles.icon_buttom}>
                            <Image style={styles.icon_buttom_image}
                                   source={require("../../../assets/User/Bottom/newspaper_gray.png")}/>
                        </View>
                    </TouchableHighlight>
                    <TouchableHighlight underlayColor={'transparent'} onPress={() => Actions.User_my_main()}>
                        <View style={styles.icon_buttom}>
                            <Image style={styles.icon_buttom_image}
                                   source={require("../../../assets/User/Bottom/user_gray.png")}/>
                        </View>
                    </TouchableHighlight>
                </View>
            </View>
        )
    }
}

export default User_request_start;
