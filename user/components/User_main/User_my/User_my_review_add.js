import React from 'react';
import {Text, View, StyleSheet, Image,TextInput,TouchableHighlight, Dimensions, Alert ,Menu} from 'react-native';
import { Actions } from 'react-native-router-flux';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import {Ionicons} from '@expo/vector-icons';
import Global from "../../../objects/Global";
import API from "../../../objects/API";


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f9f9f9',
        // backgroundColor: '#fff',
    },
    Navigator : {
        flexDirection: 'row',
        paddingTop : getStatusBarHeight(),
        width: Dimensions.get('window').width,
        height: 48 + getStatusBarHeight(),
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    Navigator_menu: {
        width: 80,
        height: 48,
    },
    Navigator_menu_button :{
        width: 48,
        height: 48,
        paddingTop: 12,
        paddingLeft: 12,
    },
    Navigator_menu_image : {
        width: 24,
        height: 24,
    },
    Navigator_title:{
        width : ((Dimensions.get('window').width) - 160),
        alignItems: 'center',
    },
    Navigator_title_text:{
        fontSize: 16,
        color:'#000000'
    },
    Navigator_start:{
        alignItems: 'flex-end',
        width: 80,
        height: 48,
        paddingTop: 12,
        paddingRight: 12,
    },
    Navigator_start_text:{
        fontSize: 16,
        color:'#7db840',
    },
    callout_box : {
        width : (Dimensions.get('window').width),
        height: 40,
        borderRadius: 2,
        backgroundColor: '#7f8fd1',
        alignItems: 'center',
        justifyContent: 'center',
        shadowOffset: {width: 2, height: 10 },
        shadowColor: 'rgba(0, 0, 0, 0.2)',
        paddingRight: 56
    },
    callout_box_text : {
        fontSize : 12,
        color: '#ffffff'
    },
    center_button:{
        width: Dimensions.get('window').width,
        height: ((Dimensions.get('window').height) - 48),
        alignItems: 'center',
        justifyContent: 'center',
    },
    buttom_button:{
        position: 'absolute',
        marginBottom : ((Dimensions.get('window').height /  2) - 120)
    },
    add_button:{
        width: Dimensions.get('window').width,
        height: 56,
    },
    add_button_text : {
        position: 'absolute',
        left: ((Dimensions.get('window').width / 2) - '2%'),
        top : ((Dimensions.get('window').height /  2) + 30)
    },
    userButton_group : {
        width: Dimensions.get('window').width,
        height: 81,
        paddingTop :  9,
        flexDirection: 'row',
    },
    userButton :{
        width: Dimensions.get('window').width / 4,
        height: 72,
        backgroundColor: '#fff',
        alignItems: 'center',
        flex: 1,
        paddingTop: 16
    },
    userButton_image: {
        width: 24,
        height: 24
    },
    userButton_text: {
        fontSize: 14
    },
    main_bottom_bar : {
        flexDirection: 'row',
        width: Dimensions.get('window').width,
        height: 56,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    icon_buttom:{
        width: Dimensions.get('window').width / 5,
        height: 56,
        alignItems: 'center'
    },
    icon_buttom_image :{
        marginTop: 19,
        width: 18,
        height: 18,
        resizeMode: 'contain'
    },
    contents : {
        paddingTop :  9,
        height: (Dimensions.get('window').height - (104 + getStatusBarHeight())),
    },
    contents_item: {
        width: (Dimensions.get('window').width - 40),
        height: 96,
        backgroundColor: '#ffffff',
        marginLeft: 20,
        marginBottom: 10,
        flexDirection: 'row'
    },
    imgicon: {
        width: 30,
        height: 30,
        resizeMode: 'contain'
    },
    star_image_dom: {
        width: 90,
        height: 96,
        paddingLeft:6,
        paddingTop:6
    },
    star_image:{
        width:84,
        height: 84
    },
    star_info: {
        width: (Dimensions.get('window').width - 124),
        height: 96,
        paddingLeft: 20
    },
    star_info_memter : {
        paddingLeft: (Dimensions.get('window').width - 194),
        fontSize : 12,
        color : '#7f8fd1',
        marginTop: 6,
    },
    star_info_store_name : {
        fontSize:12,
        color: '#7db840'
    },
    star_info_food_name : {
        fontSize: 20
    },
    star_info_extra : {
        fontSize: 12
    },
    store_more : {
        width: (Dimensions.get('window').width),
        flexDirection: 'row',
        height: 56,
        paddingLeft: 12,
        paddingRight: 12,
        marginTop: 12,
        backgroundColor: '#fff'
    },
    store_more_text : {
        marginTop: 12,
        width: (Dimensions.get('window').width - 48),
        fontSize: 16,
        lineHeight: 32
    },
    store_more_arrow : {
        marginRight: 21.3,
        marginTop: 22,
        width: 5.7,
        height: 11.3
    },
    inputtext : {
        marginTop: 8,
        width: (Dimensions.get('window').width),
        height: (Dimensions.get('window').height - (328 + getStatusBarHeight())),
        paddingLeft: 12,
        paddingRight: 12,
        paddingTop: 7,
        backgroundColor: '#fff',
        textAlignVertical: 'top'
    },
    add_image_box : {
        width: 108,
        height: 108,
        marginRight: 6
    },
    add_image_box_group : {
        width: (Dimensions.get('window').width - 12),
        paddingLeft: 12,
        flexDirection: 'row',
        // alignItems: 'center',
        // justifyContent: 'center',
        backgroundColor: '#fff'
    },
    add_image_box_sub_group : {
      flexDirection : 'row',
    },
    add_image_box_cross : {
        // position: 'absolute',
        marginTop: 100,

        width: 24,
        height: 24,
        marginLeft: -32,
        // marginTop: -54,
    }

});

class User_my_review_add extends  React.Component {
    constructor(props) {
        super(props);
        this.state = {
            text: '',
            biz_shop_data: null,
            image: null,
            imagelist: null,
            upload_image: [null, null, null]
        };
        this.serchNear();
        // Todo. 방문하지 않은 가게의 리뷰도 등록이 가능한 것인가?
    }

    serchNear() {
        navigator.geolocation.getCurrentPosition(
            (position) => {
                if (position != null) {
                    this.getNearShops(position.coords.latitude, position.coords.longitude);
                } else {
                    this.getNearShops(37.517204, 127.041289);
                }
                this.requestReservation();
            }, (error) => this.getNearShops(37.517204, 127.041289)
        );
    }

    getNearShops(latitude, longitude) {
        API.shop.getNearShops(latitude, longitude, data => {
            // todo 여기서는 이름만 가져오면 될듯
        }, error => {

        });
    }

    near_shop_list = this.props.near_shop_list;

    static delete_image(id) {
        let uploaded = this.state.upload_image.slice();
        for (let i = uploaded.length - 1; i >= id; i--) {
            if (i === uploaded.length - 1) {
                uploaded[i] = null;
            } else {
                uploaded[i] = uploaded[i + 1];
            }
        }
        this.setState({upload_image: uploaded});
    }

    displayImage(image, idx) {
        return image !== null && <View key={image} style={styles.add_image_box_sub_group}>
            <Image style={styles.add_image_box_cross} ource={require("../../../assets/common/x_icon.png")}/>
            <TouchableHighlight underlayColor={'transparent'} onPress={() => this.delete_image({idx})}>
                <Image style={styles.add_image_box} source={{uri: image}}/>
            </TouchableHighlight>
        </View>;
    }
    write_review(){
        Actions.User_my_review_add_end({command : this.state.text, image : this.state.upload_image, shopData : this.state.biz_shop_data });
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.Navigator}>
                    <View style={styles.Navigator_menu}>
                        <TouchableHighlight style={styles.Navigator_menu_button} underlayColor={'transparent'}
                                            onPress={() => Actions.pop()}>
                            <Image style={styles.Navigator_menu_image}
                                   source={require("../../../assets/back_arrow.png")}/>
                        </TouchableHighlight>
                    </View>
                    <View style={styles.Navigator_title}><Text style={styles.Navigator_title_text}>글쓰기</Text></View>
                    <TouchableHighlight underlayColor={'transparent'} onPress={() => this.write_review()}>
                        <View style={styles.Navigator_start}><Text
                            style={styles.Navigator_start_text}>작성완료</Text></View>
                    </TouchableHighlight>
                </View>
                <View style={styles.contents}>
                    <TouchableHighlight underlayColor={'transparent'} onPress={() => null}>
                        <View style={styles.store_more}>
                            {/*이거 근처 shop중에서 하나 선택할 수 있게 해야할거 같은데...?*/}
                            <Text style={styles.store_more_text}>온더보더 잠실점</Text>
                            <Image style={styles.store_more_arrow}
                                   source={require("../../../assets/common/arrow_right.png")}/>
                        </View>
                    </TouchableHighlight>
                    <TextInput style={styles.inputtext} multiline={true} placeholder={"리뷰내용을 입력해주세요."}
                               underlineColorAndroid={'transparent'}  onChangeText={(text) => this.setState({text})}/>
                    <TouchableHighlight underlayColor={'transparent'} onPress={() => Global.imageUploader.upload(result => {
                        let uploaded = this.state.upload_image.slice();
                        let addCheck = false;
                        for (let i = 0; i < uploaded.length; i++) {
                            if (uploaded[i] === null && !addCheck) {
                                uploaded[i] = result.uri;
                                addCheck = true;
                                break;
                            }
                        }
                        if (addCheck) {
                            this.setState({upload_image: uploaded});
                        } else {
                            Alert.alert(
                                '업로드 초과',
                                '최대 ' + uploaded.length + '개까지만 업로드가 됩니다.',
                                [
                                    {text: "확인", onPress: () => null}
                                ]
                            );
                        }
                    })}>
                        <View style={styles.store_more}>
                            <Text style={styles.store_more_text}> 사진첨부</Text>
                            <Image style={styles.store_more_arrow}
                                   source={require("../../../assets/common/arrow_right.png")}/>
                        </View>
                    </TouchableHighlight>
                    <View style={styles.add_image_box_group}>
                        {
                            this.state.upload_image.map(this.displayImage)
                        }
                    </View>
                </View>
            </View>
        )
    }
}

export default User_my_review_add;
