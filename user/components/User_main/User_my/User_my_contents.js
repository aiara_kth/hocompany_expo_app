import React from 'react';
import {Text, View, StyleSheet, Image,TouchableHighlight, Dimensions, ScrollView, Menu} from 'react-native';
import { Actions } from 'react-native-router-flux';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import {Ionicons} from '@expo/vector-icons';
import '../../../objects/Global.js';
import API from '../../../objects/API';
import Global from "../../../objects/Global";

var commaNumber = require('comma-number');


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f9f9f9',
        // backgroundColor: '#fff',
    },
    Navigator : {
        // flexDirection: 'row',
        marginTop : getStatusBarHeight(),
        width: Dimensions.get('window').width,
        height: 92,
        backgroundColor: '#fff',
    },
    Navigator_title: {
        fontSize: 24,
        marginTop:18,
        marginLeft: 14
    },
    Navigator_sub : {
        marginTop: 14,
        marginLeft: 14,
        color: '#90d04e',
        fontSize: 12,
        marginBottom: 9
    },
    contents : {
        // height: (Dimensions.get('window').height - (104 + getStatusBarHeight())),
        // paddingTop : (Dimensions.get('window').height / 10),
        marginTop: 9,
        backgroundColor : '#ffffff',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    main_bottom_bar : {
        flexDirection: 'row',
        width: Dimensions.get('window').width,
        height: 56,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    icon_buttom:{
        width: Dimensions.get('window').width / 5,
        height: 56,
        alignItems: 'center'
    },
    icon_buttom_image :{
        marginTop: 19,
        width: 18,
        height: 18,
        resizeMode: 'contain'
    },
    request_box: {

    },
    request_start : {
        width: 280,
        height:304,
    },
    request_btn : {
        width: 280,
        height: 56,
        backgroundColor: '#eb5847',
        fontSize: 20,
        paddingTop: 13.5,
        color: '#ffffff',
        alignItems: 'center',
        textAlign: 'center',
        fontWeight: 'bold'
    },
    reload_box : {
        marginTop: 35,
        marginBottom: 17,
        width: 137,
        height: 32,
        backgroundColor: 'rgba(255, 255, 255, 0.8)',
        borderWidth: 1,
        borderColor : '#f2f2f2',
        borderRadius: 16,
        flexDirection: 'row',
    },
    reload_name :{
        width: 104,
        fontSize: 12,
        color: '#606060',
        paddingLeft:20,
        paddingTop:6
    },
    reload_refresh : {
        width: 16,
        height: 16,
        marginLeft: 8,
        marginTop: 8
    },
    contents_where : {
        width: Dimensions.get('window').width - 24,
        marginLeft: 12,
        fontSize: 16,
        color: '#000000'
    },
    contents_sub : {
        width: Dimensions.get('window').width - 24,
        marginLeft: 12,
        height: 220
    },
    contents_profile : {
        height: 25,
        marginTop: 16,
        flexDirection: 'row'
    },
    contents_profile_image : {
        width: 28,
        height: 28
    },
    contents_profile_name : {
        fontSize: 16,
        marginLeft: 10,
        marginTop: 4
    },
    contents_profile_date :{
        marginTop: 8,
        fontSize: 10,
        color: '#c4c4c4',
        marginLeft: 4
    },
    contents_info : {
        marginTop: 10,
        width: Dimensions.get('window').width - 36,
        fontSize: 14,
        color: '#606060'
    },
    contents_extra : {
        marginTop: 16,
        fontSize:12 ,
        color: '#606060'
    },
    contents_extra_image : {
        width: 20,
        height: 20
    },
    contents_store_icon : {
        width: 16,
        height: 16
    },
    contents_store : {
        width: Dimensions.get('window').width - 36,
        height: 56,
        flexDirection: 'row',
        borderWidth: 2,
        borderColor: 'rgba(0, 0, 0, 0.1)'
    },
    contents_store_left : {
        width: Dimensions.get('window').width - 74,
        paddingLeft: 20,
        paddingTop: 8,
    },
    contents_store_right : {
        width: 9,
        height:9,
        marginLeft: 16,
        marginRight: 23,
        marginTop: 23
    },
    contents_store_left_title : {
        fontSize: 16
    },
    contents_store_left_extra :{
        fontSize: 10
    },
    contents_horizontal_scroll_item : {
        width: 328,
        height: 316,
        paddingLeft: 12,
        paddingRight:12,
    },
    contents_horizontal_scroll_item2 : {
        width: 336,
        height: 316
    },
    contents_horizontal_scroll_item_s : {
        width: 316,
        height: 304,
        paddingLeft: 12,
        paddingRight:12,
    },
    contents_horizontal_scroll_item_s2 : {
        width: 336,
        height: 304,
    },
    contents_horizontal_scroll_item_image : {
        width: 336,
        height: 200,
        marginLeft: 0,
        marginTop: 20
    },
    contents_horizontal_scroll_item_info : {

    },
    star_info: {
        width: (Dimensions.get('window').width - 124),
        height: 96,
        paddingLeft: 20
    },
    contents_horizontal_scroll_item_info_memter : {
        fontSize : 12,
        color : '#7f8fd1',
        marginTop: 6,
        textAlign: 'center',
        alignItems: 'center',
        justifyContent: 'center'
    },
    contents_horizontal_scroll_item_info_memter2 : {
        fontSize : 12,
        color : '#7f8fd1',
        marginTop: 6,
        textAlign: 'center',
        alignItems: 'center',
        justifyContent: 'center'
    },
    contents_horizontal_scroll_item_info_store_name : {
        fontSize:16,
        color: '#1d212e'
    },
    contents_horizontal_scroll_item_info_addr_name : {
        fontSize: 10,
        lineHeight: 16,
        color: '#b6b6b6'
    },
    contents_horizontal_scroll_item_info_extra : {
        fontSize: 12
    },
    imgicon: {
        width: 16,
        height: 16,
        resizeMode : 'contain'
    },
    imgicon_ios: {
        width: 12,
        height: 12,
        resizeMode : 'contain'
    },
    contents_vertical_scroll_item : {
        width: 360,
        height: 316,
        marginLeft: (Dimensions.get('window').width - 360) / 2
    },
    contents_vertical_scroll_item_s : {
        width: 340,
        height: 316,
        marginLeft: 20,
        paddingRight:20,
    },
    contents_vertical_scroll_item_image : {
        width: 336  ,
        height: 200,
        marginLeft : 12,
    },
    contents_vertical_scroll_item_info_memter : {
        width: 360,
        paddingRight: 12,
        fontSize : 12,
        color : '#7f8fd1',
        marginTop: 6,
        textAlign: 'center',
        alignItems: 'center',
        justifyContent: 'center'
    },



});

const loadMoreDummy = [
    {
        shopName: '이자카야',
        Image: '../../../assets/User/My/dummy_foods1.png',
        meter: 120,
        address: '서울시 강낭구 종로 1',
        openTime: '9:00',
        closeTime: '23:00',
        heart: 2540,
        bookmark: 112353
    },
    {
        shopName: '광교카페거리',
        Image: '../../../assets/User/My/dummy_foods2.png',
        meter: 200,
        address: '경기 수원시 광교로 244',
        openTime: '16:00',
        closeTime: '3:00',
        heart: 455,
        bookmark: 12353
    },
    {
        shopName: '이자카야',
        Image: '../../../assets/User/My/dummy_foods1.png',
        meter: 120,
        address: '서울시 강낭구 종로 1',
        openTime: '9:00',
        closeTime: '23:00',
        heart: 2540,
        bookmark: 112353
    },
    {
        shopName: '광교카페거리',
        Image: '../../../assets/User/My/dummy_foods2.png',
        meter: 200,
        address: '경기 수원시 광교로 244',
        openTime: '16:00',
        closeTime: '3:00',
        heart: 455,
        bookmark: 12353
    },
    {
        shopName: '이자카야',
        Image: '../../../assets/User/My/dummy_foods1.png',
        meter: 120,
        address: '서울시 강낭구 종로 1',
        openTime: '9:00',
        closeTime: '23:00',
        heart: 2540,
        bookmark: 112353
    },
    {
        shopName: '광교카페거리',
        Image: '../../../assets/User/My/dummy_foods2.png',
        meter: 200,
        address: '경기 수원시 광교로 244',
        openTime: '16:00',
        closeTime: '3:00',
        heart: 455,
        bookmark: 12353
    },
    {
        shopName: '이자카야',
        Image: '../../../assets/User/My/dummy_foods1.png',
        meter: 120,
        address: '서울시 강낭구 종로 1',
        openTime: '9:00',
        closeTime: '23:00',
        heart: 2540,
        bookmark: 112353
    },
    {
        shopName: '광교카페거리',
        Image: '../../../assets/User/My/dummy_foods2.png',
        meter: 200,
        address: '경기 수원시 광교로 244',
        openTime: '16:00',
        closeTime: '3:00',
        heart: 455,
        bookmark: 12353
    }
];

let currentMoreIndex = 0;
let currentScene = null;
let loadingMore = false;

class User_my_contents extends  React.Component {
    constructor(props) {
        super(props);
        currentScene = this;
        this.state = {
            text: '',
            biz_shop_data: null,
            geocode: '',
            geo_error: '',
            latitude: 0,
            longitude: 0,
            horizontal_shop_data: [
                {
                    shopName: '광교카페거리',
                    Image: '../../../assets/User/My/dummy_foods2.png',
                    meter: 200,
                    address: '경기 수원시 광교로 244',
                    openTime: '16:00',
                    closeTime: '3:00',
                    heart: 455,
                    bookmark: 12353
                },
                {
                    shopName: '이자카야',
                    Image: '../../../assets/User/My/dummy_foods1.png',
                    meter: 120,
                    address: '서울시 강낭구 종로 1',
                    openTime: '9:00',
                    closeTime: '23:00',
                    heart: 2540,
                    bookmark: 112353
                },
                {
                    shopName: '광교카페거리',
                    Image: '../../../assets/User/My/dummy_foods2.png',
                    meter: 200,
                    address: '경기 수원시 광교로 244',
                    openTime: '16:00',
                    closeTime: '3:00',
                    heart: 455,
                    bookmark: 12353
                },
                {
                    shopName: '이자카야',
                    Image: '../../../assets/User/My/dummy_foods1.png',
                    meter: 120,
                    address: '서울시 강낭구 종로 1',
                    openTime: '9:00',
                    closeTime: '23:00',
                    heart: 2540,
                    bookmark: 112353
                },
                {
                    shopName: '광교카페거리',
                    Image: '../../../assets/User/My/dummy_foods2.png',
                    meter: 200,
                    address: '경기 수원시 광교로 244',
                    openTime: '16:00',
                    closeTime: '3:00',
                    heart: 455,
                    bookmark: 12353
                },
                {
                    shopName: '이자카야',
                    Image: '../../../assets/User/My/dummy_foods1.png',
                    meter: 120,
                    address: '서울시 강낭구 종로 1',
                    openTime: '9:00',
                    closeTime: '23:00',
                    heart: 2540,
                    bookmark: 112353
                },
                {
                    shopName: '광교카페거리',
                    Image: '../../../assets/User/My/dummy_foods2.png',
                    meter: 200,
                    address: '경기 수원시 광교로 244',
                    openTime: '16:00',
                    closeTime: '3:00',
                    heart: 455,
                    bookmark: 12353
                },
                {
                    shopName: '이자카야',
                    Image: '../../../assets/User/My/dummy_foods1.png',
                    meter: 120,
                    address: '서울시 강낭구 종로 1',
                    openTime: '9:00',
                    closeTime: '23:00',
                    heart: 2540,
                    bookmark: 112353
                }
            ],
            more_data: [
                {
                    shopName: '이자카야',
                    Image: '../../../assets/User/My/dummy_foods1.png',
                    meter: 120,
                    address: '서울시 강낭구 종로 1',
                    openTime: '9:00',
                    closeTime: '23:00',
                    heart: 2540,
                    bookmark: 112353
                },
                {
                    shopName: '광교카페거리',
                    Image: '../../../assets/User/My/dummy_foods2.png',
                    meter: 200,
                    address: '경기 수원시 광교로 244',
                    openTime: '16:00',
                    closeTime: '3:00',
                    heart: 455,
                    bookmark: 12353
                },
                {
                    shopName: '이자카야',
                    Image: '../../../assets/User/My/dummy_foods1.png',
                    meter: 120,
                    address: '서울시 강낭구 종로 1',
                    openTime: '9:00',
                    closeTime: '23:00',
                    heart: 2540,
                    bookmark: 112353
                },
                {
                    shopName: '광교카페거리',
                    Image: '../../../assets/User/My/dummy_foods2.png',
                    meter: 200,
                    address: '경기 수원시 광교로 244',
                    openTime: '16:00',
                    closeTime: '3:00',
                    heart: 455,
                    bookmark: 12353
                },
                {
                    shopName: '이자카야',
                    Image: '../../../assets/User/My/dummy_foods1.png',
                    meter: 120,
                    address: '서울시 강낭구 종로 1',
                    openTime: '9:00',
                    closeTime: '23:00',
                    heart: 2540,
                    bookmark: 112353
                },
                {
                    shopName: '광교카페거리',
                    Image: '../../../assets/User/My/dummy_foods2.png',
                    meter: 200,
                    address: '경기 수원시 광교로 244',
                    openTime: '16:00',
                    closeTime: '3:00',
                    heart: 455,
                    bookmark: 12353
                },
                {
                    shopName: '이자카야',
                    Image: '../../../assets/User/My/dummy_foods1.png',
                    meter: 120,
                    address: '서울시 강낭구 종로 1',
                    openTime: '9:00',
                    closeTime: '23:00',
                    heart: 2540,
                    bookmark: 112353
                },
                {
                    shopName: '광교카페거리',
                    Image: '../../../assets/User/My/dummy_foods2.png',
                    meter: 200,
                    address: '경기 수원시 광교로 244',
                    openTime: '16:00',
                    closeTime: '3:00',
                    heart: 455,
                    bookmark: 12353
                }
            ],
            cur_page: 0,
            currentScrollViewHeight: 0
        };
    }

    componentDidMount() {
        this.get_geocode();
    }

    get_geocode(){
        navigator.geolocation.getCurrentPosition(
            (position) => {
                if (position != null) {
                    console.log(position.timestamp);
                    this.setState({
                        latitude: position.coords.latitude,
                        longitude: position.coords.longitude,
                        geocode : [position.coords.latitude, position.coords.longitude],
                        geo_error: null
                    });
                } else {
                    this.setState({
                        latitude: 37.517204,
                        longitude: 127.041289,
                        geocode : [37.517204,127.041289],
                        geo_error: null
                    });
                }
                this.get_shop_list(0);
            }, (error) => this.setState({
                latitude: 37.517204,
                longitude: 127.041289,
                geocode : [37.517204,127.041289],
                geo_error: error.message
            }),
        );
    }

    get_shop_list(page) {
        currentScene.setState({cur_page: page});
        if (Global.user !== null)
        {
            API.utility.getContents(currentScene.state.longitude, currentScene.state.latitude, currentScene.state.geocode, page, data => {
                if (data.errorCode !== "" || data.message === "Internal server error") {
                    // error
                } else {
                    // Todo API 메뉴얼에 추가되는 맞게 수정. 지금은 더미
                }
            }, error => {

            });
        }
    }

    //가까준 스토어 리스트출력
    render_store_list_for_horizontal(){
        if (this.state.horizontal_shop_data.length > 0) {
            return (
                <ScrollView horizontal={true}>
                    {
                        this.state.horizontal_shop_data.map((element, index) => this.render_horizontal_loop_data(element, index))
                    }
                </ScrollView>
            );
        }
    }
    render_horizontal_loop_data(element, index){
        return <View style={styles.contents_horizontal_scroll_item} key={index}>
            <TouchableHighlight underlayColor={'transparent'} onPress={() => Actions.User_request_step5({shopInfo: element})}>
                <View style={{overflow: 'hidden'}}>
                    <Image style={styles.contents_horizontal_scroll_item_image} source={require("../../../assets/User/My/dummy_foods3.png")}/>
                    <View style={styles.contents_horizontal_scroll_item_s}>
                        <View style={{alignItems: 'flex-end', justifyContent: 'flex-end',flexDirection: 'row'}}>
                            <Image style={styles.imgicon} source={require("../../../assets/common/pin.png")}/>
                            <Text style={styles.contents_horizontal_scroll_item_info_memter}> {commaNumber(element.meter)}M</Text>
                        </View>
                        <Text style={styles.contents_horizontal_scroll_item_info_store_name}>{element.shopName}</Text>
                        <View style={{flexDirection: 'row'}}>
                            <Text style={styles.contents_horizontal_scroll_item_info_addr_name}>{element.address} </Text>
                            <Image style={{width: 16, height: 16}} source={require("../../../assets/common/clock.png")}/>
                            <Text style={{fontSize:12 , color:'#c4c4c4'}}>{element.openTime} ~ {element.closeTime}</Text>
                        </View>
                        <View style={{flexDirection: 'row'}}>
                            <Image style={styles.imgicon } source={require("../../../assets/common/silver_heart.png")}/>
                            <Text> {commaNumber(element.heart)} </Text>
                            <Image style={styles.imgicon } source={require("../../../assets/common/silver_bookmark.png")}/>
                            <Text> {commaNumber(element.bookmark)}</Text>
                        </View>
                    </View>
                </View>
            </TouchableHighlight>
        </View>;
    }

    //식당 더보기 리스트 출력
    render_store_list_for_more() {
        return (
            <View style={{alignItems: 'center', justifyContent: 'center'}}>
                {
                    this.state.more_data.map(element => this.render_more_loop_data(element))
                }
            </View>
        );
    }
    render_more_loop_data(element){
        return <View style={styles.contents_horizontal_scroll_item2} key={"more" + currentMoreIndex++}>
            <TouchableHighlight underlayColor={'transparent'} onPress={() => Actions.User_request_step5({shopInfo: element})}>
                <View style={{overflow: 'hidden'}}>
                    <Image style={styles.contents_horizontal_scroll_item_image} source={require("../../../assets/User/My/dummy_foods3.png")}/>
                    <View style={styles.contents_horizontal_scroll_item_s2}>
                        <View style={{alignItems: 'flex-end', justifyContent: 'flex-end',flexDirection: 'row', width: 336, paddingLeft: 12, paddingRight: 6}}>
                            <Image style={styles.imgicon} source={require("../../../assets/common/pin.png")}/>
                            <Text style={styles.contents_horizontal_scroll_item_info_memter}> {element.meter}M</Text>
                        </View>
                        <Text style={styles.contents_horizontal_scroll_item_info_store_name}>{element.shopName}</Text>
                        <View style={{flexDirection: 'row'}}>
                            <Text style={styles.contents_horizontal_scroll_item_info_addr_name}>{element.shopAddress} </Text>
                            <Image style={{width: 16, height: 16}} source={require("../../../assets/common/clock.png")}/>
                            <Text style={{fontSize:12 , color:'#c4c4c4'}}>{element.openTime} ~ {element.closeTime}</Text>
                        </View>
                        <View style={{flexDirection: 'row'}}>
                            <Image style={styles.imgicon } source={require("../../../assets/common/silver_heart.png")}/>
                            <Text> {commaNumber(element.heart)} </Text>
                            <Image style={styles.imgicon } source={require("../../../assets/common/silver_bookmark.png")}/>
                            <Text> {commaNumber(element.bookmark)}</Text>
                        </View>
                    </View>
                </View>
            </TouchableHighlight>
        </View>;
    }

    async load_more() {
        let moreArray = currentScene.state.more_data;
        currentScene.get_shop_list(currentScene.state.cur_page + 1);
        loadMoreDummy.map(element => {
            moreArray.push(element);
        });
        currentScene.setState({more_data: moreArray});
    };

    handleScroll(event) {
        const currentPosition = event.nativeEvent.contentOffset.y;
        const currentHeight = currentScene.state.currentScrollViewHeight;
        if (currentHeight - currentPosition < 930) {
            if (loadingMore) {
                return;
            }
            loadingMore = true;
            currentScene.load_more();
        } else {
            loadingMore = false;
        }
    }

    handleContentSizeChange(width, height) {
        currentScene.setState({currentScrollViewHeight: height});
    }

    render() {
        return (
            <View  style={styles.container}>
                <View style={styles.Navigator}>
                    <Text style={styles.Navigator_title}>추천</Text>
                    <Text style={styles.Navigator_sub}>현재 예약가능한 식당이 주변에 10곳이 있습니다.</Text>
                </View>
                <ScrollView onScroll={this.handleScroll} onContentSizeChange={this.handleContentSizeChange} scrollEventThrottle={32}>
                    <View style={styles.contents}>
                        <Text style={[styles.contents_where,{marginTop: 20, marginBottom: 20}]}>지금 있는곳으로부터 예약 가능한 20분 거리</Text>
                        {this.render_store_list_for_horizontal()}
                        <View style={{backgroundColor: '#f9f9f9',height: 8, width: Dimensions.get('window').width}}></View>
                        <Text style={[styles.contents_where,{marginTop: 20, marginBottom: 20}]}>식당 더보기</Text>
                        {this.render_store_list_for_more()}
                    </View>
                </ScrollView>
                <View style={styles.main_bottom_bar}>
                    <TouchableHighlight underlayColor={'transparent'} onPress={() => Actions.pop()}>
                        <View style={styles.icon_buttom}>
                            <Image style={styles.icon_buttom_image} source={require("../../../assets/User/Bottom/home_gray.png")}/>
                        </View>
                    </TouchableHighlight>
                    <TouchableHighlight underlayColor={'transparent'} onPress={() => null}>
                        <View style={styles.icon_buttom}>
                            <Image style={styles.icon_buttom_image} source={require("../../../assets/User/Bottom/utensils_gray.png")}/>
                        </View>
                    </TouchableHighlight>
                    <TouchableHighlight underlayColor={'transparent'} onPress={() => null}>
                        <View style={styles.icon_buttom}>
                            <Image style={styles.icon_buttom_image} source={require("../../../assets/User/Bottom/chat_gray.png")}/>
                        </View>
                    </TouchableHighlight>
                    <TouchableHighlight underlayColor={'transparent'} onPress={() => Actions.User_my_contents()}>
                        <View style={styles.icon_buttom}>
                            <Image style={styles.icon_buttom_image} source={require("../../../assets/User/Bottom/newspaper_black.png")}/>
                        </View>
                    </TouchableHighlight>
                    <TouchableHighlight underlayColor={'transparent'} onPress={() => Actions.User_my_main()}>
                        <View style={styles.icon_buttom}>
                            <Image style={styles.icon_buttom_image} source={require("../../../assets/User/Bottom/user_gray.png")}/>
                        </View>
                    </TouchableHighlight>
                </View>
            </View>
        )
    }
}

export default User_my_contents;
