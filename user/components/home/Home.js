import React from 'react';
import {Text, View, StyleSheet, ScrollView, Platform} from 'react-native';
import Modal from 'react-native-modal';
import { Button } from 'react-native-elements';
import {Actions} from 'react-native-router-flux';
import {setCustomText, setCustomTextInput} from 'react-native-global-props';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    button : {
        marginTop: 10,
    },

});

const customTextProps = {
    style : {
        fontFamily:  (Platform.OS == 'ios' ? 'AppleSDGothicNeo-Regular': 'sans-serif'),
        includeFontPadding: false,
        color: '#123456'
    }
};

const customTextInputProps = {
    style: {
        fontFamily:  (Platform.OS == 'ios' ? 'AppleSDGothicNeo-Regular': 'sans-serif'),
        includeFontPadding: false,
        color: '#123456'
    }
};


setCustomText(customTextProps);
setCustomTextInput(customTextInputProps);



class Home extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isVisible: false
        }
    }

    toggleModal = (value) => {
        this.setState({isVisible: value})
    };

    render() {
        return (
            <View style={styles.container}>
                <Modal isVisible={this.state.isVisible}>
                    <View>
                        <Text>dummy</Text>
                        <Button title={"x"} onPress={() => this.toggleModal(false)}/>
                    </View>
                </Modal>
                <ScrollView style={{ flex: 1}}>
                    <Button style={styles.button} title={"modal 키기"} onPress={() => this.toggleModal(true)}/>
                    <Button style={styles.button} title={"Home_user"} onPress={() => Actions.Home_user()}/>
                    <Button style={styles.button} title={"페이지이동_로그인/화원가입"} onPress={() => Actions.Login()}/>
                    <Button style={styles.button} title={"페이지이동_회원가입완료"} onPress={() => Actions.Login_regist_step_end({ phone : '01012341234', nickname: '테스트', password: '1234'})}/>
                    <Button style={styles.button} title={"페이지이동_점포변경"} onPress={() => Actions.Biz_change_shop()}/>
                    <Button style={styles.button} title={"페이지이동_점포등록완료"} onPress={() =>  Actions.Biz_change_shop_regist_end({bizname: '테스트식당', biznumber: '12312123456', bizphone: '0212341234', bizaddress: '서울시 종로 1', authnum: 'test123'})}/>
                    <Button style={styles.button} title={"예약관리"} onPress={() =>  Actions.Manager_main()}/>
                    <Button style={styles.button} title={"알림"} onPress={() =>  Actions.Manager_noti()}/>
                    <Button style={styles.button} title={"설정"} onPress={() =>  Actions.Manager_settting()}/>
                    <Button style={styles.button} title={"점포운영관리"} onPress={() =>  Actions.admin_main()}/>
                    <Button style={styles.button} title={"SNS 로그인 테스트"} onPress={() =>  Actions.SNS_Login_Test()}/>
                    <Text>Home</Text>
                </ScrollView>
            </View>
        )
    }
}

export default Home;
