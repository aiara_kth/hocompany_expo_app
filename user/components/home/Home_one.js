import React from 'react';
import {Text, View, StyleSheet, ScrollView} from 'react-native';
import { Button } from 'react-native-elements';
import {Actions} from 'react-native-router-flux';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    button : {
        marginBottom: 10,
    },

});

class Home_user extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isVisible: false
        }
    }

    toggleModal = (value) => {
        this.setState({isVisible: value})
    };

    render() {
        return (
            <View style={styles.container}>
                <ScrollView style={{ flex: 1}}>
                    <Button style={styles.button} title={"페이지이동_one_shop_detail"} onPress={() => Actions.User_request_one_shop_detail()}/>
                    <Button style={styles.button} title={"페이지이동_one_wait"} onPress={() => Actions.User_request_one_wait()}/>
                    <Button style={styles.button} title={"페이지이동_one_no_response"} onPress={() => Actions.User_request_one_fail()}/>
                    <Button style={styles.button} title={"페이지이동_one_noseat_waiting"} onPress={() => Actions.User_request_one_exec()}/>
                    <Button style={styles.button} title={"페이지이동_one_noseat_waiting_yes"} onPress={() => Actions.User_request_one_exec_wait()}/>
                    <Button style={styles.button} title={"페이지이동_one_complete"} onPress={() => Actions.User_request_step6()}/>
                    <Text>Home_user</Text>
                </ScrollView>
            </View>
        )
    }
}

export default Home_user;
