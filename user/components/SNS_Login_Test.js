import React from 'react';
import {
    Platform,
    Alert,
    StyleSheet,
    Text,
    View,
    Image, TouchableHighlight,
} from 'react-native';

import RNKakaoLogins from 'react-native-kakao-logins';

import { FBLogin, FBLoginManager } from 'react-native-facebook-login';


export default class App extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            kakaoToken: '',
            facebookToken: ''
        };
        if (!RNKakaoLogins) {
            console.log('Not Linked');
        }
    }

    // 카카오 로그인 시작.
    kakaoLogin() {
        console.log('   kakaoLogin   ');
        RNKakaoLogins.login((err, result) => {
            if (err !== undefined && err !== null){
                console.log(err);
                return;
            }
            console.log(result);
            this.setState({kakaoToken: result.token});
            RNKakaoLogins.getProfile((err, result) => {
                console.log(err, result);
            });
        });
    }

    // kakaoLogout() {
    //     console.log('   kakaoLogout   ');
    //     RNKakaoLogins.logout((err, result) => {
    //         console.log(err, result);
    //         if (err){
    //             Alert.alert('error', err);
    //             return;
    //         }
    //         Alert.alert('result', result);
    //     });
    // }
    //
    // // 로그인 후 내 프로필 가져오기.
    // getProfile() {
    //     console.log('getKakaoProfile');
    //     this.setState({isKakaoLogging: true});
    //     RNKakaoLogins.getProfile((err, result) => {
    //         console.log(err, result);
    //         this.setState({isKakaoLogging: false});
    //         if (err){
    //             Alert.alert('error', err);
    //             return;
    //         }
    //         Alert.alert('result', result);
    //     });
    // }

    fbLogin = null;
    fbPermissions = ["email","user_friends"];

    onPressFacebookBtn() {
        if (this.state.facebookToken !== "") {
            Alert.alert('페이스북', '로그인 되어 있습니다. 로그아웃 하시겠습니까?',
                [
                    {text: "취소", onPress: () => null, styles: 'cancel'},
                    // {text: "확인", onPress : () => Actions.Login_regist_step_end({ phone : this.props.phone_num, nickname: this.state.nickname, password: this.state.password})}
                    {text: "확인", onPress: () => this.onFacebookPress(true)}
                ]
            );
            return;
        }
        this.onFacebookPress();
    }

    onFacebookPress(logout) {
        if (Platform.OS === 'android')
            this.fbLogin._onFacebookPress();
        else
        {
            if (logout) {
                this.fbLogin.FBLoginManager.logout((data) => {
                    // console.log(data);
                });
            } else {
                this.fbLogin.FBLoginManager.login((data) => {
                    // console.log(data);
                });
            }
        }
    }

    initUser(token) {
        fetch('https://graph.facebook.com/v2.5/me?fields=email,name,friends&access_token=' + token)
            .then((response) => response.json())
            .then((json) => {
                console.log(json);
            })
            .catch(() => {
                reject('ERROR GETTING DATA FROM FACEBOOK');
            });
    }

    render() {
        let _this = this;
        return (
            <View style={ styles.container }>
                <View style={ styles.header }>
                    <Text>LOGIN</Text>
                </View>
                <View style={ styles.content }>
                    <Text>{this.state.kakaoToken}</Text>
                    <TouchableHighlight underlayColor={'transparent'} onPress={() => this.kakaoLogin()}>
                        <Image style={styles.btn_login} source={require("../assets/Login/login_kakao.png")}/>
                    </TouchableHighlight>
                    {/*<Button*/}
                        {/*isLoading={this.state.isKakaoLogging}*/}
                        {/*onPress={() => this.kakaoLogin()}*/}
                        {/*activeOpacity={0.5}*/}
                        {/*style={styles.btnLogin}*/}
                        {/*textStyle={styles.txtLogin}*/}
                        {/*title={"login"}*/}
                   {/*/>*/}
                    {/*<Button*/}
                        {/*onPress={() => this.kakaoLogout()}*/}
                        {/*activeOpacity={0.5}*/}
                        {/*style={styles.btnLogin}*/}
                        {/*textStyle={styles.txtLogin}*/}
                        {/*title={"logout"}*/}
                   {/*/>*/}
                    {/*<Button*/}
                        {/*isLoading={this.state.isKakaoLogging}*/}
                        {/*onPress={() => this.getProfile()}*/}
                        {/*activeOpacity={0.5}*/}
                        {/*style={styles.btnLogin}*/}
                        {/*textStyle={styles.txtLogin}*/}
                        {/*title={"getProfile"}*/}
                    {/*/>*/}
                    <Text>{this.state.facebookToken}</Text>
                    <TouchableHighlight underlayColor={'transparent'} onPress={() => this.onPressFacebookBtn()}>
                        <Image style={styles.btn_login} source={require("../assets/Login/login_facebook.png")}/>
                    </TouchableHighlight>
                    <FBLogin style={{ marginBottom: 10, height: 48, display: 'none' }}
                             ref={(fbLogin) => { this.fbLogin = fbLogin }}
                             permissions={this.fbPermissions}
                             loginBehavior={FBLoginManager.LoginBehaviors.Native}
                             onLogin={data => {
                                 console.log("Logged in!");
                                 console.log(data);
                                 this.setState({ user : data.credentials, facebookToken: data.credentials.token });
                                 this.initUser(this.state.facebookToken);
                             }}
                             onLogout={() => {
                                 console.log("Logged out.");
                                 this.setState({ user : null, facebookToken: "" });
                             }}
                             onLoginFound={data => {
                                 console.log("Existing login found.");
                                 console.log(data);
                                 this.setState({ user : data.credentials, facebookToken: data.credentials.token });
                                 this.initUser(this.state.facebookToken);
                             }}
                             onLoginNotFound={() => {
                                 console.log("No user logged in.");
                                 this.setState({ user : null, facebookToken: "" });
                             }}
                             onError={data => {
                                 console.log("ERROR");
                                 console.log(data);
                             }}
                             onCancel={() => {
                                 console.log("User cancelled.");
                             }}
                             onPermissionsMissing={data => {
                                 console.log("Check permissions!");
                                 console.log(data);
                             }}
                    />
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        marginTop: Platform.OS === 'ios' ? 0 : 24,
        paddingTop: Platform.OS === 'ios' ? 24 : 0,
        backgroundColor: 'white',
    },
    header: {
        flex: 8.8,
        flexDirection: 'row',
        alignSelf: 'stretch',
        justifyContent: 'center',
        alignItems: 'center',
    },
    content: {
        flex: 87.5,
        flexDirection: 'column',
        justifyContent: 'center',
        alignSelf: 'stretch',
        alignItems: 'center',
    },
    title: {
        fontSize: 24,
        fontWeight: 'bold',
    },
    btnLogin: {
        height: 48,
        width: 240,
        alignSelf: 'center',
        backgroundColor: '#F8E71C',
        borderRadius: 0,
        borderWidth: 0,
    },
    txtLogin: {
        fontSize: 16,
        color: '#3d3d3d',
    },
    btn_login: {
        width: 316,
        height: 56,
        marginBottom: 10
    }
});
