import React from 'react';
import {
    Text,
    View,
    StyleSheet,
    Image,
    TextInput,
    TouchableHighlight,
    Dimensions,
    Alert,
    AsyncStorage
} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { Actions } from 'react-native-router-flux';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import ActionButton from 'react-native-action-button';
import {Ionicons} from '@expo/vector-icons';
import "../../../objects/Global"
import {Location, Permissions} from "expo";
import API from "../../../objects/API";
import Global from "../../../objects/Global";

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    Navigator : {
        marginTop : getStatusBarHeight(),
        width: '100%',
        height: 48,
        backgroundColor: '#fff',
    },
    back_button : {
        width:  48,
        height: 48,
        paddingTop: 12,
        paddingLeft: 12
    },
    back_button_b : {
        width: 24,
        height: 24
    },
    header : {
        marginTop: 48,
        width: '100%',
        height: 50
    },
    title: {
        paddingLeft: 13,
        width:'100%',
        height:'18%',
        justifyContent: 'center',
    },
    title_w : {
        fontSize: 32,
        height: 48,
        color: '#000000',
    },
    title_ws : {
        width: 290,
        fontSize: 14,
        height: 24,
        color: '#eb5847'
    },
    contents: {
        width: Dimensions.get('window').width,
        height: 300,
        alignItems: 'center',
        justifyContent: 'center',
    },
    input_form : {
        width: 320,
        justifyContent: 'center',
        alignItems: 'center',
    },
    input_box : {
        width: 320,
        height:56,
        borderRadius: 2,
        backgroundColor: '#fafbfb',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection:'row',
        marginBottom:20
    },
    input_dom : {
        marginLeft : 10,
        width: 300
    }

});

class Biz_change_shop_regist_three extends  React.Component {
    constructor(props) {
        super(props);
        this.state = {
            show_modal: 0,
            authnum: '',
            latitude: 0,
            longitude: 0
        };
        Permissions.askAsync(Permissions.LOCATION);
    }

    componentDidMount() {
        // this.geocoding();
    }

    async geocoding() {
        try {
            let result = await Location.geocodeAsync({
                address: this.props.bizaddress
            });
            console.log(result);

        } catch (e) {
            console.error(e);
        }
    }

    next() {
        if (this.state.authnum === '') {
            Alert.alert('점포등록', '인증번호를 입력해주세요.', [{text: "확인", onPress: () => this.refs.authnum_input.focus()}]);
            return;
        }

        Alert.alert('점포등록', '점포등록 하시겠습니까? ',
            [
                {text: "취소", onPress: () => null, styles: 'cancel'},
                {text: "확인", onPress: () => this.register()}
            ]
        );

    }

    register() {
        this.setState({show_modal: 1});
        API.shop.admin.post(this.props.bizname, parseInt(this.state.authnum), this.props.biznumber, this.props.bizaddress, data => {
            Actions.Biz_change_shop_regist_end({
                shopId: data.shopID,
                bizname: this.props.bizname,
                biznumber: this.props.biznumber,
                bizphone: this.props.bizphone,
                bizaddress: this.props.bizaddress,
                authnum: this.state.authnum
            });
            API.shop.admin.getUserShops(data => {
                AsyncStorage.setItem("@ShopListData", JSON.stringify(data));
            }, error => {

            });
        });
    }

    render() {
        return (
            <View style={styles.container} behavior="padding" enabled>
                <View style={[this.state.show_modal == 1 ? {} : {
                    display: 'none',
                    position: 'relative'
                }, {
                    width: Dimensions.get('window').width,
                    height: Dimensions.get('window').height,
                    backgroundColor: 'black',
                    opacity: 0.6
                }]}></View>
                <View style={styles.Navigator}>
                    <TouchableHighlight underlayColor={'transparent'} onPress={() => Actions.pop()}>
                        <View style={styles.back_button}>
                            <Image style={styles.back_button_b} source={require("../../../assets/back_arrow.png")}/>
                        </View>
                    </TouchableHighlight>
                </View>
                <KeyboardAwareScrollView>
                    <View style={styles.header}>
                        <View style={styles.title}>
                            <Text style={styles.title_w}>인증번호 등록</Text>
                            <Text style={styles.title_ws}>예약을 완료할 수 있는 인증번호를 등록해주세요.</Text>
                        </View>
                    </View>
                    <View style={styles.contents}>
                        <View style={styles.input_form}>
                            <View style={styles.input_box}>
                                <TextInput ref="authnum_input" style={styles.input_dom} keyboardType='numeric'
                                           placeholder={'인증번호'} underlineColorAndroid='transparent' retrunKeyType="done"
                                           onChangeText={(text) => this.setState({authnum: text})}/>
                            </View>
                        </View>
                    </View>
                </KeyboardAwareScrollView>
                <ActionButton hideShadow={false}
                              buttonColor={this.state.authnum === '' ? '#838383' : '#eb5847'}
                              renderIcon={(active) => (active ? (
                                  <Ionicons name='ios-arrow-forward' size={20} color={'#FFFFFF'}/>) : (
                                  <Ionicons name='ios-arrow-forward' size={20} color={'#FFFFFF'}/>))}
                              onPress={() => this.next()}>
                </ActionButton>
            </View>
        )
    }
}

export default Biz_change_shop_regist_three;
