import React from 'react';
import {Text, View, StyleSheet, Image,TouchableHighlight, Dimensions, ScrollView, } from 'react-native';
import { Actions } from 'react-native-router-flux';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import {Ionicons} from '@expo/vector-icons';
import Global from "../../../objects/Global";
import API from "../../../objects/API";


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f9f9f9',
    },
    Navigator : {
        flexDirection: 'row',
        marginTop : getStatusBarHeight(),
        width: Dimensions.get('window').width,
        height: 48,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    Navigator_menu: {
        width: 80,
        height: 48,
    },
    Navigator_menu_button :{
        width: 48,
        height: 48,
        paddingTop: 12,
        paddingLeft: 12,
    },
    Navigator_menu_image : {
        width: 24,
        height: 24
    },
    Navigator_title:{
        width : ((Dimensions.get('window').width) - 160),
        alignItems: 'center',
    },
    Navigator_title_text:{
        fontSize: 20,
        color:'#000000'
    },
    Navigator_start:{
        alignItems: 'flex-end',
        width: 80,
        height: 48,
        paddingRight: 12,
    },
    Navigator_start_text:{
        fontSize: 16,
        color:'#7db840',
        marginTop: 12,
        marginBottom: 12
    },
    sub_Navigator : {
        flexDirection: 'row',
        width: Dimensions.get('window').width,
        height: 40,
        backgroundColor: '#fff',
        alignItems: 'center',
    },
    sub_Navigator_menu: {
        width: 60,
        height: 40,
        alignItems: 'flex-end',
        justifyContent: 'center',
    },
    sub_Navigator_menu_on: {
        width: 40,
        height: 40,
        alignItems: 'center',
        justifyContent: 'center',
        borderBottomWidth: 2,
        borderBottomColor: '#000000',
    },
    sub_Navigator_menu_off: {
        width: 40,
        height: 40,
        alignItems: 'center',
        justifyContent: 'center',
        borderBottomWidth: 2,
        borderBottomColor: 'transparent',
    },
    sub_Navigator_name_on : {
        fontSize : 14,
        fontWeight: 'bold',
    },
    sub_Navigator_name_off : {
        fontSize : 14,
    },
    contents : {
        paddingTop :  9,
    },
    top_count : {
        width: Dimensions.get('window').width,
        height: 48,
        backgroundColor : '#30cbba',
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: 8
    },
    top_count_text : {
        fontSize: 16,
        color: '#fff'
    },
    contents_item : {
        width: Dimensions.get('window').width,
        height: 64,
        backgroundColor: '#fff',
        paddingBottom: 10,
        alignItems: 'center',
        justifyContent: 'center',
    },
    contents_item_sub : {
        flexDirection: 'row',
        width: Dimensions.get('window').width,
        backgroundColor: '#fff',
        marginBottom: 8,
        paddingLeft: 10,
        paddingRight: 10,
        alignItems: 'center',
        justifyContent: 'center',
    },
    item_date : {
        fontSize: 16,
        color: '#838383',
        fontWeight: '600',
        height: 20
    },
    item_group : {
        flex: 1,
        paddingLeft: 12
    },
    item_top: {
        marginTop: 12,
        fontSize: 14,
        height: 20
    },
    item_bottom: {
        marginTop: 4,
        fontSize: 12,
        color: '#c4c4c4'
    },
    item_image_group : {
        marginLeft: 12,
        width: 24,
        height: 24
    },
    item_image : {
        width: 24,
        height: 24
    }
});


class Manager_noti extends  React.Component {
    constructor(props) {
        super(props);
        this.state = {
            menu : 1
        };
        if (this.props.shopId !== undefined && this.props.shopId !== null) {
            API.shop.admin.getNotification(this.props.shopId, data => {
                console.log(data);
                // todo 각 알림의 종류에 따라 배열에다가 나둬 담고 render에서 나눈 배열에 따라 함수 호출시키자
            });
        }
    }

    comment(receiver) {
        return <View style={styles.contents_item}>
            <View style={styles.contents_item_sub}>
                <View style={styles.item_group}>
                    <Text style={styles.item_top}><Text style={{fontWeight: 'bold'}}>{receiver}</Text> 님이 작성하신리뷰에 댓글을 남겼습니다.</Text>
                    <Text style={styles.item_bottom}> 5시간 전</Text>
                </View>
            </View>
        </View>;
    }

    reservation(store, count, time) {
        return <View style={styles.contents_item}>
            <View style={styles.contents_item_sub}>
                <View style={styles.item_group}>
                    <Text style={styles.item_top}><Text style={{fontWeight: 'bold'}}>{store}</Text> 예약요청 {count}건 왔습니다.</Text>
                    <Text style={styles.item_bottom}> {time}</Text>
                </View>
            </View>
        </View>;
    }

    message(msg, time) {
        return <View style={styles.contents_item}>
            <View style={styles.contents_item_sub}>
                <View style={styles.item_image_group}>
                    <Image style={styles.item_image} source={require("../../../assets/common/logo_very_small.png")}/>
                </View>
                <View style={styles.item_group}>
                    <Text style={styles.item_top}>{msg}</Text>
                    <Text style={styles.item_bottom}> {time}</Text>
                </View>
            </View>
        </View>;
    }

    reservationSuccess(time) {
        return <View style={styles.contents_item}>
            <View style={styles.contents_item_sub}>
                <View style={styles.item_group}>
                    <Text style={styles.item_top}>예약이 성공적으로 되었습니다.</Text>
                    <Text style={styles.item_bottom}> {time}</Text>
                </View>
            </View>
        </View>;
    }

    render() {
        return (
            <View  style={styles.container}>
                <View style={styles.Navigator}>
                    <View style={styles.Navigator_menu}>
                        <TouchableHighlight style={styles.Navigator_menu_button} underlayColor={'transparent'} onPress={() => Actions.pop()}>
                            <Image style={styles.Navigator_menu_image} source={require("../../../assets/back_arrow.png")}/>
                        </TouchableHighlight>
                    </View>
                    <View style={styles.Navigator_title}><Text style={styles.Navigator_title_text}>알림</Text></View>
                    <TouchableHighlight underlayColor={'transparent'} onPress={() => null}>
                        <View style={styles.Navigator_start}><Text style={styles.Navigator_start_text}></Text></View>
                    </TouchableHighlight>
                </View>
                <ScrollView ref="queue" style={{marginTop: 9}}>
                    {this.comment("김민지")}
                    {this.reservation("치킨어때 명동점", 2, "5시간 전")}
                    {this.message("자리있소 자체 메세지", "5시간 전")}
                    {this.reservationSuccess("5시간 전")}
                    {this.reservationSuccess("5시간 전")}
                </ScrollView>
            </View>
        )
    }
}

export default Manager_noti;