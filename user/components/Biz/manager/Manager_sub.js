import React from 'react';
import {Text, View, StyleSheet, Image,TouchableHighlight, Dimensions, ScrollView, } from 'react-native';
import { Actions } from 'react-native-router-flux';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import {Ionicons} from '@expo/vector-icons';
import Global from "../../../objects/Global";


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f9f9f9',
    },
    Navigator : {
        flexDirection: 'row',
        marginTop : getStatusBarHeight(),
        width: Dimensions.get('window').width,
        height: 48,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    Navigator_menu: {
        width: 80,
        height: 48,
    },
    Navigator_menu_button :{
        width: 48,
        height: 48,
        paddingTop: 12,
        paddingLeft: 12,
    },
    Navigator_menu_image : {
        width: 24,
        height: 24
    },
    Navigator_title:{
        width : ((Dimensions.get('window').width) - 160),
        alignItems: 'center',
    },
    Navigator_title_text:{
        fontSize: 20,
        color:'#000000'
    },
    Navigator_start:{
        alignItems: 'flex-end',
        width: 80,
        height: 48,
        paddingRight: 12,
        paddingTop: 12
    },
    Navigator_start_text:{
        fontSize: 16,
        paddingTop: 2,
        color:'#eb5847',
        marginBottom: 12
    },
    sub_Navigator : {
        flexDirection: 'row',
        width: Dimensions.get('window').width,
        height: 40,
        backgroundColor: '#fff',
        alignItems: 'center',
    },
    sub_Navigator_menu: {
        width: 60,
        height: 40,
        alignItems: 'flex-end',
        justifyContent: 'center',
    },
    sub_Navigator_menu_on: {
        width: 40,
        height: 40,
        alignItems: 'center',
        justifyContent: 'center',
        borderBottomWidth: 2,
        borderBottomColor: '#000000',
    },
    sub_Navigator_menu_off: {
        width: 40,
        height: 40,
        alignItems: 'center',
        justifyContent: 'center',
        borderBottomWidth: 2,
        borderBottomColor: 'transparent',
    },
    sub_Navigator_name_on : {
        fontSize : 14,
        fontWeight: 'bold',
    },
    sub_Navigator_name_off : {
        fontSize : 14,
    },
    contents : {
        paddingTop :  9,
    },
    top_count : {
        width: Dimensions.get('window').width,
        height: 48,
        backgroundColor : '#30cbba',
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: 8
    },
    top_count_text : {
        fontSize: 16,
        color: '#fff'
    },
    contents_item : {
        width: Dimensions.get('window').width,
        height: 64,
        backgroundColor: '#fff',
        marginBottom: 10,
        alignItems: 'center',
        justifyContent: 'center',
    },
    contents_item_sub : {
        flexDirection: 'row',
        width: Dimensions.get('window').width,
        backgroundColor: '#fff',
        marginBottom: 8,
        paddingLeft: 10,
        paddingRight: 10,
        alignItems: 'center',
        justifyContent: 'center',
    },
    item_date : {
        fontSize: 16,
        color: '#838383',
        fontWeight: '300',
        height: 20
    },
    item_name: {
        paddingLeft: 10,
        fontSize: 16,
        fontWeight: '600',
        height: 20
    },
    item_per_count: {
        textAlign: 'right',
        fontSize: 16,
        fontWeight: '600',
        height: 20
    }


});

class Manager_sub extends  React.Component {
    constructor(props) {
        super(props);
        this.state = {
            menu : 1,
            record: [
                {
                    date: "2018.02.02",
                    name: "조현준",
                    count: 5
                },
                {
                    date: "2018.02.22",
                    name: "조현준",
                    count: 4
                },
                {
                    date: "2018.03.07",
                    name: "조현준",
                    count: 3
                },
                {
                    date: "2018.03.07",
                    name: "조현준2",
                    count: 3
                }
            ]
        };
        // todo 내역 받아와서 record에 넣기
        // todo 단골등록은 뭐지? api는 있나?
    }

    filterRecord() {
        let filtered = [];
        this.state.record.map(record => {
            if (record.name === this.props.customorName) {
                filtered.push(record);
            }
        });
        return filtered;
    }

    printRecord(record, index) {
        return <View style={styles.contents_item} key={"record" + index}>
            <View style={styles.contents_item_sub}>
                <View style={{width: Dimensions.get('window').width - 120, flexDirection: 'row', flex: 1 }}>
                    <Text style={styles.item_date}>{record.date}</Text>
                    <Text style={styles.item_name}>{record.name}</Text>
                </View>
                <View style={{width: 100}}>
                    <Text style={styles.item_per_count}>{record.count}명</Text>
                </View>
            </View>
        </View>;
    }

    render() {
        return (
            <View  style={styles.container}>
                <View style={styles.Navigator}>
                    <View style={styles.Navigator_menu}>
                        <TouchableHighlight style={styles.Navigator_menu_button} underlayColor={'transparent'} onPress={() => Actions.pop()}>
                            <Image style={styles.Navigator_menu_image} source={require("../../../assets/back_arrow.png")}/>
                        </TouchableHighlight>
                    </View>
                    <View style={styles.Navigator_title}><Text style={styles.Navigator_title_text}>{this.props.customorName}님 방문기록</Text></View>
                    <TouchableHighlight underlayColor={'transparent'} onPress={() => null}>
                        <View style={styles.Navigator_start}><Text style={styles.Navigator_start_text}>단골등록</Text></View>
                    </TouchableHighlight>
                </View>
                <ScrollView ref="queue" style={{marginTop: 9}}>
                    {
                        this.filterRecord().map((record, index) => this.printRecord(record, index))
                    }
                </ScrollView>
            </View>
        )
    }
}

export default Manager_sub;