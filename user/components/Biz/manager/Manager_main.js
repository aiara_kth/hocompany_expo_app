import React from 'react';
import {Text, View, StyleSheet, Image,TouchableHighlight, Dimensions, ScrollView, Menu} from 'react-native';
import { Actions } from 'react-native-router-flux';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import {Ionicons} from '@expo/vector-icons';
import TimerCountdown from "react-native-timer-countdown";
import Global from "../../../objects/Global";

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f9f9f9',
    },
    Navigator : {
        flexDirection: 'row',
        paddingTop : getStatusBarHeight(),
        width: Dimensions.get('window').width,
        height: 48 + getStatusBarHeight(),
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    Navigator_menu: {
        width: 80,
        height: 48,
    },
    Navigator_menu_button :{
        width: 48,
        height: 48,
        paddingTop: 12,
        paddingLeft: 12,
    },
    Navigator_menu_image : {
        width: 24,
        height: 24
    },
    Navigator_title:{
        width : ((Dimensions.get('window').width) - 160),
        alignItems: 'center',
    },
    Navigator_title_text:{
        fontSize: 16,
        color:'#000000'
    },
    Navigator_start:{
        alignItems: 'flex-end',
        width: 80,
        height: 48,
        paddingTop: 12,
        paddingRight: 12,
    },
    Navigator_start_text:{
        fontSize: 16,
        color:'#7db840',
    },
    sub_Navigator : {
        flexDirection: 'row',
        width: Dimensions.get('window').width,
        height: 40,
        backgroundColor: '#fff',
        alignItems: 'center',
    },
    sub_Navigator_menu: {
        width: 60,
        height: 40,
        alignItems: 'flex-end',
        justifyContent: 'center',
    },
    sub_Navigator_menu_on: {
        width: 40,
        height: 40,
        alignItems: 'center',
        justifyContent: 'center',
        borderBottomWidth: 2,
        borderBottomColor: '#000000',
    },
    sub_Navigator_menu_off: {
        width: 40,
        height: 40,
        alignItems: 'center',
        justifyContent: 'center',
        borderBottomWidth: 2,
        borderBottomColor: 'transparent',
    },
    sub_Navigator_name_on : {
        fontSize : 14,
        fontWeight: 'bold',
    },
    sub_Navigator_name_off : {
        fontSize : 14,
        color : '#a4a4a4'
    },
    contents : {
        paddingTop :  9,
    },
    top_count : {
        width: Dimensions.get('window').width,
        height: 48,
        backgroundColor : '#30cbba',
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: 8
    },
    top_count_text : {
        fontSize: 16,
        color: '#fff'
    },
    contents_item : {
        width: Dimensions.get('window').width,
        height: 182,
        backgroundColor: '#fff',
        marginBottom: 10
    },
    contents_item_sub : {
        width: Dimensions.get('window').width,
        height: 114,
        backgroundColor: '#fff',
        marginBottom: 8,
        paddingLeft: 20,
        paddingRight: 20
    },
    contents_item_info:{
        height: 142,
        paddingLeft: 20,
        paddingRight: 20
    },
    contents_item_button:{
        width: Dimensions.get('window').width,
        flexDirection: 'row',
        height: 40
    },
    contents_item_btn : {
        width: Dimensions.get('window').width,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        height: 40
    },
    contents_item_btn_half : {
        width: Dimensions.get('window').width / 2,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        height: 40
    },
    button_color_red : {
        backgroundColor : '#a25757'
    },
    button_color_green : {
        backgroundColor : '#7db840'
    },
    button_color_purple : {
        backgroundColor : 'rgba(127, 143, 209,0.5)'
    },
    button_color_gray : {
        backgroundColor : '#c4c4c4'
    },
    button_image : {
        width: 24,
        height: 24
    },
    button_text : {
        fontSize: 12,
        paddingLeft: 4,
        color: '#fff'
    },

    info_person : {
        flexDirection: 'row',
        width: Dimensions.get('window').width,
        height: 54,
        // justifyContent: 'bottom',
    },
    info_person_count :{
        marginTop: 4,
        fontSize: 40,
        color: '#eb5847',
        fontWeight: 'bold'
    },
    info_person_count_s : {
        width: Dimensions.get('window').width - 96,
    },
    info_person_call_btn : {
        marginTop: 18 ,
        width: 56,
        height: 24,
        borderWidth: 1,
        borderColor: '#eb5847',
        borderRadius: 12,
        alignItems: 'center',
        justifyContent: 'center'
    },
    info_person_call_word : {
        fontSize: 12,
        color: '#eb5947'
    },
    info_person_unit : {
        marginLeft: 4,
        height: 54,
        width: 12,
        paddingTop: 3,
        alignItems: 'center',
        justifyContent: 'center',
    },
    info_reserv : {
        paddingTop: 8,
        width: Dimensions.get('window').width,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },

    info_reserv_name : {
        width: 100,
        textAlign:'left',
        color: '#606060',
        fontSize: 14,
    },

    info_reserv_name_b : {
        width: 100,
        textAlign:'left',
        color: '#000000',
        fontSize: 14,
    },

    info_reserv_name_s : {
        width: 100,
        textAlign:'left',
        color: '#000000',
        fontSize: 14,
        // fontWeight: '400'
    },

    info_reserv_data : {
        width: Dimensions.get('window').width - 100,
        paddingRight: 40,
        textAlign:'right',
        fontSize: 14,
        fontWeight: '500'
    },

    info_reserv_data_s : {
        width: Dimensions.get('window').width - 100,
        paddingRight: 40,
        textAlign:'right',
        fontSize: 14,
        fontWeight: '400',
        color: '#838383'
    },

    info_person_name: {
        flexDirection: 'row',
        width: Dimensions.get('window').width,
        height: 46,
        // justifyContent: 'bottom',
    },

    info_person_name_s : {
        marginTop : 16,
        fontSize: 16,
        fontWeight: "700"
    },
    c_modal : {
        zIndex: 9999,
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height - getStatusBarHeight(),
        flexDirection: 'row',
        position: 'absolute',
        // backgroundColor: '#fff',
        left: 0,
        top: getStatusBarHeight()
    },
    c_modal_sidebar : {
        width: 256,
        height: Dimensions.get('window').height,
    },
    c_modal_shadow : {
        width: Dimensions.get('window').width - 256,
        height: Dimensions.get('window').height - getStatusBarHeight(),
        backgroundColor: 'rgba(0, 0, 0, 0.4)'
    },
    c_modal_user : {
        width: 256,
        height: 208,
        paddingTop: 48,
        paddingLeft: 12,
        backgroundColor: '#7f8fd1'
    },
    c_modal_user_images : {
        width: 48,
        height: 48,
        marginLeft: 12
    },
    c_modal_user_name : {
        marginTop: 16,
        fontSize: 20,
        color: "#ffffff"
    },
    c_modal_user_email : {
        fontSize: 12,
        color: "rgba(255,255,255,0.4)"
    },
    c_modal_button_group : {
        width: 256,
        height: Dimensions.get('window').height - (getStatusBarHeight()+208),
        backgroundColor: '#ffffff'
    },
    c_modal_button : {
        width: 256,
        height: 48,
        paddingLeft: 12,
        paddingTop: 12,
        fontSize: 16
    },store_open_switch : {
        width: 232,
        height: 40,
        marginTop: 39,
        flexDirection: 'row',
    },store_open_btn : {
        width: 116,
        height :  40,
        textAlign: 'center',
        paddingTop:  12,
    },store_open_btn_on : {
        backgroundColor: '#ffffff',
        color: '#7f8fd1'
    }, store_open_btn_off : {
        backgroundColor : '#7f8fd1',
        color: '#ffffff',
        borderColor: '#ffffff',
        borderWidth : 1
    },noti_dom : {
        zIndex: 9999,
        width: Dimensions.get('window').width,
        height: 97,
        position: 'absolute',
        alignItems: 'center',
        justifyContent: 'center',
        left: 0,
        top: getStatusBarHeight(),
        backgroundColor: '#ffffff',
        borderColor: 'rgba(0, 0, 0, 0.19)',
        borderBottomWidth: 1
    },noti_top:{
        width: 360,
        height: 89,
        flexDirection: 'row'
    },noti_bottom: {
    },noti_left : {
        paddingLeft: 10,
        paddingRight: 10,
        width: 234,
        alignItems: 'center',
        marginTop: 26,
        justifyContent: 'center'
    },noti_word : {
        lineHeight:24,
        fontSize:20,
    },noti_extra : {
        lineHeight: 20,
        fontSize: 14,
        color:'#c4c4c4'
    },noti_btn:{
        marginTop: 29,
        width: 56,
        height:40,
        marginRight: 7,
        borderRadius:20.5,
        alignItems : 'center',
        justifyContent: 'center',
    },noti_dom_s : {
        zIndex: 9999,
        width: Dimensions.get('window').width,
        height: 72,
        position: 'absolute',
        left: 0,
        top: getStatusBarHeight(),
        backgroundColor: '#ffffff',
        borderColor: 'rgba(0, 0, 0, 0.19)',
        borderBottomWidth: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },noti_top_s:{
        width: 360,
        height: 61,
        flexDirection: 'row',
    },noti_word_s : {
        lineHeight:24,
        fontSize:16,
        marginTop: 23,
        paddingLeft: 10
        // textAlign:'center'
    },

});

const RESERVATION_STATE_NEW = -1;
const RESERVATION_STATE_AFTER_CALL = 0;
const RESERVATION_STATE_DENIED = 1;
const RESERVATION_STATE_COMING = 2;

let reservations = [
    {
        index: 0,
        count: 6,
        name: "조현준",
        distance: 250,
        time: 30,
        acceptTime: 0,
        state: RESERVATION_STATE_AFTER_CALL,
        canDeny: true
    },
    {
        index: 1,
        count: 6,
        name: "조현준",
        distance: 250,
        time: 30,
        acceptTime: 0,
        state: RESERVATION_STATE_NEW,
        canDeny: true
    },
    {
        index: 2,
        count: 6,
        name: "조현준",
        distance: 250,
        time: 30,
        acceptTime: 0,
        state: RESERVATION_STATE_AFTER_CALL,
        canDeny: false
    },
    {
        index: 3,
        count: 6,
        name: "조현준",
        distance: 250,
        time: 30,
        acceptTime: 0,
        state: RESERVATION_STATE_NEW,
        canDeny: true
    },
    {
        index: 4,
        count: 6,
        name: "조현준",
        distance: 250,
        time: 30,
        acceptTime: 0,
        state: RESERVATION_STATE_NEW,
        canDeny: true
    },
    {
        index: 5,
        count: 6,
        name: "조현준",
        distance: 250,
        time: 8,
        acceptTime: 0,
        state: RESERVATION_STATE_COMING,
        canDeny: false
    }
];

let instance = null;
class Manager_main extends  React.Component {
    constructor(props) {
        super(props);
        this.state = {
            menu: 1,
            sidemenu: 0,
            store_open: 1,
            noti: 0,
            noti2: 0,
            selected_reservation: this.empty_reservation,
            reservations: reservations,
            reservationComingTime: []
        };
        instance = this;
    }

    componentDidMount() {
        let reservationComingTime = [];
        for(let i = 0; i <= this.getMaxReservationIndex(); i++) {
            let index = reservations.findIndex(res => res.index === i && res.state === RESERVATION_STATE_COMING);
            let time = index > 0 ? reservations[index].time * 60 : 0;
            reservationComingTime.push(time);
        }
        // console.log(reservationComingTime);
        this.setState({reservationComingTime: reservationComingTime});
    }

    sidemenu() {
        return <View style={[styles.c_modal, this.state.sidemenu === 1 ? {} : {display: 'none', position: 'relative'}]}>
            <View style={styles.c_modal_sidebar}>
                <View style={styles.c_modal_user}>
                    <Text style={styles.c_modal_user_name}>치킨어때?</Text>
                    <Text style={styles.c_modal_user_email}>현재 활성화된 점포</Text>
                    <TouchableHighlight underlayColor={'transparent'}
                                        onPress={() => this.setState({store_open: this.state.store_open === 1 ? 0 : 1})}>
                        <View style={styles.store_open_switch}>
                            <Text
                                style={[styles.store_open_btn, this.state.store_open === 1 ? styles.store_open_btn_on : styles.store_open_btn_off]}>영업중</Text>
                            <Text
                                style={[styles.store_open_btn, this.state.store_open === 0 ? styles.store_open_btn_on : styles.store_open_btn_off]}>영업종료</Text>
                        </View>
                    </TouchableHighlight>
                </View>
                <View style={styles.c_modal_button_group}>
                    <ScrollView>
                        <Text style={styles.c_modal_button} onPress={() => Actions.admin_main()}>점포관리</Text>
                        <Text style={styles.c_modal_button} onPress={() => Actions.Biz_change_shop()}>점포변경</Text>
                        <Text style={styles.c_modal_button} onPress={() => Actions.Manager_user_info()}>회원정보</Text>
                        <Text style={styles.c_modal_button} onPress={() => Actions.Manager_settting()}>설정</Text>
                        <Text style={styles.c_modal_button} onPress={() => Actions.Manager_noti()}>알림</Text>
                        <Text style={styles.c_modal_button} onPress={() => Actions.Manager_notice()}>공지사항</Text>
                        <Text style={styles.c_modal_button} onPress={() => Actions.Manager_guide()}>사용방법</Text>
                        <Text style={styles.c_modal_button} onPress={() => Actions.pop()}>뒤로가기</Text>
                    </ScrollView>
                </View>
            </View>
            <TouchableHighlight underlayColor={'transparent'} onPress={() => this.setState({sidemenu: 0})}>
                <View style={styles.c_modal_shadow}>

                </View>
            </TouchableHighlight>
        </View>;
    }

    navigator() {
        return <View style={styles.Navigator}>
            <View style={styles.Navigator_menu}>
                <TouchableHighlight style={styles.Navigator_menu_button} underlayColor={'transparent'}
                                    onPress={() => this.setState({sidemenu: 1})}>
                    <Image style={styles.Navigator_menu_image} source={require("../../../assets/menu.png")}/>
                </TouchableHighlight>
            </View>
            <View style={styles.Navigator_title}><Text style={styles.Navigator_title_text}>치킨어때?</Text></View>
            <TouchableHighlight underlayColor={'transparent'} onPress={() => null}>
                <View style={styles.Navigator_start}><Text style={styles.Navigator_start_text}> </Text></View>
            </TouchableHighlight>
        </View>;
    }

    navigator_tab_menu() {
        return <View style={styles.sub_Navigator}>
            <TouchableHighlight underlayColor={'transparent'} onPress={() => this.setState({menu: 1})}>
                <View style={styles.sub_Navigator_menu}>
                    <View
                        style={this.state.menu === 1 ? styles.sub_Navigator_menu_on : styles.sub_Navigator_menu_off}>
                        <Text
                            style={this.state.menu === 1 ? styles.sub_Navigator_name_on : styles.sub_Navigator_name_off}>대기중</Text>
                    </View>
                </View>
            </TouchableHighlight>
            <TouchableHighlight underlayColor={'transparent'} onPress={() => this.setState({menu: 2})}>
                <View style={styles.sub_Navigator_menu}>
                    <View
                        style={this.state.menu === 2 ? styles.sub_Navigator_menu_on : styles.sub_Navigator_menu_off}>
                        <Text
                            style={this.state.menu === 2 ? styles.sub_Navigator_name_on : styles.sub_Navigator_name_off}>오는중</Text>
                    </View>
                </View>
            </TouchableHighlight>
            <TouchableHighlight underlayColor={'transparent'} onPress={() => this.setState({menu: 3})}>
                <View style={styles.sub_Navigator_menu}>
                    <View
                        style={this.state.menu === 3 ? styles.sub_Navigator_menu_on : styles.sub_Navigator_menu_off}>
                        <Text
                            style={this.state.menu === 3 ? styles.sub_Navigator_name_on : styles.sub_Navigator_name_off}>선주문</Text>
                    </View>
                </View>
            </TouchableHighlight>
            <TouchableHighlight underlayColor={'transparent'} onPress={() => this.setState({menu: 4})}>
                <View style={styles.sub_Navigator_menu}>
                    <View
                        style={this.state.menu === 4 ? styles.sub_Navigator_menu_on : styles.sub_Navigator_menu_off}>
                        <Text
                            style={this.state.menu === 4 ? styles.sub_Navigator_name_on : styles.sub_Navigator_name_off}>대기열</Text>
                    </View>
                </View>
            </TouchableHighlight>
        </View>;
    }

    empty_reservation = {
        count: 0,
        name: "",
        distance: 0,
        time: 0,
        acceptTime: 0
    };

    openAcceptReservationNoti(reservation) {
        this.setState({
            noti: 1,
            noti2: 0,
            selected_reservation: reservation
        });
    }

    closeAcceptReservationNoti() {
        this.setState({
            noti: 0,
            selected_reservation: this.empty_reservation
        });
    }

    openAcceptReservationCompleteNoti() {
        this.acceptReservation(this.state.selected_reservation);
        this.setState({
            noti: 0,
            noti2: 1
        });
    }

    closeAcceptReservationCompleteNoti() {
        this.setState({
            noti2: 0,
            selected_reservation: this.empty_reservation
        });
    }

    acceptReservation(reservation) {
        let index = reservations.findIndex(res => res.index === reservation.index);
        if (index === -1) {
            return;
        }
        reservations[index].acceptTime = 20;
        reservations[index].state = RESERVATION_STATE_COMING;
        this.setState({reservations: reservations});
        // todo 예약 수락하는 api가 뭘까
    }

    denyReservation(reservation) {
        let index = reservations.findIndex(res => res.index === reservation.index);
        if (index === -1) {
            return;
        }
        reservations[index].state = RESERVATION_STATE_DENIED;
        this.setState({reservations: reservations});
        // todo 예약 거절하는 api는 뭐지
    }

    receiveCall(reservation) {
        let index = reservations.findIndex(res => res.index === reservation.index);
        if (index === -1) {
            return;
        }
        reservations[index].state = RESERVATION_STATE_AFTER_CALL;
        this.setState({reservations: reservations});
        // todo 콜받기 api는 뭐냐
    }

    newReservationAlarm(count) {
        if (count !== 0) {
            return <View style={styles.top_count}>
                <Text style={styles.top_count_text}>예약요청이 <Text
                    style={{fontWeight: "bold"}}>{count}건</Text> 있습니다.</Text>
            </View>;
        } else {
            return <View></View>;
        }
    }

    acceptReservationNoti() {
        return <View style={[styles.noti_dom, this.state.noti === 1 ? {} : {display: 'none', position: 'relative'}]}>
            <View style={styles.noti_top}>
                <View style={styles.noti_left}>
                    <Text style={styles.noti_word}>{this.state.selected_reservation.name}님의 자리있소?</Text>
                    <Text style={styles.noti_extra}>{this.state.selected_reservation.count} 명 | {this.state.selected_reservation.distance}m | {this.state.selected_reservation.count}분거리</Text>
                </View>
                <TouchableHighlight underlayColor={'transparent'} onPress={() => this.closeAcceptReservationNoti()}>
                    <View style={[styles.noti_btn, {backgroundColor: '#a25757'}]}>
                        <Image style={styles.button_image} source={require("../../../assets/common/circle_x.png")}/>
                    </View>
                </TouchableHighlight>
                <TouchableHighlight underlayColor={'transparent'} onPress={() => this.openAcceptReservationCompleteNoti()}>
                    <View style={[styles.noti_btn, {backgroundColor: '#7db840'}]}>
                        <Image style={styles.button_image} source={require("../../../assets/common/circle_check.png")}/>
                    </View>
                </TouchableHighlight>
            </View>
            <View style={styles.noti_bottom}>
                <TouchableHighlight underlayColor={'transparent'} onPress={() => this.closeAcceptReservationNoti()}>
                    <Text style={{width: 27, height: 4, backgroundColor: '#c4c4c4', borderRadius: 2}}></Text>
                </TouchableHighlight>
            </View>
        </View>;
    }

    acceptReservationCompleteNoti() {
        return <View style={[styles.noti_dom_s, this.state.noti2 === 1 ? {} : {display: 'none', position: 'relative'}]}>
            <TouchableHighlight underlayColor={'transparent'} onPress={() => this.closeAcceptReservationCompleteNoti()}>
                <View style={styles.noti_top_s}>
                    <Image style={[styles.button_image, {marginTop: 24, marginLeft: 20}]}
                           source={require("../../../assets/common/checkbox_sss.png")}/>
                    <Text style={[styles.noti_word_s, {}]}>{this.state.selected_reservation.name} 님 예약승인이 완료되었습니다.</Text>
                </View>
            </TouchableHighlight>
            <View style={styles.noti_bottom}>
                <TouchableHighlight underlayColor={'transparent'} onPress={() => this.closeAcceptReservationCompleteNoti()}>
                    <Text style={{width: 27, height: 4, backgroundColor: '#c4c4c4', borderRadius: 2}}></Text>
                </TouchableHighlight>
            </View>
        </View>;
    }

    queueReservation(reservation) {
        var hour = Math.floor(reservation.time / 60);
        if (hour < 10)
            hour = '0' + hour;
        var min = reservation.time % 60;
        if (min < 10)
            min = '0' + min;
        return <View style={styles.contents_item} key={"queueReservation" + reservation.index}>
            <View style={styles.contents_item_info}>
                <View style={styles.info_person}>
                    <Text style={styles.info_person_count}>{reservation.count}</Text>
                    <View style={styles.info_person_unit}>
                        <Text style={{fontSize: 12}}>명</Text>
                    </View>
                </View>
                <View style={styles.info_reserv}>
                    <Text style={styles.info_reserv_name}>예약자 명</Text>
                    <Text style={styles.info_reserv_data}>{reservation.name}</Text>
                </View>
                <View style={styles.info_reserv}>
                    <Text style={styles.info_reserv_name}>거리</Text>
                    <Text style={styles.info_reserv_data}>{reservation.distance}m</Text>
                </View>
                <View style={styles.info_reserv}>
                    <Text style={styles.info_reserv_name}>시간</Text>
                    <Text style={styles.info_reserv_data}>{hour}:{min}</Text>
                </View>
            </View>
            {
                reservation.state === RESERVATION_STATE_NEW
                    ? (
                        <View style={styles.contents_item_button}>
                            <TouchableHighlight underlayColor={'transparent'}
                                                onPress={() => this.receiveCall(reservation)}>
                                <View style={[styles.contents_item_btn, styles.button_color_purple]}>
                                    <Image style={styles.button_image}
                                           source={require("../../../assets/common/circle_check.png")}/>
                                    <Text style={styles.button_text}>콜 받기</Text>
                                </View>
                            </TouchableHighlight>
                        </View>
                    )
                    : (
                        <View style={styles.contents_item_button}>
                            <TouchableHighlight underlayColor={'transparent'}
                                                onPress={() => reservation.canDeny ? this.denyReservation(reservation) : null}>
                                <View
                                    style={[styles.contents_item_btn_half, reservation.canDeny ? styles.button_color_red : styles.button_color_gray]}>
                                    <Image style={styles.button_image}
                                           source={require("../../../assets/common/circle_x.png")}/>
                                    <Text style={styles.button_text}>거절</Text>
                                </View>
                            </TouchableHighlight>
                            <TouchableHighlight underlayColor={'transparent'}
                                                onPress={() => this.openAcceptReservationNoti(reservation)}>
                                <View style={[styles.contents_item_btn_half, styles.button_color_green]}>
                                    <Image style={styles.button_image}
                                           source={require("../../../assets/common/circle_check.png")}/>
                                    <Text style={styles.button_text}>수락</Text>
                                </View>
                            </TouchableHighlight>
                        </View>
                    )
            }
        </View>;
    }

    timerOnTick(reservation) {
        let index = reservations.findIndex(res => res.index === reservation.index);
        let timers = this.state.reservationComingTime;
        if (timers[index] === undefined)
            return;
        if (timers[index] > 0)
            timers[index]--;
        else
        {
            // todo 시간이 지나면 예약은 어떻게 되는거지... 그냥 둬도 되나?
            timers[index] = 0;
        }
        reservations[index].time = timers[index] / 60;
        this.setState({reservations: reservations, reservationComingTime: timers});
    }

    comingReservation(reservation) {
        var hour = Math.floor(reservation.time / 60);
        if (hour < 10)
            hour = '0' + hour;
        var min = Math.floor(reservation.time % 60);
        if (min < 10)
            min = '0' + min;
        return <View style={styles.contents_item} key={"comingReservation" + reservation.index}>
            <View style={styles.contents_item_info}>
                <View style={styles.info_person}>
                    <Text style={[styles.info_person_count, styles.info_person_count_s]}>{hour}:{min}</Text>
                    <TimerCountdown initialSecondsRemaining={reservation.time * 60}
                        //onTick={() => this.timerOnTick()}
                                    onTimeElapsed={() => this.timerOnTick(reservation)}
                                    style={{display: 'none'}}/>
                    <View style={styles.info_person_call_btn}>
                        <Text style={styles.info_person_call_word}>전화</Text>
                    </View>
                </View>
                <View style={styles.info_reserv}>
                    <Text style={[styles.info_reserv_name_b, {fontWeight: '500'}]}>예약자 명</Text>
                    <Text style={styles.info_reserv_data}>{reservation.name}</Text>
                </View>
                <View style={styles.info_reserv}>
                    <Text style={styles.info_reserv_name_b}>인원</Text>
                    <Text style={styles.info_reserv_data}>{reservation.count}명</Text>
                </View>
                <View style={styles.info_reserv}>
                    <Text style={styles.info_reserv_name_b}>거리</Text>
                    <Text style={styles.info_reserv_data}>{reservation.distance}m</Text>
                </View>
                <View style={styles.info_reserv}>
                    <Text style={styles.info_reserv_name_b}>시간</Text>
                    <Text style={styles.info_reserv_data}>유호기간 {reservation.acceptTime}분</Text>
                </View>
            </View>
        </View>;
    }

    getNewReservationsCount() {
        let count = 0;
        this.state.reservations.map(reservation => {
            if (reservation.state === RESERVATION_STATE_NEW) {
                count++;
            }
        });
        return count;
    }

    getMaxReservationIndex() {
        let max = 0;
        this.state.reservations.map(reservation => {
            if (max < reservation.index)
                max = reservation.index;
        });
        return max;
    }

    render() {
        return (
            <View style={styles.container}>
                {this.sidemenu()}
                {this.acceptReservationNoti()}
                {this.acceptReservationCompleteNoti()}
                {this.navigator()}
                {this.navigator_tab_menu()}
                <ScrollView ref="queue" style={[styles.contents, this.state.menu === 1 ? null : {display: 'none'}]}>
                    {this.newReservationAlarm(this.getNewReservationsCount())}
                    {this.state.reservations.map(reservation => {
                        return reservation.state === RESERVATION_STATE_NEW || reservation.state === RESERVATION_STATE_AFTER_CALL
                            ? this.queueReservation(reservation) : (<View key={"reservation__" + reservation.index} style={{display: 'none'}}></View>)
                    })}
                </ScrollView>
                <ScrollView ref="come" style={[styles.contents, this.state.menu === 2 ? null : {display: 'none'}]}>
                    {this.newReservationAlarm(this.getNewReservationsCount())}
                    {this.state.reservations.map(reservation => {
                        return reservation.state === RESERVATION_STATE_COMING
                            ? this.comingReservation(reservation) : (<View key={"reservation__" + reservation.index} style={{display: 'none'}}></View>)
                    })}
                </ScrollView>
                <ScrollView ref="order" style={[styles.contents, this.state.menu === 3 ? null : {display: 'none'}]}>
                    {this.newReservationAlarm(this.getNewReservationsCount())}
                </ScrollView>
                <ScrollView ref="waiting" style={[styles.contents, this.state.menu === 4 ? null : {display: 'none'}]}>
                    {this.newReservationAlarm(this.getNewReservationsCount())}
                    <TouchableHighlight underlayColor={'transparent'} onPress={() => Actions.Manager_sub({customorName: "조현준"})}>
                        <View style={styles.contents_item_sub}>
                            <View style={styles.info_person_name}>
                                <Text style={styles.info_person_name_s}>조현준</Text>
                            </View>
                            <View style={styles.info_reserv}>
                                <Text style={styles.info_reserv_name_s}>최근 방문날짜</Text>
                                <Text style={styles.info_reserv_data_s}>2018.02.02</Text>
                            </View>
                            <View style={styles.info_reserv}>
                                <Text style={styles.info_reserv_name_s}>방문횟수</Text>
                                <Text style={[styles.info_reserv_data_s, {fontWeight: 'bold'}]}>5회</Text>
                            </View>
                        </View>
                    </TouchableHighlight>
                </ScrollView>
            </View>
        )
    }
}

export default Manager_main;