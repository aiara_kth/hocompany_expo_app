import React from 'react';
import {Text, View, StyleSheet, Image,TouchableHighlight, Dimensions, ScrollView} from 'react-native';
import { Actions } from 'react-native-router-flux';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import {Ionicons} from '@expo/vector-icons';
import '../../../objects/Global';
import '../../../assets/Policy/shopPolicy.js';
import '../../../assets/Policy/userPolicy.js';
import '../../../assets/Policy/privacyPolicy.js';
import '../../../assets/Policy/locationPolicy.js';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f9f9f9',
    },
    Navigator : {
        flexDirection: 'row',
        marginTop : getStatusBarHeight(),
        width: Dimensions.get('window').width,
        height: 48,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    Navigator_menu: {
        width: 80,
        height: 48,
    },
    Navigator_menu_button :{
        width: 48,
        height: 48,
        paddingTop: 12,
        paddingLeft: 12,
    },
    Navigator_menu_image : {
        width: 24,
        height: 24
    },
    Navigator_title:{
        width : ((Dimensions.get('window').width) - 160),
        alignItems: 'center',
    },
    Navigator_title_text:{
        fontSize: 20,
        color:'#000000'
    },
    Navigator_start:{
        alignItems: 'flex-end',
        width: 80,
        height: 48,
        paddingRight: 12,
    },
    Navigator_start_text:{
        fontSize: 16,
        color:'#7db840',
        marginTop: 12,
        marginBottom: 12
    },
    sub_Navigator : {
        flexDirection: 'row',
        width: Dimensions.get('window').width,
        height: 40,
        backgroundColor: '#fff',
        alignItems: 'center',
    },
    sub_Navigator_menu: {
        width: 60,
        height: 40,
        alignItems: 'flex-end',
        justifyContent: 'center',
    },
    sub_Navigator_menu_on: {
        width: 40,
        height: 40,
        alignItems: 'center',
        justifyContent: 'center',
        borderBottomWidth: 2,
        borderBottomColor: '#000000',
    },
    sub_Navigator_menu_off: {
        width: 40,
        height: 40,
        alignItems: 'center',
        justifyContent: 'center',
        borderBottomWidth: 2,
        borderBottomColor: 'transparent',
    },
    sub_Navigator_name_on : {
        fontSize : 14,
        fontWeight: 'bold',
    },
    sub_Navigator_name_off : {
        fontSize : 14,
    },
    text_policy : {
        padding : 25
    }
});

class Manager_policy_detail extends  React.Component {
    constructor(props) {
        super(props);
    }

    policyTitle() {
        switch (this.props.policy) {
            case 0:
                return shopPolicyTitle;
            case 1:
                return userPolicyTitle;
            case 2:
                return privacyPolicyTitle;
            case 3:
                return locationPolicyTitle;
        }
    }

    policyText() {
        switch (this.props.policy) {
            case 0:
                return shopPolicy;
            case 1:
                return userPolicy;
            case 2:
                return privacyPolicy;
            case 3:
                return locationPolicy;
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.Navigator}>
                    <View style={styles.Navigator_menu}>
                        <TouchableHighlight style={styles.Navigator_menu_button} underlayColor={'transparent'}
                                            onPress={() => Actions.pop()}>
                            <Image style={styles.Navigator_menu_image}
                                   source={require("../../../assets/back_arrow.png")}/>
                        </TouchableHighlight>
                    </View>
                    <View style={styles.Navigator_title}><Text style={styles.Navigator_title_text}>{this.policyTitle()}</Text></View>
                    <TouchableHighlight underlayColor={'transparent'} onPress={() => null}>
                        <View style={styles.Navigator_start}><Text style={styles.Navigator_start_text}></Text></View>
                    </TouchableHighlight>
                </View>
                <ScrollView>
                    <Text style={styles.text_policy}>{this.policyText()}</Text>
                </ScrollView>
            </View>
        )
    }
}

export default Manager_policy_detail;