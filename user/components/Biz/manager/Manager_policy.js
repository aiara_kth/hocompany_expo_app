import React from 'react';
import {Text, View, StyleSheet, Image,TouchableHighlight, Dimensions} from 'react-native';
import { Actions } from 'react-native-router-flux';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import {Ionicons} from '@expo/vector-icons';
import '../../../objects/Global';
import Global from "../../../objects/Global";

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f9f9f9',
    },
    Navigator : {
        flexDirection: 'row',
        marginTop : getStatusBarHeight(),
        width: Dimensions.get('window').width,
        height: 48,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    Navigator_menu: {
        width: 80,
        height: 48,
    },
    Navigator_menu_button :{
        width: 48,
        height: 48,
        paddingTop: 12,
        paddingLeft: 12,
    },
    Navigator_menu_image : {
        width: 24,
        height: 24
    },
    Navigator_title:{
        width : ((Dimensions.get('window').width) - 160),
        alignItems: 'center',
    },
    Navigator_title_text:{
        fontSize: 20,
        color:'#000000'
    },
    Navigator_start:{
        alignItems: 'flex-end',
        width: 80,
        height: 48,
        paddingRight: 12,
    },
    Navigator_start_text:{
        fontSize: 16,
        color:'#7db840',
        marginTop: 12,
        marginBottom: 12
    },
    sub_Navigator : {
        flexDirection: 'row',
        width: Dimensions.get('window').width,
        height: 40,
        backgroundColor: '#fff',
        alignItems: 'center',
    },
    sub_Navigator_menu: {
        width: 60,
        height: 40,
        alignItems: 'flex-end',
        justifyContent: 'center',
    },
    sub_Navigator_menu_on: {
        width: 40,
        height: 40,
        alignItems: 'center',
        justifyContent: 'center',
        borderBottomWidth: 2,
        borderBottomColor: '#000000',
    },
    sub_Navigator_menu_off: {
        width: 40,
        height: 40,
        alignItems: 'center',
        justifyContent: 'center',
        borderBottomWidth: 2,
        borderBottomColor: 'transparent',
    },
    sub_Navigator_name_on : {
        fontSize : 14,
        fontWeight: 'bold',
    },
    sub_Navigator_name_off : {
        fontSize : 14,
    },
    contents_item : {
        width: Dimensions.get('window').width,
        height: 48,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    contents_item_sub : {
        flexDirection: 'row',
        width: Dimensions.get('window').width,
        backgroundColor: '#fff',
        marginBottom: 8,
        paddingLeft: 10,
        paddingRight: 10,
        alignItems: 'center',
        justifyContent: 'center',
    },
    item_group : {
        paddingLeft: 12,
        flex: 1
    },
    item_text: {
        marginTop: 12,
        fontSize: 14,
        height: 20
    },
});

class Manager_policy extends  React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.Navigator}>
                    <View style={styles.Navigator_menu}>
                        <TouchableHighlight style={styles.Navigator_menu_button} underlayColor={'transparent'}
                                            onPress={() => Actions.pop()}>
                            <Image style={styles.Navigator_menu_image}
                                   source={require("../../../assets/back_arrow.png")}/>
                        </TouchableHighlight>
                    </View>
                    <View style={styles.Navigator_title}><Text style={styles.Navigator_title_text}>약관 및 정책</Text></View>
                    <TouchableHighlight underlayColor={'transparent'} onPress={() => null}>
                        <View style={styles.Navigator_start}><Text style={styles.Navigator_start_text}></Text></View>
                    </TouchableHighlight>
                </View>
                <TouchableHighlight underlayColor={'transparent'} onPress={() => Actions.Manager_policy_detail({policy: 0})}>
                    <View style={styles.contents_item}>
                        <View style={styles.contents_item_sub}>
                            <View style={styles.item_group}>
                                <Text style={styles.item_text}>업주용 이용약관</Text>
                            </View>
                        </View>
                    </View>
                </TouchableHighlight>
                <TouchableHighlight underlayColor={'transparent'} onPress={() => Actions.Manager_policy_detail({policy: 1})}>
                    <View style={styles.contents_item}>
                        <View style={styles.contents_item_sub}>
                            <View style={styles.item_group}>
                                <Text style={styles.item_text}>소비자 이용약관</Text>
                            </View>
                        </View>
                    </View>
                </TouchableHighlight>
                <TouchableHighlight underlayColor={'transparent'} onPress={() => Actions.Manager_policy_detail({policy: 2})}>
                    <View style={styles.contents_item}>
                        <View style={styles.contents_item_sub}>
                            <View style={styles.item_group}>
                                <Text style={styles.item_text}>개인정보 이용약관</Text>
                            </View>
                        </View>
                    </View>
                </TouchableHighlight>
                <TouchableHighlight underlayColor={'transparent'} onPress={() => Actions.Manager_policy_detail({policy: 3})}>
                    <View style={styles.contents_item}>
                        <View style={styles.contents_item_sub}>
                            <View style={styles.item_group}>
                                <Text style={styles.item_text}>위치정보 이용약관</Text>
                            </View>
                        </View>
                    </View>
                </TouchableHighlight>
            </View>
        )
    }
}

export default Manager_policy;