import React from 'react';
import {Text, View, StyleSheet, Image,TextInput,TouchableHighlight, Dimensions, Alert ,AsyncStorage,Menu} from 'react-native';
import { Actions } from 'react-native-router-flux';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import {Ionicons} from '@expo/vector-icons';
import Global from "../../../objects/Global";
import API from "../../../objects/API";

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f9f9f9',
    },
    Navigator : {
        flexDirection: 'row',
        paddingTop : getStatusBarHeight(),
        width: Dimensions.get('window').width,
        height: 48 + getStatusBarHeight(),
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    Navigator_menu: {
        width: 80,
        height: 48,
    },
    Navigator_menu_button :{
        width: 48,
        height: 48,
        paddingTop: 12,
        paddingLeft: 12,
    },
    Navigator_menu_image : {
        width: 24,
        height: 24
    },
    Navigator_title:{
        width : ((Dimensions.get('window').width) - 160),
        alignItems: 'center',
    },
    Navigator_title_text:{
        fontSize: 16,
        color:'#000000'
    },
    Navigator_start:{
        alignItems: 'flex-end',
        width: 80,
        height: 48,
        paddingTop: 12,
        paddingRight: 12,
    },
    Navigator_start_text:{
        fontSize: 16,
        color:'#7db840',
    },
    sub_Navigator : {
        flexDirection: 'row',
        width: Dimensions.get('window').width,
        height: 40,
        backgroundColor: '#fff',
        alignItems: 'center',
    },
    sub_Navigator_menu: {
        width: 85,
        height: 40,
        alignItems: 'flex-end',
        justifyContent: 'center',
        marginLeft: 15,
        marginRight: 15
    },
    sub_Navigator_menu_on: {
        width: 65,
        height: 40,
        alignItems: 'center',
        justifyContent: 'center',
        borderBottomWidth: 2,
        borderBottomColor: '#000000',
    },
    sub_Navigator_menu_off: {
        width: 65,
        height: 40,
        alignItems: 'center',
        justifyContent: 'center',
        borderBottomWidth: 2,
        borderBottomColor: 'transparent',
    },
    sub_Navigator_name_on : {
        fontSize : 14,
        fontWeight: 'bold',
        alignItems: 'center',
    },
    sub_Navigator_name_off : {
        fontSize : 14,
        alignItems: 'center',
    },
    title_ws : {
        fontSize: 12,
        height: 24,
        color: '#7f8fd1'
    },
    contents : {
        // paddingTop :  9,
        width: Dimensions.get('window').width,
        // backgroundColor: '#ffffff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    contents_list : {
        width: (Dimensions.get('window').width ),
        marginTop : 8,
        backgroundColor: '#ffffff',
    },

    header_1 : {
        paddingTop:9,
        width: (Dimensions.get('window').width ),
        backgroundColor: '#ffffff',
        height: 128
    },
    title: {
        paddingLeft: 13,
        width: (Dimensions.get('window').width ),
        height: 128,
        justifyContent: 'center',
        paddingRight:56
    },
    title_w : {
        fontSize: 32,
        height: 48,
        color: '#000000',
    },
    item_menu : {
        width: (Dimensions.get('window').width ),
        flexDirection: 'row',
    },
    item_menu_text : {
        width: (Dimensions.get('window').width - 116),
        paddingLeft: 12,
        paddingTop : 16,
    },
    item_menu_text_name : {
        fontSize: 16,
        color: '#000000',

    },
    item_menu_text_info : {
        fontSize: 14,
        color: '#606060'
    },
    item_menu_text_price : {
        fontSize: 10,
        fontWeight: '500',
        color: '#606060'
    },
    item_menu_image_box : {
        width: 116,
        paddingTop: 16,
        paddingLeft: 8,
        paddingRight: 8
    },
    item_menu_image : {
        width: 96,
        height: 96,
        resizeMode : 'contain'
    },
    add_admin_button : {
        width: 100,
        height: 32,
        borderRadius: 12,
        borderColor: '#e8e8e8',
        borderWidth: 1,
        paddingTop : 7,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    add_admin_button_text : {
        fontSize: 12,
        height: 32,
        color: '#8e8e8e'
    },
    form_bottom : {
        width: (Dimensions.get('window').width - 40),
        height: 96,
    },
    input_t2 : {
        width: (Dimensions.get('window').width - 40),
        height: 96,
        paddingLeft: 12,
        paddingRight : 12,
        paddingTop :10,
        paddingBottom : 10,
    },
    form_bottom_text : {
        width: Dimensions.get('window').width,
        height: 24,
        lineHeight:24,
        paddingLeft: 12,
        paddingRight : 12,
        // verticalAlign: 'top',
        textAlign: 'right',
        color: '#606060',
        fontSize: 8
    }

});

const text_length_limit = 2000;

class admin_add_info extends  React.Component {
    constructor(props) {
        super(props);
        this.state = {
            show_modal: false,
            desc: this.props.shop !== undefined && this.props.shop !== null ? this.props.shop.description : ''
        };
    }

    onChangeText(text) {
        if (text.length >= text_length_limit) {
            Alert.alert('매장 정보 수정', text_length_limit + '자 까지만 입력해 주세요.', [{text: "확인", onPress: () => null}]);
            this.refs.input.focus();
            return;
        }
        this.setState({desc: text});
    }

    endScene() {
        Alert.alert('매장 정보 수정', '저장하시겠습니까?',
            [
                {text: "취소", onPress: () => null, styles: 'cancel'},
                {text: "확인", onPress: () => this.saveAndPop()}
            ]
        );
    }

    saveAndPop() {
        if (this.props.shop === undefined || this.props.shop === null) {
            Actions.pop();
            return;
        }
        let shop = this.props.shop;
        shop.description = this.state.desc;
        AsyncStorage.setItem("@shop", JSON.stringify(shop), () => {
            API.shop.admin.put(shop.shopId, {
                shopDescription: shop.description
            }, data => {
                Actions.pop({refresh: {shop: shop}});
            }, error => {
                Actions.pop({refresh: {shop: shop}});
            });
        });
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.Navigator}>
                    <View style={styles.Navigator_menu}>
                        <TouchableHighlight style={styles.Navigator_menu_button} underlayColor={'transparent'}
                                            onPress={() => Actions.pop()}>
                            <Image style={styles.Navigator_menu_image}
                                   source={require("../../../assets/back_arrow.png")}/>
                        </TouchableHighlight>
                    </View>
                    <View style={styles.Navigator_title}><Text style={styles.Navigator_title_text}> </Text></View>
                    <TouchableHighlight underlayColor={'transparent'} onPress={() => this.endScene()}>
                        <View style={styles.Navigator_start}><Text style={styles.Navigator_start_text}>완료</Text></View>
                    </TouchableHighlight>
                </View>
                <View style={styles.contents}>
                    <View style={styles.header_1}>
                        <View style={styles.title}>
                            <Text style={styles.title_w}>소개등록</Text>
                            <Text style={styles.title_ws}>본인의 점포의 대한 소개를 등록해주세요.</Text>
                            <Text style={styles.title_ws}>좋은 소개는 고객의 유입을 이끌어냅니다!</Text>
                        </View>
                    </View>
                    <View style={styles.contents_list}>
                        <View style={styles.form_bottom}>
                            <TextInput style={styles.input_t2} multiline={true} placeholder={'점포소개를 입력해주세요.'}
                                       underlineColorAndroid='transparent' returnKeyType="done" value={this.state.desc} ref="input"
                                       onChangeText={text => this.onChangeText(text)}/>
                        </View>
                        <Text style={styles.form_bottom_text}>{this.state.desc.length}/2000</Text>
                    </View>
                </View>
            </View>
        )
    }
}

export default admin_add_info;