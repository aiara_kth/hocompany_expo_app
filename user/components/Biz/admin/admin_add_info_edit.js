import React from 'react';
import {Text, View, StyleSheet, Image,TextInput,TouchableHighlight, Dimensions, ScrollView, Alert ,AsyncStorage,Menu} from 'react-native';
import { Actions } from 'react-native-router-flux';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import {Ionicons} from '@expo/vector-icons';
import DateTimePicker from 'react-native-modal-datetime-picker';
import Global from "../../../objects/Global";
import API from "../../../objects/API";

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f9f9f9',
    },
    Navigator : {
        flexDirection: 'row',
        marginTop : getStatusBarHeight(),
        width: Dimensions.get('window').width,
        height: 48,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    Navigator_menu: {
        width: 80,
        height: 48,
    },
    Navigator_menu_button :{
        width: 48,
        height: 48,
        paddingTop: 12,
        paddingLeft: 12,
    },
    Navigator_menu_image : {
        width: 24,
        height: 24
    },
    Navigator_title:{
        width : ((Dimensions.get('window').width) - 160),
        alignItems: 'center',
    },
    Navigator_title_text:{
        fontSize: 16,
        color:'#000000'
    },
    Navigator_start:{
        alignItems: 'flex-end',
        width: 80,
        height: 48,
        paddingTop: 12,
        paddingRight: 12,
    },
    Navigator_start_text:{
        fontSize: 16,
        color:'#7db840',
    },
    sub_Navigator : {
        flexDirection: 'row',
        width: Dimensions.get('window').width,
        height: 40,
        backgroundColor: '#fff',
        alignItems: 'center',
    },
    sub_Navigator_menu: {
        width: 85,
        height: 40,
        alignItems: 'flex-end',
        justifyContent: 'center',
        marginLeft: 15,
        marginRight: 15
    },
    sub_Navigator_menu_on: {
        width: 65,
        height: 40,
        alignItems: 'center',
        justifyContent: 'center',
        borderBottomWidth: 2,
        borderBottomColor: '#000000',
    },
    sub_Navigator_menu_off: {
        width: 65,
        height: 40,
        alignItems: 'center',
        justifyContent: 'center',
        borderBottomWidth: 2,
        borderBottomColor: 'transparent',
    },
    sub_Navigator_name_on : {
        fontSize : 14,
        fontWeight: 'bold',
        alignItems: 'center',
    },
    sub_Navigator_name_off : {
        fontSize : 14,
        alignItems: 'center',
    },
    title_ws : {
        fontSize: 12,
        height: 24,
        color: '#7f8fd1'
    },
    contents : {
        // marginTop :  9,
        paddingTop: 11,
        paddingBottom:11,
        width: Dimensions.get('window').width,
        // backgroundColor: '#ffffff',
        alignItems: 'center',
        justifyContent: 'center',
        paddingLeft: 20,
        paddingRight: 20,
    },
    form_top: {
        flexDirection: 'row',
        width: Dimensions.get('window').width,
        height: 104,
        paddingLeft: 20,
    },
    inputfile : {
        width: 104,
        height: 104
    },
    form_top_input_group : {
        width: (Dimensions.get('window').width - 124),
    },
    input_t1_form : {
        width: (Dimensions.get('window').width - 24),
        height: 56,
        // marginLeft : 12,
        // marginRight : 12,
        backgroundColor: '#fafbfb',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection:'row'
    },
    input_t1: {
        width: (Dimensions.get('window').width - 48),
    },
    form_bottom : {
        width: (Dimensions.get('window').width - 40),
        height: 110,
        paddingLeft: 20,
        marginTop: 8,
        backgroundColor: '#fafbfb',
    },
    input_t2 : {
        width: (Dimensions.get('window').width - 40),
        height: 100,
    },
    inputform : {
        width: (Dimensions.get('window').width ),
        height: 92,
        paddingLeft: 12,
        paddingTop: 18,
        backgroundColor: '#ffffff',
        marginBottom: 8
    },
    inputform_2 : {
        width: (Dimensions.get('window').width ),
        height: 164,
        paddingLeft: 12,
        paddingTop: 18,
        backgroundColor: '#ffffff',
        marginBottom: 8
    },
    inputform_dummy : {
        width: (Dimensions.get('window').width ),
        height: 92,
        paddingLeft: 12,
        paddingTop: 18,
        // backgroundColor: '#ffffff'
    },


});

class admin_add_info_edit extends  React.Component {
    constructor(props) {
        super(props);
        this.state = {
            show_modal: false,
            photo: this.props.shop !== undefined && this.props.shop !== null && this.props.shop.photo !== null ? this.props.shop.photo : require("../../../assets/common/add_image_dummy.png"),
            name: this.props.shop !== undefined && this.props.shop !== null ? this.props.shop.name : '',
            openTime: this.props.shop !== undefined && this.props.shop !== null ? this.props.shop.openTime : '',
            closeTime: this.props.shop !== undefined && this.props.shop !== null ? this.props.shop.closeTime : '',
            address: this.props.shop !== undefined && this.props.shop !== null ? this.props.shop.address : '',
            showOpenTimePicker: false,
            showCloseTimePicker: false
        };
    }

    add_start() {
        if (this.state.name === '') {
            Alert.alert('매장 정보 수정', '식당명을 입력해주세요.', [{text: "확인", onPress: () => null}]);
            this.refs.shopName.focus();
            return;
        }

        if (this.state.openTime === '') {
            Alert.alert('매장 정보 수정', '운영 시작 시간을 입력해주세요.', [{text: "확인", onPress: () => null}]);
            this.refs.openTime.focus();
            return;
        }

        if (this.state.closeTime === '') {
            Alert.alert('매장 정보 수정', '운영 종료 시간을 입력해주세요.', [{text: "확인", onPress: () => null}]);
            this.refs.closeTime.focus();
            return;
        }

        if (this.state.address === '') {
            Alert.alert('매장 정보 수정', '주소를 입력해주세요.', [{text: "확인", onPress: () => null}]);
            this.refs.shopAddress.focus();
            return;
        }

        Alert.alert('매장 정보 수정', '저장하시겠습니까?',
            [
                {text: "취소", onPress: () => null, styles: 'cancel'},
                {text: "확인", onPress: () => this.saveAndPop()}
            ]
        );
    }

    saveAndPop() {
        if (this.props.shop === undefined || this.props.shop === null) {
            Actions.pop();
            return;
        }
        let shop = this.props.shop;
        shop.photo = this.state.photo;
        shop.name = this.state.name;
        shop.openTime = this.state.openTime;
        shop.closeTime = this.state.closeTime;
        shop.address = this.state.address;
        AsyncStorage.setItem("@shop", JSON.stringify(shop), () => {
            API.shop.admin.put(shop.shopId, {
                mainProfileUrl: shop.photo,
                shopName: shop.name,
                operationHours: [
                    {
                        days: [
                            "mon", "tue", "wed"
                        ],
                        openHours: shop.openTime + "-" + shop.closeTime
                    }
                ],
                shopAddress: shop.address
            }, data => {
                Actions.pop({refresh: {shop: shop}});
            }, error => {
                Actions.pop({refresh: {shop: shop}});
            });
        });
    }

    dateTimePickerOnConfirm(isOpen, time) {
        if (isOpen)
            this.setState({showOpenTimePicker: false, openTime: this.timeToString(time)});
        else
            this.setState({showCloseTimePicker: false, closeTime: this.timeToString(time)});
    }

    dateTimePickerOnCancel(isOpen) {
        if (isOpen)
            this.setState({showOpenTimePicker: false});
        else
            this.setState({showCloseTimePicker: false});
    }

    timeToString(time) {
        let hour = time.getHours();
        let minute = time.getMinutes();
        if (hour < 10) {
            hour = '0' + hour;
        }
        if (minute < 10) {
            minute = '0' + minute;
        }
        return hour + ':' + minute;
    }

    getInitialTime(isOpen) {
        let splited = [];
        if (isOpen) {
            splited = this.state.openTime.split(':');
        } else {
            splited = this.state.closeTime.split(':');
        }
        let date = new Date();
        let hour = parseInt(splited[0]);
        let minute = parseInt(splited[1]);
        date.setHours(hour);
        date.setMinutes(minute);
        return date;
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.Navigator}>
                    <View style={styles.Navigator_menu}>
                        <TouchableHighlight style={styles.Navigator_menu_button} underlayColor={'transparent'}
                                            onPress={() => Actions.pop()}>
                            <Image style={styles.Navigator_menu_image}
                                   source={require("../../../assets/back_arrow.png")}/>
                        </TouchableHighlight>
                    </View>
                    <View style={styles.Navigator_title}><Text style={styles.Navigator_title_text}>정보수정</Text></View>
                    <TouchableHighlight underlayColor={'transparent'} onPress={() => this.add_start()}>
                        <View style={styles.Navigator_start}><Text style={styles.Navigator_start_text}>완료</Text></View>
                    </TouchableHighlight>
                </View>
                <View style={styles.contents}>
                    <ScrollView style={{width: Dimensions.get('window').width}}>
                        <TouchableHighlight underlayColor={'transparent'}
                                            onPress={() => Global.imageUploader.upload(result => {
                                                this.setState({photo: Global.imageUploader.toRequiredImage(result)});
                                            })}>
                            <Image style={{
                                width: Dimensions.get('window').width,
                                height: Dimensions.get('window').width * 0.75
                            }} source={this.state.photo}/>
                        </TouchableHighlight>
                        <View style={{width: Dimensions.get('window').width}}>
                            <View style={styles.inputform}>
                                <View style={[styles.input_t1_form, {marginBottom: 8}]}>
                                    <TextInput style={styles.input_t1} placeholder={'식당명을 입력해주세요.'}
                                               underlineColorAndroid='transparent' returnKeyType="done"
                                               ref="shopName" value={this.state.name}
                                               onChangeText={text => this.setState({name: text})}/>
                                </View>
                            </View>
                            <View style={styles.inputform_2}>
                                <TouchableHighlight underlayColor={'transparent'}
                                                    onPress={() => this.setState({showOpenTimePicker: true})}>
                                    <View style={[styles.input_t1_form, {marginBottom: 10}]}>
                                        <Text style={styles.input_t1}>
                                            {this.state.openTime === '' ? '시작시간 선택' : this.state.openTime}
                                        </Text>
                                        <DateTimePicker isVisible={this.state.showOpenTimePicker} mode="time"
                                                        date={this.getInitialTime(true)}
                                                        onConfirm={time => this.dateTimePickerOnConfirm(true, time)}
                                                        onCancel={() => this.dateTimePickerOnCancel(true)}/>
                                    </View>
                                </TouchableHighlight>
                                <TouchableHighlight underlayColor={'transparent'}
                                                    onPress={() => this.setState({showCloseTimePicker: true})}>
                                    <View style={[styles.input_t1_form, {marginBottom: 10}]}>
                                        <Text style={styles.input_t1}>
                                            {this.state.closeTime === '' ? '종료시간 선택' : this.state.closeTime}
                                        </Text>
                                        <DateTimePicker isVisible={this.state.showCloseTimePicker} mode="time"
                                                        date={this.getInitialTime(false)}
                                                        onConfirm={time => this.dateTimePickerOnConfirm(false, time)}
                                                        onCancel={() => this.dateTimePickerOnCancel(false)}/>
                                    </View>
                                </TouchableHighlight>
                            </View>
                            <View style={styles.inputform}>
                                <View style={[styles.input_t1_form, {marginBottom: 8}]}>
                                    <TextInput style={styles.input_t1} placeholder={'주소를 입력해주세요.'}
                                               underlineColorAndroid='transparent' returnKeyType="done"
                                               ref="shopAddress" value={this.state.address}
                                               onChangeText={text => this.setState({address: text})}/>
                                </View>
                            </View>
                            <View style={styles.inputform_dummy}></View>
                        </View>
                    </ScrollView>
                </View>
            </View>
        )
    }
}

export default admin_add_info_edit;