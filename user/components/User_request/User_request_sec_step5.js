import React from 'react';
import {Text, View, StyleSheet, Image,TouchableHighlight, Dimensions, ScrollView, Menu} from 'react-native';
import { Actions } from 'react-native-router-flux';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import {Ionicons} from '@expo/vector-icons';
import Global from "../../objects/Global";


const styles = StyleSheet.create({
    container: {
        flex: 1,
        // backgroundColor: '#f9f9f9',
        backgroundColor: '#fff',
    },
    Navigator : {
        flexDirection: 'row',
        marginTop : getStatusBarHeight(),
        width: Dimensions.get('window').width,
        height: 48,
        backgroundColor: '#fff',
    },
    Navigator_menu: {
        width: 80,
        height: 48,
    },
    Navigator_menu_button :{
        width: 48,
        height: 48,
        paddingTop: 12,
        paddingLeft: 12,
    },
    Navigator_menu_image : {
        width: 24,
        height: 24
    },
    Navigator_title:{
        width : ((Dimensions.get('window').width) - 160),
        alignItems: 'center',
    },
    Navigator_title_text:{
        fontSize: 16,
        color:'#000000'
    },
    Navigator_start:{
        alignItems: 'flex-end',
        width: 80,
        height: 48,
        paddingTop: 12,
        paddingRight: 12,
    },
    Navigator_start_text:{
        fontSize: 16,
        color:'#7f8fd1',
    },
    callout_box : {
        width : (Dimensions.get('window').width),
        height: 40,
        borderRadius: 2,
        backgroundColor: '#7f8fd1',
        alignItems: 'center',
        justifyContent: 'center',
        shadowOffset: {width: 2, height: 10 },
        shadowColor: 'rgba(0, 0, 0, 0.2)',
        paddingRight: 56
    },
    callout_box_text : {
        fontSize : 12,
        color: '#ffffff'
    },
    contents : {
        height: (Dimensions.get('window').height - (121 + getStatusBarHeight())),
        flex: 1,
        paddingTop : 10,
        alignItems: 'center'
    },
    contents_title : {
        fontSize: 24,
    },
    contents_subt : {
        marginTop : 10,
        fontSize: 12,
        color: '#7db840',
        marginBottom: 30
    },
    c_picker_group : {
        flexDirection: 'row',
        // height: (Dimensions.get('window').height - (108 + getStatusBarHeight())),
        flex: 1,
        marginTop: -100,
        justifyContent: 'center',
        alignItems: 'center'
    },
    c_picker: {
        width: 100
    },
    c_picker_word : {
        fontSize: 16
    },
    main_bottom_bar : {
        flexDirection: 'row',
        width: Dimensions.get('window').width,
        height: 42,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
        paddingTop: 11,
        paddingBottom: 11,
        borderColor: 'rgba(230, 230, 230, 0.5)',
        borderTopWidth: 1,
    },
    main_bottom_bar2 : {
        flexDirection: 'row',
        width: Dimensions.get('window').width,
        height: 56,
        backgroundColor: '#C3C3C3',
        alignItems: 'center',
        justifyContent: 'center',
        paddingTop: 11,
        paddingBottom: 11
    },
    main_bottom_bar3 : {
        flexDirection: 'row',
        width: Dimensions.get('window').width,
        height: 56,
        backgroundColor: '#77B844',
        alignItems: 'center',
        justifyContent: 'center',
        paddingTop: 11,
        paddingBottom: 11
    },
    bigbtn : {
        color: '#fff'
    },
    mini_icon : {
        width: 15,
        height: 15,
        resizeMode: 'contain'
    },
    mini_word : {
        fontSize: 12,
        color: '#606060'
    },
    contents_item: {
        width:  Dimensions.get('window').width - 40,
        height: 96,
        borderWidth : 1,
        borderRadius: 2,
        borderColor: '#ededed',
        flexDirection: 'row'
    },

    contents_item_check: {
        width:  Dimensions.get('window').width - 40,
        height: 96,
        backgroundColor : 'rgba(180, 221, 116, 0.8)',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: -96
    },
    request_image_dom: {
        width: 90,
        height: 96,
        paddingLeft:6,
        paddingTop:6
    },
    request_image:{
        width:84,
        height: 84
    },
    request_info: {
        width: (Dimensions.get('window').width - 191),
        height: 96,
        paddingLeft: 20
    },
    request_info_memter : {
        marginRight:  0,
        marginLeft: (Dimensions.get('window').width - 220),
        width: 52,
        fontSize : 12,
        color : '#7f8fd1',
        marginTop: 6,
        flexDirection: 'row',
    },
    request_info_store_name : {
        fontSize:12,
        color: '#7db840',
        marginTop: 25
    },
    request_info_food_name : {
        fontSize: 16
    },
    request_info_delete: {
        width: 100,
        height: 96,
        flex: 1,
        marginTop: 39,
        flexDirection: 'row'
    },
    request_info_delete_w1 : {
        width: 10,
        height: 10,
        backgroundColor: '#eb5847',
        marginRight:4,
        borderRadius: 5
    },
    request_info_delete_w2 : {
        width: 10,
        height: 10,
        backgroundColor: 'rgba(235, 88, 71, 0.6)',
        marginRight:4,
        borderRadius: 5
    },
    request_info_delete_w3 : {
        width: 10,
        height: 10,
        backgroundColor: 'rgba(235, 88, 71, 0.2)',
        marginRight:4,
        borderRadius: 5
    },
    service_noti : {
        paddingLeft: 20,
        flexDirection: 'row',
        height: 32,
        marginTop: 7,
        marginBottom: 34
    },

    service_noti_line : {
        width: 10,
        height: 16,
        marginTop: -8,
        marginLeft: -16,
        borderColor: '#f1a9a2',
        borderWidth: 1,
        borderBottomStartRadius: 5,
        borderTopWidth : 0,
        borderRightWidth: 0
    },
    service_noti_icon: {
        backgroundColor: 'rgba(235, 88, 71, 0.23)',
        width: 60,
        height: 16,
        borderColor: '#f1a9a2',
        borderWidth: 1,
        borderRadius: 12,
        // paddingLeft: 6.5,
        paddingTop:1.5,
    },
    service_noti_icon_word : {
        width: 60,
        fontSize: 10,
        color: '#eb5847',
        textAlign:'center'
    },
    service_noti_name : {
        marginLeft: 8,
        paddingTop: 2,
        fontSize: 12,
        color: '#eb5847'
    },
    setting_icon_image : {
        width: 20,
        height: 20
    },

    request_info_delete_btn : {
        fontSize: 12,
        color: '#eb5847'
    },
    request_info_delete_btn_s : {
        fontSize: 12,
        color: '#7db840'
    },
    check_image : {
        width: 48,
        height: 48
    }

});

class User_request_step5 extends  React.Component {
    constructor(props) {
        super(props);
        this.state = {item_switch: 1};
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.Navigator}>
                    <View style={styles.Navigator_menu}>
                        <TouchableHighlight style={styles.Navigator_menu_button} underlayColor={'transparent'}
                                            onPress={() => Actions.pop()}>
                            <Image style={styles.Navigator_menu_image} source={require("../../assets/back_arrow.png")}/>
                        </TouchableHighlight>
                    </View>
                </View>
                <View style={styles.contents}>
                    <Text style={styles.contents_title}>예약이 가능한곳이 있습니다.</Text>
                    <Text style={styles.contents_subt}>선택을 하시고 예약을 완료하세요.</Text>
                    <ScrollView>
                        <TouchableHighlight underlayColor={'transparent'}
                                            onPress={() => this.setState({item_switch: 1})}>
                            <View style={styles.contents_item}>
                                <View style={styles.request_image_dom}>
                                    <Image style={styles.request_image}
                                           source={require("../../assets/User/My/dummy_menu_images.png")}/>
                                </View>
                                <View style={styles.request_info}>
                                    <Text style={styles.request_info_store_name}>이자카야</Text>
                                    <Text style={styles.request_info_food_name}>공릉동 술깨비</Text>
                                </View>
                                <View style={styles.request_info_delete}>
                                    <Text style={styles.request_info_delete_btn}>예약불가</Text>
                                </View>
                            </View>
                        </TouchableHighlight>
                        <View style={styles.service_noti}>
                            <View style={styles.service_noti_line}/>
                            <View style={styles.service_noti_icon}><Text style={styles.service_noti_icon_word}>서비스
                                제안</Text></View>
                            <Text style={styles.service_noti_name}>서비스 제안 내용이 들어갑니다.</Text>
                        </View>

                        <TouchableHighlight underlayColor={'transparent'}
                                            onPress={() => this.setState({item_switch: 2})}>
                            <View>
                                <View style={styles.contents_item}>
                                    <View style={styles.request_image_dom}>
                                        <Image style={styles.request_image}
                                               source={require("../../assets/User/My/dummy_menu_images.png")}/>
                                    </View>
                                    <View style={styles.request_info}>
                                        <Text style={styles.request_info_store_name}>이자카야</Text>
                                        <Text style={styles.request_info_food_name}>공릉동 술깨비</Text>
                                    </View>
                                    <View style={styles.request_info_delete}>
                                        <Text style={[styles.request_info_delete_btn, {color: '#7db840'}]}>예약가능</Text>
                                    </View>
                                </View>
                                <View style={[styles.contents_item_check, this.state.item_switch === 1 ? {
                                    display: 'none',
                                    position: 'relative'
                                } : {}]}>
                                    <Image style={styles.check_image}
                                           source={require("../../assets/common/check_circle_fill.png")}/>
                                </View>
                            </View>
                        </TouchableHighlight>
                        <View style={styles.service_noti}>
                            <View style={styles.service_noti_line}/>
                            <View style={styles.service_noti_icon}><Text style={styles.service_noti_icon_word}>서비스
                                제안</Text></View>
                            <Text style={styles.service_noti_name}>서비스 제안 내용이 들어갑니다.</Text>
                        </View>
                    </ScrollView>
                </View>
                <View style={styles.main_bottom_bar}>
                    <Image style={styles.mini_icon} source={require("../../assets/common/user_gray.png")}/>
                    <Text style={styles.mini_word}> 인원 {this.props.count} </Text>
                    <Image style={styles.mini_icon} source={require("../../assets/common/pin_gray.png")}/>
                    <Text style={styles.mini_word}> 거리 {this.props.distance}M </Text>
                    <Image style={styles.mini_icon} source={require("../../assets/common/clock_gray.png")}/>
                    <Text style={styles.mini_word}> {this.props.time}분 이내 출발</Text>
                </View>
                <TouchableHighlight style={this.state.item_switch === 2 ? {display: 'none'} : {}}
                                    underlayColor={'transparent'} onPress={() => Actions.User_request_sec_fail()}>
                    <View style={styles.main_bottom_bar2}>
                        <Text style={styles.bigbtn}>선택 예약하기</Text>
                    </View>
                </TouchableHighlight>
                <TouchableHighlight style={this.state.item_switch === 1 ? {display: 'none'} : {}}
                                    underlayColor={'transparent'} onPress={() => Actions.User_request_step6()}>
                    <View style={styles.main_bottom_bar3}>
                        <Text style={styles.bigbtn}>선택 예약완료하기</Text>
                    </View>
                </TouchableHighlight>
            </View>
        )
    }
}

export default User_request_step5;
