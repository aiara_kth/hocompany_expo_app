import React from 'react';
import {
    Text,
    View,
    StyleSheet,
    Image,
    TouchableHighlight,
    Dimensions,
    ScrollView,
    Menu,
    PickerIOS,
    Platform
} from 'react-native';
import PickerAndroid from 'react-native-picker-android';
import { Actions } from 'react-native-router-flux';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import {Ionicons} from '@expo/vector-icons';
import Global from "../../objects/Global";

var commaNumber = require('comma-number');

let Picker = Platform.OS === 'ios' ? PickerIOS : PickerAndroid;
let PickerItem = Platform.OS === 'ios' ? PickerIOS.Item : PickerAndroid.Item;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f9f9f9',
        // backgroundColor: '#fff'
    },
    Navigator : {
        flexDirection: 'row',
        paddingTop : getStatusBarHeight(),
        width: Dimensions.get('window').width,
        height: 48 + getStatusBarHeight(),
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    Navigator_menu: {
        width: 80,
        height: 48,
    },
    Navigator_menu_button :{
        width: 48,
        height: 48,
        paddingTop: 12,
        paddingLeft: 12,
    },
    Navigator_menu_image : {
        width: 24,
        height: 24
    },
    Navigator_title:{
        width : ((Dimensions.get('window').width) - 160),
        alignItems: 'center',
    },
    Navigator_title_text:{
        fontSize: 16,
        color:'#000000'
    },
    Navigator_start:{
        alignItems: 'flex-end',
        width: 80,
        height: 48,
        paddingTop: 12,
        paddingRight: 12,
    },
    Navigator_start_text:{
        fontSize: 16,
        color:'#7db840',
    },
    contents : {
        paddingTop :  9,
    },
    contents_item : {
        width: Dimensions.get('window').width,
        backgroundColor: '#fff',
        marginBottom: 10
    },
    contents_item_sub : {
        width: Dimensions.get('window').width,
        height: 114,
        backgroundColor: '#fff',
        marginBottom: 8,
        paddingLeft: 20,
        paddingRight: 20
    },
    contents_item_info:{
        flexDirection: 'row',
        flex: 1,
        height: 48,
        paddingLeft: 12,
        paddingRight: 12,
        paddingTop: 16,
        paddingBottom: 16
    },
    admin_type_top: {
        width: 45
    },
    admin_type : {
        fontSize: 16
    },
    admin_name_top : {
        width: (Dimensions.get('window').width - 185),
    },
    admin_name : {
        paddingLeft: 10,
        fontSize: 16,
    },
    admin_phone_top: {
        width: 130,
    },
    admin_phone : {
        fontSize: 16,
    },
    color_red : {
        color : '#a25757'
    },
    color_green : {
        color: '#7db840'
    },
    color_purple : {
        color:  'rgba(127, 143, 209,0.5)'
    },
    color_gray : {
        color:  '#c4c4c4'
    },

    shop_main_info : {
        height: 'auto',
        backgroundColor : '#fff'
    },

    shop_name : {
        marginTop :12,
        fontSize: 28,
        width: (Dimensions.get('window').width),
        textAlign: 'center'
    },

    shop_status : {
        marginTop: 3,
        width: (Dimensions.get('window').width),
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
    },


    shop_main_image_bin : {
        width: (Dimensions.get('window').width),
        height: 'auto'
    },
    shop_main_image : {
        width: (Dimensions.get('window').width),
        height: (Dimensions.get('window').width / 1.8),
        resizeMode: 'stretch'
    },

    ViewLeft : {
        width: '50%',
    },
    ViewRight : {
        width: '50%',
        alignItems: 'flex-end',
        justifyContent: 'center',
    },
    center_button:{
        width: Dimensions.get('window').width,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 8
    },
    buttom_button:{
        position: 'absolute',
        marginBottom : ((Dimensions.get('window').height /  2) - 120)
    },
    add_button:{
        width: Dimensions.get('window').width,
        height: 56,
    },
    add_button_text : {
        position: 'absolute',
        left: ((Dimensions.get('window').width / 2) - '2%'),
        top : ((Dimensions.get('window').height /  2) + 30)
    },
    contents_top: {
        width: Dimensions.get('window').width,
        flexDirection: 'row',
        height: 76,
        paddingTop: 11,
        paddingLeft : 12,
        paddingRight: 24,
        paddingBottom : 11,
        backgroundColor: '#ffffff'
    },
    contents_top_image : {
        width: 40,
        height: 40
    },
    contents_top_info: {
        width: Dimensions.get('window').width - 76,
        flexDirection: 'row',
        height: 40,
        paddingLeft: 10,
        paddingRight: 8,
        paddingTop: 7
    },
    contents_top_name : {
        width: Dimensions.get('window').width - 231,
        fontSize: 16
    },
    contents_top_price : {
        width: 80,
        textAlign: 'right',
        color: '#606060',
        fontSize: 16,
        fontWeight: '500'
    },
    contents_top_count: {
        width: 25,
        height: 25,
        textAlign: 'center',
        color: '#606060',
        fontSize: 16,
        fontWeight: '500',
        marginLeft: 10,
        borderWidth: 1,
        borderColor: '#f2f2f2'
    },
    contents_top_check: {
        width: 25,
        height: 25,
        marginLeft: 10
    },
    contents_top_extra : {
        width : Dimensions.get('window').width - 76,
        flexDirection: 'column',
        color: '#c4c4c4',
        fontSize: 10,
        position: 'absolute',
        marginTop: 42,
        marginLeft: 69,
        lineHeight: 20
    },
    contents_block : {
        width : Dimensions.get('window').width,
        height: 140,
        paddingLeft: 12,
        paddingRight: 12,
        paddingTop : 18,
        paddingBottom : 18,
        marginTop: 10.,
        backgroundColor:'#ffffff'
    },
    contents_block_top : {
        flexDirection: 'row',
    },
    contents_block_top_name : {
        width : Dimensions.get('window').width - 56,
        fontSize: 16,
        marginTop: 2,
    },
    contents_block_top_check: {
        width: 24,
        height: 24
    },
    contents_block_sub : {
        marginTop: 10,
        width : Dimensions.get('window').width - 24,
    },
    contents_block_extra : {
        width : Dimensions.get('window').width - 24,
        color: '#c4c4c4',
        fontSize: 10,
        marginTop: 4
    },
    contents_block_sub_block : {
        width : Dimensions.get('window').width - 24,
        flexDirection: 'row'
    },
    contents_block_sub_name : {
        width: 35,
        color: '#c4c4c4',
        fontSize: 16,
        lineHeight: 24
    },
    contents_block_sub_info : {
        width: Dimensions.get('window').width - 69,
        height: 27,
        alignItems:'flex-end',
        justifyContent: 'flex-end'
    },
    contents_block_sub_info_block : {
        width: 25,
        height: 25,
        textAlign: 'center',
        color: '#606060',
        fontSize: 16,
        fontWeight: '500',
        marginLeft: 10,
        borderWidth: 1,
        borderColor: '#f2f2f2'
    },
    contents_block_sub_info_ow : {
        width: Dimensions.get('window').width - 69,
        height: 27,
        alignItems:'flex-end',
        justifyContent: 'flex-end'
    },
    contents_block_sub_info_block_ow:{
        width: 80,
        height: 27,
        textAlign: 'right',
        color: '#606060',
        fontSize: 16,
        fontWeight: '500',
        marginLeft: 10,
    },
    v_modal: {
        zIndex: 1000,
        position: 'absolute',
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height
    },
    v_modal_bg : {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height - 300,
        backgroundColor: 'rgba(0, 0, 0, 0.4)'
    },
    v_modal_picker: {
        backgroundColor: '#fff',
        width: Dimensions.get('window').width,
        height: 244,
        // flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    v_modal_title : {
        fontSize: 14,
        color: '#606060',
        marginTop: 14,
    },
    v_modal_title_s : {
        fontSize: 14,
        color: '#606060',
        marginTop: 14,
        marginBottom:14,
    },
    v_modal_btn : {
        backgroundColor: '#606060',
        width: Dimensions.get('window').width,
        height: 56,
        lineHeight: 56,
        textAlign: 'center',
        // paddingTop: 16,
        color: '#ffffff',
        fontSize: 16,
    },
    c_picker: {
        width: 100,
        height: 144
    },
    main_bottom_bar : {
        width: Dimensions.get('window').width,
        height: 98,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
        paddingTop: 11,
        paddingBottom: 11
    },
    contents_line :{
        width: Dimensions.get('window').width - 30,
        marginLeft: 15,
        height: 1,
        backgroundColor: '#eef0f2',
    },

});

class User_preorder_list extends  React.Component {
    constructor(props) {
        super(props);
        this.state = {
            menu: 1,
            count: 1,
            modal: 0,
            selectedMenuCount: 0,
            totalPrice: 0,
            menus: [
                {
                    menuId: 0,
                    name: "냠냠파전",
                    description: "냠냠파전은 바삭바삭한 끝 부분을 살리고 해산물을 가득 살린 메뉴",
                    price: 35000
                }
            ],
            selectedMenu: [],
            modal_target: -1
        };
        // todo 선택한 shop의 메뉴를 props로 받기
    }

    countModal() {
        return <View style={this.state.modal === 1 ? styles.v_modal : {display: 'none', position: 'relative'}}>
            <View style={styles.v_modal_bg}><Text> </Text></View>
            <View style={styles.v_modal_picker}>
                <Text style={styles.v_modal_title_s}>수량을 선택해주세요.</Text>
                <Picker style={styles.c_picker} selectedValue={this.state.count} onValueChange={(count) => this.setState({count: count})}>
                    <PickerItem label="1" value="1"/>
                    <PickerItem label="2" value="2"/>
                    <PickerItem label="3" value="3"/>
                    <PickerItem label="4" value="4"/>
                    <PickerItem label="5" value="5"/>
                    <PickerItem label="6" value="6"/>
                    <PickerItem label="7" value="7"/>
                    <PickerItem label="8" value="8"/>
                    <PickerItem label="9" value="9"/>
                    <PickerItem label="10" value="10"/>
                </Picker>
            </View>
            <Text style={styles.v_modal_btn} onPress={() => this.changeSelectedMenuCount(this.state.modal_target, this.state.count)}>확인</Text>
        </View>;
    }

    changeSelectedMenuCount(menuIdx, count) {
        let selMenu = this.state.selectedMenu;
        let idx = selMenu.findIndex(sel => sel.idx === menuIdx);
        if (idx !== -1) {
            selMenu[idx].count = count * 1;
            selMenu[idx].select = true;
        } else {
            selMenu.push({
                idx: menuIdx,
                count: count,
                select: true
            });
        }
        this.setState({selectedMenu: selMenu, modal: 0, modal_target: -1, count: "1"});
        this.calculate();
    }

    calculate() {
        let count = 0;
        let price = 0;
        this.state.selectedMenu.map(menu => {
            if (menu.select) {
                count += menu.count;
                price += menu.count * this.state.menus[menu.idx].price;
            }
        });
        this.setState({selectedMenuCount: count, totalPrice: price});
    }

    displayMenu(menu) {
        return <View key={menu.menuId}>
            <View style={styles.contents}>
                <TouchableHighlight underlayColor={'transparent'} onPress={() => this.setState({modal: 1, modal_target: menu.menuId})}>
                    <View style={styles.contents_top}>
                        <View style={styles.contents_top_image}><Image style={styles.contents_top_image}
                                                                       source={require("../../assets/User/My/dummy_menu_images.png")}/></View>
                        <View style={styles.contents_top_info}>
                            <Text style={styles.contents_top_name}>{menu.name}</Text>
                            <Text style={styles.contents_top_price}>{commaNumber(menu.price)}원</Text>
                        </View>
                        <Text style={styles.contents_top_extra}>{menu.description}</Text>
                    </View>
                </TouchableHighlight>
            </View>
            <View style={styles.contents_line}/>
        </View>;
    }

    toggleSelect(menuIdx) {
        let selMenu = this.state.selectedMenu;
        let idx = selMenu.findIndex(sel => sel.idx === menuIdx);
        if (idx !== -1) {
            selMenu[idx].select = !selMenu[idx].select;
        }
        this.setState({selectedMenu: selMenu});
        this.calculate();
    }

    displaySelectedMenu(selection, idx) {
        return <View key={'selection_' + idx} style={styles.contents_block}>
            <View style={styles.contents_block_top}>
                <Text style={styles.contents_block_top_name}>{this.state.menus[selection.idx].name}</Text>
                <TouchableHighlight underlayColor={'transparent'} onPress={() => this.toggleSelect(idx)}>
                    <Image style={styles.contents_block_top_check}
                           source={selection.select ? require("../../assets/common/check_circle_menu_checked.png") : require("../../assets/common/check_circle_menu_none.png")}/>
                </TouchableHighlight>
            </View>
            <Text style={styles.contents_block_extra}>{this.state.menus[selection.idx].description}</Text>
            <View style={styles.contents_block_sub}>
                <View style={styles.contents_block_sub_block}>
                    <Text style={styles.contents_block_sub_name}>수령</Text>
                    <View style={styles.contents_block_sub_info}>
                        <TouchableHighlight underlayColor={'transparent'} onPress={() => this.setState({modal: 1, modal_target: selection.idx, count: selection.count})}>
                            <Text style={styles.contents_block_sub_info_block}>{selection.count}</Text>
                        </TouchableHighlight>
                    </View>
                </View>
                <View style={styles.contents_block_sub_block}>
                    <Text style={styles.contents_block_sub_name}>가격</Text>
                    <View style={styles.contents_block_sub_info_ow}>
                        <Text style={styles.contents_block_sub_info_block_ow}>{commaNumber(this.state.menus[selection.idx].price)}원</Text>
                    </View>
                </View>
            </View>
        </View>;
    }

    render() {
        return (
            <View style={styles.container}>
                {this.countModal()}
                <View style={styles.Navigator}>
                    <View style={styles.Navigator_menu}>
                        <TouchableHighlight style={styles.Navigator_menu_button} underlayColor={'transparent'}
                                            onPress={() => Actions.pop()}>
                            <Image style={styles.Navigator_menu_image} source={require("../../assets/back_arrow.png")}/>
                        </TouchableHighlight>
                    </View>
                    <View style={styles.Navigator_title}><Text style={styles.Navigator_title_text}>선주문하기</Text></View>
                    <TouchableHighlight underlayColor={'transparent'} onPress={() => null}>
                        <View style={styles.Navigator_start}><Text style={styles.Navigator_start_text}> </Text></View>
                    </TouchableHighlight>
                </View>
                <ScrollView style={{paddingBottom: 100}}>
                    {
                        this.state.menus.map(menu => this.displayMenu(menu))
                    }
                    {
                        this.state.selectedMenu.map((selection, idx) => this.displaySelectedMenu(selection, idx))
                    }
                </ScrollView>
                <View style={styles.main_bottom_bar}>
                    <Text style={{
                        width: Dimensions.get('window').width,
                        height: 42,
                        textAlign: 'center',
                        paddingTop: 12,
                        color: '#606060',
                        backgroundColor: 'rgba(230, 230, 230, 0.5)'
                    }}>{this.state.selectedMenuCount}개주문 총 가격 {commaNumber(this.state.totalPrice)}원</Text>
                    <TouchableHighlight underlayColor={'transparent'} onPress={() => Actions.User_preorder_end()}>
                        <Text style={{
                            width: Dimensions.get('window').width,
                            height: 56,
                            textAlign: 'center',
                            paddingTop: 16,
                            color: '#ffffff',
                            backgroundColor: '#EB5847'
                        }}>{this.state.selectedMenuCount}개 미리 주문하기</Text>
                    </TouchableHighlight>
                </View>
            </View>
        )
    }
}

export default User_preorder_list;