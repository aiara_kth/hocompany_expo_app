import React from 'react';
import {Text, View, StyleSheet, Image,TouchableHighlight, Dimensions, ScrollView, Menu} from 'react-native';
import { Actions } from 'react-native-router-flux';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import {Ionicons} from '@expo/vector-icons';
import Global from "../../objects/Global";


const styles = StyleSheet.create({
    container: {
        flex: 1,
        // backgroundColor: '#f9f9f9',
        backgroundColor: '#fff',
    },
    Navigator : {
        flexDirection: 'row',
        marginTop : getStatusBarHeight(),
        width: Dimensions.get('window').width,
        height: 48,
        backgroundColor: '#fff',
    },
    Navigator_menu: {
        width: 80,
        height: 48,
    },
    Navigator_menu_button :{
        width: 48,
        height: 48,
        paddingTop: 12,
        paddingLeft: 12,
    },
    Navigator_menu_image : {
        width: 24,
        height: 24
    },
    Navigator_title:{
        width : ((Dimensions.get('window').width) - 160),
        alignItems: 'center',
    },
    Navigator_title_text:{
        fontSize: 16,
        color:'#000000'
    },
    Navigator_start:{
        alignItems: 'flex-end',
        width: 80,
        height: 48,
        paddingTop: 12,
        paddingRight: 12,
    },
    Navigator_start_text:{
        fontSize: 16,
        color:'#7f8fd1',
    },
    callout_box : {
        width : (Dimensions.get('window').width),
        height: 40,
        borderRadius: 2,
        backgroundColor: '#7f8fd1',
        alignItems: 'center',
        justifyContent: 'center',
        shadowOffset: {width: 2, height: 10 },
        shadowColor: 'rgba(0, 0, 0, 0.2)',
        paddingRight: 56
    },
    callout_box_text : {
        fontSize : 12,
        color: '#ffffff'
    },
    contents : {
        height: (Dimensions.get('window').height - (121 + getStatusBarHeight())),
        flex: 1,
        paddingTop : 10,
        alignItems: 'center'
    },
    contents_title : {
        fontSize: 24,
    },
    contents_subt : {
        marginTop : 10,
        fontSize: 12,
        color: '#7db840'
    },
    c_picker_group : {
        flexDirection: 'row',
        // height: (Dimensions.get('window').height - (108 + getStatusBarHeight())),
        flex: 1,
        marginTop: -100,
        justifyContent: 'center',
        alignItems: 'center'
    },
    c_picker: {
        width: 100
    },
    c_picker_word : {
        fontSize: 16
    },
    main_bottom_bar : {
        flexDirection: 'row',
        width: Dimensions.get('window').width,
        height: 42,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
        paddingTop: 11,
        paddingBottom: 11,
        borderTopWidth:1,
        borderColor: 'rgba(230, 230, 230, 0.5)'
    },
    mini_icon : {
        width: 20,
        height: 20,
        resizeMode: 'contain'
    },
    mini_icon_s : {
        width: 12,
        height: 12 ,
        resizeMode: 'contain'
    },
    mini_word : {
        lineHeight: 20,
        fontSize: 12,
        color: '#606060'
    },
    animeweb : {
        width: 256,
        height: 256
    },
    contents_item: {
        width: (Dimensions.get('window').width - 40),
        height: 96,
        backgroundColor: '#ffffff',
        // marginLeft: 20,
        marginBottom: 10,
        flexDirection: 'row',
        borderWidth: 1,
        borderColor: 'rgba(0, 0, 0, 0.1)'
    },
    imgicon: {
        width: 30,
        height: 30,
        resizeMode : 'cover'
    },
    imgicon_s: {
        width: 16,
        height: 16,
        resizeMode: 'contain'
    },
    imgicon_pin: {
        width: 10,
        height: 14,
        marginTop:1,
        resizeMode: 'contain'
    },
    imgicon_ios: {
        width: 12,
        height: 12,
        resizeMode : 'cover'
    },

    request_image_dom: {
        width: 90,
        height: 96,
        paddingLeft:6,
        paddingTop:6
    },
    request_image:{
        width:84,
        height: 84
    },
    request_info: {
        width: (Dimensions.get('window').width - 124),
        height: 96,
        paddingLeft: 20
    },

    request_info_memter : {
        paddingLeft: (Dimensions.get('window').width - 215),
        marginTop: 6,
        // paddingRight: 16,
        flexDirection: 'row'
    },
    request_info_memter_text : {
        fontSize : 12,
        color : '#7f8fd1',
    },


    request_info_store_name : {
        fontSize:12,
        color: '#7db840'
    },
    request_info_food_name : {
        fontSize: 16
    },
    request_info_extra : {
       flexDirection: 'row',
        marginTop: 12
    },
    service_noti : {
        paddingLeft: 20,
        flexDirection: 'row',
        height: 32,
    },
    service_noti_line : {
      width: 10,
      height: 16,
      marginTop: -8,
      marginLeft: -16,
      borderColor: '#f1a9a2',
      borderWidth: 1,
      borderBottomStartRadius: 5,
      borderTopWidth : 0,
      borderRightWidth: 0
    },
    service_noti_icon: {
        backgroundColor: 'rgba(235, 88, 71, 0.23)',
        width: 60,
        height: 16,
        borderColor: '#f1a9a2',
        borderWidth: 1,
        borderRadius: 12,
        paddingLeft: 6.5,
        paddingTop:1.5,
    },
    service_noti_icon_word : {
        fontSize: 10,
        color: '#eb5847'
    },
    service_noti_name : {
        marginLeft: 8,
        fontSize: 12,
        color: '#eb5847'
    },
    setting_icon_image : {
        width: 20,
        height: 20
    },
    sort_word : {
        fontSize: 14,
        color: '#838383'
    },
    sortable: {
        flexDirection: 'row',
        marginTop: 30,
        marginBottom: 15,
        width: Dimensions.get('window').width - 40,
    },
    sortable_block : {
        width: '50%',
        flexDirection: 'row',

    },
    right: {
        alignItems: 'flex-end',
        justifyContent: 'flex-end'
    }





});

class User_request_step4 extends  React.Component {
    constructor(props) {
        super(props);
        this.state = {
            text : '',
            count: '1',
            foundShops: this.props.foundShops
        };
        if (this.state.foundShops == null || this.state.foundShops.length === 0) {
            this.state.foundShops = [
                {
                    shopId: 0,
                    name: "이자카야",
                    description: "공릉동 술깨비",
                    distance: 200,
                    startTime: "16:00",
                    endTime: "3:00",
                    service: ""
                },
                {
                    shopId: 1,
                    name: "이자카야",
                    description: "공릉동 술깨비",
                    distance: 200,
                    startTime: "16:00",
                    endTime: "3:00",
                    service: "서비스 제안 내용이 들어갑니다."
                },
                {
                    shopId: 2,
                    name: "이자카야",
                    description: "공릉동 술깨비",
                    distance: 200,
                    startTime: "16:00",
                    endTime: "3:00",
                    service: ""
                },
                {
                    shopId: 3,
                    name: "이자카야",
                    description: "공릉동 술깨비",
                    distance: 200,
                    startTime: "16:00",
                    endTime: "3:00",
                    service: ""
                },
                {
                    shopId: 4,
                    name: "이자카야",
                    description: "공릉동 술깨비",
                    distance: 200,
                    startTime: "16:00",
                    endTime: "3:00",
                    service: ""
                },
                {
                    shopId: 5,
                    name: "이자카야",
                    description: "공릉동 술깨비",
                    distance: 200,
                    startTime: "16:00",
                    endTime: "3:00",
                    service: ""
                },
                {
                    shopId: 6,
                    name: "이자카야",
                    description: "공릉동 술깨비",
                    distance: 200,
                    startTime: "16:00",
                    endTime: "3:00",
                    service: ""
                },
                {
                    shopId: 7,
                    name: "이자카야",
                    description: "공릉동 술깨비",
                    distance: 200,
                    startTime: "16:00",
                    endTime: "3:00",
                    service: ""
                }
            ];
        }
    }

    shopElement(shop) {
        return <View key={shop.shopId}>
            <TouchableHighlight underlayColor={'transparent'} onPress={() => Actions.User_request_step5({shopInfo: shop, selected_shop: shop.shopId, from_request: true})}>
                <View style={styles.contents_item}>
                    <View style={styles.request_image_dom}>
                        <Image style={styles.request_image}
                               source={require("../../assets/User/My/dummy_menu_images.png")}/>
                        <Image style={{zIndex: 9998, width: 48, height: 16, marginTop: -88, marginLeft: -4}}
                               source={require("../../assets/common/event_label_w.png")}/>
                    </View>
                    <View style={styles.request_info}>
                        <View style={styles.request_info_memter}><Image style={styles.imgicon_pin}
                                                                        source={require("../../assets/common/pin.png")}/><Text
                            style={styles.request_info_memter_text}> {shop.distance}M</Text></View>
                        <Text style={styles.request_info_store_name}>{shop.name}</Text>
                        <Text style={styles.request_info_food_name}>{shop.description}</Text>
                        <View style={styles.request_info_extra}>
                            <Image style={styles.imgicon_s} source={require("../../assets/common/heart.png")}/>
                            <Text style={{color: '#eb5847', lineHeight: 16, fontSize: 12}}> 6</Text>
                            <View style={{width: 13, height: 16, paddingTop: 6}}>
                                <View style={{width: 1, height: 4, backgroundColor: '#c4c4c4', marginLeft: 6}}/>
                            </View>
                            <Image style={styles.imgicon_s} source={require("../../assets/common/clock.png")}/>
                            <Text style={{
                                color: '#c4c4c4',
                                lineHeight: 16,
                                fontSize: 12
                            }}> {shop.startTime} ~ {shop.endTime}</Text>
                        </View>
                    </View>
                </View>
            </TouchableHighlight>
            {
                shop.service !== "" && (
                    <View style={styles.service_noti}>
                        <View style={styles.service_noti_line}></View>
                        <View style={styles.service_noti_icon}><Text style={styles.service_noti_icon_word}>서비스 제안</Text></View>
                        <Text style={styles.service_noti_name}>{shop.service}</Text>
                    </View>)
            }
        </View>;
    }

    render() {
        return (
            <View  style={styles.container}>
                <View style={styles.Navigator}>
                    <View style={styles.Navigator_menu}>
                        <TouchableHighlight style={styles.Navigator_menu_button} underlayColor={'transparent'} onPress={() => Actions.pop()}>
                            <Image style={styles.Navigator_menu_image} source={require("../../assets/back_arrow.png")}/>
                        </TouchableHighlight>
                    </View>
                </View>
                <View style={styles.contents}>
                    <Text style={styles.contents_title}>실시간 검색결과</Text>
                    <View style={styles.sortable}>
                        <View style={styles.sortable_block}>
                            <Text style={styles.sort_word}>좋아요 순</Text>
                            <Image style={styles.setting_icon_image} source={require("../../assets/common/dropbox.png")}/>
                        </View>
                        <View style={[styles.sortable_block,styles.right]}>
                            <Text style={styles.sort_word}>필터</Text>
                            <Image style={styles.setting_icon_image} source={require("../../assets/common/setting_icon.png")}/>
                        </View>
                    </View>
                    <ScrollView style={{marginTop:15}}>
                        {this.state.foundShops.map(shop => (this.shopElement(shop)))}
                    </ScrollView>
                </View>
                <View style={styles.main_bottom_bar}>
                    <Image style={styles.mini_icon} source={require("../../assets/common/user_gray.png")}/>
                    <Text style={styles.mini_word}>인원 {this.props.count}명   </Text>
                    {/*<Image style={styles.mini_icon} source={require("../../assets/common/pin_gray.png")}/>*/}
                    {/*<Text style={styles.mini_word}>거리 200M   </Text>*/}
                    <Image style={styles.mini_icon} source={require("../../assets/common/clock_gray.png")}/>
                    <Text style={styles.mini_word}>{this.props.time}분 이내 출발</Text>
                </View>
            </View>
        )
    }
}

export default User_request_step4;
