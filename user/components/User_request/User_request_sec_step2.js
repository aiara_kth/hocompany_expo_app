import React from 'react';
import {
    Text,
    View,
    StyleSheet,
    Image,
    TouchableHighlight,
    Dimensions,
    Menu,
    PickerIOS,
    Platform
} from 'react-native';
import PickerAndroid from 'react-native-picker-android';
import { Actions } from 'react-native-router-flux';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import ActionButton from 'react-native-action-button';
import {Ionicons} from '@expo/vector-icons';
import Global from "../../objects/Global";

let Picker = Platform.OS === 'ios' ? PickerIOS : PickerAndroid;
let PickerItem = Platform.OS === 'ios' ? PickerIOS.Item : PickerAndroid.Item;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        // backgroundColor: '#f9f9f9',
        backgroundColor: '#fff',
    },
    Navigator : {
        flexDirection: 'row',
        marginTop : getStatusBarHeight(),
        width: Dimensions.get('window').width,
        height: 48,
        backgroundColor: '#fff',
    },
    Navigator_menu: {
        width: 80,
        height: 48,
    },
    Navigator_menu_button :{
        width: 48,
        height: 48,
        paddingTop: 12,
        paddingLeft: 12,
    },
    Navigator_menu_image : {
        width: 24,
        height: 24
    },
    Navigator_title:{
        width : ((Dimensions.get('window').width) - 160),
        alignItems: 'center',
    },
    Navigator_title_text:{
        fontSize: 16,
        color:'#000000'
    },
    Navigator_start:{
        alignItems: 'flex-end',
        width: 80,
        height: 48,
        paddingTop: 12,
        paddingRight: 12,
    },
    Navigator_start_text:{
        fontSize: 16,
        color:'#7f8fd1',
    },
    callout_box : {
        width : (Dimensions.get('window').width),
        height: 40,
        borderRadius: 2,
        backgroundColor: '#7f8fd1',
        alignItems: 'center',
        justifyContent: 'center',
        shadowOffset: {width: 2, height: 10 },
        shadowColor: 'rgba(0, 0, 0, 0.2)',
        paddingRight: 56
    },
    callout_box_text : {
        fontSize : 12,
        color: '#ffffff'
    },
    contents : {
        height: (Dimensions.get('window').height - (121 + getStatusBarHeight())),
        flex: 1,
        paddingTop : 10,
        alignItems: 'center'
    },
    contents_title : {
        fontSize: 24,
    },
    contents_subt : {
        marginTop : 10,
        fontSize: 12,
        color: '#7f8fd1'
    },
    c_picker_group : {
        flexDirection: 'row',
        // height: (Dimensions.get('window').height - (108 + getStatusBarHeight())),
        flex: 1,
        marginTop: -100,
        justifyContent: 'center',
        alignItems: 'center'
    },
    c_picker: {
        width: 100
    },
    c_picker_word : {
        fontSize: 16
    },
    main_bottom_bar : {
        flexDirection: 'row',
        width: Dimensions.get('window').width,
        height: 42,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
        paddingTop: 11,
        paddingBottom: 11,
        borderColor: 'rgba(230, 230, 230, 0.5)',
        borderTopWidth: 1,
    },
    mini_icon : {
        width: 15,
        height: 15,
        resizeMode: 'contain'
    },
    mini_word : {
        fontSize: 12,
        color: '#606060'
    }


});

class User_request_sec_step2 extends  React.Component {
    constructor(props) {
        super(props);
        this.state = {text: '', count: '1'}
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.Navigator}>
                    <View style={styles.Navigator_menu}>
                        <TouchableHighlight style={styles.Navigator_menu_button} underlayColor={'transparent'}
                                            onPress={() => Actions.pop()}>
                            <Image style={styles.Navigator_menu_image} source={require("../../assets/back_arrow.png")}/>
                        </TouchableHighlight>
                    </View>
                </View>
                <View style={styles.contents}>
                    <Text style={styles.contents_title}>예약가능한 인원 수 선택</Text>
                    <Text style={styles.contents_subt}>예약하시려는 인원의 숫자를 선택해주세요.</Text>
                    <View style={styles.c_picker_group}>
                        <Picker style={styles.c_picker} selectedValue={this.state.count} onValueChange={(count) => {
                            this.setState({count: count})
                        }}>
                            <PickerItem label="1" value="1"/>
                            <PickerItem label="2" value="2"/>
                            <PickerItem label="3" value="3"/>
                            <PickerItem label="4" value="4"/>
                            <PickerItem label="5" value="5"/>
                            <PickerItem label="6" value="6"/>
                            <PickerItem label="7" value="7"/>
                            <PickerItem label="8" value="8"/>
                            <PickerItem label="9" value="9"/>
                            <PickerItem label="10" value="10"/>
                        </Picker>
                        {/*<Text style={styles.c_picker_word}>이네 출발합니다.</Text>*/}
                    </View>
                    <ActionButton hideShadow={false} buttonColor={'#eb5847'} renderIcon={(active) => <Ionicons name='ios-arrow-forward' size={20} color={'#FFFFFF'}/>}
                                  onPress={() => Actions.User_request_sec_step3({shopIds: this.props.shopIds, distance: this.props.distance, count: this.state.count})}>
                    </ActionButton>
                </View>
                <View style={styles.main_bottom_bar}>
                    <Image style={styles.mini_icon} source={require("../../assets/common/pin_gray.png")}/>
                    <Text style={styles.mini_word}> 거리 {this.props.distance}M</Text>
                </View>
            </View>
        )
    }
}

export default User_request_sec_step2;
