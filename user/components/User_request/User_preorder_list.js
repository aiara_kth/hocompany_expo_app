import React from 'react';
import {Text, View, StyleSheet, Image,TouchableHighlight, Dimensions, Menu} from 'react-native';
import { Actions } from 'react-native-router-flux';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import {Ionicons} from '@expo/vector-icons';
import Global from "../../objects/Global";
import API from "../../objects/API";

var commaNumber = require('comma-number');

const styles = StyleSheet.create({
    container: {
        flex: 1,
        // backgroundColor: '#f9f9f9',
        backgroundColor: '#f9f9f9'
    },
    Navigator : {
        flexDirection: 'row',
        paddingTop : getStatusBarHeight(),
        width: Dimensions.get('window').width,
        height: 48 + getStatusBarHeight() ,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    Navigator_menu: {
        width: 80,
        height: 48,
    },
    Navigator_menu_button :{
        width: 48,
        height: 48,
        paddingTop: 12,
        paddingLeft: 12,
    },
    Navigator_menu_image : {
        width: 24,
        height: 24
    },
    Navigator_title:{
        width : ((Dimensions.get('window').width) - 160),
        alignItems: 'center',
    },
    Navigator_title_text:{
        fontSize: 16,
        color:'#000000'
    },
    Navigator_start:{
        alignItems: 'flex-end',
        width: 80,
        height: 48,
        paddingTop: 12,
        paddingRight: 12,
    },
    Navigator_start_text:{
        fontSize: 16,
        color:'#7db840',
    },
    sub_Navigator : {
        flexDirection: 'row',
        width: Dimensions.get('window').width,
        height: 40,
        backgroundColor: '#fff',
        alignItems: 'center',
    },
    sub_Navigator_menu: {
        width: 85,
        height: 40,
        alignItems: 'flex-end',
        justifyContent: 'center',
        marginLeft: 15,
        marginRight: 15
    },
    sub_Navigator_menu_on: {
        width: 65,
        height: 40,
        alignItems: 'center',
        justifyContent: 'center',
        borderBottomWidth: 2,
        borderBottomColor: '#000000',
    },
    sub_Navigator_menu_off: {
        width: 65,
        height: 40,
        alignItems: 'center',
        justifyContent: 'center',
        borderBottomWidth: 2,
        borderBottomColor: 'transparent',
    },
    sub_Navigator_name_on : {
        fontSize : 14,
        fontWeight: 'bold',
        alignItems: 'center',
    },
    sub_Navigator_name_off : {
        fontSize : 14,
        alignItems: 'center',
    },
    contents : {
        marginTop :  9,
        backgroundColor: '#fff'
    },
    contents_item : {
        width: Dimensions.get('window').width,
        backgroundColor: '#fff',
        marginBottom: 10
    },
    contents_item_sub : {
        width: Dimensions.get('window').width,
        height: 114,
        backgroundColor: '#fff',
        marginBottom: 8,
        paddingLeft: 20,
        paddingRight: 20
    },
    contents_item_info:{
        flexDirection: 'row',
        flex: 1,
        height: 48,
        paddingLeft: 12,
        paddingRight: 12,
        paddingTop: 16,
        paddingBottom: 16
    },
    admin_type_top: {
        width: 45
    },
    admin_type : {
        fontSize: 16
    },
    admin_name_top : {
        width: (Dimensions.get('window').width - 185),
    },
    admin_name : {
        paddingLeft: 10,
        fontSize: 16,
    },
    admin_phone_top: {
        width: 130,
    },
    admin_phone : {
        fontSize: 16,
    },
    color_red : {
        color : '#a25757'
    },
    color_green : {
        color: '#7db840'
    },
    color_purple : {
        color:  'rgba(127, 143, 209,0.5)'
    },
    color_gray : {
        color:  '#c4c4c4'
    },

    shop_main_info : {
        height: 'auto',
        backgroundColor : '#fff'
    },

    shop_name : {
        marginTop :12,
        fontSize: 28,
        width: (Dimensions.get('window').width),
        textAlign: 'center'
    },

    shop_status : {
        marginTop: 3,
        width: (Dimensions.get('window').width),
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
    },


    shop_main_image_bin : {
        width: (Dimensions.get('window').width),
        height: 'auto'
    },
    shop_main_image : {
        width: (Dimensions.get('window').width),
        height: (Dimensions.get('window').width / 1.8),
        resizeMode: 'stretch'
    },

    ViewLeft : {
        width: '50%',
    },
    ViewRight : {
        width: '50%',
        alignItems: 'flex-end',
        justifyContent: 'center',
    },

    shop_process_bar_group : {
        marginTop: 8,
        flex : 1,
        flexDirection: 'row',
        paddingLeft: 12
    },
    shop_process_bar : {
        textAlign: 'right',
        width: ((Dimensions.get('window').width - 40) / 5),
        height: 6,
        marginRight: 4,
    },
    shop_main_noti : {
        width: (Dimensions.get('window').width - 24) ,
        marginTop: 10,
        marginLeft: 12,
        marginBottom: 17,
        paddingLeft: 12,
        paddingTop: 8,
        height: 56,
        backgroundColor: '#30cbba'
    },
    timescircle:{
        width:12,
        height: 12,
        position: 'absolute',
        right: 8,
        top: 8
    },
    shop_manager_button_group : {
        width: Dimensions.get('window').width ,
        flex : 1,
        flexDirection: 'row',
        height: 72,
        marginTop: 8,
        backgroundColor: '#ffffff'
    },
    shop_manager_button : {
        width: (Dimensions.get('window').width / 4),
        height: 72,
        alignItems: 'center',
        justifyContent: 'center',
    },
    shop_manager_button_image : {
        width: 24,
        height: 24,
        marginBottom:2
    },
    shop_manager_button_text : {
        fontSize: 14,
    },
    add_admin_button_group : {
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 24
    },
    add_admin_button : {
        width: 100,
        height: 32,
        borderRadius: 12,
        borderColor: '#e8e8e8',
        borderWidth: 1,
        paddingTop : 7,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    add_admin_button_text : {
        fontSize: 12,
        height: 32,
        color: '#8e8e8e'
    },
    callout_box : {
        width: 195,
        height: 40,
        borderRadius: 2,
        backgroundColor: '#7f8fd1',
        alignItems: 'center',
        justifyContent: 'center',
        shadowOffset: {width: 2, height: 10 },
        shadowColor: 'rgba(0, 0, 0, 0.2)'
    },
    callout_box_text : {
        fontSize : 12,
        color: '#ffffff'
    },
    center_button:{
        width: Dimensions.get('window').width,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 8
    },
    buttom_button:{
        position: 'absolute',
        marginBottom : ((Dimensions.get('window').height /  2) - 120)
    },
    add_button:{
        width: Dimensions.get('window').width,
        height: 56,
    },
    add_button_text : {
        position: 'absolute',
        left: ((Dimensions.get('window').width / 2) - '2%'),
        top : ((Dimensions.get('window').height /  2) + 30)
    },
    contents_top: {
        width: Dimensions.get('window').width - 40.5,
        flexDirection: 'row',
        height: 76,
        paddingTop: 11,
        paddingLeft : 12
    },
    contents_top_image : {
        width: 40,
        height: 40
    },
    contents_top_info: {
        width: Dimensions.get('window').width - 92.5,
        flexDirection: 'row',
        height: 40,
        paddingLeft: 10,
        paddingRight: 8,
        paddingTop: 7
    },
    contents_top_name : {
        width: Dimensions.get('window').width - 192.5,
        fontSize: 16,
        lineHeight: 20,
        fontWeight: '500'
    },
    contents_top_price : {
        width: 100,
        textAlign: 'right',
        color: '#606060',
        fontSize: 16,
        fontWeight: '500'
    },
    contents_top_extra : {
        width : Dimensions.get('window').width - 92.5,
        flexDirection: 'column',
        color: '#c4c4c4',
        fontSize: 10,
        position: 'absolute',
        marginTop: 42,
        marginLeft: 69
    },
    contents_block : {
        width : Dimensions.get('window').width,
        height: 116,
        paddingLeft: 12,
        paddingRight: 28.5,
        paddingTop : 18,
        marginTop: 10,
        backgroundColor: '#fff'
    },
    contents_block_sub : {
        marginTop: 10,
        width : Dimensions.get('window').width - 40.5,

    },
    contents_block_extra : {
        width : Dimensions.get('window').width - 40.5,
        color: '#c4c4c4',
        fontSize: 10,
        marginTop: 4
    },
    contents_block_sub_block : {
        width : Dimensions.get('window').width - 40.5,
        flexDirection: 'row'
    },
    contents_block_sub_name : {
        width: 35,
        color: '#c4c4c4',
        fontSize: 16,
        lineHeight: 24
    },
    contents_block_sub_info : {
        width: Dimensions.get('window').width - 75.5,
        fontSize: 16,
        lineHeight: 24,
        fontWeight: '500',
        color: '#606060',
        textAlign: 'right'
    }

});

const menuDummy = [
    {
        img: require("../../assets/User/My/dummy_menu_images.png"),
        name: "냠냠파전",
        price: 35000,
        description: "냠냠파전은 바삭바삭한 끝에부분을 살리고 해산물을 가득살린 메뉴"
    }
];

const orderDummy = [
    {
        menuIndex: 0,
        count: 2
    },
    {
        menuIndex: 0,
        count: 2
    },
    {
        menuIndex: 0,
        count: 2
    }
];

class User_preorder_list extends  React.Component {
    constructor(props) {
        super(props);
        this.state = {
            menu: 1,
            shop_success: 40,
            menuArray: this.props.menuList !== undefined ? this.props.menuList : menuDummy,
            orderArray: this.props.orderList !== undefined ? this.props.orderList : orderDummy
        };
        API.user.getUserJRequestHistory(data => {
            // todo 받아온걸로 render
        }, error => {

        });
    }

    printMenu(menu, index) {
        return <View style={styles.contents} key={"menu_" + index}>
            <View style={styles.contents_top}>
                <View style={styles.contents_top_image}><Image style={styles.contents_top_image} source={menu.img}/></View>
                <View style={styles.contents_top_info}>
                    <Text style={styles.contents_top_name}>{menu.name}</Text>
                    <Text style={styles.contents_top_price}>{commaNumber(menu.price)}원</Text>
                </View>
                <Text style={styles.contents_top_extra}>{menu.description}</Text>
            </View>
        </View>;
    }

    printOrder(order, index) {
        const menu = menuDummy[order.menuIndex];
        return <View style={styles.contents_block} key={"order_" + index}>
            <Text style={styles.contents_top_name}>{menu.name}</Text>
            <Text style={styles.contents_block_extra}>{menu.description}</Text>
            <View style={styles.contents_block_sub}>
                <View style={styles.contents_block_sub_block}>
                    <Text style={styles.contents_block_sub_name}>수량</Text>
                    <Text style={styles.contents_block_sub_info}>{order.count}</Text>
                </View>
                <View style={styles.contents_block_sub_block}>
                    <Text style={styles.contents_block_sub_name}>가격</Text>
                    <Text style={styles.contents_block_sub_info}>{commaNumber(menu.price * order.count)}원</Text>
                </View>
            </View>
        </View>;
    }

    render() {
        return (
            <View  style={styles.container}>
                <View style={styles.Navigator}>
                    <View style={styles.Navigator_menu}>
                        <TouchableHighlight style={styles.Navigator_menu_button} underlayColor={'transparent'} onPress={() => Actions.pop()}>
                            <Image style={styles.Navigator_menu_image} source={require("../../assets/back_arrow.png")}/>
                        </TouchableHighlight>
                    </View>
                    <View style={styles.Navigator_title}><Text style={styles.Navigator_title_text}>선주문내역</Text></View>
                    <TouchableHighlight underlayColor={'transparent'} onPress={() => null}>
                        <View style={styles.Navigator_start}><Text style={styles.Navigator_start_text}> </Text></View>
                    </TouchableHighlight>
                </View>
                {
                    this.state.menuArray.map((menu, index) => this.printMenu(menu, index))
                }
                {
                    this.state.orderArray.map((order, index) => this.printOrder(order, index))
                }
            </View>
        )
    }
}

export default User_preorder_list;