import React from 'react';
import {
    Text,
    View,
    StyleSheet,
    Image,
    TouchableHighlight,
    Dimensions,
    Menu} from 'react-native';
import { Actions } from 'react-native-router-flux';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import {Ionicons} from '@expo/vector-icons';
import Global from "../../objects/Global";
import API from "../../objects/API";


const styles = StyleSheet.create({
    container: {
        flex: 1,
        // backgroundColor: '#f9f9f9',
        backgroundColor: '#fff',
    },
    Navigator : {
        flexDirection: 'row',
        marginTop : getStatusBarHeight(),
        width: Dimensions.get('window').width,
        height: 48,
        backgroundColor: '#fff',
    },
    Navigator_menu: {
        width: 80,
        height: 48,
    },
    Navigator_menu_button :{
        width: 48,
        height: 48,
        paddingTop: 12,
        paddingLeft: 12,
    },
    Navigator_menu_image : {
        width: 24,
        height: 24
    },
    Navigator_title:{
        width : ((Dimensions.get('window').width) - 160),
        alignItems: 'center',
    },
    Navigator_title_text:{
        fontSize: 16,
        color:'#000000'
    },
    Navigator_start:{
        alignItems: 'flex-end',
        width: 80,
        height: 48,
        paddingTop: 12,
        paddingRight: 12,
    },
    Navigator_start_text:{
        fontSize: 16,
        color:'#7f8fd1',
    },
    callout_box : {
        width : (Dimensions.get('window').width),
        height: 40,
        borderRadius: 2,
        backgroundColor: '#7f8fd1',
        alignItems: 'center',
        justifyContent: 'center',
        shadowOffset: {width: 2, height: 10 },
        shadowColor: 'rgba(0, 0, 0, 0.2)',
        paddingRight: 56
    },
    callout_box_text : {
        fontSize : 12,
        color: '#ffffff'
    },
    contents : {
        height: (Dimensions.get('window').height - (121 + getStatusBarHeight())),
        flex: 1,
        paddingTop : 10,
        alignItems: 'center',
        justifyContent: 'center'
    },
    contents_title : {
        width : Dimensions.get('window').width,
        fontSize: 24,
        textAlign: 'center'
    },
    contents_subt : {
        width : Dimensions.get('window').width,
        marginTop : 10,
        fontSize: 12,
        color: '#7db840',
        textAlign: 'center'
    },
    service_noti : {
        width: 320,
        paddingLeft: 20,
        paddingTop: 8,
        flexDirection: 'row',
        height: 32
    },
    service_noti_icon: {
        backgroundColor: 'rgba(235, 88, 71, 0.23)',
        width: 60,
        height: 16,
        borderColor: '#f1a9a2',
        borderWidth: 1,
        borderRadius: 12,
    },
    service_noti_icon_word : {
        width: 60,
        fontSize: 10,
        color: '#eb5847',
        textAlign: 'center'
    },
    service_noti_name : {
        marginLeft: 8,
        fontSize: 12,
        color: '#eb5847'
    },
    service_noti_line : {
        width: 10,
        height: 16,
        marginTop: -8,
        marginLeft: -16,
        borderColor: '#f1a9a2',
        borderWidth: 1,
        borderBottomStartRadius: 5,
        borderTopWidth : 0,
        borderRightWidth: 0
    },
    setting_icon_image : {
        width: 20,
        height: 20
    },
    main_bottom_bar : {
        flexDirection: 'row',
        width: Dimensions.get('window').width,
        height: 56,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    extra_icon : {
      width: 16,
      height: 16
    },
    imgicon: {
        width: 16,
        height: 16,
        resizeMode : 'cover'
    },
    imgicon_ios: {
        width: 12,
        height: 12,
        resizeMode : 'cover'
    },
    contents_sub : {
        height: Dimensions.get('window').height - (200 +  getStatusBarHeight()),
        width: '100%',
        // flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    reserved_box : {
        width: 320,
        height: 288,
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth : 1,
        borderColor: '#efefef',
        marginTop: -30,
    },
    memter_text : {
        fontSize:12,
        color: '#7f8fd1'
    },
    reserved_box_memter: {
        width: 320,
        marginTop: 13,
        paddingRight: 16,
        marginBottom: 8,
        alignItems: 'flex-end',
        justifyContent: 'flex-end',
        flexDirection: 'row'
    },
    reserved_box_store_name : {
        fontSize: 12,
        color: '#7db840'
    },
    reserved_box_food_name : {
        fontSize: 20,
        lineHeight: 32
    },
    reserved_box_extra : {
        marginTop: 10,
        flexDirection: 'row'
    },
    extra_word : {
        fontSize: 10,
        lineHeight: 16
    },
    extra_word2 : {
        fontSize: 10,
        lineHeight: 16,
        color: '#838383'
    },
    reserved_box_person : {
        width: 320,
        marginTop: 44,
        paddingLeft: 16,
        paddingRight: 16
    },
    reserved_box_person_row : {
        width: 288,
        height: 24,
        marginBottom: 8,
        flexDirection: 'row',
    },
    reserved_box_person_name :{
        width: 55,
        fontSize : 14,
        lineHeight : 24,
        color: '#838383'
    },
    reserved_box_person_info_dom : {
        width: 233,
        alignItems : 'flex-end',
        justifyContent: 'flex-end'
    },
    reserved_box_person_info : {
        textAlign: 'right',
       fontSize:14
    },
    reserved_box_person_time : {
        textAlign: 'right',
        fontSize:14,
        color: '#eb5847',
        fontWeight: 'bold'

    },
    pre_icon : {
        width: Dimensions.get('window').width / 2,
        height: 56,
        backgroundColor: '#eb5c48',
        alignItems : 'center',
        justifyContent: 'center'
    },

    ok_icon : {
        width: Dimensions.get('window').width / 2,
        height: 56,
        backgroundColor: '#77b844',
        alignItems : 'center',
        justifyContent: 'center'
    },

    pre_icon_text : {
        color: '#fff',
        fontSize: 16,
    },

});

class User_request_step6 extends  React.Component {
    constructor(props) {
        super(props);
        this.state = {text : '', count: '1'};
        // todo 예약 정보 데이터 받아오기
        // API.shop.admin.getJRequest();
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.Navigator}>
                    <View style={styles.Navigator_menu}>
                        <TouchableHighlight style={styles.Navigator_menu_button} underlayColor={'transparent'} onPress={() => Actions.pop()}>
                            <Image style={styles.Navigator_menu_image} source={require("../../assets/back_arrow.png")}/>
                        </TouchableHighlight>
                    </View>
                </View>
                <View style={styles.contents}>
                    <Text style={styles.contents_title}>예약완료</Text>
                    <Text style={styles.contents_subt}>아레 예약내용을 확인해주세요.</Text>
                    <View style={styles.contents_sub}>
                        <View style={styles.reserved_box}>
                            <View style={styles.reserved_box_memter}><Image style={styles.imgicon} source={require("../../assets/common/pin.png")}/><Text style={styles.memter_text}> 200M</Text></View>
                            <Text style={styles.reserved_box_store_name}>이자카야</Text>
                            <Text style={styles.reserved_box_food_name}>온더보더 김문점</Text>
                            <View style={styles.reserved_box_extra}>
                                <Image style={styles.extra_icon} source={require("../../assets/common/silver_heart.png")}/>
                                <Text style={styles.extra_word}> 4.3 <Text style={styles.extra_word2}>(355)</Text>  </Text>
                                <Image style={styles.extra_icon} source={require("../../assets/common/silver_bookmark.png")}/>
                                <Text style={styles.extra_word}> 12.353</Text>
                            </View>
                            <View style={styles.reserved_box_person}>
                                <View style={styles.reserved_box_person_row}>
                                    <Text style={styles.reserved_box_person_name}>예약자</Text>
                                    <View style={styles.reserved_box_person_info_dom}>
                                        <Text style={styles.reserved_box_person_info}>김문수</Text>
                                    </View>
                                </View>
                                <View style={styles.reserved_box_person_row}>
                                    <Text style={styles.reserved_box_person_name}>예약인원</Text>
                                    <View style={styles.reserved_box_person_info_dom}>
                                        <Text style={styles.reserved_box_person_info}>10명</Text>
                                    </View>
                                </View>
                                <View style={styles.reserved_box_person_row}>
                                    <Text style={styles.reserved_box_person_name}>예약일시</Text>
                                    <View style={styles.reserved_box_person_info_dom}>
                                        <Text style={styles.reserved_box_person_info}>2018년 2월 23일 오후 6시 34분</Text>
                                    </View>
                                </View>
                                <View style={styles.reserved_box_person_row}>
                                    <Text style={styles.reserved_box_person_name}>유효시간</Text>
                                    <View style={styles.reserved_box_person_info_dom}>
                                        <Text style={styles.reserved_box_person_time}>10:00</Text>
                                    </View>
                                </View>
                            </View>
                        </View>
                        <View style={styles.service_noti}>
                            <View style={styles.service_noti_line}></View>
                            <View style={styles.service_noti_icon}><Text style={styles.service_noti_icon_word}>서비스 제안</Text></View>
                            <Text style={styles.service_noti_name}>서비스 제안 내용이 들어갑니다.</Text>
                        </View>
                    </View>
                </View>
                <View style={styles.main_bottom_bar}>
                    <TouchableHighlight underlayColor={'transparent'} onPress={() => Actions.User_preorder_menu()}>
                        <View style={styles.pre_icon}><Text style={styles.pre_icon_text}>미리주문</Text></View>
                    </TouchableHighlight>
                    <TouchableHighlight underlayColor={'transparent'} onPress={() => (Global.user === null ? Actions.User_Register_Phone_Step1() : Actions.User_main())}>
                        <View style={styles.ok_icon}><Text style={styles.pre_icon_text}>확인</Text></View>
                    </TouchableHighlight>
                </View>
            </View>
        )
    }
}

export default User_request_step6;
