import React from 'react';
import {Text, View, StyleSheet, Image,TouchableHighlight, Dimensions, Alert ,Menu} from 'react-native';
import { Permissions, Location } from 'expo';
import { Actions } from 'react-native-router-flux';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import {Ionicons} from '@expo/vector-icons';
import Global from "../../objects/Global";


const styles = StyleSheet.create({
    container: {
        flex: 1,
        // backgroundColor: '#f9f9f9',
        backgroundColor: '#fff',
    },
    Navigator : {
        flexDirection: 'row',
        marginTop : getStatusBarHeight(),
        width: Dimensions.get('window').width,
        height: 48,
        backgroundColor: '#fff',
    },
    Navigator_title: {
        fontSize: 24,
        marginTop:18,
        marginLeft: 14
    },
    contents : {
        height: (Dimensions.get('window').height - (104 + getStatusBarHeight())),
        // paddingTop : (Dimensions.get('window').height / 10),
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    main_bottom_bar : {
        flexDirection: 'row',
        width: Dimensions.get('window').width,
        height: 56,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    icon_buttom:{
        width: Dimensions.get('window').width / 5,
        height: 56,
        alignItems: 'center'
    },
    icon_buttom_image :{
        marginTop: 19,
        width: 18,
        height: 18,
        resizeMode: 'contain'
    },
    request_box: {

    },
    request_start : {
        width: 280,
        height:304,
    },
    request_btn : {
        width: 280,
        height: 56,
        backgroundColor: '#eb5847',
        fontSize: 20,
        paddingTop: 13.5,
        color: '#ffffff',
        alignItems: 'center',
        textAlign: 'center',
        fontWeight: 'bold'
    },
    reload_box : {
        marginTop: 35,
        marginBottom: 17,
        width: 137,
        height: 32,
        backgroundColor: 'rgba(255, 255, 255, 0.8)',
        borderWidth: 1,
        borderColor : '#f2f2f2',
        borderRadius: 16,
        flexDirection: 'row',
    },
    reload_name :{
        width: 104,
        fontSize: 12,
        color: '#606060',
        paddingLeft:20,
        paddingTop:6
    },
    reload_refresh : {
        width: 16,
        height: 16,
        marginLeft: 8,
        marginTop: 8
    }


});

class User_request_start extends  React.Component {
    constructor(props) {
        super(props);
        Permissions.askAsync(Permissions.LOCATION);
        this.state = {
            text : '',
            biz_shop_data: null,
            latitude: null,
            longitude: null,
            geo_error: null,
            address: "논현2동"
        };
    }

    componentDidMount() {
        this.onPress_reload_refresh();
    }

    onPress_reload_refresh() {
        navigator.geolocation.getCurrentPosition(
            (position) => {
                let changeCheck = false;
                if (position != null) {
                    if (this.state.latitude !== position.coords.latitude || this.state.longitude !== position.coords.longitude) {
                        changeCheck = true;
                    }
                    this.setState({
                        latitude: position.coords.latitude,
                        longitude: position.coords.longitude,
                        geo_error: null
                    });
                } else {
                    if (this.state.latitude !== 37.517204 || this.state.longitude !== 127.041289) {
                        changeCheck = true;
                    }
                    this.setState({
                        latitude: 37.517204,
                        longitude: 127.041289,
                        geo_error: null
                    });
                }
                if (changeCheck)
                    this.geocoding();
            }, (error) => this.setState({
                latitude: 37.517204,
                longitude: 127.041289,
                geo_error: error.message
            }),
        );
    }

    async geocoding() {
        try {
            let result = await Location.reverseGeocodeAsync({
                latitude: this.state.latitude,
                longitude: this.state.longitude
            });
            console.log(result);
            if (result != null && result.length > 0)
                this.setState({address: result[0].street});
            else {
                this.onPress_reload_refresh();
            }
        } catch (e) {
            console.error(e);
        }
    }

    gotoNextPage() {
        if (this.state.latitude === null || this.state.longitude === null || this.state.address === "") {
            Alert.alert('실시간 자리잡기', '현재 위치를 찾고 있습니다.\n잠시후 다시 시도해 주세요.', [{text: "확인", onPress: () => null}]);
            return;
        }
        Actions.User_request_step1({latitude: this.state.latitude, longitude: this.state.longitude, address: this.state.address});
    }

    render() {
        return (
            <View  style={styles.container}>
                <View style={styles.Navigator}>
                    <Text style={styles.Navigator_title}>실시간 자리잡기</Text>
                </View>
                <View style={styles.contents}>
                    <View style={styles.request_box}>
                        <Image style={styles.request_start} source={require("../../assets/User/Request/start.png")}/>
                        <Text style={styles.request_btn}  onPress={() => this.gotoNextPage()}>시작</Text>
                    </View>
                    <View style={styles.reload_box}>
                        <Text style={styles.reload_name}>{this.state.address}</Text>
                        <View style={{borderColor: '#f2f2f2',width:1,height:32}} />
                        <TouchableHighlight underlayColor={'transparent'} onPress={() => this.onPress_reload_refresh()}>
                            <Image style={styles.reload_refresh} source={require("../../assets/common/refresh.png")}/>
                        </TouchableHighlight>
                    </View>
                </View>
                <View style={styles.main_bottom_bar}>
                    <TouchableHighlight underlayColor={'transparent'} onPress={() => Actions.pop()}>
                        <View style={styles.icon_buttom}>
                            <Image style={styles.icon_buttom_image} source={require("../../assets/User/Bottom/home_gray.png")}/>
                        </View>
                    </TouchableHighlight>
                    <TouchableHighlight underlayColor={'transparent'} onPress={() => null}>
                        <View style={styles.icon_buttom}>
                            <Image style={styles.icon_buttom_image} source={require("../../assets/User/Bottom/utensils_black.png")}/>
                        </View>
                    </TouchableHighlight>
                    <TouchableHighlight underlayColor={'transparent'} onPress={() => null}>
                        <View style={styles.icon_buttom}>
                            <Image style={styles.icon_buttom_image} source={require("../../assets/User/Bottom/chat_gray.png")}/>
                        </View>
                    </TouchableHighlight>
                    <TouchableHighlight underlayColor={'transparent'} onPress={() => null}>
                        <View style={styles.icon_buttom}>
                            <Image style={styles.icon_buttom_image} source={require("../../assets/User/Bottom/newspaper_gray.png")}/>
                        </View>
                    </TouchableHighlight>
                    <TouchableHighlight underlayColor={'transparent'} onPress={() => Actions.User_my_main()}>
                        <View style={styles.icon_buttom}>
                            <Image style={styles.icon_buttom_image} source={require("../../assets/User/Bottom/user_gray.png")}/>
                        </View>
                    </TouchableHighlight>
                </View>
            </View>
        )
    }
}

export default User_request_start;
