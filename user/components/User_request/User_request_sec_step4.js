import React from 'react';
import {
    Text,
    View,
    StyleSheet,
    Image,
    TouchableHighlight,
    Dimensions,
    ScrollView,
    Menu,
    Animated,
    Easing
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import {Ionicons} from '@expo/vector-icons';
import Global from "../../objects/Global";
import API from "../../objects/API";


const styles = StyleSheet.create({
    container: {
        flex: 1,
        // backgroundColor: '#f9f9f9',
        backgroundColor: '#fff',
    },
    Navigator : {
        flexDirection: 'row',
        marginTop : getStatusBarHeight(),
        width: Dimensions.get('window').width,
        height: 48,
        backgroundColor: '#fff',
    },
    Navigator_menu: {
        width: 80,
        height: 48,
    },
    Navigator_menu_button :{
        width: 48,
        height: 48,
        paddingTop: 12,
        paddingLeft: 12,
    },
    Navigator_menu_image : {
        width: 24,
        height: 24
    },
    Navigator_title:{
        width : ((Dimensions.get('window').width) - 160),
        alignItems: 'center',
    },
    Navigator_title_text:{
        fontSize: 16,
        color:'#000000'
    },
    Navigator_start:{
        alignItems: 'flex-end',
        width: 80,
        height: 48,
        paddingTop: 12,
        paddingRight: 12,
    },
    Navigator_start_text:{
        fontSize: 16,
        color:'#7f8fd1',
    },
    callout_box : {
        width : (Dimensions.get('window').width),
        height: 40,
        borderRadius: 2,
        backgroundColor: '#7f8fd1',
        alignItems: 'center',
        justifyContent: 'center',
        shadowOffset: {width: 2, height: 10 },
        shadowColor: 'rgba(0, 0, 0, 0.2)',
        paddingRight: 56
    },
    callout_box_text : {
        fontSize : 12,
        color: '#ffffff'
    },
    contents : {
        height: (Dimensions.get('window').height - (121 + getStatusBarHeight())),
        flex: 1,
        paddingTop : 10,
        alignItems: 'center'
    },
    contents_title : {
        fontSize: 24,
    },
    contents_subt : {
        marginTop : 10,
        fontSize: 12,
        color: '#7db840',
        marginBottom: 37
    },
    c_picker_group : {
        flexDirection: 'row',
        // height: (Dimensions.get('window').height - (108 + getStatusBarHeight())),
        flex: 1,
        marginTop: -100,
        justifyContent: 'center',
        alignItems: 'center'
    },
    c_picker: {
        width: 100
    },
    c_picker_word : {
        fontSize: 16
    },
    main_bottom_bar : {
        flexDirection: 'row',
        width: Dimensions.get('window').width,
        height: 42,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
        paddingTop: 11,
        paddingBottom: 11,
        borderColor: 'rgba(230, 230, 230, 0.5)',
        borderTopWidth: 1,
    },
    main_bottom_bar2 : {
        flexDirection: 'row',
        width: Dimensions.get('window').width,
        height: 56,
        backgroundColor: '#C3C3C3',
        alignItems: 'center',
        justifyContent: 'center',
        paddingTop: 11,
        paddingBottom: 11
    },
    bigbtn : {
        color: '#fff'
    },
    mini_icon : {
        width: 15,
        height: 15,
        resizeMode: 'contain'
    },
    mini_word : {
        fontSize: 12,
        color: '#606060'
    },
    contents_item: {
        width:  Dimensions.get('window').width - 40,
        height: 96,
        borderWidth : 1,
        borderRadius: 2,
        borderColor: '#ededed',
        flexDirection: 'row',
        marginBottom: 10
    },
    request_image_dom: {
        width: 90,
        height: 96,
        paddingLeft:6,
        paddingTop:6
    },
    request_image:{
        width:84,
        height: 84
    },
    request_info: {
        width: (Dimensions.get('window').width - 191),
        height: 96,
        paddingLeft: 20
    },
    request_info_memter : {
        marginRight:  0,
        marginLeft: (Dimensions.get('window').width - 220),
        width: 52,
        fontSize : 12,
        color : '#7f8fd1',
        marginTop: 6,
        flexDirection: 'row',
    },
    request_info_store_name : {
        fontSize:12,
        color: '#7db840',
        marginTop: 25
    },
    request_info_food_name : {
        fontSize: 16
    },
    request_info_delete: {
        width: 100,
        height: 96,
        flex: 1,
        marginTop: 39,
        flexDirection: 'row'
    },
    request_info_delete_w1 : {
        width: 10,
        height: 10,
        backgroundColor: '#eb5847',
        marginRight:4,
        borderRadius: 5
    },
    request_info_delete_w2 : {
        width: 10,
        height: 10,
        backgroundColor: '#eb5847',
        marginRight:4,
        borderRadius: 5
    },
    request_info_delete_w3 : {
        width: 10,
        height: 10,
        backgroundColor: '#eb5847',
        marginRight:4,
        borderRadius: 5
    }

});

class User_request_step4 extends  React.Component {
    constructor(props) {
        super(props);
        this.state = {
            text: '',
            count: '1',
            latitude: 37.517204,
            longitude: 127.041289,
            geo_error: null,
            endRequestReservation: false
        };
        this.componentDidMount();
    }

    acceptedReservation = [];

    alpha = [
        new Animated.Value(0), new Animated.Value(0), new Animated.Value(0)
    ];
    fade = [
        true, true, true
    ];

    animationDuration = 1200;

    componentDidMount() {
        for(let i = 0; i < 3; i++) {
            this.startAnimation(i, true);
        }
        this.setLocation();
    }

    setLocation() {
        navigator.geolocation.getCurrentPosition(
            (position) => {
                if (position != null) {
                    this.setState({
                        latitude: position.coords.latitude,
                        longitude: position.coords.longitude,
                        geo_error: null
                    });
                } else {
                    this.setState({
                        latitude: 37.517204,
                        longitude: 127.041289,
                        geo_error: null
                    });
                }
                this.requestReservation();
            }, (error) => this.setState({
                latitude: 37.517204,
                longitude: 127.041289,
                geo_error: error.message
            }),
        );
    }

    requestReservation() {
        // this.setState({endRequestReservation: true});
        this.props.shopIds.map(shopId => shopIDArray.push(shopId + ""));
        let shopIDArray = [];
        API.jRequest.post(new Location(this.state.latitude, this.state.longitude), shopIDArray, data => {
            this.setState({endRequestReservation: true});
        }, error => {
            Alert.alert('예약 요청', '서버문제로 예약을 요청하는데에 실패하였습니다. \n' + error, [{
                text: "확인",
                onPress: () => Actions.pop()
            }]);
        });
    }

    startAnimation(idx, isFirst = false) {
        Animated.timing(
            this.alpha[idx],
            isFirst
                ? (
                    idx === 0
                        ? {
                            toValue: this.fade[idx] ? 1 : 0,
                            duration: this.animationDuration,
                            easing: Easing.linear
                        }
                        : idx === 1
                        ? {
                            toValue: this.fade[idx] ? 1 : 0,
                            duration: this.animationDuration,
                            easing: Easing.linear,
                            delay: Math.floor(this.animationDuration / 3)
                        }
                        : {
                            toValue: this.fade[idx] ? 1 : 0,
                            duration: this.animationDuration,
                            easing: Easing.linear,
                            delay: Math.floor(this.animationDuration / 3) * 2
                        }
                )
                : {
                    toValue: this.fade[idx] ? 1 : 0,
                    duration: this.animationDuration,
                    easing: Easing.linear
                }
        ).start(() => {
            for (let i = 0; i < 3; i++) {
                if (this.alpha[i].__getValue() === 0) {
                    if (!this.fade[i]) {
                        this.fade[i] = true;
                        this.startAnimation(i);
                    }
                } else if (this.alpha[i].__getValue() === 1) {
                    if (this.fade[i]) {
                        this.fade[i] = false;
                        this.startAnimation(i);
                    }
                }
            }
        });
    }

    shopElement(shop) {
        if (this.props.shopIds.findIndex(s => s === shop.shopId) !== -1) {
            return <View key={shop.shopId} style={styles.contents_item}>
                <View style={styles.request_image_dom}>
                    <Image style={styles.request_image}
                           source={require("../../assets/User/My/dummy_menu_images.png")}/>
                </View>
                <View style={styles.request_info}>
                    <Text style={styles.request_info_store_name}>{shop.type}</Text>
                    <Text style={styles.request_info_food_name}>{shop.name}</Text>
                </View>
                <View style={styles.request_info_delete}>
                    <Animated.View style={[styles.request_info_delete_w1, {opacity: this.alpha[0]}]}/>
                    <Animated.View style={[styles.request_info_delete_w2, {opacity: this.alpha[1]}]}/>
                    <Animated.View style={[styles.request_info_delete_w3, {opacity: this.alpha[2]}]}/>
                </View>
            </View>;
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.Navigator}>
                    <View style={styles.Navigator_menu}>
                        <TouchableHighlight style={styles.Navigator_menu_button} underlayColor={'transparent'}
                                            onPress={() => Actions.pop()}>
                            <Image style={styles.Navigator_menu_image} source={require("../../assets/back_arrow.png")}/>
                        </TouchableHighlight>
                    </View>
                </View>
                <View style={styles.contents}>
                    <Text style={styles.contents_title}>사장님의 응답을</Text>
                    <Text style={styles.contents_title}>기다리고 있어요</Text>
                    <Text style={styles.contents_subt}>응답 대기시간은 최대 2분입니다.</Text>
                    <ScrollView>
                        {
                            Global.cart.map(shop => this.shopElement(shop))
                        }
                    </ScrollView>
                </View>
                <View style={styles.main_bottom_bar}>
                    <Image style={styles.mini_icon} source={require("../../assets/common/user_gray.png")}/>
                    <Text style={styles.mini_word}> 인원 {this.props.count} </Text>
                    <Image style={styles.mini_icon} source={require("../../assets/common/pin_gray.png")}/>
                    <Text style={styles.mini_word}> 거리 {this.props.distance}M </Text>
                    <Image style={styles.mini_icon} source={require("../../assets/common/clock_gray.png")}/>
                    <Text style={styles.mini_word}> {this.props.time}분 이내 출발</Text>
                </View>
                <TouchableHighlight underlayColor={'transparent'} onPress={this.state.endRequestReservation ? Actions.User_request_sec_step5({shopIds: this.props.shopIds, distance: this.props.distance, count: this.props.count, time: this.props.time}) : null}>
                    <View style={[styles.main_bottom_bar2, {backgroundColor: this.state.endRequestReservation ? "#eb5847" : "#C3C3C3"}]}>
                        <Text style={styles.bigbtn}>선택예약</Text>
                    </View>
                </TouchableHighlight>
            </View>
        )
    }
}

export default User_request_step4;
