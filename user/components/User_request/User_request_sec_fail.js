import React from 'react';
import {
    Text,
    View,
    StyleSheet,
    Image,
    TouchableHighlight,
    Dimensions,
    ScrollView,
    Menu} from 'react-native';
import { Actions } from 'react-native-router-flux';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import {Ionicons} from '@expo/vector-icons';
import Global from "../../objects/Global";


const styles = StyleSheet.create({
    container: {
        flex: 1,
        // backgroundColor: '#f9f9f9',
        backgroundColor: '#fff',
    },
    Navigator : {
        flexDirection: 'row',
        marginTop : getStatusBarHeight(),
        width: Dimensions.get('window').width,
        height: 48,
        backgroundColor: '#fff',
    },
    Navigator_menu: {
        width: 80,
        height: 48,
    },
    Navigator_menu_button :{
        width: 48,
        height: 48,
        paddingTop: 12,
        paddingLeft: 12,
    },
    Navigator_menu_image : {
        width: 24,
        height: 24
    },
    Navigator_title:{
        width : ((Dimensions.get('window').width) - 160),
        alignItems: 'center',
    },
    Navigator_title_text:{
        fontSize: 16,
        color:'#000000'
    },
    Navigator_start:{
        alignItems: 'flex-end',
        width: 80,
        height: 48,
        paddingTop: 12,
        paddingRight: 12,
    },
    Navigator_start_text:{
        fontSize: 16,
        color:'#7f8fd1',
    },
    callout_box : {
        width : (Dimensions.get('window').width),
        height: 40,
        borderRadius: 2,
        backgroundColor: '#7f8fd1',
        alignItems: 'center',
        justifyContent: 'center',
        shadowOffset: {width: 2, height: 10 },
        shadowColor: 'rgba(0, 0, 0, 0.2)',
        paddingRight: 56
    },
    callout_box_text : {
        fontSize : 12,
        color: '#ffffff'
    },
    contents : {
        height: (Dimensions.get('window').height - (121 + getStatusBarHeight())),
        flex: 1,
        paddingTop : 10,
        alignItems: 'center',
        justifyContent: 'center'
    },
    contents_title : {
        fontSize: 24,
        marginLeft: 20,
        width: Dimensions.get('window').width - 50,
        textAlign: 'center'
    },
    contents_subt : {
        marginTop : 10,
        fontSize: 12,
        color: '#eb5847'
    },
    service_noti : {
        width: 320,
        paddingLeft: 20,
        paddingTop: 11,

        flexDirection: 'row',
        height: 32
    },
    service_noti_icon: {
        backgroundColor: 'rgba(235, 88, 71, 0.23)',
        width: 60,
        height: 16,
        borderRadius: 12,
        paddingLeft: 6.5,
        paddingTop:1.5,
    },
    service_noti_icon_word : {
        fontSize: 10,
        color: '#eb5847'
    },
    service_noti_name : {
        marginLeft: 8,
        fontSize: 12,
        color: '#eb5847'
    },
    setting_icon_image : {
        width: 20,
        height: 20
    },
    main_bottom_bar : {
        flexDirection: 'row',
        width: Dimensions.get('window').width,
        height: 56,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    extra_icon : {
        width: 20,
        height: 20
    },
    imgicon: {
        width: 16,
        height: 16,
        resizeMode : 'contain'
    },
    imgicon_ios: {
        width: 12,
        height: 12,
        resizeMode : 'cover'
    },
    contents_item: {
        width: (Dimensions.get('window').width - 40),
        height: 96,
        backgroundColor: '#ffffff',
        // marginLeft: 20,
        marginBottom: 10,
        flexDirection: 'row',
        borderWidth: 1,
        borderColor: 'rgba(0, 0, 0, 0.1)',
        shadowColor: '#000',
        shadowOffset: { width: 5 , height: 5  },
        shadowOpacity: 0.1,
        shadowRadius: 2,
        elevation: 1,
    },


    request_image_dom: {
        width: 90,
        height: 96,
        paddingLeft:6,
        paddingTop:6
    },
    request_image:{
        width:84,
        height: 84
    },
    request_info: {
        width: (Dimensions.get('window').width - 124),
        height: 96,
        paddingLeft: 20
    },
    request_info_memter : {
        width: (Dimensions.get('window').width - 144),
        marginTop: 6,
        paddingRight: 16,
        alignItems: 'flex-end',
        justifyContent: 'flex-end',
        flexDirection: 'row'
    },
    memter_text : {
        fontSize:12,
        color: '#7f8fd1'
    },
    request_info_store_name : {
        // marginTop: 2,
        fontSize:12,
        color: '#7db840'
    },
    request_info_food_name : {
        fontSize: 16,
        fontWeight: '500'
    },
    request_info_extra : {
        marginTop: 8,
        // fontSize: 12,
        flexDirection: 'row'
    },
    pre_icon : {
        width: Dimensions.get('window').width / 2,
        height: 56,
        backgroundColor: '#eb5c48',
        paddingTop: 12,
        alignItems : 'center',
        justifyContent: 'center'
    },

    ok_icon : {
        width: Dimensions.get('window').width,
        height: 56,
        backgroundColor: '#77b844',
        alignItems : 'center',
        justifyContent: 'center'
    },

    pre_icon_text : {
        color: '#fff',
        fontSize: 16,
    },
    contents_talk : {
        width: '100%',
        fontSize: 16,
        fontWeight: '600',
        marginTop: 61,
        marginLeft: 22,
        paddingLeft: 20,
        marginBottom : 10
    },
    imgicon_s: {
        width: 16,
        height: 16,
        resizeMode: 'contain'
    },
    imgicon_pin: {
        width: 10,
        height: 14,
        marginTop:1,
        resizeMode: 'contain'
    },
    contents_item_check: {
        width:  Dimensions.get('window').width - 40,
        height: 96,
        backgroundColor : 'rgba(180, 221, 116, 0.8)',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute'
    },
    check_image : {
        width: 48,
        height: 48
    }
});

class User_request_step6 extends  React.Component {
    constructor(props) {
        super(props);
        this.state = {
            text: '',
            count: '1',
            selected: -1,
            recommand_shops: [
                {
                    shopId: 0,
                    type: "이자카야",
                    name: "공릉동 술깨비 1",
                    distance: 200,
                    like: 6,
                    startTime: "16:00",
                    endTime: "3:00"
                },
                {
                    shopId: 1,
                    type: "이자카야",
                    name: "공릉동 술깨비 2",
                    distance: 200,
                    like: 6,
                    startTime: "16:00",
                    endTime: "3:00"
                },
                {
                    shopId: 2,
                    type: "이자카야",
                    name: "공릉동 술깨비 3",
                    distance: 200,
                    like: 6,
                    startTime: "16:00",
                    endTime: "3:00"
                }
            ]
        };
        // todo 실패한 예약 정보 데이터 props로 받아오고 추천 shop 받아오기
    }

    shop(shop, idx) {
        return <TouchableHighlight key={shop.shopId} underlayColor={'transparent'} onPress={() => this.setState({selected: idx})}>
            <View style={styles.contents_item}>
                <View style={styles.request_image_dom}>
                    <Image style={styles.request_image}
                           source={require("../../assets/User/My/dummy_menu_images.png")}/>
                    <Image style={{zIndex: 9998, width: 48, height: 16, marginTop: -88, marginLeft: -4}}
                           source={require("../../assets/common/event_label_w.png")}/>
                </View>
                <View style={styles.request_info}>
                    <View style={styles.request_info_memter}>
                        <Image style={styles.imgicon} source={require("../../assets/common/pin.png")}/>
                        <Text style={styles.memter_text}> {shop.distance}M</Text>
                    </View>
                    <Text style={styles.request_info_store_name}>{shop.type}</Text>
                    <Text style={styles.request_info_food_name}>{shop.name}</Text>
                    <View style={styles.request_info_extra}>
                        <Image style={styles.imgicon_s} source={require("../../assets/common/heart.png")}/>
                        <Text style={{color: '#eb5847', lineHeight: 16, fontSize: 12}}> {shop.like}</Text>
                        <View style={{width: 13, height: 16, paddingTop: 6}}>
                            <View style={{
                                width: 1,
                                height: 4,
                                backgroundColor: '#c4c4c4',
                                marginLeft: 6
                            }}/>
                        </View>
                        <Image style={styles.imgicon_s} source={require("../../assets/common/clock.png")}/>
                        <Text style={{
                            color: '#c4c4c4',
                            lineHeight: 16,
                            fontSize: 12
                        }}> {shop.startTime} ~ {shop.endTime}</Text>
                    </View>
                </View>
                <View style={[styles.contents_item_check, this.state.selected !== idx ? {
                    display: 'none',
                    position: 'relative'
                } : {}]}>
                    <Image style={styles.check_image}
                           source={require("../../assets/common/check_circle_fill.png")}/>
                </View>
            </View>
        </TouchableHighlight>;
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.Navigator}>
                    <View style={styles.Navigator_menu}>
                        <TouchableHighlight style={styles.Navigator_menu_button} underlayColor={'transparent'}
                                            onPress={() => Actions.pop()}>
                            <Image style={styles.Navigator_menu_image} source={require("../../assets/back_arrow.png")}/>
                        </TouchableHighlight>
                    </View>
                </View>
                <View style={styles.contents}>
                    <Text style={styles.contents_title}>예약실패</Text>
                    <Text style={styles.contents_subt}>사장님이 응답하지 않습니다 :(</Text>
                    <Text style={styles.contents_talk}>이런 가게는 어떠세요?</Text>
                    <ScrollView>
                        {
                            this.state.recommand_shops.map((shop, idx) => this.shop(shop, idx))
                        }
                    </ScrollView>
                </View>
                <View style={styles.main_bottom_bar}>
                    <TouchableHighlight underlayColor={'transparent'} onPress={() => (Global.user === null ? Actions.User_Register_Phone_Step1() : Actions.User_main())}>
                        <View style={styles.ok_icon}><Text style={styles.pre_icon_text}>다른곳 예약하기</Text></View>
                    </TouchableHighlight>
                </View>
            </View>
        )
    }
}

export default User_request_step6;
