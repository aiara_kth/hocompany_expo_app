package host.exp.exponent;


import android.content.Intent;

import com.facebook.CallbackManager;
import com.facebook.react.ReactPackage;

import java.util.Arrays;
import java.util.List;

import expolib_v1.okhttp3.OkHttpClient;

// Needed for `react-native link`
// import com.facebook.react.ReactApplication;
import com.zyu.ReactNativeWheelPickerPackage;
import com.oblador.vectoricons.VectorIconsPackage;
import com.dieam.reactnativepushnotification.ReactNativePushNotificationPackage;
import com.beefe.picker.PickerViewPackage;
import com.reactnativenavigation.bridge.NavigationReactPackage;
import com.airbnb.android.react.maps.MapsPackage;
import com.dooboolab.kakaologins.RNKakaoLoginsPackage;
import io.codebakery.imagerotate.ImageRotatePackage;
import com.devfd.RNGeocoder.RNGeocoderPackage;
import com.facebook.reactnative.androidsdk.FBSDKPackage;
import com.dylanvann.fastimage.FastImageViewPackage;
import com.magus.fblogin.FacebookLoginPackage;

public class MainApplication extends ExpoApplication {

  @Override
  public boolean isDebug() {
    return BuildConfig.DEBUG;
  }

  // Needed for `react-native link`
  public List<ReactPackage> getPackages() {
    return Arrays.<ReactPackage>asList(
        // Add your own packages here!
        // TODO: add native modules!

        // Needed for `react-native link`
        // new MainReactPackage(),
            new ReactNativeWheelPickerPackage(),
            new VectorIconsPackage(),
            new ReactNativePushNotificationPackage(),
            new PickerViewPackage(),
            new NavigationReactPackage(),
            new MapsPackage(),
            new RNKakaoLoginsPackage(),
            new ImageRotatePackage(),
            new RNGeocoderPackage(),
            new FBSDKPackage(new CallbackManager() {
              @Override
              public boolean onActivityResult(int requestCode, int resultCode, Intent data) {
                return false;
              }
            }),
            new FastImageViewPackage(),
            new FacebookLoginPackage(),
            new ReactNativeWheelPickerPackage()
    );
  }

  @Override
  public String gcmSenderId() {
    return getString(R.string.gcm_defaultSenderId);
  }

  @Override
  public boolean shouldUseInternetKernel() {
    return BuildVariantConstants.USE_INTERNET_KERNEL;
  }

  public static OkHttpClient.Builder okHttpClientBuilder(OkHttpClient.Builder builder) {
    // Customize/override OkHttp client here
    return builder;
  }
}
